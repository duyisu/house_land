package com.zhongkai.model.business;

import java.util.Date;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 * TWfInstance entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="T_WF_INSTANCE")
public class TWfInstance implements java.io.Serializable {
	//STATUS
	public final static String StartUp = "00";
	public final static String WaitingForSignature = "01";
	public final static String Processing = "02";
	public final static String HoldOn = "03";
	public final static String Finished = "09";
	public final static String Aborted = "99";

	// Fields
	@Id
	@SequenceGenerator(name="generator",allocationSize=1,initialValue=1, sequenceName="SEQ_WF_INSTANCE")
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator = "generator")
	@Column(name="INS_ID")
	private Long insId;
	
	@Column(name="DEF_ID")
	private Long defId;
	
	@Column(name="TITLE")
	private String title;
	
	@Column(name="STATUS")
	private String status;
	
	@Column(name="STEP_ID")
	private Long stepId;
	
	@Column(name="FROMUSER")
	private Long fromuser;
	
	@Column(name="FROMTIME")
	private Date fromtime;
	
	@Column(name="TOUSER")
	private Long touser;
	
	@Column(name="TOTIME")
	private Date totime;
	
	@Column(name="CREATOR")
	private Long creator;
	
	@Column(name="CREATETIME")
	private Date createtime;
	
	@Column(name="MODIFIER")
	private Long modifier;
	
	@Column(name="MODIFITIME")
	private Date modifitime;
	
	@Column(name="ECCLASSNAME")
	private String ecclassname;
	
	@Column(name="FROMUSERDEPT")
	private Long fromuserDept;
	
	@Column(name="TOUSERDEPT")
	private Long touserDept;
	
	@Column(name="CREATORDEPT")
	private Long creatorDept;
	
	@Version
	private Long version;
	// Constructors
	
	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	/** default constructor */
	public TWfInstance() {
	}

	/** minimal constructor */
	public TWfInstance(Long defId, String status, Long stepId, Long fromuser,
			Date fromtime, Long touser, Date totime, Long creator,
			Date createtime, Long modifier, Date modifitime) {
		this.defId = defId;
		this.status = status;
		this.stepId = stepId;
		this.fromuser = fromuser;
		this.fromtime = fromtime;
		this.touser = touser;
		this.totime = totime;
		this.creator = creator;
		this.createtime = createtime;
		this.modifier = modifier;
		this.modifitime = modifitime;
	}

	/** full constructor */
	public TWfInstance(Long defId, String title, String status, Long stepId,
			Long fromuser, Date fromtime, Long touser, Date totime,
			Long creator, Date createtime, Long modifier, Date modifitime,
			Set TWfProcesses, Set TUmsmEvents) {
		this.defId = defId;
		this.title = title;
		this.status = status;
		this.stepId = stepId;
		this.fromuser = fromuser;
		this.fromtime = fromtime;
		this.touser = touser;
		this.totime = totime;
		this.creator = creator;
		this.createtime = createtime;
		this.modifier = modifier;
		this.modifitime = modifitime;
	}

	// Property accessors

	public Long getInsId() {
		return this.insId;
	}

	public void setInsId(Long insId) {
		this.insId = insId;
	}

	public Long getDefId() {
		return this.defId;
	}

	public void setDefId(Long defId) {
		this.defId = defId;
	}

	public String getTitle() {
		return this.title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getStatus() {
		return this.status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Long getStepId() {
		return this.stepId;
	}

	public void setStepId(Long stepId) {
		this.stepId = stepId;
	}

	public Long getFromuser() {
		return this.fromuser;
	}

	public void setFromuser(Long fromuser) {
		this.fromuser = fromuser;
	}

	public Date getFromtime() {
		return this.fromtime;
	}

	public void setFromtime(Date fromtime) {
		this.fromtime = fromtime;
	}

	public Long getTouser() {
		return this.touser;
	}

	public void setTouser(Long touser) {
		this.touser = touser;
	}

	public Date getTotime() {
		return this.totime;
	}

	public void setTotime(Date totime) {
		this.totime = totime;
	}

	public Long getCreator() {
		return this.creator;
	}

	public void setCreator(Long creator) {
		this.creator = creator;
	}

	public Date getCreatetime() {
		return this.createtime;
	}

	public void setCreatetime(Date createtime) {
		this.createtime = createtime;
	}

	public Long getModifier() {
		return this.modifier;
	}

	public void setModifier(Long modifier) {
		this.modifier = modifier;
	}

	public Date getModifitime() {
		return this.modifitime;
	}

	public void setModifitime(Date modifitime) {
		this.modifitime = modifitime;
	}

	public String getEcclassname() {
		return ecclassname;
	}

	public void setEcclassname(String ecclassname) {
		this.ecclassname = ecclassname;
	}

	public Long getTouserDept() {
		return touserDept;
	}

	public void setTouserDept(Long touserDept) {
		this.touserDept = touserDept;
	}

	public Long getCreatorDept() {
		return creatorDept;
	}

	public void setCreatorDept(Long creatorDept) {
		this.creatorDept = creatorDept;
	}

	public Long getFromuserDept() {
		return fromuserDept;
	}

	public void setFromuserDept(Long fromuserDept) {
		this.fromuserDept = fromuserDept;
	}

}