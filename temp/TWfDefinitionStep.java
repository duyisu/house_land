package com.zhongkai.model.business;

import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * TWfDefinitionStep entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="T_WF_DEFINITION_STEP")
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
public class TWfDefinitionStep implements java.io.Serializable {
	//StepType
	public static String StartUpStepType = "01";	//启动步骤
	public static String ProcessStepType = "02";	//中间步骤
	public static String FinishStepType = "09";		//结束步骤
	
	//MODE
	public static String NoSignature = "01"; //无需签收
	public static String Signature = "02";	//需要签收
	
	//SELECTUSERMODE
	public static String ToSomeone = "01"; 									//指定具体人
	public static String ToDepartmentUnder = "02";							//指定部门内人员
	public static String ToCharacter = "03";								//指定角色内人员
	public static String ToDepartmentLeader = "04";							//指定部门内领导
	public static String ToDepartmentUpperLeader = "05";					//指定部门内上级领导
	public static String ToUnconfirmPerson = "07";							//全市机构人员
	public static String ToInstanceCreater = "08";							//指定创建人
	public static String ToWhoUWant = "09";									//缺省处理部门
	public static String ToMyDepartmentUser = "10";							//发给本人所在部门的成员
	public static String ToPriorStepUser = "11";							//发给指定环节的操作者
	public static String ToMydepartmentHigherdeptUser = "12";				//发给本人所在机构的成员
	public static String ToMyHigherdeptRoleUser = "13";						//发给属于某角色，并且与本人在同一个机构中的人员

	public static Long TurnMunicipalPlatformStep = (long)24;	//可以由二级平台案件转向一级平台案件的步骤ID
	// Fields
	@Id
	@SequenceGenerator(name="generator",allocationSize=1,initialValue=1, sequenceName="SEQ_WF_DEFINITION_STEP")
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator = "generator")
	@Column(name="STEP_ID")
	private Long stepId;
	
	@Column(name="DEF_ID")
	private Long defId;
	
	@Column(name="STEP_NAME")
	private String stepName;
	
	@Column(name="STEP_TYPE")
	private String stepType;
	
	@Column(name="DYNAMICCOND")
	private String dynamiccond;
	
	@Column(name="SIGNMODE")
	private String signmode;
	
	@Column(name="ISEDITABLE")
	private String iseditable;
	
	@Column(name="SELECTUSERMODE")
	private String selectusermode;
	
	@Column(name="SELECTION")
	private Long selection;
	
	@Column(name="DAYS1")
	private Long days1;
	
	@Column(name="DAYS2")
	private Long days2;
	
	@Column(name="SELECTUSERACTION")
	private String selectuseraction;
	
	@Column(name="SENDACTION")
	private String sendaction;
	
	@Column(name="TERMINATEACTION")
	private String terminateaction;
	
	@Column(name="ISPHONE")
	private String isphone;
	
	@Column(name="ISUPLOADFILE")
	private String isuploadfile;
	@Version
	private Long version;
	
	@Column(name="ISDELAY")
	private String isDelay;
	
	@Column(name="SORT_CODE")
	private Integer sortCode;
	

	// Constructors

	public String getIsuploadfile() {
		return isuploadfile;
	}

	public void setIsuploadfile(String isuploadfile) {
		this.isuploadfile = isuploadfile;
	}

	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	/** default constructor */
	public TWfDefinitionStep() {
	}

	/** minimal constructor */
	public TWfDefinitionStep(Long defId, String iseditable,
			String selectusermode, Long selection, Long days1, Long days2) {
		this.defId = defId;
		this.iseditable = iseditable;
		this.selectusermode = selectusermode;
		this.selection = selection;
		this.days1 = days1;
		this.days2 = days2;
	}

	/** full constructor */
	public TWfDefinitionStep(Long defId, String stepName,
			String stepType, String dynamiccond, String iseditable,
			String selectusermode, Long selection, Long days1, Long days2,
			Set TWfDefinitionSteprelasForNextstepId,
			Set TWfDefinitionSteprelasForPriorstepId) {
		this.defId = defId;
		this.stepName = stepName;
		this.stepType = stepType;
		this.dynamiccond = dynamiccond;
		this.iseditable = iseditable;
		this.selectusermode = selectusermode;
		this.selection = selection;
		this.days1 = days1;
		this.days2 = days2;
	}

	// Property accessors

	public Long getStepId() {
		return this.stepId;
	}

	public void setStepId(Long stepId) {
		this.stepId = stepId;
	}

	public String getStepName() {
		return this.stepName;
	}

	public void setStepName(String stepName) {
		this.stepName = stepName;
	}

	public String getStepType() {
		return this.stepType;
	}

	public void setStepType(String stepType) {
		this.stepType = stepType;
	}

	public String getDynamiccond() {
		return this.dynamiccond;
	}

	public void setDynamiccond(String dynamiccond) {
		this.dynamiccond = dynamiccond;
	}

	public String getIseditable() {
		return this.iseditable;
	}

	public void setIseditable(String iseditable) {
		this.iseditable = iseditable;
	}

	public String getSelectusermode() {
		return this.selectusermode;
	}

	public void setSelectusermode(String selectusermode) {
		this.selectusermode = selectusermode;
	}

	public Long getSelection() {
		return this.selection;
	}

	public void setSelection(Long selection) {
		this.selection = selection;
	}

	public Long getDays1() {
		return this.days1;
	}

	public void setDays1(Long days1) {
		this.days1 = days1;
	}

	public Long getDays2() {
		return this.days2;
	}

	public void setDays2(Long days2) {
		this.days2 = days2;
	}

	public String getSignmode() {
		return signmode;
	}

	public void setSignmode(String signmode) {
		this.signmode = signmode;
	}

	public Long getDefId() {
		return defId;
	}

	public void setDefId(Long defId) {
		this.defId = defId;
	}

	public String getSelectuseraction() {
		return selectuseraction;
	}

	public void setSelectuseraction(String selectuseraction) {
		this.selectuseraction = selectuseraction;
	}

	public String getSendaction() {
		return sendaction;
	}

	public void setSendaction(String sendaction) {
		this.sendaction = sendaction;
	}

	public String getTerminateaction() {
		return terminateaction;
	}

	public void setTerminateaction(String terminateaction) {
		this.terminateaction = terminateaction;
	}

	public String getIsphone() {
		return isphone;
	}

	public void setIsphone(String isphone) {
		this.isphone = isphone;
	}

	public String getIsDelay() {
		return isDelay;
	}

	public void setIsDelay(String isDelay) {
		this.isDelay = isDelay;
	}

	public Integer getSortCode() {
		return sortCode;
	}

	public void setSortCode(Integer sortCode) {
		this.sortCode = sortCode;
	}
}