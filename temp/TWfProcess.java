package com.zhongkai.model.business;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 * TWfProcess entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="T_WF_PROCESS")
public class TWfProcess implements java.io.Serializable {
	//PROCESSSTATUS
	public static String STATUS_NotOpen = "01";
	public static String STATUS_Processing = "02";
	public static String STATUS_Finished = "03";
	public static String STATUS_Backward = "04"; 
	public static String STATUS_Abandoned = "99"; //废弃
	
	//PROCESSTYPE
	public static String TYPE_Process = "01";
	public static String TYPE_Urgency = "02";
	public static String TYPE_Forward = "03";

	// Fields
	@Id
	@SequenceGenerator(name="generator",allocationSize=1,initialValue=1, sequenceName="SEQ_WF_PROCESS")
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator = "generator")
	@Column(name="PROCESS_ID")
	private Long processId;
	
	@Column(name="INS_ID")
	private Long insId;
	
	@Column(name="STEP_ID")
	private Long stepId;
	
	@Column(name="LASTPROCESS_ID")
	private Long lastprocessId;
	
	@Column(name="PROCESSSTATUS")
	private String processstatus;
	
	@Column(name="PROCESSTYPE")
	private String processtype;
	
	@Column(name="FROMUSER")
	private Long fromuser;
	
	@Column(name="FROMTIME")
	private Date fromtime;
	
	@Column(name="TOUSER")
	private Long touser;
	
	@Column(name="TOTIME")
	private Date totime;
	
	@Column(name="OPINION")
	private String opinion;
	
	@Column(name="FINISHTIME")
	private Date finishtime;
	
	@Column(name="WORKTIME1")
	private Date worktime1;
	
	@Column(name="WORKTIME2")
	private Date worktime2;
	
	@Column(name="MALICIOUS_ROLLBACK")
	private String maliciousRollback;
	
	@Column(name="FROMUSERDEPT")
	private Long fromuserDept;
	
	@Column(name="TOUSERDEPT")
	private Long touserDept;
	
	@Column(name="HANDLE_STATUS")
	private String handleStatus;
	
	public String getHandleStatus() {
		return handleStatus;
	}

	public void setHandleStatus(String handleStatus) {
		this.handleStatus = handleStatus;
	}

	@Version
	private Long version;
	// Constructors
	
	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	/** default constructor */
	public TWfProcess() {
	}

	/** minimal constructor */
	public TWfProcess(Long insId, Long stepId, Long lastprocessId, String processstatus,
			String processtype, Long fromuser, Date fromtime, Long touser,
			Date totime) {
		this.insId = insId;
		this.stepId = stepId;
		this.lastprocessId = lastprocessId;
		this.processstatus = processstatus;
		this.processtype = processtype;
		this.fromuser = fromuser;
		this.fromtime = fromtime;
		this.touser = touser;
		this.totime = totime;
	}

	/** full constructor */
	public TWfProcess(Long insId, Long stepId, Long lastprocessId, String processstatus,
			String processtype, Long fromuser, Date fromtime, Long touser,
			Date totime, String opinion, Date finishtime, Date worktime1,
			Date worktime2) {
		this.insId = insId;
		this.stepId = stepId;
		this.lastprocessId = lastprocessId;
		this.processstatus = processstatus;
		this.processtype = processtype;
		this.fromuser = fromuser;
		this.fromtime = fromtime;
		this.touser = touser;
		this.totime = totime;
		this.opinion = opinion;
		this.finishtime = finishtime;
		this.worktime1 = worktime1;
		this.worktime2 = worktime2;
	}

	// Property accessors

	public Long getProcessId() {
		return this.processId;
	}

	public void setProcessId(Long processId) {
		this.processId = processId;
	}

	public String getProcessstatus() {
		return this.processstatus;
	}

	public void setProcessstatus(String processstatus) {
		this.processstatus = processstatus;
	}

	public String getProcesstype() {
		return this.processtype;
	}

	public void setProcesstype(String processtype) {
		this.processtype = processtype;
	}

	public Long getFromuser() {
		return this.fromuser;
	}

	public void setFromuser(Long fromuser) {
		this.fromuser = fromuser;
	}

	public Date getFromtime() {
		return this.fromtime;
	}

	public void setFromtime(Date fromtime) {
		this.fromtime = fromtime;
	}

	public Long getTouser() {
		return this.touser;
	}

	public void setTouser(Long touser) {
		this.touser = touser;
	}

	public Date getTotime() {
		return this.totime;
	}

	public void setTotime(Date totime) {
		this.totime = totime;
	}

	public String getOpinion() {
		return this.opinion;
	}

	public void setOpinion(String opinion) {
		this.opinion = opinion;
	}

	public Date getFinishtime() {
		return this.finishtime;
	}

	public void setFinishtime(Date finishtime) {
		this.finishtime = finishtime;
	}

	public Date getWorktime1() {
		return this.worktime1;
	}

	public void setWorktime1(Date worktime1) {
		this.worktime1 = worktime1;
	}

	public Date getWorktime2() {
		return this.worktime2;
	}

	public void setWorktime2(Date worktime2) {
		this.worktime2 = worktime2;
	}

	public Long getInsId() {
		return insId;
	}

	public void setInsId(Long insId) {
		this.insId = insId;
	}

	public Long getStepId() {
		return stepId;
	}

	public void setStepId(Long stepId) {
		this.stepId = stepId;
	}

	public Long getLastprocessId() {
		return lastprocessId;
	}

	public void setLastprocessId(Long lastprocessId) {
		this.lastprocessId = lastprocessId;
	}

	public String getMaliciousRollback() {
		return maliciousRollback;
	}

	public void setMaliciousRollback(String maliciousRollback) {
		this.maliciousRollback = maliciousRollback;
	}

	public Long getFromuserDept() {
		return fromuserDept;
	}

	public void setFromuserDept(Long fromuserDept) {
		this.fromuserDept = fromuserDept;
	}

	public Long getTouserDept() {
		return touserDept;
	}

	public void setTouserDept(Long touserDept) {
		this.touserDept = touserDept;
	}


}