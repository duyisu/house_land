package com.zhongkai.model.business;

import java.util.HashSet;
import java.util.Set;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

import org.hibernate.annotations.Cache;
import org.hibernate.annotations.CacheConcurrencyStrategy;

/**
 * TWfDefinition entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="T_WF_DEFINITION")
@Cache(usage=CacheConcurrencyStrategy.READ_WRITE)
public class TWfDefinition implements java.io.Serializable {
	
	public static String CityareaGeneraldef="3"; //市区一般流程
	public static String TownshipGeneraldef="4"; //镇区一般流程
	public static String CityareaSimpledef="5"; //市区简易流程
	public static String TownshipSimpledef="6"; //镇区简易流程
	
	public static String UrbanPlat="1";	//市一级平台
	public static String TownPlat="2";	//镇二级平台

	// Fields
	@Id
	@SequenceGenerator(name="generator",allocationSize=1,initialValue=1, sequenceName="SEQ_WF_DEFINITION")
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator = "generator")
	@Column(name="DEF_ID")
	private Long defId;
	
	@Column(name="DEF_NAME")
	private String defName;
	
	@Column(name="FORMTABLE")
	private String formtable;
	
	@Column(name="FORMOBJECT")
	private String formobject;
	
	@Column(name="NOTE")
	private String note;
	
	@Column(name="DEF_LEVEL")
	private Long defLevel;
	
	@Version
	private Long version;
	
	// Constructors
	
	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	/** default constructor */
	public TWfDefinition() {
	}
	
	// Property accessors

	public Long getDefId() {
		return this.defId;
	}

	public void setDefId(Long defId) {
		this.defId = defId;
	}

	public String getDefName() {
		return this.defName;
	}

	public void setDefName(String defName) {
		this.defName = defName;
	}

	public String getFormtable() {
		return this.formtable;
	}

	public void setFormtable(String formtable) {
		this.formtable = formtable;
	}

	public String getFormobject() {
		return this.formobject;
	}

	public void setFormobject(String formobject) {
		this.formobject = formobject;
	}

	public String getNote() {
		return note;
	}

	public void setNote(String note) {
		this.note = note;
	}

	public Long getDefLevel() {
		return defLevel;
	}

	public void setDefLevel(Long defLevel) {
		this.defLevel = defLevel;
	}

}