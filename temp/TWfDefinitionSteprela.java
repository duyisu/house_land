package com.zhongkai.model.business;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.Version;

/**
 * TWfDefinitionSteprela entity.
 * 
 * @author MyEclipse Persistence Tools
 */
@Entity
@Table(name="T_WF_DEFINITION_STEPRELA")
public class TWfDefinitionSteprela implements java.io.Serializable {
	
	@Id
	@SequenceGenerator(name="generator",allocationSize=1,initialValue=1, sequenceName="SEQ_WF_DEFINITION_STEPRELA")
	@GeneratedValue(strategy=GenerationType.SEQUENCE,generator = "generator")
	@Column(name="STEPRELA_ID")
	private Long steprelaId;
	
	@Column(name="PRIORSTEP_ID")
	private Long priorstepId;
	
	@Column(name="TIPS")
	private String tips;
	
	@Version
	private Long version;
	
	@Column(name="NEXTSTEP_ID")
	private Long nextstepId;

	public Long getPriorstepId() {
		return priorstepId;
	}

	public void setPriorstepId(Long priorstepId) {
		this.priorstepId = priorstepId;
	}

	public Long getNextstepId() {
		return nextstepId;
	}

	public void setNextstepId(Long nextstepId) {
		this.nextstepId = nextstepId;
	}

	public Long getSteprelaId() {
		return steprelaId;
	}

	public void setSteprelaId(Long steprelaId) {
		this.steprelaId = steprelaId;
	}
	
	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	public String getTips() {
		return tips;
	}

	public void setTips(String tips) {
		this.tips = tips;
	}



}