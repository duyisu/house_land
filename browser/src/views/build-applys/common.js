export const applyKinds = [

    {
        value: 11,
        label:'新建-集中建房'
    },
    {
        value: 12,
        label:'新建-分散建房'
    },
    {
        value: 21,
        label:'拆旧建新-原址重建'
    },
    {
        value: 22,
        label:'拆旧建新-异地新建'
    },
    {
        value: 31,
        label:'扩建-原址加层'
    },
    {
        value: 32,
        label:'扩建-原址扩建'
    }
];
export const applyReasonSet = [
    {
        code: 1,
        desc: '(1)无自有住房'
    },
    {
        code: 2,
        desc: '(2)现有住房人均建筑面积低于30平方米'
    },
    {
        code: 3,
        desc: '(3)住房因国家建设项目征收或者乡镇、村公共设施和公益事业建设占用的'
    },
    {
        code: 4,
        desc: '(4)因自然灾害、政策性移民等，需要搬迁安置的'
    },
    {
        code: 5,
        desc: '(5)退出原有宅基地向集镇或者农村居民集中建房点集聚的'
    },
    {
        code: 6,
        desc: '(6)现有住房经鉴定属于危房需要拆除的'
    },
    {
        code: 7,
        desc: '(7)符合条例，为改善居住条件等原因需要拆旧建新的'
    },
    {
        code: 8,
        desc: '(8)可以申请住房建设的其他情形'
    },

];
export const landKindSet = [
    {
        code: 1,
        desc: '集中安置地'
    },
    {
        code: 2,
        desc: '宅基地'
    },
    {
        code: 3,
        desc: '自然保护地'
    },
    {
        code: 4,
        desc: '空闲地'
    },
    {
        code: 5,
        desc: '耕地'
    },
    {
        code: 6,
        desc: '其他未利用地'
    },

]
