import request from '@/utils/request'

export function add(data) {
  return request({
    url: 'api/illegal-two/case-records',
    method: 'post',
    data
  })
}

export function get(data){
  return request({
    url:'/api/illegal-two/caseRecordMyWorkList',
    method:'post',
    data
  })
}

// export function getById(id) {
//   return request({
//     url: 'api/case-record-flow-task-files/' + id,
//     method: 'get'
//   })
// }

// export function getByPage(page, size) {
//   return request({
//     url: 'api/case-records',
//     method: 'get',
//     data: {
//       page,
//       size
//     }
//   })
// }
