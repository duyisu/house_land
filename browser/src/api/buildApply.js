import request from '@/utils/request'
//分页查询
export function getBuildApplys(params) {
    return request({
        url: '/api/build-applys',
        method: 'get',
        params
    })
}
//草稿列表
export function getBuildApplyDraft(params) {
    return request({
        url: '/api/build-applys/buildApplyDraft',
        method: 'get',
        params
    })
}
//添加建房申请
export function addBuildApplys(params) {
    return request({
        url: '/api/build-applys',
        method: 'post',
        data: params
    })
}
//条件查询建房申请
export function getBuildApplysByCondition(params) {
    return request({
        url: '/api/build-applys/query',
        method: 'post',
        data: params
    })
}
//更新建房申请
export function updateBuildApplys(params) {
    return request({
        url: '/api/build-applys',
        method: 'put',
        params
    })
}
//删除建房申请
export function deleteBuildApplys(params) {
    return request({
        url: '/api/build-applys',
        method: 'delete',
        params
    })
}
//查看案件
export function viewBuildApply(params){
    return request({
        url: '/api/build-applys/viewBuildApply',
        method: 'post',
        params
    })
}

//根据当前流程步骤获取下一步骤列表
export function buildNextStep(params){
    return request({
        url: '/api/build-applys/buildNextStep',
        method: 'post',
        params
    })
}
//流程下一步发送至用户
export function buildNextStepUser(params){
    return request({
        url: '/api/build-applys/buildNextStepUser',
        method: 'post',
        params
    })
}
//处理案件
export function handleBuildApply(params){
    return request({
        url: '/api/build-applys/handleBuildApply',
        method: 'post',
        data:params
    })
}
//签收案件
export function signBuildApply(params){
    return request({
        url: '/api/build-applys/signBuildApply',
        method: 'post',
        data:params
    })
}
