import request from '@/utils/request'

export function login(username, password) {
  return request({
    url: 'login',
    method: 'post',
    data: {
      userName: username,
      password: password
    }
  })
}

export function getInfo() {
  return request({
    url: 'login/info',
    method: 'get'
  })
}

