import request from '@/utils/request'

export function getDeptsTree(params) {
  return request({
    url: 'api/departments/selected-tree',
    method: 'get',
    params
  })
}

export function add(data) {
  return request({
    url: 'api/departments',
    method: 'post',
    data
  })
}

export function del(id) {
  return request({
    url: 'api/departments/' + id,
    method: 'delete'
  })
}

export function edit(data) {
  return request({
    url: 'api/departments',
    method: 'put',
    data
  })
}
