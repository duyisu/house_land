const dict_use_states = [
  { key: '1', label: '正常' },
  { key: '0', label: '停用' }
]
const dict_flow_states = [
  { key: '1', label: '正常' },
  { key: '0', label: '停用' }
]
function getStateName(key, arr) {
  for (var i = 0; i < arr.length; i++) {
    if (arr[i].key === key) {
      return arr[i].label
    }
  }
}
export default{
  dict_use_states,
  dict_flow_states,
  getStateName
}

