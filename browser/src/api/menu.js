import request from '@/utils/request'

// 获取所有的菜单树
export function getMenusTree() {
  return request({
    url: 'api/menus/selected-tree',
    method: 'get'
  })
}

export function buildMenus() {
  return request({
    url: 'api/menus/route',
    method: 'get'
  })
}

export function getAllMenuPermissions() {
  return request({
    url: 'api/menus/permissions',
    method: 'get'
  })
}

export function add(data) {
  return request({
    url: 'api/menus',
    method: 'post',
    data
  })
}

export function del(id) {
  return request({
    url: 'api/menus/' + id,
    method: 'delete'
  })
}

export function edit(data) {
  return request({
    url: 'api/menus',
    method: 'put',
    data
  })
}
