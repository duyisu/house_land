import request from '@/utils/request'

export function getAreas(params) {
    return request({
        url: '/api/areas',
        method: 'get',
        params
    })
}

export function addArea(params) {
    return request({
        url: '/api/areas',
        method: 'post',
        params
    })
}

export function updateArea(params) {
    return request({
        url: '/api/areas',
        method: 'put',
        params
    })
}

export function deleteArea(params) {
    return request({
        url: '/api/areas',
        method: 'delete',
        params
    })
}
