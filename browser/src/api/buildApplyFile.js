import request from '@/utils/request'
//分页查询
export function addBuildApplyFiles(params) {
    return request({
        url: '/api/build-apply-files',
        method: 'post',
        data:params
    })
}
