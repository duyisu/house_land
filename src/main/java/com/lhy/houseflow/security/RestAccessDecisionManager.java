package com.lhy.houseflow.security;

import java.util.Collection;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.access.AccessDecisionManager;
import org.springframework.security.access.AccessDeniedException;
import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.authentication.InsufficientAuthenticationException;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class RestAccessDecisionManager implements AccessDecisionManager {
    @Override
    public void decide(Authentication authentication, Object object, Collection<ConfigAttribute> configAttributes)
        throws AccessDeniedException, InsufficientAuthenticationException {
        FilterInvocation filterInvocation = (FilterInvocation)object;
        HttpServletRequest request = filterInvocation.getHttpRequest();
        if (authentication == null) {
            log.info("Authentication is null");
            return;
        }
        
        log.info("start decide, Authentication name:{}; url:{}", authentication.getName(), filterInvocation.getRequestUrl());
        String url, method;
        for (GrantedAuthority ga : authentication.getAuthorities()) {
            if (ga instanceof RestGrantedAuthority) {
                RestGrantedAuthority grantedAuthority = (RestGrantedAuthority)ga;
                url = grantedAuthority.getUrl();
                if (grantedAuthority.isAllMethod()) {
                    method = null;
                } else {
                    method = grantedAuthority.getMethod();
                }
                if (matchers(url, request, method)) {
                    log.info("decide ok, Authentication name:{}; url:{}; method:{}", authentication.getName(), url, method);
                    return;
                }
            }
        }

        throw new AccessDeniedException("no right");
    }

    @Override
    public boolean supports(ConfigAttribute attribute) {
        return true;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return true;
    }

    private boolean matchers(String url, HttpServletRequest request, String method) {
        AntPathRequestMatcher matcher = new AntPathRequestMatcher(url, method);
        if (matcher.matches(request)) {
            return true;
        }
        return false;
    }

    private boolean matchers(String url, HttpServletRequest request) {
        return matchers(url, request, null);
    }

}
