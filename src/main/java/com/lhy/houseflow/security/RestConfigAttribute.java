package com.lhy.houseflow.security;

import javax.servlet.http.HttpServletRequest;

import org.springframework.security.access.ConfigAttribute;

public class RestConfigAttribute  implements ConfigAttribute {

    /**
     * 
     */
    private static final long serialVersionUID = -6809019509971324007L;
    private final HttpServletRequest httpServletRequest;

    public RestConfigAttribute(HttpServletRequest httpServletRequest) {
        this.httpServletRequest = httpServletRequest;
        httpServletRequest.getMethod();
    }


    @Override
    public String getAttribute() {
        return null;
    }

    public HttpServletRequest getHttpServletRequest() {
        return httpServletRequest;
    }
}