package com.lhy.houseflow.security;

import java.util.Collection;
import java.util.HashSet;

import org.springframework.security.access.ConfigAttribute;
import org.springframework.security.access.SecurityConfig;
import org.springframework.security.web.FilterInvocation;
import org.springframework.security.web.access.intercept.FilterInvocationSecurityMetadataSource;
import org.springframework.stereotype.Service;

import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
public class RestMetadataSourceService implements FilterInvocationSecurityMetadataSource {

    private static final Collection<ConfigAttribute> allAttributes = new HashSet<ConfigAttribute>();
    private static final String SECURITY_CONFIG_NONE = "none";

    @Override
    public Collection<ConfigAttribute> getAttributes(Object object) throws IllegalArgumentException {
        //此处不判断，留在AccessDecisionManager decide 判断
        if (allAttributes.isEmpty()){
            allAttributes.add(new SecurityConfig(SECURITY_CONFIG_NONE));
        }
        log.info("FilterInvocationSecurityMetadataSource getAttributes size {}", allAttributes.size());
        return allAttributes;
    }

    @Override
    public Collection<ConfigAttribute> getAllConfigAttributes() {
        return null;
    }

    @Override
    public boolean supports(Class<?> clazz) {
        return FilterInvocation.class.isAssignableFrom(clazz);
//        return true;
    }
}
