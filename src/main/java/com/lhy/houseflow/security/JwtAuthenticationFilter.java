package com.lhy.houseflow.security;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.web.filter.OncePerRequestFilter;

import com.alibaba.fastjson.JSON;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.JwtUser;
import com.lhy.houseflow.util.JwtUtil;

import io.jsonwebtoken.Claims;
import lombok.extern.slf4j.Slf4j;

@Slf4j
// @Component
public class JwtAuthenticationFilter extends OncePerRequestFilter {
    public JwtAuthenticationFilter() {}

    // @Qualifier("JwtUserDetailsServiceImpl")
    @Autowired
    private UserDetailsService jwtUserDetailsServiceImpl;// = new JwtUserDetailsServiceImpl();

    @Override
    protected void doFilterInternal(HttpServletRequest request, HttpServletResponse response, FilterChain chain)
        throws IOException, ServletException {
        try {
            log.info("url:{}  Method:{}", request.getRequestURI(), request.getMethod());
            if ("OPTIONS".equals(request.getMethod())) {
                // 过滤该请求，不往下级服务去转发请求，到此结束

                response.setHeader("Access-Control-Allow-Origin", "*");
                response.setHeader("Access-Control-Allow-Credentials", "true");
                response.setHeader("Access-Control-Allow-Methods", "GET, HEAD, POST, PUT, DELETE, OPTIONS, PATCH");
                response.setHeader("Access-Control-Max-Age", "3600");
                response.addHeader("Access-Control-Allow-Headers", "token,accesstoken,Content-type");
                return;
            }
            if (isPermit(request)) {
                log.info("permit url:{}", request.getRequestURI());
                setCors(response);
                chain.doFilter(request, response);
                return;
            }

            String token = JwtUtil.getTokenFromRequest(request);
            log.info("url:{};method:{};token:{}", request.getRequestURI(), request.getMethod(), token);
            // 没有token
            if (StringUtils.isBlank(token)) {
                log.info("no token return url:{}", request.getRequestURI());
                errorFilter(response, ResultInfo.Auth_UNAUTHORIZED);
                return;
            }

            Claims claims = JwtUtil.parseClaims(token);

            if (JwtUtil.isNotExpiration(claims)) {
                String userName = JwtUtil.parseUserName(claims);
                JwtUser jwtUser = null;
                if (StringUtils.isNotBlank(userName))
                    jwtUser = (JwtUser)jwtUserDetailsServiceImpl.loadUserByUsername(userName);
                if (jwtUser == null) {
                    log.error("can not find username:{}; jwtuser is null! ", userName);
                    errorFilter(response, ResultInfo.Auth_JWT_Expired);
                    return;
                }
                log.info("jwt user : {}", jwtUser);

                log.info("token username:{}", userName);
                SecurityContextHolder.getContext().setAuthentication(getAuthentication(jwtUser));
                chain.doFilter(request, response);
            } else {
                log.error("token Expiration");
                errorFilter(response, ResultInfo.Auth_JWT_Expired);
                //chain.doFilter(request, response);
            }
        } catch (Exception e) {
            e.printStackTrace();
            log.error(e.getMessage());
            errorFilter(response, ResultInfo.Unhandle_error);
        }

    }

    private UsernamePasswordAuthenticationToken getAuthentication(JwtUser jwtUser) {
        return new UsernamePasswordAuthenticationToken(jwtUser, null, jwtUser.getAuthorities());
    }

    private void errorFilter(HttpServletResponse response, ResultInfo resultInfo) {

        SecurityContextHolder.clearContext();
        // throw new CustomException(resultInfo);
        try {
            response.setCharacterEncoding("UTF-8");
            response.setContentType("application/json; charset=utf-8");
            response.getWriter().write(JSON.toJSONString(new ResponseInfo<String>(resultInfo)));
        } catch (IOException e) {
            e.printStackTrace();
            log.error(e.getMessage());
        }
    }

    private void setCors(HttpServletResponse response) {
        response.setHeader("Access-Control-Allow-Origin", "*");
        response.setHeader("Access-Control-Allow-Methods", "POST, GET, OPTIONS, DELETE, PUT");
        response.setHeader("Access-Control-Max-Age", "3600");
        response.setHeader("Access-Control-Allow-Headers",
            "Authorization, Content-Type, Accept, x-requested-with, Cache-Control");
    }

    private boolean isPermit(HttpServletRequest request) {

        for (AntPathRequestMatcher matcher : matchers) {
            if (matcher.matches(request)) {
                return true;
            }
        }
        return false;
    }

    private static final String POST_STR = "POST";
    private static final String GET_STR = "GET";
    private static final String GET_OPTIONS = "OPTIONS";

    private static List<AntPathRequestMatcher> matchers = new ArrayList<AntPathRequestMatcher>();
    private static AntPathRequestMatcher MATCHER_LOGIN_POST = new AntPathRequestMatcher("/login", POST_STR);
    static {
        
        matchers.add(new AntPathRequestMatcher("/login", GET_OPTIONS));
        matchers.add(new AntPathRequestMatcher("/applogin", POST_STR));
        matchers.add(new AntPathRequestMatcher("/app-download/**", GET_STR));
        matchers.add(MATCHER_LOGIN_POST);
        matchers.add(new AntPathRequestMatcher("/*.html", GET_STR));
        matchers.add(new AntPathRequestMatcher("/**/*.html", GET_STR));
        matchers.add(new AntPathRequestMatcher("/**/*.css", GET_STR));
        matchers.add(new AntPathRequestMatcher("/**/*.js", GET_STR));
        matchers.add(new AntPathRequestMatcher("/resources/**", GET_STR));

        matchers.add(new AntPathRequestMatcher("/swagger-ui.html**"));
        matchers.add(new AntPathRequestMatcher("/swagger-resources/**"));
        matchers.add(new AntPathRequestMatcher("/images/**"));
        matchers.add(new AntPathRequestMatcher("/webjars/**"));
        matchers.add(new AntPathRequestMatcher("/v2/api-docs"));
        matchers.add(new AntPathRequestMatcher("/springfox-swagger-ui/*"));
    }

}