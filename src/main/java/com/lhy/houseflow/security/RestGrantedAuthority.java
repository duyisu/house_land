package com.lhy.houseflow.security;

import org.apache.commons.lang3.StringUtils;
import org.springframework.security.core.GrantedAuthority;

public class RestGrantedAuthority implements GrantedAuthority {

    /**
     * 
     */
    private static final long serialVersionUID = -5782283159044773518L;

    private static final String SPLIT_STRING = "==";

    public static final String ALL_METHOD = "ALL";

    private String url;
    private String method;
    private String code;

    public RestGrantedAuthority(String url, String method) {
        this.url = url;
        this.method = method;
    }
    
    public RestGrantedAuthority(String url, String method, String code) {
        this.url = url;
        this.method = method;
        this.code = code;
    }    

    public RestGrantedAuthority(String authority) {
        String[] strs = StringUtils.splitByWholeSeparator(authority, SPLIT_STRING);
        if (strs != null && strs.length == 2) {
            url = strs[0];
            method = strs[1];
        }

    }

    public boolean isAllMethod() {
        return StringUtils.isBlank(method) || StringUtils.equalsAnyIgnoreCase(ALL_METHOD, method);
    }

    @Override
    public String getAuthority() {
        return url + SPLIT_STRING + method;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
