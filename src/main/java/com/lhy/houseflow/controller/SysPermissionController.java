/**
 * @filename:SysPermissionController 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.SysPermission;
import com.lhy.houseflow.entity.vo.MenuVo;
import com.lhy.houseflow.entity.vo.SysPermissionVo;
import com.lhy.houseflow.service.SysMenuService;
import com.lhy.houseflow.service.SysPermissionService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**   
 * 
 * @Description:  权限接口层
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Api(tags={"权限"})
@RestController
@RequestMapping("/api/permissions")
@Slf4j
public class SysPermissionController extends BaseController {
	
	@Autowired
	public SysPermissionService sysPermissionServiceImpl;
	@Autowired 
	private SysMenuService sysMenuService;
	
	/**
	 * @explain 查询权限对象  <swagger GET请求>
	 * @param   对象参数：id
	 * @return  sysPermission
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "获取权限信息", notes = "获取权限信息[sysPermission]，作者：lhy")
	@ApiImplicitParam(paramType="path", name = "id", value = "权限id", required = true, dataType = "Long")
	@RequestMapping(value = "/{id}",method = RequestMethod.GET, produces = "application/json") 
	public ResponseInfo<SysPermission> getSysPermissionById(@PathVariable("id")Long id){
		SysPermission result=sysPermissionServiceImpl.findById(id);
		return new ResponseInfo<SysPermission>(ResultInfo.Success, result);
	}
	
	/**
	 * @explain 添加权限对象
	 * @param   对象参数：sysPermission
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "添加权限", notes = "添加权限[sysPermission],作者：lhy")
	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseInfo<SysPermission> add(@RequestBody SysPermission sysPermission){
		sysPermissionServiceImpl.add(sysPermission);
		return new ResponseInfo<SysPermission>(ResultInfo.Success, sysPermission);
	}
	
	/**
	 * @explain 删除权限对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@DeleteMapping(value = "/{id}")
	@ApiOperation(value = "删除权限", notes = "删除权限,作者：lhy")
	@ApiImplicitParam(paramType="query", name = "id", value = "权限id", required = true, dataType = "Long")
	public ResponseInfo<String> deleteById(@PathVariable("id") Long id){
		sysPermissionServiceImpl.deleteById(id);
		return new ResponseInfo<String>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 修改权限对象
	 * @param   对象参数：sysPermission
	 * @return  sysPermission
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "修改权限", notes = "修改权限[sysPermission],作者：lhy")
	@RequestMapping(method = RequestMethod.PUT, produces = "application/json")
	public ResponseInfo<SysPermission> update(@RequestBody SysPermission sysPermission){
		sysPermissionServiceImpl.update(sysPermission);
		return new ResponseInfo<SysPermission>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 获取匹配权限
	 * @param   对象参数：sysPermission
	 * @return  List<SysPermission>
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "条件查询权限", notes = "条件查询[sysPermission],作者：lhy")
	@PostMapping("/query")
	public ResponseInfo<List<SysPermission>> querySysPermissionList(@RequestBody SysPermission sysPermission){
		List<SysPermission> list = sysPermissionServiceImpl.querySysPermissionList(sysPermission);
		return new ResponseInfo<List<SysPermission>>(ResultInfo.Success, list);	
	}
	
	/**
	 * @explain 分页条件查询权限   
	 * @param   对象参数：AppPage<SysPermission>
	 * @return  PageDataVo<SysPermission>
	 * @author  lhy
	 * @time    2019年5月5日
	 */

	@ApiOperation(value = "分页查询", notes = "分页查询返回对象[PageDataVo<SysPermission>],作者：lhy")
	@ApiImplicitParams({
        @ApiImplicitParam(paramType="query", name = "num", value = "当前页", required = false, dataType = "int"),
        @ApiImplicitParam(paramType="query", name = "size", value = "页行数", required = false, dataType = "int")
    })
	@RequestMapping(value = "",method = RequestMethod.GET, produces = "application/json")
	public ResponseInfo<PageInfo<SysPermissionVo>> getSysPermissionBySearch(
		@RequestParam(value="num",defaultValue ="1")  Integer num,
		@RequestParam(value="size",defaultValue ="100")  Integer size){
		AppPage<SysPermission> page =new AppPage<SysPermission>();
		page.setPageNum(num);
		page.setPageSize(size);

		PageInfo<SysPermission> pageInfo = sysPermissionServiceImpl.list(page);
		List<SysPermissionVo> list = new ArrayList<SysPermissionVo>();
		for (SysPermission info: pageInfo.getList()){
		    SysPermissionVo vo = new SysPermissionVo();
		    BeanUtils.copyProperties(info, vo);
		    MenuVo menuVo = new MenuVo();
		    BeanUtils.copyProperties(sysMenuService.findById(info.getMenuId()), menuVo);
		    vo.setMenu(menuVo);
		    list.add(vo);
		}
		PageInfo<SysPermissionVo> result = new PageInfo<SysPermissionVo>(list);
		result.setTotal(pageInfo.getTotal());
		result.setSize(pageInfo.getSize());
		return new ResponseInfo<PageInfo<SysPermissionVo>>(ResultInfo.Success, result);	
	}
}