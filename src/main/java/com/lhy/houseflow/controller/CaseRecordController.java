/**
 * @filename:CaseRecordController 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.CaseRecord;
import com.lhy.houseflow.service.CaseRecordService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**   
 * 
 * @Description:  立案登记接口层
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
//@Api(tags={"立案登记"})
//@RestController
//@RequestMapping("/api/case-records")
@Slf4j
public class CaseRecordController extends BaseController {
	
	@Autowired
	public CaseRecordService caseRecordServiceImpl;
	
	/**
	 * @explain 查询立案登记对象  <swagger GET请求>
	 * @param   对象参数：id
	 * @return  caseRecord
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "获取立案登记信息", notes = "获取立案登记信息[caseRecord]，作者：lhy")
	@ApiImplicitParam(paramType="path", name = "id", value = "立案登记id", required = true, dataType = "Long")
	@RequestMapping(value = "/{id}",method = RequestMethod.GET, produces = "application/json") 
	public ResponseInfo<CaseRecord> getCaseRecordById(@PathVariable("id")Long id){
		CaseRecord result=caseRecordServiceImpl.findById(id);
		return new ResponseInfo<CaseRecord>(ResultInfo.Success, result);
	}
	
	/**
	 * @explain 添加立案登记对象
	 * @param   对象参数：caseRecord
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "添加立案登记", notes = "添加立案登记[caseRecord],作者：lhy")
	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseInfo<CaseRecord> add(@RequestBody CaseRecord caseRecord){
		caseRecordServiceImpl.add(caseRecord);
		return new ResponseInfo<CaseRecord>(ResultInfo.Success, caseRecord);
	}
	
	/**
	 * @explain 删除立案登记对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@RequestMapping(method = RequestMethod.DELETE, produces = "application/json")
	@ApiOperation(value = "删除立案登记", notes = "删除立案登记,作者：lhy")
	@ApiImplicitParam(paramType="query", name = "id", value = "立案登记id", required = true, dataType = "Long")
	public ResponseInfo<String> deleteById(Long id){
		caseRecordServiceImpl.deleteById(id);
		return new ResponseInfo<String>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 修改立案登记对象
	 * @param   对象参数：caseRecord
	 * @return  caseRecord
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "修改立案登记", notes = "修改立案登记[caseRecord],作者：lhy")
	@RequestMapping(method = RequestMethod.PUT, produces = "application/json")
	public ResponseInfo<CaseRecord> update(@RequestBody CaseRecord caseRecord){
		caseRecordServiceImpl.update(caseRecord);
		return new ResponseInfo<CaseRecord>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 获取匹配立案登记
	 * @param   对象参数：caseRecord
	 * @return  List<CaseRecord>
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "条件查询立案登记", notes = "条件查询[caseRecord],作者：lhy")
	@PostMapping("/query")
	public ResponseInfo<List<CaseRecord>> queryCaseRecordList(@RequestBody CaseRecord caseRecord){
		List<CaseRecord> list = caseRecordServiceImpl.queryCaseRecordList(caseRecord);
		return new ResponseInfo<List<CaseRecord>>(ResultInfo.Success, list);	
	}
	
	/**
	 * @explain 分页条件查询立案登记   
	 * @param   对象参数：AppPage<CaseRecord>
	 * @return  PageDataVo<CaseRecord>
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@GetMapping("/")
	@ApiOperation(value = "分页查询", notes = "分页查询返回对象[PageDataVo<CaseRecord>],作者：lhy")
	@ApiImplicitParams({
        @ApiImplicitParam(paramType="query", name = "page", value = "当前页", required = true, dataType = "int"),
        @ApiImplicitParam(paramType="query", name = "size", value = "页行数", required = true, dataType = "int")
    })
	
	public ResponseInfo<PageInfo<CaseRecord>> getCaseRecordBySearch(
		@RequestParam(value="page",defaultValue ="1")  Integer page,
		@RequestParam(value="size",defaultValue ="100")  Integer size){
		AppPage<CaseRecord> aPage =new AppPage<CaseRecord>();
		aPage.setPageNum(page);
		aPage.setPageSize(size);
		//其他参数
//		CaseRecord caseRecord=new CaseRecord();
//		page.setParam(caseRecord);
		//分页数据
		PageInfo<CaseRecord> pageInfo = caseRecordServiceImpl.list(aPage);
		return new ResponseInfo<PageInfo<CaseRecord>>(ResultInfo.Success, pageInfo);	
	}
}