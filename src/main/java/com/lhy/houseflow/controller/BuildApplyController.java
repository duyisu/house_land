/**
 * @filename:BuildApplyController 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.constant.FlowConst;
import com.lhy.houseflow.entity.BuildApply;
import com.lhy.houseflow.entity.CaseRecord;
import com.lhy.houseflow.entity.SysUser;
import com.lhy.houseflow.entity.WfDefinitionStep;
import com.lhy.houseflow.entity.vo.BuildApplyVo;
import com.lhy.houseflow.entity.vo.CaseRecordBaseInfoVo;
import com.lhy.houseflow.entity.vo.QryBuildApplyWorkVo;
import com.lhy.houseflow.service.BuildApplyService;
import com.lhy.houseflow.service.IllegalBuildingLandService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**   
 * 
 * @Description:  建房申请接口层
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Api(tags={"建房申请"})
@RestController
@RequestMapping("/api/build-applys")
@Slf4j
public class BuildApplyController extends BaseController {
	
	@Autowired
	public BuildApplyService buildApplyServiceImpl;
	@Autowired
	private IllegalBuildingLandService illegalBuildingLandService;
	
	/**
	 * @explain 添加建房申请对象
	 * @param   对象参数：buildApply
	 */
	@ApiOperation(value = "添加建房申请", notes = "添加建房申请[buildApply],此时可区分保存与提交按钮：保存即未草稿，保存status=1,提交设置status=2，下一步的列表需要从后端查询，选中的把相应的stepid传到后台")
	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseInfo<BuildApply> add(@RequestBody BuildApply buildApply){
		buildApplyServiceImpl.add(buildApply, currUser().getId());
		return new ResponseInfo<BuildApply>(ResultInfo.Success, buildApply);
	}
	@ApiOperation(value = "流程下一步发送至用户", notes = "查看或处理时得到stepId,根据stepId获取流程流转人员")
    @ApiImplicitParams({
    @ApiImplicitParam(paramType = "path", name = "stepId", value = "流程步骤ID", required = true, dataType = "Long")})
    @RequestMapping(value = "/buildNextStepUser", method = RequestMethod.POST, produces = "application/json")
    public ResponseInfo<List<SysUser>> buildNextStepUser(@RequestParam(value = "stepId", required = true) Long stepId) {
        List<SysUser> list = illegalBuildingLandService.wfToUser(Long.valueOf(stepId));
        return new ResponseInfo<List<SysUser>>(ResultInfo.Success, list);
    }
    
	/**
	 * 根据当前流程处理步骤 查询下一步骤 遇到多个 需要在页面上做出选择
	 * @param stepId
	 * @return
	 */
	@ApiOperation(value = "根据当前流程步骤获取下一步骤列表", notes = "查看案件后，根据案件的当前流程查询下一步可能的步骤，在页面上把该列表作为分支，选择的单选框即代表流程的走向")
	@ApiImplicitParams({
	    @ApiImplicitParam(paramType = "path", name = "stepId", value = "流程步骤ID", required = true, dataType = "Long")})
	@RequestMapping(value = "/buildNextStep", method = RequestMethod.POST,produces = "application/json")
    public ResponseInfo<List<WfDefinitionStep>> buildNextStep(@RequestParam(value = "stepId") Long stepId){
		List<WfDefinitionStep> defStepList = illegalBuildingLandService.findNextSteps(stepId);
    	return new ResponseInfo<List<WfDefinitionStep>>(ResultInfo.Success,defStepList);
    }
    
	@ApiOperation(value = "查看案件", notes = "根据insId查看案件 在页面中需要向后台加载案件信息,附件列表，已经处理的流程列表，当前的状态是否已签收，是否处于办结状态 等等")
	@ApiImplicitParams({
	    @ApiImplicitParam(paramType = "path", name = "insId", value = "流程实例ID", required = true, dataType = "Long")})
	@RequestMapping(value = "/viewBuildApply", method = RequestMethod.POST,produces = "application/json")
    public ResponseInfo<BuildApplyVo> viewBuildApply(@RequestParam(value = "insId") Long insId){
		BuildApplyVo buildApplyVo = buildApplyServiceImpl.viewBuildApply(insId);
    	return new ResponseInfo<BuildApplyVo>(ResultInfo.Success,buildApplyVo);
    }
    
	@ApiOperation(value = "签收案件", notes = "根据insId查看案件后，未签收的案件 进行签收，签收后继续查看案件详情，可维持下一步操作")
	@ApiImplicitParams({
	    @ApiImplicitParam(paramType = "path", name = "insId", value = "流程实例ID", required = true, dataType = "Long")})
	@RequestMapping(value = "/signBuildApply", method = RequestMethod.POST,produces = "application/json")
    public ResponseInfo<BuildApplyVo> signBuildApply(@RequestParam(value = "insId") Long insId){
		illegalBuildingLandService.signProcess(currUser().getId(), insId);
		BuildApplyVo buildApplyVo = buildApplyServiceImpl.viewBuildApply(insId);
    	return new ResponseInfo<BuildApplyVo>(ResultInfo.Success,buildApplyVo);
    }
	
	@ApiOperation(value = "处理案件", notes = "根据insId查看案件后，未签收的案件 进行签收，签收后继续查看案件详情，可继续处理，可维持下一步操作")
	@ApiImplicitParams({
	    @ApiImplicitParam(paramType = "path", name = "insId", value = "流程实例ID", required = true, dataType = "Long"),
	    @ApiImplicitParam(paramType = "path", name = "currStepId", value = "当前处理流程ID", required = true, dataType = "Long"),
	    @ApiImplicitParam(paramType = "path", name = "nextStepId", value = "下一步处理流程ID", required = false, dataType = "Long"),
	    @ApiImplicitParam(paramType = "path", name = "processOpinion", value = "处理意见", required = false, dataType = "String"),
	    @ApiImplicitParam(paramType = "path", name = "processOpinionDesc", value = "处理意见描述", required = false, dataType = "String")})
	@RequestMapping(value = "/handleBuildApply", method = RequestMethod.POST,produces = "application/json")
    public ResponseInfo<BuildApplyVo> handleBuildApply(@RequestParam(value = "insId") Long insId,
    		@RequestParam(value = "currStepId") Long currStepId,
    		@RequestParam(value = "nextStepId") Long nextStepId,
    		@RequestParam(value = "processOpinion") String processOpinion,
    		@RequestParam(value = "processOpinionDesc") String processOpinionDesc){
		buildApplyServiceImpl.handleProcess(currUser().getId(), insId, currStepId, nextStepId, processOpinion);
		BuildApplyVo buildApplyVo = buildApplyServiceImpl.viewBuildApply(insId);
    	return new ResponseInfo<BuildApplyVo>(ResultInfo.Success,buildApplyVo);
    }
	@ApiOperation(value = "建房审批流程办结", notes = "根据insId办结流程")
    @RequestMapping(value = "/finishBuildApply", method = RequestMethod.POST, produces = "application/json")
    public ResponseInfo<BuildApplyVo> finishBuildApply( @RequestParam(value = "insId") Long insId,String processOpinion ) {
		buildApplyServiceImpl.finishProcess(currUser().getId(), insId, processOpinion,FlowConst.CASE_STATUS_FINISH);
		BuildApplyVo buildApplyVo = buildApplyServiceImpl.viewBuildApply(insId);
        return new ResponseInfo<BuildApplyVo>(ResultInfo.Success, buildApplyVo);
    }
	
	@ApiOperation(value = "建房审批流程销案", notes = "根据insId销案流程")
    @RequestMapping(value = "/cancelBuildApply", method = RequestMethod.POST, produces = "application/json")
    public ResponseInfo<BuildApplyVo> cancelBuildApply( @RequestParam(value = "insId") Long insId,String processOpinion ) {
		buildApplyServiceImpl.finishProcess(currUser().getId(), insId, processOpinion,FlowConst.CASE_STATUS_ABORTED);
		BuildApplyVo buildApplyVo = buildApplyServiceImpl.viewBuildApply(insId);
        return new ResponseInfo<BuildApplyVo>(ResultInfo.Success, buildApplyVo);
    }
	
	
	
	
	/**
	 * @explain 查询建房申请对象  <swagger GET请求>
	 * @param   对象参数：id
	 * @return  buildApply
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "获取建房申请信息", notes = "获取建房申请信息[buildApply]，作者：lhy")
	@ApiImplicitParam(paramType="path", name = "id", value = "建房申请id", required = true, dataType = "Long")
	@RequestMapping(value = "/{id}",method = RequestMethod.GET, produces = "application/json") 
	public ResponseInfo<BuildApply> getBuildApplyById(@PathVariable("id")Long id){
		BuildApply result=buildApplyServiceImpl.findById(id);
		return new ResponseInfo<BuildApply>(ResultInfo.Success, result);
	}
	
	
	
	/**
	 * @explain 删除建房申请对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@RequestMapping(method = RequestMethod.DELETE, produces = "application/json")
	@ApiOperation(value = "删除建房申请", notes = "删除建房申请,作者：lhy")
	@ApiImplicitParam(paramType="query", name = "id", value = "建房申请id", required = true, dataType = "Long")
	public ResponseInfo<String> deleteById(Long id){
		buildApplyServiceImpl.deleteById(id);
		return new ResponseInfo<String>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 修改建房申请对象
	 * @param   对象参数：buildApply
	 * @return  buildApply
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "修改建房申请", notes = "修改建房申请[buildApply],作者：lhy")
	@RequestMapping(method = RequestMethod.PUT, produces = "application/json")
	public ResponseInfo<BuildApply> update(@RequestBody BuildApply buildApply){
		buildApplyServiceImpl.update(buildApply);
		return new ResponseInfo<BuildApply>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 获取匹配建房申请
	 * @param   对象参数：buildApply
	 * @return  List<BuildApply>
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "条件查询建房申请", notes = "条件查询[buildApply],作者：lhy")
	@PostMapping("/query")
	public ResponseInfo<List<BuildApply>> queryBuildApplyList(@RequestBody BuildApply buildApply){
		List<BuildApply> list = buildApplyServiceImpl.queryBuildApplyList(buildApply);
		return new ResponseInfo<List<BuildApply>>(ResultInfo.Success, list);	
	}
	
	@ApiOperation(value = "草稿列表，未进入流程", notes = "分页查询返回对象[PageDataVo<BuildApply>],作者：lhy")
	@ApiImplicitParams({
    @ApiImplicitParam(paramType="query", name = "page", value = "当前页", required = true, dataType = "int"),
    @ApiImplicitParam(paramType="query", name = "size", value = "页行数", required = true, dataType = "int")
    })
	@RequestMapping(value = "/buildApplyDraft",method = RequestMethod.GET, produces = "application/json")
	public ResponseInfo<PageInfo<BuildApply>> buildApplyDraft(
		@RequestParam(value="page",defaultValue ="1")  Integer page,
		@RequestParam(value="size",defaultValue ="100")  Integer size){
		AppPage<BuildApply> apage =new AppPage<BuildApply>();
		apage.setPageNum(page);
		apage.setPageSize(size);
		//其他参数
//		BuildApply buildApply=new BuildApply();
//		page.setParam(buildApply);
		//分页数据
		PageInfo<BuildApply> pageInfo = buildApplyServiceImpl.draftList(apage);
		return new ResponseInfo<PageInfo<BuildApply>>(ResultInfo.Success, pageInfo);	
	}
	
	@ApiOperation(value = "待办", notes = "分页查询返回对象<BuildApply>]")
	@RequestMapping(value = "/buildApplyWaitTodo",method = RequestMethod.GET, produces = "application/json")
	public ResponseInfo<PageInfo<BuildApply>> buildApplyWaitTodo(@RequestBody  QryBuildApplyWorkVo qryVo){
		AppPage<BuildApply> apage =new AppPage<BuildApply>();
		apage.setPageNum(qryVo.getPageIndex());
		apage.setPageSize(qryVo.getPageSize());
		//分页数据
		PageInfo<BuildApply> pageInfo = buildApplyServiceImpl.myWorkList(currUser().getId(),FlowConst.MY_WORK_TODO, apage);
		return new ResponseInfo<PageInfo<BuildApply>>(ResultInfo.Success, pageInfo);	
	}
	
	@ApiOperation(value = "已办", notes = "分页查询返回对象<BuildApply>]")
	@RequestMapping(value = "/buildApplyhasDone",method = RequestMethod.GET, produces = "application/json")
	public ResponseInfo<PageInfo<BuildApply>> buildApplyhasDone(@RequestBody QryBuildApplyWorkVo qryVo){
		AppPage<BuildApply> apage =new AppPage<BuildApply>();
		apage.setPageNum(qryVo.getPageIndex());
		apage.setPageSize(qryVo.getPageSize());
		//分页数据
		PageInfo<BuildApply> pageInfo = buildApplyServiceImpl.myWorkList(currUser().getId(),FlowConst.MY_WORK_HAS_DONE, apage);
		return new ResponseInfo<PageInfo<BuildApply>>(ResultInfo.Success, pageInfo);	
	}
	
	/**
	 * @explain 分页条件查询建房申请   
	 * @param   对象参数：AppPage<BuildApply>
	 * @return  PageDataVo<BuildApply>
	 * @author  lhy
	 * @time    2019年5月5日
	 */

	@ApiOperation(value = "分页查询", notes = "分页查询返回对象[PageDataVo<BuildApply>],作者：lhy")
	@ApiImplicitParams({
    @ApiImplicitParam(paramType="query", name = "page", value = "当前页", required = true, dataType = "int"),
    @ApiImplicitParam(paramType="query", name = "size", value = "页行数", required = true, dataType = "int")
    })
	@RequestMapping(value = "",method = RequestMethod.GET, produces = "application/json")
	public ResponseInfo<PageInfo<BuildApply>> getBuildApplyBySearch(
		@RequestParam(value="page",defaultValue ="1")  Integer page,
		@RequestParam(value="size",defaultValue ="100")  Integer size){
		AppPage<BuildApply> apage =new AppPage<BuildApply>();
		apage.setPageNum(page);
		apage.setPageSize(size);
		//其他参数
//		BuildApply buildApply=new BuildApply();
//		page.setParam(buildApply);
		//分页数据
		PageInfo<BuildApply> pageInfo = buildApplyServiceImpl.list(apage);
		return new ResponseInfo<PageInfo<BuildApply>>(ResultInfo.Success, pageInfo);	
	}
}