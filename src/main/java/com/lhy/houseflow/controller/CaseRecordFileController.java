/**
 * @filename:CaseRecordFileController 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.CaseRecordFile;
import com.lhy.houseflow.service.CaseRecordFileService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**   
 * 
 * @Description:  立案登记附件表接口层
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Api(tags={"立案登记附件表"})
@RestController
@RequestMapping("/api/case-record-files")
@Slf4j
public class CaseRecordFileController extends BaseController {
	
	@Autowired
	public CaseRecordFileService caseRecordFileServiceImpl;
	
	/**
	 * @explain 查询立案登记附件表对象  <swagger GET请求>
	 * @param   对象参数：id
	 * @return  caseRecordFile
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "获取立案登记附件表信息", notes = "获取立案登记附件表信息[caseRecordFile]，作者：lhy")
	@ApiImplicitParam(paramType="path", name = "id", value = "立案登记附件表id", required = true, dataType = "Long")
	@RequestMapping(value = "/{id}",method = RequestMethod.GET, produces = "application/json") 
	public ResponseInfo<CaseRecordFile> getCaseRecordFileById(@PathVariable("id")Long id){
		CaseRecordFile result=caseRecordFileServiceImpl.findById(id);
		return new ResponseInfo<CaseRecordFile>(ResultInfo.Success, result);
	}
	
	/**
	 * @explain 添加立案登记附件表对象
	 * @param   对象参数：caseRecordFile
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "添加立案登记附件表", notes = "添加立案登记附件表[caseRecordFile],作者：lhy")
	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseInfo<CaseRecordFile> add(@RequestBody CaseRecordFile caseRecordFile){
		caseRecordFileServiceImpl.add(caseRecordFile);
		return new ResponseInfo<CaseRecordFile>(ResultInfo.Success, caseRecordFile);
	}
	
	/**
	 * @explain 删除立案登记附件表对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@RequestMapping(method = RequestMethod.DELETE, produces = "application/json")
	@ApiOperation(value = "删除立案登记附件表", notes = "删除立案登记附件表,作者：lhy")
	@ApiImplicitParam(paramType="query", name = "id", value = "立案登记附件表id", required = true, dataType = "Long")
	public ResponseInfo<String> deleteById(Long id){
		caseRecordFileServiceImpl.deleteById(id);
		return new ResponseInfo<String>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 修改立案登记附件表对象
	 * @param   对象参数：caseRecordFile
	 * @return  caseRecordFile
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "修改立案登记附件表", notes = "修改立案登记附件表[caseRecordFile],作者：lhy")
	@RequestMapping(method = RequestMethod.PUT, produces = "application/json")
	public ResponseInfo<CaseRecordFile> update(@RequestBody CaseRecordFile caseRecordFile){
		caseRecordFileServiceImpl.update(caseRecordFile);
		return new ResponseInfo<CaseRecordFile>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 获取匹配立案登记附件表
	 * @param   对象参数：caseRecordFile
	 * @return  List<CaseRecordFile>
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "条件查询立案登记附件表", notes = "条件查询[caseRecordFile],作者：lhy")
	@PostMapping("/query")
	public ResponseInfo<List<CaseRecordFile>> queryCaseRecordFileList(@RequestBody CaseRecordFile caseRecordFile){
		List<CaseRecordFile> list = caseRecordFileServiceImpl.queryCaseRecordFileList(caseRecordFile);
		return new ResponseInfo<List<CaseRecordFile>>(ResultInfo.Success, list);	
	}
	
	/**
	 * @explain 分页条件查询立案登记附件表   
	 * @param   对象参数：AppPage<CaseRecordFile>
	 * @return  PageDataVo<CaseRecordFile>
	 * @author  lhy
	 * @time    2019年5月5日
	 */

	@ApiOperation(value = "分页查询", notes = "分页查询返回对象[PageDataVo<CaseRecordFile>],作者：lhy")
	@ApiImplicitParams({
        @ApiImplicitParam(paramType="query", name = "num", value = "当前页", required = true, dataType = "int"),
        @ApiImplicitParam(paramType="query", name = "size", value = "页行数", required = true, dataType = "int")
    })
	@RequestMapping(value = "",method = RequestMethod.GET, produces = "application/json")
	public ResponseInfo<PageInfo<CaseRecordFile>> getCaseRecordFileBySearch(
		@RequestParam(value="page",defaultValue ="1")  Integer page,
		@RequestParam(value="size",defaultValue ="100")  Integer size){
		AppPage<CaseRecordFile> aPage =new AppPage<CaseRecordFile>();
		aPage.setPageNum(page);
		aPage.setPageSize(size);
		//其他参数
//		CaseRecordFile caseRecordFile=new CaseRecordFile();
//		page.setParam(caseRecordFile);
		//分页数据
		PageInfo<CaseRecordFile> pageInfo = caseRecordFileServiceImpl.list(aPage);
		return new ResponseInfo<PageInfo<CaseRecordFile>>(ResultInfo.Success, pageInfo);	
	}
}