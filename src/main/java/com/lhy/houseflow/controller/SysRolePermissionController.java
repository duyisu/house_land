/**
 * @filename:SysRolePermissionController 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.SysRolePermission;
import com.lhy.houseflow.service.SysRolePermissionService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**   
 * 
 * @Description:  角色权限接口层
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Api(tags={"角色权限"})
@RestController
@RequestMapping("/api/sys-role-permissions")
@Slf4j
public class SysRolePermissionController extends BaseController {
	
	@Autowired
	public SysRolePermissionService sysRolePermissionServiceImpl;
	
	/**
	 * @explain 查询角色权限对象  <swagger GET请求>
	 * @param   对象参数：id
	 * @return  sysRolePermission
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "获取角色权限信息", notes = "获取角色权限信息[sysRolePermission]，作者：lhy")
	@ApiImplicitParam(paramType="path", name = "id", value = "角色权限id", required = true, dataType = "Long")
	@RequestMapping(value = "/{id}",method = RequestMethod.GET, produces = "application/json") 
	public ResponseInfo<SysRolePermission> getSysRolePermissionById(@PathVariable("id")Long id){
		SysRolePermission result=sysRolePermissionServiceImpl.findById(id);
		return new ResponseInfo<SysRolePermission>(ResultInfo.Success, result);
	}
	
	/**
	 * @explain 添加角色权限对象
	 * @param   对象参数：sysRolePermission
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "添加角色权限", notes = "添加角色权限[sysRolePermission],作者：lhy")
	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseInfo<SysRolePermission> add(@RequestBody SysRolePermission sysRolePermission){
		sysRolePermissionServiceImpl.add(sysRolePermission);
		return new ResponseInfo<SysRolePermission>(ResultInfo.Success, sysRolePermission);
	}
	
	/**
	 * @explain 删除角色权限对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@RequestMapping(method = RequestMethod.DELETE, produces = "application/json")
	@ApiOperation(value = "删除角色权限", notes = "删除角色权限,作者：lhy")
	@ApiImplicitParam(paramType="query", name = "id", value = "角色权限id", required = true, dataType = "Long")
	public ResponseInfo<String> deleteById(Long id){
		sysRolePermissionServiceImpl.deleteById(id);
		return new ResponseInfo<String>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 修改角色权限对象
	 * @param   对象参数：sysRolePermission
	 * @return  sysRolePermission
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "修改角色权限", notes = "修改角色权限[sysRolePermission],作者：lhy")
	@RequestMapping(method = RequestMethod.PUT, produces = "application/json")
	public ResponseInfo<SysRolePermission> update(@RequestBody SysRolePermission sysRolePermission){
		sysRolePermissionServiceImpl.update(sysRolePermission);
		return new ResponseInfo<SysRolePermission>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 获取匹配角色权限
	 * @param   对象参数：sysRolePermission
	 * @return  List<SysRolePermission>
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "条件查询角色权限", notes = "条件查询[sysRolePermission],作者：lhy")
	@PostMapping("/query")
	public ResponseInfo<List<SysRolePermission>> querySysRolePermissionList(@RequestBody SysRolePermission sysRolePermission){
		List<SysRolePermission> list = sysRolePermissionServiceImpl.querySysRolePermissionList(sysRolePermission);
		return new ResponseInfo<List<SysRolePermission>>(ResultInfo.Success, list);	
	}
	
	/**
	 * @explain 分页条件查询角色权限   
	 * @param   对象参数：AppPage<SysRolePermission>
	 * @return  PageDataVo<SysRolePermission>
	 * @author  lhy
	 * @time    2019年5月5日
	 */

	@ApiOperation(value = "分页查询", notes = "分页查询返回对象[PageDataVo<SysRolePermission>],作者：lhy")
	@ApiImplicitParams({
        @ApiImplicitParam(paramType="query", name = "page", value = "当前页", required = true, dataType = "int"),
        @ApiImplicitParam(paramType="query", name = "size", value = "页行数", required = true, dataType = "int")
    })
	@RequestMapping(value = "",method = RequestMethod.GET, produces = "application/json")
	public ResponseInfo<PageInfo<SysRolePermission>> getSysRolePermissionBySearch(
		@RequestParam(value="page",defaultValue ="1")  Integer page,
		@RequestParam(value="size",defaultValue ="100")  Integer size){
		AppPage<SysRolePermission> aPage =new AppPage<SysRolePermission>();
		aPage.setPageNum(page);
		aPage.setPageSize(size);
		//其他参数
//		SysRolePermission sysRolePermission=new SysRolePermission();
//		page.setParam(sysRolePermission);
		//分页数据
		PageInfo<SysRolePermission> pageInfo = sysRolePermissionServiceImpl.list(aPage);
		return new ResponseInfo<PageInfo<SysRolePermission>>(ResultInfo.Success, pageInfo);	
	}
}