/**
 * @filename:SysUserController 2019年5月5日
 * @project HouseFlow V1.0 Copyright(c) 2018 lhy Co. Ltd. All right reserved.
 */
package com.lhy.houseflow.controller;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.SysUser;
import com.lhy.houseflow.entity.dto.UserDto;
import com.lhy.houseflow.entity.vo.DepartmentVo;
import com.lhy.houseflow.entity.vo.SysUserVo;
import com.lhy.houseflow.service.DepartmentService;
import com.lhy.houseflow.service.SysUserService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @Description: 用户接口层
 * @Author: lhy
 * @CreateDate: 2019年5月5日
 * @Version: V1.0
 * 
 */
@Api(tags = {"用户"})
@RestController
@RequestMapping("/api/users")
@Slf4j
public class SysUserController extends BaseController {

    @Autowired
    private SysUserService sysUserServiceImpl;
    @Autowired
    private DepartmentService departmentService;

    /**
     * @explain 查询用户对象 <swagger GET请求>
     * @param 对象参数：id
     * @return sysUser
     * @author lhy
     * @time 2019年5月5日
     */

    @ApiOperation(value = "获取用户信息", notes = "获取用户信息[sysUser]，作者：lhy")
    @ApiImplicitParam(paramType = "path", name = "id", value = "用户id", required = true, dataType = "Long")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
    public ResponseInfo<SysUser> getSysUserById(@PathVariable("id") Long id) {
        SysUser user = sysUserServiceImpl.findById(id);
        return new ResponseInfo<SysUser>(ResultInfo.Success, user);
    }

    /**
     * @explain 添加用户对象
     * @param 对象参数：sysUser
     * @return int
     * @author lhy
     * @time 2019年5月5日
     */
    @ApiOperation(value = "添加用户", notes = "添加用户[sysUser],作者：lhy")
    @RequestMapping(method = RequestMethod.POST, produces = "application/json")
    public ResponseInfo<UserDto> add(@RequestBody UserDto user) {
        sysUserServiceImpl.add(user);
        return new ResponseInfo<UserDto>(ResultInfo.Success, user);
    }

    /**
     * @explain 删除用户对象
     * @param 对象参数：id
     * @return int
     * @author lhy
     * @time 2019年5月5日
     */
    @DeleteMapping(value = "/{id}")
    @ApiOperation(value = "删除用户", notes = "删除用户,作者：lhy")
    @ApiImplicitParam(paramType = "query", name = "id", value = "用户id", required = true, dataType = "Long")
    public ResponseInfo<String> deleteById(@PathVariable("id") Long id) {
        sysUserServiceImpl.deleteById(id);
        return new ResponseInfo<String>(ResultInfo.Success, null);
    }

    /**
     * @explain 修改用户对象
     * @param 对象参数：sysUser
     * @return sysUser
     * @author lhy
     * @time 2019年5月5日
     */
    @ApiOperation(value = "修改用户", notes = "修改用户[sysUser],作者：lhy")
    @RequestMapping(method = RequestMethod.PUT, produces = "application/json")
    public ResponseInfo<UserDto> update(@RequestBody UserDto user) {
        log.info(user.toString());
        sysUserServiceImpl.update(user);
        return new ResponseInfo<UserDto>(ResultInfo.Success, null);
    }

    /**
     * @explain 获取匹配用户
     * @param 对象参数：sysUser
     * @return List<SysUser>
     * @author lhy
     * @time 2019年5月5日
     */
    @ApiOperation(value = "条件查询用户", notes = "条件查询[sysUser],作者：lhy")
    @PostMapping("/query")
    public ResponseInfo<List<UserDto>> querySysUserList(@RequestBody SysUser sysUser) {
        List<UserDto> list = sysUserServiceImpl.querySysUserList(sysUser);
        return new ResponseInfo<List<UserDto>>(ResultInfo.Success, list);
    }

    /**
     * @explain 分页条件查询用户
     * @param 对象参数：AppPage<SysUser>
     * @return PageDataVo<SysUser>
     * @author lhy
     * @time 2019年5月5日
     */

    @ApiOperation(value = "分页查询", notes = "分页查询返回对象[PageDataVo<SysUser>],作者：lhy")
    @ApiImplicitParams({
        @ApiImplicitParam(paramType = "query", name = "page", value = "当前页", required = true, dataType = "int"),
        @ApiImplicitParam(paramType = "query", name = "size", value = "页行数", required = true, dataType = "int")})
    @RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
    public ResponseInfo<PageInfo<UserDto>> getSysUserBySearch(
        @RequestParam(value = "page", defaultValue = "1") Integer page,
        @RequestParam(value = "size", defaultValue = "100") Integer size) {
        AppPage<SysUser> aPage = new AppPage<SysUser>();
        aPage.setPageNum(page);
        aPage.setPageSize(size);
        // 其他参数
        // SysUser sysUser=new SysUser();
        // page.setParam(sysUser);
        // 分页数据
        PageInfo<UserDto> pageInfo = sysUserServiceImpl.list(aPage);
        return new ResponseInfo<PageInfo<UserDto>>(ResultInfo.Success, pageInfo);
    }

    @ApiOperation(value = "分页查询", notes = "分页查询返回对象[PageDataVo<SysUser>],作者：lhy")
    @ApiImplicitParams({
        @ApiImplicitParam(paramType = "query", name = "page", value = "当前页", required = false, dataType = "int"),
        @ApiImplicitParam(paramType = "query", name = "size", value = "页行数", required = false, dataType = "int"),
        @ApiImplicitParam(paramType = "query", name = "name", value = "用戶名", required = false, dataType = "String"),
        @ApiImplicitParam(paramType = "query", name = "departId", value = "机构编号", required = false, dataType = "Long"),
        @ApiImplicitParam(paramType = "query", name = "tel", value = "电话", required = false, dataType = "String"),
        @ApiImplicitParam(paramType = "query", name = "enabled", value = "状态", required = false, dataType = "int")})
    @RequestMapping(value = "/", method = RequestMethod.GET, produces = "application/json")
    public ResponseInfo<PageInfo<UserDto>> getSysUserByLikeName(
        @RequestParam(value = "page", defaultValue = "1") Integer page,
        @RequestParam(value = "size", defaultValue = "100") Integer size,
        @RequestParam(value = "name") String name,
        @RequestParam(value = "departId") Long departId,
        @RequestParam(value = "tel") String tel,
        @RequestParam(value = "enabled") Integer enabled) {
        AppPage<SysUser> aPage = new AppPage<SysUser>();
        aPage.setPageNum(page);
        aPage.setPageSize(size);
        SysUser user = new SysUser();
        aPage.setParam(user);
        user.setUserName(name);
        user.setDepartId(departId);
        user.setUserTel(tel);
        user.setEnabled(enabled);
        

        PageInfo<UserDto> pageInfo = sysUserServiceImpl.list(aPage);
        return new ResponseInfo<PageInfo<UserDto>>(ResultInfo.Success, pageInfo);
    }

}