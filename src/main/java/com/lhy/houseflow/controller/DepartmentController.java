/**
 * @filename:DepartmentController 2019年5月5日
 * @project HouseFlow V1.0 Copyright(c) 2018 lhy Co. Ltd. All right reserved.
 */
package com.lhy.houseflow.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.Department;
import com.lhy.houseflow.entity.vo.DepartmentVo;
import com.lhy.houseflow.entity.vo.SimpleTreeData;
import com.lhy.houseflow.service.DepartmentService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @Description: 部门接口层
 * @Author: lhy
 * @CreateDate: 2019年5月5日
 * @Version: V1.0
 * 
 */
@Api(tags = {"部门"})
@RestController
@RequestMapping("/api/departments")
@Slf4j
public class DepartmentController extends BaseController {

    @Autowired
    public DepartmentService departmentServiceImpl;

    /**
     * @explain 查询部门对象 <swagger GET请求>
     * @param 对象参数：id
     * @return department
     * @author lhy
     * @time 2019年5月5日
     */
    @ApiOperation(value = "获取部门信息", notes = "获取部门信息[department]，作者：lhy")
    @ApiImplicitParam(paramType = "path", name = "id", value = "部门id", required = true, dataType = "Long")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
    public ResponseInfo<Department> getDepartmentById(@PathVariable("id") Long id) {
        Department result = departmentServiceImpl.findById(id);
        return new ResponseInfo<Department>(ResultInfo.Success, result);
    }

    /**
     * @explain 添加部门对象
     * @param 对象参数：department
     * @return int
     * @author lhy
     * @time 2019年5月5日
     */
    @ApiOperation(value = "添加部门", notes = "添加部门[department],作者：lhy")
    @RequestMapping(method = RequestMethod.POST, produces = "application/json")
    public ResponseInfo<Department> add(@RequestBody Department department) {
        departmentServiceImpl.add(department);
        return new ResponseInfo<Department>(ResultInfo.Success, department);
    }

    /**
     * @explain 删除部门对象
     * @param 对象参数：id
     * @return int
     * @author lhy
     * @time 2019年5月5日
     */
    @RequestMapping(method = RequestMethod.DELETE, produces = "application/json")
    @ApiOperation(value = "删除部门", notes = "删除部门,作者：lhy")
    @ApiImplicitParam(paramType = "query", name = "id", value = "部门id", required = true, dataType = "Long")
    public ResponseInfo<String> deleteById(Long id) {
        departmentServiceImpl.deleteById(id);
        return new ResponseInfo<String>(ResultInfo.Success, null);
    }

    /**
     * @explain 修改部门对象
     * @param 对象参数：department
     * @return department
     * @author lhy
     * @time 2019年5月5日
     */
    @ApiOperation(value = "修改部门", notes = "修改部门[department],作者：lhy")
    @RequestMapping(method = RequestMethod.PUT, produces = "application/json")
    public ResponseInfo<Department> update(@RequestBody Department department) {
        departmentServiceImpl.update(department);
        return new ResponseInfo<Department>(ResultInfo.Success, null);
    }

    /**
     * @explain 获取匹配部门
     * @param 对象参数：department
     * @return List<Department>
     * @author lhy
     * @time 2019年5月5日
     */
    @ApiOperation(value = "条件查询部门", notes = "条件查询[department],作者：lhy")
    @PostMapping("/query")
    public ResponseInfo<List<Department>> queryDepartmentList(@RequestBody Department department) {
        List<Department> list = departmentServiceImpl.queryDepartmentList(department);
        return new ResponseInfo<List<Department>>(ResultInfo.Success, list);
    }

    /**
     * @explain 分页条件查询部门
     * @param 对象参数：AppPage<Department>
     * @return PageDataVo<Department>
     * @author lhy
     * @time 2019年5月5日
     */

    @ApiOperation(value = "分页查询", notes = "分页查询返回对象[PageDataVo<Department>],作者：lhy")
    @ApiImplicitParams({
        @ApiImplicitParam(paramType = "query", name = "page", value = "当前页", required = false, dataType = "int"),
        @ApiImplicitParam(paramType = "query", name = "size", value = "页行数", required = false, dataType = "int")})
    @RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
    public ResponseInfo<PageInfo<DepartmentVo>> getDepartmentBySearch(
        @RequestParam(value = "page", defaultValue = "1") Integer page,
        @RequestParam(value = "size", defaultValue = "100") Integer size) {
        AppPage<Department> aPage = new AppPage<Department>();
        aPage.setPageNum(page);
        aPage.setPageSize(size);
        PageInfo<Department> pageInfo = departmentServiceImpl.list(aPage);
        List<DepartmentVo> tree = departmentServiceImpl.buildTree(pageInfo.getList());
        PageInfo<DepartmentVo> result = new PageInfo<DepartmentVo>(tree);
        result.setTotal(pageInfo.getTotal());
        return new ResponseInfo<PageInfo<DepartmentVo>>(ResultInfo.Success, result);
    }

    @ApiOperation(value = "获取全部部门树", notes = "获取选择菜单[department]，作者：lhy")
    @GetMapping(value = "/tree")
    public ResponseInfo<PageInfo<DepartmentVo>> getSimpleTreeDatas() {
        PageInfo<Department> pageInfo = departmentServiceImpl.list(null);
        List<DepartmentVo> tree = departmentServiceImpl.buildTree(pageInfo.getList());
        PageInfo<DepartmentVo> result = new PageInfo<DepartmentVo>(tree);
        result.setTotal(pageInfo.getTotal());        
        return new ResponseInfo<PageInfo<DepartmentVo>>(ResultInfo.Success, result);
    }
    
    @ApiOperation(value = "获取选择菜单", notes = "获取选择菜单[sysMenu]，作者：lhy")
    @GetMapping(value = "/selected-tree")     
    public ResponseInfo<List<SimpleTreeData>> getSimpleTreeDatas1() {
        PageInfo<Department> page =  departmentServiceImpl.list(null);
        List<DepartmentVo> list=departmentServiceImpl.buildTree(page.getList());        
        List<SimpleTreeData> result =  departmentServiceImpl.getSimpleTreeData(list);
        return new ResponseInfo<List<SimpleTreeData>>(ResultInfo.Success, result);
    }    
}