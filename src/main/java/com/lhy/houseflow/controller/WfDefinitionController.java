/**
 * @filename:WfDefinitionController 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.WfDefinition;
import com.lhy.houseflow.service.WfDefinitionService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**   
 * 
 * @Description:  工作流定义表接口层
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Api(tags={"工作流定义表"})
@RestController
@RequestMapping("/api/wf-definitions")
@Slf4j
public class WfDefinitionController extends BaseController {
	
	@Autowired
	public WfDefinitionService wfDefinitionServiceImpl;
	
	/**
	 * @explain 查询工作流定义表对象  <swagger GET请求>
	 * @param   对象参数：id
	 * @return  wfDefinition
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "获取工作流定义表信息", notes = "获取工作流定义表信息[wfDefinition]，作者：lhy")
	@ApiImplicitParam(paramType="path", name = "id", value = "工作流定义表id", required = true, dataType = "Long")
	@RequestMapping(value = "/{id}",method = RequestMethod.GET, produces = "application/json") 
	public ResponseInfo<WfDefinition> getWfDefinitionById(@PathVariable("id")Long id){
		WfDefinition result=wfDefinitionServiceImpl.findById(id);
		return new ResponseInfo<WfDefinition>(ResultInfo.Success, result);
	}
	
	/**
	 * @explain 添加工作流定义表对象
	 * @param   对象参数：wfDefinition
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "添加工作流定义表", notes = "添加工作流定义表[wfDefinition],作者：lhy")
	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseInfo<WfDefinition> add(@RequestBody WfDefinition wfDefinition){
		wfDefinitionServiceImpl.add(wfDefinition);
		return new ResponseInfo<WfDefinition>(ResultInfo.Success, wfDefinition);
	}
	
	/**
	 * @explain 删除工作流定义表对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@RequestMapping(method = RequestMethod.DELETE, produces = "application/json")
	@ApiOperation(value = "删除工作流定义表", notes = "删除工作流定义表,作者：lhy")
	@ApiImplicitParam(paramType="query", name = "id", value = "工作流定义表id", required = true, dataType = "Long")
	public ResponseInfo<String> deleteById(Long id){
		wfDefinitionServiceImpl.deleteById(id);
		return new ResponseInfo<String>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 修改工作流定义表对象
	 * @param   对象参数：wfDefinition
	 * @return  wfDefinition
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "修改工作流定义表", notes = "修改工作流定义表[wfDefinition],作者：lhy")
	@RequestMapping(method = RequestMethod.PUT, produces = "application/json")
	public ResponseInfo<WfDefinition> update(@RequestBody WfDefinition wfDefinition){
		wfDefinitionServiceImpl.update(wfDefinition);
		return new ResponseInfo<WfDefinition>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 获取匹配工作流定义表
	 * @param   对象参数：wfDefinition
	 * @return  List<WfDefinition>
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "条件查询工作流定义表", notes = "条件查询[wfDefinition],作者：lhy")
	@PostMapping("/query")
	public ResponseInfo<List<WfDefinition>> queryWfDefinitionList(@RequestBody WfDefinition wfDefinition){
		List<WfDefinition> list = wfDefinitionServiceImpl.queryWfDefinitionList(wfDefinition);
		return new ResponseInfo<List<WfDefinition>>(ResultInfo.Success, list);	
	}
	
	/**
	 * @explain 分页条件查询工作流定义表   
	 * @param   对象参数：AppPage<WfDefinition>
	 * @return  PageInfo<WfDefinition>
	 * @author  lhy
	 * @time    2019年5月5日
	 */

	@ApiOperation(value = "分页查询", notes = "分页查询返回对象[PageInfo<WfDefinition>],作者：lhy")
	@ApiImplicitParams({
        @ApiImplicitParam(paramType="query", name = "page", value = "当前页", required = true, dataType = "int"),
        @ApiImplicitParam(paramType="query", name = "size", value = "页行数", required = true, dataType = "int")
    })
	@RequestMapping(value = "",method = RequestMethod.GET, produces = "application/json")
	public ResponseInfo<PageInfo<WfDefinition>> getWfDefinitionBySearch(
		@RequestParam(value="page",defaultValue ="1")  Integer page,
		@RequestParam(value="size",defaultValue ="100")  Integer size){
		AppPage<WfDefinition> aPage =new AppPage<WfDefinition>();
		aPage.setPageNum(page);
		aPage.setPageSize(size);
		//其他参数
//		WfDefinition wfDefinition=new WfDefinition();
//		page.setParam(wfDefinition);
		//分页数据
		PageInfo<WfDefinition> pageInfo = wfDefinitionServiceImpl.list(aPage);
		return new ResponseInfo<PageInfo<WfDefinition>>(ResultInfo.Success, pageInfo);	
	}
}