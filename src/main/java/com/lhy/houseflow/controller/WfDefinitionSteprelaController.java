/**
 * @filename:WfDefinitionSteprelaController 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.WfDefinitionSteprela;
import com.lhy.houseflow.service.WfDefinitionSteprelaService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**   
 * 
 * @Description:  步骤关系接口层
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Api(tags={"步骤关系"})
@RestController
@RequestMapping("/api/wf-definition-steprelas")
@Slf4j
public class WfDefinitionSteprelaController extends BaseController {
	
	@Autowired
	public WfDefinitionSteprelaService wfDefinitionSteprelaServiceImpl;
	
	/**
	 * @explain 查询步骤关系对象  <swagger GET请求>
	 * @param   对象参数：id
	 * @return  wfDefinitionSteprela
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "获取步骤关系信息", notes = "获取步骤关系信息[wfDefinitionSteprela]，作者：lhy")
	@ApiImplicitParam(paramType="path", name = "id", value = "步骤关系id", required = true, dataType = "Long")
	@RequestMapping(value = "/{id}",method = RequestMethod.GET, produces = "application/json") 
	public ResponseInfo<WfDefinitionSteprela> getWfDefinitionSteprelaById(@PathVariable("id")Long id){
		WfDefinitionSteprela result=wfDefinitionSteprelaServiceImpl.findById(id);
		return new ResponseInfo<WfDefinitionSteprela>(ResultInfo.Success, result);
	}
	
	/**
	 * @explain 添加步骤关系对象
	 * @param   对象参数：wfDefinitionSteprela
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "添加步骤关系", notes = "添加步骤关系[wfDefinitionSteprela],作者：lhy")
	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseInfo<WfDefinitionSteprela> add(@RequestBody WfDefinitionSteprela wfDefinitionSteprela){
		wfDefinitionSteprelaServiceImpl.add(wfDefinitionSteprela);
		return new ResponseInfo<WfDefinitionSteprela>(ResultInfo.Success, wfDefinitionSteprela);
	}
	
	/**
	 * @explain 删除步骤关系对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@RequestMapping(method = RequestMethod.DELETE, produces = "application/json")
	@ApiOperation(value = "删除步骤关系", notes = "删除步骤关系,作者：lhy")
	@ApiImplicitParam(paramType="query", name = "id", value = "步骤关系id", required = true, dataType = "Long")
	public ResponseInfo<String> deleteById(Long id){
		wfDefinitionSteprelaServiceImpl.deleteById(id);
		return new ResponseInfo<String>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 修改步骤关系对象
	 * @param   对象参数：wfDefinitionSteprela
	 * @return  wfDefinitionSteprela
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "修改步骤关系", notes = "修改步骤关系[wfDefinitionSteprela],作者：lhy")
	@RequestMapping(method = RequestMethod.PUT, produces = "application/json")
	public ResponseInfo<WfDefinitionSteprela> update(@RequestBody WfDefinitionSteprela wfDefinitionSteprela){
		wfDefinitionSteprelaServiceImpl.update(wfDefinitionSteprela);
		return new ResponseInfo<WfDefinitionSteprela>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 获取匹配步骤关系
	 * @param   对象参数：wfDefinitionSteprela
	 * @return  List<WfDefinitionSteprela>
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "条件查询步骤关系", notes = "条件查询[wfDefinitionSteprela],作者：lhy")
	@PostMapping("/query")
	public ResponseInfo<List<WfDefinitionSteprela>> queryWfDefinitionSteprelaList(@RequestBody WfDefinitionSteprela wfDefinitionSteprela){
		List<WfDefinitionSteprela> list = wfDefinitionSteprelaServiceImpl.queryWfDefinitionSteprelaList(wfDefinitionSteprela);
		return new ResponseInfo<List<WfDefinitionSteprela>>(ResultInfo.Success, list);	
	}
	
	/**
	 * @explain 分页条件查询步骤关系   
	 * @param   对象参数：AppPage<WfDefinitionSteprela>
	 * @return  PageInfo<WfDefinitionSteprela>
	 * @author  lhy
	 * @time    2019年5月5日
	 */

	@ApiOperation(value = "分页查询", notes = "分页查询返回对象[PageInfo<WfDefinitionSteprela>],作者：lhy")
	@ApiImplicitParams({
        @ApiImplicitParam(paramType="query", name = "page", value = "当前页", required = true, dataType = "int"),
        @ApiImplicitParam(paramType="query", name = "size", value = "页行数", required = true, dataType = "int")
    })
	@RequestMapping(value = "",method = RequestMethod.GET, produces = "application/json")
	public ResponseInfo<PageInfo<WfDefinitionSteprela>> getWfDefinitionSteprelaBySearch(
		@RequestParam(value="page",defaultValue ="1")  Integer page,
		@RequestParam(value="size",defaultValue ="100")  Integer size){
		AppPage<WfDefinitionSteprela> aPage =new AppPage<WfDefinitionSteprela>();
		aPage.setPageNum(page);
		aPage.setPageSize(size);
		//其他参数
//		WfDefinitionSteprela wfDefinitionSteprela=new WfDefinitionSteprela();
//		page.setParam(wfDefinitionSteprela);
		//分页数据
		PageInfo<WfDefinitionSteprela> pageInfo = wfDefinitionSteprelaServiceImpl.list(aPage);
		return new ResponseInfo<PageInfo<WfDefinitionSteprela>>(ResultInfo.Success, pageInfo);	
	}
}