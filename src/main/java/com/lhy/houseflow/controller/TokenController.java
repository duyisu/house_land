package com.lhy.houseflow.controller;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.util.JwtUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**   
 * 
 * @Description:  刷新Token
 * @Author:       lhy   
 * @CreateDate:   2019年9月22日
 * @Version:      V1.0
 *    
 */
@Api(tags={"刷新Token"})
@RestController
@RequestMapping("/api/token")
@Slf4j
public class TokenController  extends BaseController {
    /**
     * @explain refresh-token
     * @param   
     * @return  String
     * @author  lhy
     * @time    2019年5月5日
     */
    @ApiOperation(value = "token", notes = "refresh-token,作者：lhy")
    @RequestMapping(method = RequestMethod.POST, produces = "application/json")
    public ResponseInfo<String> refreshToken() { 
        if (currUser() == null) return new ResponseInfo<String>(ResultInfo.Auth_JWT_Error, "");   
        String token = JwtUtil.generateHeadToken(currUser().getUsername());
        return new ResponseInfo<String>(ResultInfo.Success, token);    
    }
}
