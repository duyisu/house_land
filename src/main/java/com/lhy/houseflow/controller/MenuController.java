/**
 * @filename:MenuController 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.Menu;
import com.lhy.houseflow.service.MenuService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**   
 * 
 * @Description:  菜单资源表接口层
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Api(tags={"菜单资源表"})
@RestController
@RequestMapping("/menus")
@Slf4j
public class MenuController {
	
	@Autowired
	public MenuService menuServiceImpl;
	
	/**
	 * @explain 查询菜单资源表对象  <swagger GET请求>
	 * @param   对象参数：id
	 * @return  menu
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "获取菜单资源表信息", notes = "获取菜单资源表信息[menu]，作者：lhy")
	@ApiImplicitParam(paramType="path", name = "id", value = "菜单资源表id", required = true, dataType = "Long")
	@RequestMapping(value = "/{id}",method = RequestMethod.GET, produces = "application/json") 
	public ResponseInfo<Menu> getMenuById(@PathVariable("id")Long id){
		Menu result=menuServiceImpl.findById(id);
		return new ResponseInfo<Menu>(ResultInfo.Success, result);
	}
	
	/**
	 * @explain 添加菜单资源表对象
	 * @param   对象参数：menu
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "添加菜单资源表", notes = "添加菜单资源表[menu],作者：lhy")
	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseInfo<Menu> add(@RequestBody Menu menu){
		menuServiceImpl.add(menu);
		return new ResponseInfo<Menu>(ResultInfo.Success, menu);
	}
	
	/**
	 * @explain 删除菜单资源表对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@RequestMapping(method = RequestMethod.DELETE, produces = "application/json")
	@ApiOperation(value = "删除菜单资源表", notes = "删除菜单资源表,作者：lhy")
	@ApiImplicitParam(paramType="query", name = "id", value = "菜单资源表id", required = true, dataType = "Long")
	public ResponseInfo<String> deleteById(Long id){
		menuServiceImpl.deleteById(id);
		return new ResponseInfo<String>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 修改菜单资源表对象
	 * @param   对象参数：menu
	 * @return  menu
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "修改菜单资源表", notes = "修改菜单资源表[menu],作者：lhy")
	@RequestMapping(method = RequestMethod.PUT, produces = "application/json")
	public ResponseInfo<Menu> update(@RequestBody Menu menu){
		menuServiceImpl.update(menu);
		return new ResponseInfo<Menu>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 获取匹配菜单资源表
	 * @param   对象参数：menu
	 * @return  List<Menu>
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "条件查询菜单资源表", notes = "条件查询[menu],作者：lhy")
	@PostMapping("/queryMenuList")
	public ResponseInfo<List<Menu>> queryMenuList(@RequestBody Menu menu){
		List<Menu> list = menuServiceImpl.queryMenuList(menu);
		return new ResponseInfo<List<Menu>>(ResultInfo.Success, list);	
	}
	
	/**
	 * @explain 分页条件查询菜单资源表   
	 * @param   对象参数：AppPage<Menu>
	 * @return  PageInfo<Menu>
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@GetMapping("/getPageMenu")
	@ApiOperation(value = "分页查询", notes = "分页查询返回对象[PageInfo<Menu>],作者：lhy")
	@ApiImplicitParams({
        @ApiImplicitParam(paramType="query", name = "pageNum", value = "当前页", required = true, dataType = "int"),
        @ApiImplicitParam(paramType="query", name = "pageSize", value = "页行数", required = true, dataType = "int")
    })
	public ResponseInfo<PageInfo<Menu>> getMenuBySearch(Integer pageNum,Integer pageSize){
		AppPage<Menu> page =new AppPage<Menu>();
		page.setPageNum(pageNum);
		page.setPageSize(pageSize);
		//其他参数
		Menu menu=new Menu();
		page.setParam(menu);
		//分页数据
		PageInfo<Menu> pageInfo = menuServiceImpl.getMenuBySearch(page);
		return new ResponseInfo<PageInfo<Menu>>(ResultInfo.Success, pageInfo);	
	}
}