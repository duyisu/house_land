/**
 * @filename:UploadFileController 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.UploadFile;
import com.lhy.houseflow.service.UploadFileService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**   
 * 
 * @Description:  上传文件接口层
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Api(tags={"上传文件"})
//@RestController
@RequestMapping("/api/upload-files")
@Slf4j
public class UploadFileController extends BaseController {
	
	@Autowired
	public UploadFileService uploadFileServiceImpl;
	
	/**
	 * @explain 查询上传文件对象  <swagger GET请求>
	 * @param   对象参数：id
	 * @return  uploadFile
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "获取上传文件信息", notes = "获取上传文件信息[uploadFile]，作者：lhy")
	@ApiImplicitParam(paramType="path", name = "id", value = "上传文件id", required = true, dataType = "Long")
	@RequestMapping(value = "/{id}",method = RequestMethod.GET, produces = "application/json") 
	public ResponseInfo<UploadFile> getUploadFileById(@PathVariable("id")Long id){
		UploadFile result=uploadFileServiceImpl.findById(id);
		return new ResponseInfo<UploadFile>(ResultInfo.Success, result);
	}
	
	/**
	 * @explain 添加上传文件对象
	 * @param   对象参数：uploadFile
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "添加上传文件", notes = "添加上传文件[uploadFile],作者：lhy")
	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseInfo<UploadFile> add(@RequestBody UploadFile uploadFile){
		uploadFileServiceImpl.add(uploadFile);
		return new ResponseInfo<UploadFile>(ResultInfo.Success, uploadFile);
	}
	
	/**
	 * @explain 删除上传文件对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@RequestMapping(method = RequestMethod.DELETE, produces = "application/json")
	@ApiOperation(value = "删除上传文件", notes = "删除上传文件,作者：lhy")
	@ApiImplicitParam(paramType="query", name = "id", value = "上传文件id", required = true, dataType = "Long")
	public ResponseInfo<String> deleteById(Long id){
		uploadFileServiceImpl.deleteById(id);
		return new ResponseInfo<String>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 修改上传文件对象
	 * @param   对象参数：uploadFile
	 * @return  uploadFile
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "修改上传文件", notes = "修改上传文件[uploadFile],作者：lhy")
	@RequestMapping(method = RequestMethod.PUT, produces = "application/json")
	public ResponseInfo<UploadFile> update(@RequestBody UploadFile uploadFile){
		uploadFileServiceImpl.update(uploadFile);
		return new ResponseInfo<UploadFile>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 获取匹配上传文件
	 * @param   对象参数：uploadFile
	 * @return  List<UploadFile>
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "条件查询上传文件", notes = "条件查询[uploadFile],作者：lhy")
	@PostMapping("/query")
	public ResponseInfo<List<UploadFile>> queryUploadFileList(@RequestBody UploadFile uploadFile){
		List<UploadFile> list = uploadFileServiceImpl.queryUploadFileList(uploadFile);
		return new ResponseInfo<List<UploadFile>>(ResultInfo.Success, list);	
	}
	
	/**
	 * @explain 分页条件查询上传文件   
	 * @param   对象参数：AppPage<UploadFile>
	 * @return  PageDataVo<UploadFile>
	 * @author  lhy
	 * @time    2019年5月5日
	 */

	@ApiOperation(value = "分页查询", notes = "分页查询返回对象[PageDataVo<UploadFile>],作者：lhy")
	@ApiImplicitParams({
        @ApiImplicitParam(paramType="query", name = "num", value = "当前页", required = true, dataType = "int"),
        @ApiImplicitParam(paramType="query", name = "size", value = "页行数", required = true, dataType = "int")
    })
	@RequestMapping(value = "",method = RequestMethod.GET, produces = "application/json")
	public ResponseInfo<PageInfo<UploadFile>> getUploadFileBySearch(
		@RequestParam(value="num",defaultValue ="1")  Integer num,
		@RequestParam(value="size",defaultValue ="100")  Integer size){
		AppPage<UploadFile> page =new AppPage<UploadFile>();
		page.setPageNum(num);
		page.setPageSize(size);
		//其他参数
//		UploadFile uploadFile=new UploadFile();
//		page.setParam(uploadFile);
		//分页数据
		PageInfo<UploadFile> pageInfo = uploadFileServiceImpl.list(page);
		return new ResponseInfo<PageInfo<UploadFile>>(ResultInfo.Success, pageInfo);	
	}
}