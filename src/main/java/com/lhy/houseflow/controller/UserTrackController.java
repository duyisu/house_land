/**
 * @filename:UserTrackController 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.UserTrack;
import com.lhy.houseflow.service.UserTrackService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**   
 * 
 * @Description:  人员轨迹接口层
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Api(tags={"人员轨迹"})
@RestController
@RequestMapping("/api/user-tracks")
@Slf4j
public class UserTrackController extends BaseController {
	
	@Autowired
	public UserTrackService userTrackServiceImpl;
	
	/**
	 * @explain 查询人员轨迹对象  <swagger GET请求>
	 * @param   对象参数：id
	 * @return  userTrack
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "获取人员轨迹信息", notes = "获取人员轨迹信息[userTrack]，作者：lhy")
	@ApiImplicitParam(paramType="path", name = "id", value = "人员轨迹id", required = true, dataType = "Long")
	@RequestMapping(value = "/{id}",method = RequestMethod.GET, produces = "application/json") 
	public ResponseInfo<UserTrack> getUserTrackById(@PathVariable("id")Long id){
		UserTrack result=userTrackServiceImpl.findById(id);
		return new ResponseInfo<UserTrack>(ResultInfo.Success, result);
	}
	
	/**
	 * @explain 添加人员轨迹对象
	 * @param   对象参数：userTrack
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "添加人员轨迹", notes = "添加人员轨迹[userTrack],作者：lhy")
	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseInfo<UserTrack> add(@RequestBody UserTrack userTrack){
		userTrackServiceImpl.add(userTrack);
		return new ResponseInfo<UserTrack>(ResultInfo.Success, userTrack);
	}
	
	/**
	 * @explain 删除人员轨迹对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@RequestMapping(method = RequestMethod.DELETE, produces = "application/json")
	@ApiOperation(value = "删除人员轨迹", notes = "删除人员轨迹,作者：lhy")
	@ApiImplicitParam(paramType="query", name = "id", value = "人员轨迹id", required = true, dataType = "Long")
	public ResponseInfo<String> deleteById(Long id){
		userTrackServiceImpl.deleteById(id);
		return new ResponseInfo<String>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 修改人员轨迹对象
	 * @param   对象参数：userTrack
	 * @return  userTrack
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "修改人员轨迹", notes = "修改人员轨迹[userTrack],作者：lhy")
	@RequestMapping(method = RequestMethod.PUT, produces = "application/json")
	public ResponseInfo<UserTrack> update(@RequestBody UserTrack userTrack){
		userTrackServiceImpl.update(userTrack);
		return new ResponseInfo<UserTrack>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 获取匹配人员轨迹
	 * @param   对象参数：userTrack
	 * @return  List<UserTrack>
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "条件查询人员轨迹", notes = "条件查询[userTrack],作者：lhy")
	@PostMapping("/query")
	public ResponseInfo<List<UserTrack>> queryUserTrackList(@RequestBody UserTrack userTrack){
		List<UserTrack> list = userTrackServiceImpl.queryUserTrackList(userTrack);
		return new ResponseInfo<List<UserTrack>>(ResultInfo.Success, list);	
	}
	
	/**
	 * @explain 分页条件查询人员轨迹   
	 * @param   对象参数：AppPage<UserTrack>
	 * @return  PageDataVo<UserTrack>
	 * @author  lhy
	 * @time    2019年5月5日
	 */

	@ApiOperation(value = "分页查询", notes = "分页查询返回对象[PageDataVo<UserTrack>],作者：lhy")
	@ApiImplicitParams({
        @ApiImplicitParam(paramType="query", name = "page", value = "当前页", required = true, dataType = "int"),
        @ApiImplicitParam(paramType="query", name = "size", value = "页行数", required = true, dataType = "int")
    })
	@RequestMapping(value = "",method = RequestMethod.GET, produces = "application/json")
	public ResponseInfo<PageInfo<UserTrack>> getUserTrackBySearch(
		@RequestParam(value="page",defaultValue ="1")  Integer page,
		@RequestParam(value="size",defaultValue ="100")  Integer size){
		AppPage<UserTrack> aPage =new AppPage<UserTrack>();
		aPage.setPageNum(page);
		aPage.setPageSize(size);
		//其他参数
//		UserTrack userTrack=new UserTrack();
//		page.setParam(userTrack);
		//分页数据
		PageInfo<UserTrack> pageInfo = userTrackServiceImpl.list(aPage);
		return new ResponseInfo<PageInfo<UserTrack>>(ResultInfo.Success, pageInfo);	
	}
}