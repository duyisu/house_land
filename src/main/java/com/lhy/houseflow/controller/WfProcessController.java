/**
 * @filename:WfProcessController 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.WfProcess;
import com.lhy.houseflow.service.WfProcessService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**   
 * 
 * @Description:  实例处理任务表接口层
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Api(tags={"实例处理任务表"})
@RestController
@RequestMapping("/api/wf-processs")
@Slf4j
public class WfProcessController extends BaseController {
	
	@Autowired
	public WfProcessService wfProcessServiceImpl;
	
	/**
	 * @explain 查询实例处理任务表对象  <swagger GET请求>
	 * @param   对象参数：id
	 * @return  wfProcess
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "获取实例处理任务表信息", notes = "获取实例处理任务表信息[wfProcess]，作者：lhy")
	@ApiImplicitParam(paramType="path", name = "id", value = "实例处理任务表id", required = true, dataType = "Long")
	@RequestMapping(value = "/{id}",method = RequestMethod.GET, produces = "application/json") 
	public ResponseInfo<WfProcess> getWfProcessById(@PathVariable("id")Long id){
		WfProcess result=wfProcessServiceImpl.findById(id);
		return new ResponseInfo<WfProcess>(ResultInfo.Success, result);
	}
	
	/**
	 * @explain 添加实例处理任务表对象
	 * @param   对象参数：wfProcess
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "添加实例处理任务表", notes = "添加实例处理任务表[wfProcess],作者：lhy")
	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseInfo<WfProcess> add(@RequestBody WfProcess wfProcess){
		wfProcessServiceImpl.add(wfProcess);
		return new ResponseInfo<WfProcess>(ResultInfo.Success, wfProcess);
	}
	
	/**
	 * @explain 删除实例处理任务表对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@RequestMapping(method = RequestMethod.DELETE, produces = "application/json")
	@ApiOperation(value = "删除实例处理任务表", notes = "删除实例处理任务表,作者：lhy")
	@ApiImplicitParam(paramType="query", name = "id", value = "实例处理任务表id", required = true, dataType = "Long")
	public ResponseInfo<String> deleteById(Long id){
		wfProcessServiceImpl.deleteById(id);
		return new ResponseInfo<String>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 修改实例处理任务表对象
	 * @param   对象参数：wfProcess
	 * @return  wfProcess
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "修改实例处理任务表", notes = "修改实例处理任务表[wfProcess],作者：lhy")
	@RequestMapping(method = RequestMethod.PUT, produces = "application/json")
	public ResponseInfo<WfProcess> update(@RequestBody WfProcess wfProcess){
		wfProcessServiceImpl.update(wfProcess);
		return new ResponseInfo<WfProcess>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 获取匹配实例处理任务表
	 * @param   对象参数：wfProcess
	 * @return  List<WfProcess>
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "条件查询实例处理任务表", notes = "条件查询[wfProcess],作者：lhy")
	@PostMapping("/query")
	public ResponseInfo<List<WfProcess>> queryWfProcessList(@RequestBody WfProcess wfProcess){
		List<WfProcess> list = wfProcessServiceImpl.queryWfProcessList(wfProcess);
		return new ResponseInfo<List<WfProcess>>(ResultInfo.Success, list);	
	}
	
	/**
	 * @explain 分页条件查询实例处理任务表   
	 * @param   对象参数：AppPage<WfProcess>
	 * @return  PageInfo<WfProcess>
	 * @author  lhy
	 * @time    2019年5月5日
	 */

	@ApiOperation(value = "分页查询", notes = "分页查询返回对象[PageInfo<WfProcess>],作者：lhy")
	@ApiImplicitParams({
        @ApiImplicitParam(paramType="query", name = "page", value = "当前页", required = true, dataType = "int"),
        @ApiImplicitParam(paramType="query", name = "size", value = "页行数", required = true, dataType = "int")
    })
	@RequestMapping(value = "",method = RequestMethod.GET, produces = "application/json")
	public ResponseInfo<PageInfo<WfProcess>> getWfProcessBySearch(
		@RequestParam(value="page",defaultValue ="1")  Integer page,
		@RequestParam(value="size",defaultValue ="100")  Integer size){
		AppPage<WfProcess> aPage =new AppPage<WfProcess>();
		aPage.setPageNum(page);
		aPage.setPageSize(size);
		//其他参数
//		WfProcess wfProcess=new WfProcess();
//		page.setParam(wfProcess);
		//分页数据
		PageInfo<WfProcess> pageInfo = wfProcessServiceImpl.list(aPage);
		return new ResponseInfo<PageInfo<WfProcess>>(ResultInfo.Success, pageInfo);	
	}
}