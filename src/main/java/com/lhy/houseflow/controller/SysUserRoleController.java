/**
 * @filename:SysUserRoleController 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.SysUserRole;
import com.lhy.houseflow.service.SysUserRoleService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**   
 * 
 * @Description:  用户角色接口层
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Api(tags={"用户角色"})
@RestController
@RequestMapping("/api/sys-user-roles")
@Slf4j
public class SysUserRoleController extends BaseController {
	
	@Autowired
	public SysUserRoleService sysUserRoleServiceImpl;
	
	/**
	 * @explain 查询用户角色对象  <swagger GET请求>
	 * @param   对象参数：id
	 * @return  sysUserRole
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "获取用户角色信息", notes = "获取用户角色信息[sysUserRole]，作者：lhy")
	@ApiImplicitParam(paramType="path", name = "id", value = "用户角色id", required = true, dataType = "Long")
	@RequestMapping(value = "/{id}",method = RequestMethod.GET, produces = "application/json") 
	public ResponseInfo<SysUserRole> getSysUserRoleById(@PathVariable("id")Long id){
		SysUserRole result=sysUserRoleServiceImpl.findById(id);
		return new ResponseInfo<SysUserRole>(ResultInfo.Success, result);
	}
	
	/**
	 * @explain 添加用户角色对象
	 * @param   对象参数：sysUserRole
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "添加用户角色", notes = "添加用户角色[sysUserRole],作者：lhy")
	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseInfo<SysUserRole> add(@RequestBody SysUserRole sysUserRole){
		sysUserRoleServiceImpl.add(sysUserRole);
		return new ResponseInfo<SysUserRole>(ResultInfo.Success, sysUserRole);
	}
	
	/**
	 * @explain 删除用户角色对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@RequestMapping(method = RequestMethod.DELETE, produces = "application/json")
	@ApiOperation(value = "删除用户角色", notes = "删除用户角色,作者：lhy")
	@ApiImplicitParam(paramType="query", name = "id", value = "用户角色id", required = true, dataType = "Long")
	public ResponseInfo<String> deleteById(Long id){
		sysUserRoleServiceImpl.deleteById(id);
		return new ResponseInfo<String>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 修改用户角色对象
	 * @param   对象参数：sysUserRole
	 * @return  sysUserRole
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "修改用户角色", notes = "修改用户角色[sysUserRole],作者：lhy")
	@RequestMapping(method = RequestMethod.PUT, produces = "application/json")
	public ResponseInfo<SysUserRole> update(@RequestBody SysUserRole sysUserRole){
		sysUserRoleServiceImpl.update(sysUserRole);
		return new ResponseInfo<SysUserRole>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 获取匹配用户角色
	 * @param   对象参数：sysUserRole
	 * @return  List<SysUserRole>
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "条件查询用户角色", notes = "条件查询[sysUserRole],作者：lhy")
	@PostMapping("/query")
	public ResponseInfo<List<SysUserRole>> querySysUserRoleList(@RequestBody SysUserRole sysUserRole){
		List<SysUserRole> list = sysUserRoleServiceImpl.querySysUserRoleList(sysUserRole);
		return new ResponseInfo<List<SysUserRole>>(ResultInfo.Success, list);	
	}
	
	/**
	 * @explain 分页条件查询用户角色   
	 * @param   对象参数：AppPage<SysUserRole>
	 * @return  PageDataVo<SysUserRole>
	 * @author  lhy
	 * @time    2019年5月5日
	 */

	@ApiOperation(value = "分页查询", notes = "分页查询返回对象[PageDataVo<SysUserRole>],作者：lhy")
	@ApiImplicitParams({
        @ApiImplicitParam(paramType="query", name = "page", value = "当前页", required = true, dataType = "int"),
        @ApiImplicitParam(paramType="query", name = "size", value = "页行数", required = true, dataType = "int")
    })
	@RequestMapping(value = "",method = RequestMethod.GET, produces = "application/json")
	public ResponseInfo<PageInfo<SysUserRole>> getSysUserRoleBySearch(
		@RequestParam(value="page",defaultValue ="1")  Integer page,
		@RequestParam(value="size",defaultValue ="100")  Integer size){
		AppPage<SysUserRole> aPage =new AppPage<SysUserRole>();
		aPage.setPageNum(page);
		aPage.setPageSize(size);
		//其他参数
//		SysUserRole sysUserRole=new SysUserRole();
//		page.setParam(sysUserRole);
		//分页数据
		PageInfo<SysUserRole> pageInfo = sysUserRoleServiceImpl.list(aPage);
		return new ResponseInfo<PageInfo<SysUserRole>>(ResultInfo.Success, pageInfo);	
	}
}