package com.lhy.houseflow.controller;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.JwtUserFactory;
import com.lhy.houseflow.entity.SysUser;
import com.lhy.houseflow.entity.vo.BaseAuth;
import com.lhy.houseflow.entity.vo.UserInfo;
import com.lhy.houseflow.service.LoginService;
import com.lhy.houseflow.service.SysPermissionService;
import com.lhy.houseflow.service.SysUserService;
import com.lhy.houseflow.util.JwtUtil;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @Description: 区域表接口层
 * @Author: lhy
 * @CreateDate: 2019年5月5日
 * @Version: V1.0
 * 
 */
@Api(tags = {"登录"})
@RestController
@RequestMapping("/login")
@Slf4j
public class LoginController extends BaseController {
    @Autowired
    private LoginService loginService;
    @Autowired
    private SysPermissionService sysPermissionService;
    @Autowired
    private SysUserService sysUserService;

    /**
     * @explain login
     * @param name,
     *            password
     * @return String
     * @author lhy
     * @time 2019年5月5日
     */
    @ApiOperation(value = "login", notes = "login,作者：lhy")
    @RequestMapping(method = RequestMethod.POST, produces = "application/json")
    public ResponseInfo<UserInfo> login(@RequestBody BaseAuth baseAuth) {
        log.info("userName:{};password:{}", baseAuth.getUserName(), baseAuth.getPassword());
        SysUser user = loginService.login(baseAuth.getUserName(), baseAuth.getPassword());
        UserInfo info = new UserInfo();
        BeanUtils.copyProperties(user, info);
        info.setToken(JwtUtil.generateToken(info.getUserName()));
        info.setAuthorities(JwtUserFactory.createAuthorities(sysPermissionService.findByUsername(info.getUserName())));
        return new ResponseInfo<UserInfo>(ResultInfo.Success, info);
    }

    /**
     * @explain login
     * @param name,
     *            password
     * @return String
     * @author lhy
     * @time 2019年5月5日
     */
    @ApiOperation(value = "refresh-token", notes = "refresh-token,作者：lhy")
    @RequestMapping(value = "/refresh-token", method = RequestMethod.POST, produces = "application/json")
    public ResponseInfo<UserInfo> refreshToken() {
        log.info("username:{}", currUser().getUsername());
        UserInfo info = new UserInfo();
        BeanUtils.copyProperties(currUser(), info);
        info.setToken(JwtUtil.generateToken(info.getUserName()));
        return new ResponseInfo<UserInfo>(ResultInfo.Success, info);
    }

    /**
     * 获取用户信息
     * 
     * @return
     */
    @GetMapping(value = "info")
    public ResponseInfo<UserInfo> getUserInfo() {
        log.info("username:{}", currUser().getUsername());
        SysUser user = sysUserService.findByUsername(currUser().getUsername());
        UserInfo info = new UserInfo();
        BeanUtils.copyProperties(user, info);
        info.setToken(JwtUtil.generateToken(info.getUserName()));
        info.setAuthorities(JwtUserFactory.createAuthorities(sysPermissionService.findByUsername(info.getUserName())));
        
        return new ResponseInfo<UserInfo>(ResultInfo.Success, info);
    }

}
