/**
 * @filename:SysRoleController 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.RoleMenusVo;
import com.lhy.houseflow.entity.SysRole;
import com.lhy.houseflow.entity.dto.RoleDto;
import com.lhy.houseflow.service.SysRoleService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**   
 * 
 * @Description:  角色接口层
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Api(tags={"角色"})
@RestController
@RequestMapping("/api/roles")
@Slf4j
public class SysRoleController extends BaseController {
	
	@Autowired
	public SysRoleService sysRoleServiceImpl;
	
	/**
	 * @explain 查询角色对象  <swagger GET请求>
	 * @param   对象参数：id
	 * @return  sysRole
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "获取角色信息", notes = "获取角色信息[sysRole]，作者：lhy")
	@ApiImplicitParam(paramType="path", name = "id", value = "角色id", required = true, dataType = "Long")
	@RequestMapping(value = "/{id}",method = RequestMethod.GET, produces = "application/json") 
	public ResponseInfo<RoleDto> getSysRoleById(@PathVariable("id")Long id){
	    RoleDto result=sysRoleServiceImpl.findRoleDtoById(id);
		return new ResponseInfo<RoleDto>(ResultInfo.Success, result);
	}
	
	/**
	 * @explain 添加角色对象
	 * @param   对象参数：sysRole
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "添加角色", notes = "添加角色[sysRole],作者：lhy")
	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseInfo<RoleDto> add(@RequestBody RoleDto sysRole){
		sysRoleServiceImpl.add(sysRole);
		return new ResponseInfo<RoleDto>(ResultInfo.Success, sysRole);
	}
	
	/**
	 * @explain 删除角色对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@DeleteMapping(value = "/{id}")
	@ApiOperation(value = "删除角色", notes = "删除角色,作者：lhy")
	@ApiImplicitParam(paramType="query", name = "id", value = "角色id", required = true, dataType = "Long")
	public ResponseInfo<String> deleteById(@PathVariable("id") Long id){
		sysRoleServiceImpl.deleteById(id);
		return new ResponseInfo<String>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 修改角色对象
	 * @param   对象参数：sysRole
	 * @return  sysRole
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "修改角色", notes = "修改角色[sysRole],作者：lhy")
	@RequestMapping(method = RequestMethod.PUT, produces = "application/json")
	public ResponseInfo<RoleDto> update(@RequestBody RoleDto sysRole){
		sysRoleServiceImpl.update(sysRole);
		return new ResponseInfo<RoleDto>(ResultInfo.Success, sysRole);
	}
	
    /**
     * @explain 修改角色菜单资源
     * @param   对象参数：sysRole
     * @return  sysRole
     * @author  lhy
     * @time    2019年5月5日
     */
    @ApiOperation(value = "修改角色菜单资源", notes = "修改角色[sysRole],作者：lhy")
    @PutMapping(value="/menu")
    public ResponseInfo<String> updateRoleMenu(@RequestBody RoleMenusVo sysRole){
/*        sysRoleServiceImpl.updateMenu(sysRole,roleService.findById(resources.getId()));
        .update(sysRole);
        */
        return new ResponseInfo<String>(ResultInfo.Success, null);
    }	
	
	
	
	/**
	 * @explain 获取匹配角色
	 * @param   对象参数：sysRole
	 * @return  List<SysRole>
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "条件查询角色", notes = "条件查询[sysRole],作者：lhy")
	@PostMapping("/query")
	public ResponseInfo<List<SysRole>> querySysRoleList(@RequestBody SysRole sysRole){
		List<SysRole> list = sysRoleServiceImpl.querySysRoleList(sysRole);
		return new ResponseInfo<List<SysRole>>(ResultInfo.Success, list);	
	}
	
	/**
	 * @explain 分页条件查询角色   
	 * @param   对象参数：AppPage<SysRole>
	 * @return  PageDataVo<SysRole>
	 * @author  lhy
	 * @time    2019年5月5日
	 */

	@ApiOperation(value = "分页查询", notes = "分页查询返回对象[PageDataVo<SysRole>],作者：lhy")
	@ApiImplicitParams({
        @ApiImplicitParam(paramType="query", name = "page", value = "当前页", required = true, dataType = "int"),
        @ApiImplicitParam(paramType="query", name = "size", value = "页行数", required = true, dataType = "int")
    })
	@RequestMapping(value = "",method = RequestMethod.GET, produces = "application/json")
	public ResponseInfo<PageInfo<SysRole>> getSysRoleBySearch(
		@RequestParam(value="page",defaultValue ="1")  Integer page,
		@RequestParam(value="size",defaultValue ="100")  Integer size){
		AppPage<SysRole> aPage =new AppPage<SysRole>();
		aPage.setPageNum(page);
		aPage.setPageSize(size);
		//其他参数
//		SysRole sysRole=new SysRole();
//		page.setParam(sysRole);
		//分页数据
		PageInfo<SysRole> pageInfo = sysRoleServiceImpl.list(aPage);
		return new ResponseInfo<PageInfo<SysRole>>(ResultInfo.Success, pageInfo);	
	}
	
    @ApiOperation(value = "分页查询", notes = "分页查询返回对象[PageDataVo<SysRole>],作者：lhy")
    @ApiImplicitParams({
        @ApiImplicitParam(paramType="query", name = "name", value = "角色名称", required = false, dataType = "String"),
        @ApiImplicitParam(paramType="query", name = "page", value = "当前页", required = false, dataType = "int"),
        @ApiImplicitParam(paramType="query", name = "size", value = "页行数", required = false, dataType = "int")
    })
    @RequestMapping(value = "/menu-permissions",method = RequestMethod.GET, produces = "application/json")
    public ResponseInfo<PageInfo<RoleDto>> getRoles(
        @RequestParam(value="name", required = false)  String name,
        @RequestParam(value="page",defaultValue ="1")  Integer page,
        @RequestParam(value="size",defaultValue ="1000")  Integer size){
        AppPage<String> aPage =new AppPage<String>();
        aPage.setPageNum(page);
        aPage.setPageSize(size);
        PageInfo<RoleDto> pageInfo = sysRoleServiceImpl.getRoles(aPage);
        return new ResponseInfo<PageInfo<RoleDto>>(ResultInfo.Success, pageInfo);   
    }
	
}