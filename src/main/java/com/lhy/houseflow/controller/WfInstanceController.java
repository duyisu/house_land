/**
 * @filename:WfInstanceController 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.WfInstance;
import com.lhy.houseflow.service.WfInstanceService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**   
 * 
 * @Description:  工作流实例表接口层
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Api(tags={"工作流实例表"})
@RestController
@RequestMapping("/api/wf-instances")
@Slf4j
public class WfInstanceController extends BaseController {
	
	@Autowired
	public WfInstanceService wfInstanceServiceImpl;
	
	/**
	 * @explain 查询工作流实例表对象  <swagger GET请求>
	 * @param   对象参数：id
	 * @return  wfInstance
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "获取工作流实例表信息", notes = "获取工作流实例表信息[wfInstance]，作者：lhy")
	@ApiImplicitParam(paramType="path", name = "id", value = "工作流实例表id", required = true, dataType = "Long")
	@RequestMapping(value = "/{id}",method = RequestMethod.GET, produces = "application/json") 
	public ResponseInfo<WfInstance> getWfInstanceById(@PathVariable("id")Long id){
		WfInstance result=wfInstanceServiceImpl.findById(id);
		return new ResponseInfo<WfInstance>(ResultInfo.Success, result);
	}
	
	/**
	 * @explain 添加工作流实例表对象
	 * @param   对象参数：wfInstance
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "添加工作流实例表", notes = "添加工作流实例表[wfInstance],作者：lhy")
	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseInfo<WfInstance> add(@RequestBody WfInstance wfInstance){
		wfInstanceServiceImpl.add(wfInstance);
		return new ResponseInfo<WfInstance>(ResultInfo.Success, wfInstance);
	}
	
	/**
	 * @explain 删除工作流实例表对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@RequestMapping(method = RequestMethod.DELETE, produces = "application/json")
	@ApiOperation(value = "删除工作流实例表", notes = "删除工作流实例表,作者：lhy")
	@ApiImplicitParam(paramType="query", name = "id", value = "工作流实例表id", required = true, dataType = "Long")
	public ResponseInfo<String> deleteById(Long id){
		wfInstanceServiceImpl.deleteById(id);
		return new ResponseInfo<String>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 修改工作流实例表对象
	 * @param   对象参数：wfInstance
	 * @return  wfInstance
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "修改工作流实例表", notes = "修改工作流实例表[wfInstance],作者：lhy")
	@RequestMapping(method = RequestMethod.PUT, produces = "application/json")
	public ResponseInfo<WfInstance> update(@RequestBody WfInstance wfInstance){
		wfInstanceServiceImpl.update(wfInstance);
		return new ResponseInfo<WfInstance>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 获取匹配工作流实例表
	 * @param   对象参数：wfInstance
	 * @return  List<WfInstance>
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "条件查询工作流实例表", notes = "条件查询[wfInstance],作者：lhy")
	@PostMapping("/query")
	public ResponseInfo<List<WfInstance>> queryWfInstanceList(@RequestBody WfInstance wfInstance){
		List<WfInstance> list = wfInstanceServiceImpl.queryWfInstanceList(wfInstance);
		return new ResponseInfo<List<WfInstance>>(ResultInfo.Success, list);	
	}
	
	/**
	 * @explain 分页条件查询工作流实例表   
	 * @param   对象参数：AppPage<WfInstance>
	 * @return  PageInfo<WfInstance>
	 * @author  lhy
	 * @time    2019年5月5日
	 */

	@ApiOperation(value = "分页查询", notes = "分页查询返回对象[PageInfo<WfInstance>],作者：lhy")
	@ApiImplicitParams({
        @ApiImplicitParam(paramType="query", name = "page", value = "当前页", required = true, dataType = "int"),
        @ApiImplicitParam(paramType="query", name = "size", value = "页行数", required = true, dataType = "int")
    })
	@RequestMapping(value = "",method = RequestMethod.GET, produces = "application/json")
	public ResponseInfo<PageInfo<WfInstance>> getWfInstanceBySearch(
		@RequestParam(value="page",defaultValue ="1")  Integer page,
		@RequestParam(value="size",defaultValue ="100")  Integer size){
		AppPage<WfInstance> aPage =new AppPage<WfInstance>();
		aPage.setPageNum(page);
		aPage.setPageSize(size);
		//其他参数
//		WfInstance wfInstance=new WfInstance();
//		page.setParam(wfInstance);
		//分页数据
		PageInfo<WfInstance> pageInfo = wfInstanceServiceImpl.list(aPage);
		return new ResponseInfo<PageInfo<WfInstance>>(ResultInfo.Success, pageInfo);	
	}
}