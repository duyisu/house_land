/**
 * @filename:CarTrackController 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.CarTrack;
import com.lhy.houseflow.service.CarTrackService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**   
 * 
 * @Description:  车辆轨迹接口层
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Api(tags={"车辆轨迹"})
@RestController
@RequestMapping("/api/car-tracks")
@Slf4j
public class CarTrackController extends BaseController {
	
	@Autowired
	public CarTrackService carTrackServiceImpl;
	
	/**
	 * @explain 查询车辆轨迹对象  <swagger GET请求>
	 * @param   对象参数：id
	 * @return  carTrack
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "获取车辆轨迹信息", notes = "获取车辆轨迹信息[carTrack]，作者：lhy")
	@ApiImplicitParam(paramType="path", name = "id", value = "车辆轨迹id", required = true, dataType = "Long")
	@RequestMapping(value = "/{id}",method = RequestMethod.GET, produces = "application/json") 
	public ResponseInfo<CarTrack> getCarTrackById(@PathVariable("id")Long id){
		CarTrack result=carTrackServiceImpl.findById(id);
		return new ResponseInfo<CarTrack>(ResultInfo.Success, result);
	}
	
	/**
	 * @explain 添加车辆轨迹对象
	 * @param   对象参数：carTrack
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "添加车辆轨迹", notes = "添加车辆轨迹[carTrack],作者：lhy")
	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseInfo<CarTrack> add(@RequestBody CarTrack carTrack){
		carTrackServiceImpl.add(carTrack);
		return new ResponseInfo<CarTrack>(ResultInfo.Success, carTrack);
	}
	
	/**
	 * @explain 删除车辆轨迹对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@RequestMapping(method = RequestMethod.DELETE, produces = "application/json")
	@ApiOperation(value = "删除车辆轨迹", notes = "删除车辆轨迹,作者：lhy")
	@ApiImplicitParam(paramType="query", name = "id", value = "车辆轨迹id", required = true, dataType = "Long")
	public ResponseInfo<String> deleteById(Long id){
		carTrackServiceImpl.deleteById(id);
		return new ResponseInfo<String>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 修改车辆轨迹对象
	 * @param   对象参数：carTrack
	 * @return  carTrack
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "修改车辆轨迹", notes = "修改车辆轨迹[carTrack],作者：lhy")
	@RequestMapping(method = RequestMethod.PUT, produces = "application/json")
	public ResponseInfo<CarTrack> update(@RequestBody CarTrack carTrack){
		carTrackServiceImpl.update(carTrack);
		return new ResponseInfo<CarTrack>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 获取匹配车辆轨迹
	 * @param   对象参数：carTrack
	 * @return  List<CarTrack>
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "条件查询车辆轨迹", notes = "条件查询[carTrack],作者：lhy")
	@PostMapping("/query")
	public ResponseInfo<List<CarTrack>> queryCarTrackList(@RequestBody CarTrack carTrack){
		List<CarTrack> list = carTrackServiceImpl.queryCarTrackList(carTrack);
		return new ResponseInfo<List<CarTrack>>(ResultInfo.Success, list);	
	}
	
	/**
	 * @explain 分页条件查询车辆轨迹   
	 * @param   对象参数：AppPage<CarTrack>
	 * @return  PageDataVo<CarTrack>
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@GetMapping("/")
	@ApiOperation(value = "分页查询", notes = "分页查询返回对象[PageDataVo<CarTrack>],作者：lhy")
	@ApiImplicitParams({
        @ApiImplicitParam(paramType="query", name = "page", value = "当前页", required = true, dataType = "int"),
        @ApiImplicitParam(paramType="query", name = "size", value = "页行数", required = true, dataType = "int")
    })
	
	public ResponseInfo<PageInfo<CarTrack>> getCarTrackBySearch(
		@RequestParam(value="page",defaultValue ="1")  Integer page,
		@RequestParam(value="size",defaultValue ="100")  Integer size){
		AppPage<CarTrack> aPage =new AppPage<CarTrack>();
		aPage.setPageNum(page);
		aPage.setPageSize(size);
		//其他参数
//		CarTrack carTrack=new CarTrack();
//		page.setParam(carTrack);
		//分页数据
		PageInfo<CarTrack> pageInfo = carTrackServiceImpl.list(aPage);
		return new ResponseInfo<PageInfo<CarTrack>>(ResultInfo.Success, pageInfo);	
	}
}