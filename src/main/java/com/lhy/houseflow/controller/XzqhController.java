/**
 * @filename:XzqhController 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.Xzqh;
import com.lhy.houseflow.service.XzqhService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**   
 * 
 * @Description:  地区接口层
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Api(tags={"地区"})
@RestController
@RequestMapping("/api/areas")
@Slf4j
public class XzqhController extends BaseController {
	
	@Autowired
	public XzqhService xzqhServiceImpl;
	
	/**
	 * @explain 查询地区对象  <swagger GET请求>
	 * @param   对象参数：id
	 * @return  xzqh
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "获取地区信息", notes = "获取地区信息[xzqh]，作者：lhy")
	@ApiImplicitParam(paramType="path", name = "id", value = "地区id", required = true, dataType = "Long")
	@RequestMapping(value = "/{id}",method = RequestMethod.GET, produces = "application/json") 
	public ResponseInfo<Xzqh> getXzqhById(@PathVariable("id")Long id){
		Xzqh result=xzqhServiceImpl.findById(id);
		return new ResponseInfo<Xzqh>(ResultInfo.Success, result);
	}
	
	/**
	 * @explain 添加地区对象
	 * @param   对象参数：xzqh
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "添加地区", notes = "添加地区[xzqh],作者：lhy")
	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseInfo<Xzqh> add(@RequestBody Xzqh xzqh){
		xzqhServiceImpl.add(xzqh);
		return new ResponseInfo<Xzqh>(ResultInfo.Success, xzqh);
	}
	
	/**
	 * @explain 删除地区对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@RequestMapping(method = RequestMethod.DELETE, produces = "application/json")
	@ApiOperation(value = "删除地区", notes = "删除地区,作者：lhy")
	@ApiImplicitParam(paramType="query", name = "id", value = "地区id", required = true, dataType = "Long")
	public ResponseInfo<String> deleteById(Long id){
		xzqhServiceImpl.deleteById(id);
		return new ResponseInfo<String>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 修改地区对象
	 * @param   对象参数：xzqh
	 * @return  xzqh
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "修改地区", notes = "修改地区[xzqh],作者：lhy")
	@RequestMapping(method = RequestMethod.PUT, produces = "application/json")
	public ResponseInfo<Xzqh> update(@RequestBody Xzqh xzqh){
		xzqhServiceImpl.update(xzqh);
		return new ResponseInfo<Xzqh>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 获取匹配地区
	 * @param   对象参数：xzqh
	 * @return  List<Xzqh>
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "条件查询地区", notes = "条件查询[xzqh],作者：lhy")
	@PostMapping("/query")
	public ResponseInfo<List<Xzqh>> queryXzqhList(@RequestBody Xzqh xzqh){
		List<Xzqh> list = xzqhServiceImpl.queryXzqhList(xzqh);
		return new ResponseInfo<List<Xzqh>>(ResultInfo.Success, list);	
	}
	
	/**
	 * @explain 分页条件查询地区   
	 * @param   对象参数：AppPage<Xzqh>
	 * @return  PageInfo<Xzqh>
	 * @author  lhy
	 * @time    2019年5月5日
	 */

	@ApiOperation(value = "分页查询", notes = "分页查询返回对象[PageInfo<Xzqh>],作者：lhy")
	@ApiImplicitParams({
        @ApiImplicitParam(paramType="query", name = "page", value = "当前页", required = true, dataType = "int"),
        @ApiImplicitParam(paramType="query", name = "size", value = "页行数", required = true, dataType = "int")
    })
	@RequestMapping(value = "",method = RequestMethod.GET, produces = "application/json")
	public ResponseInfo<PageInfo<Xzqh>> getXzqhBySearch(
		@RequestParam(value="page",defaultValue ="1")  Integer page,
		@RequestParam(value="size",defaultValue ="100")  Integer size){
		AppPage<Xzqh> aPage =new AppPage<Xzqh>();
		aPage.setPageNum(page);
		aPage.setPageSize(size);
		//其他参数
//		Xzqh xzqh=new Xzqh();
//		page.setParam(xzqh);
		//分页数据
		PageInfo<Xzqh> pageInfo = xzqhServiceImpl.list(aPage);
		return new ResponseInfo<PageInfo<Xzqh>>(ResultInfo.Success, pageInfo);	
	}
}