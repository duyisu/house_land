/**
 * @filename:SysRoleMenuController 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.SysRoleMenu;
import com.lhy.houseflow.service.SysRoleMenuService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**   
 * 
 * @Description:  角色菜单接口层
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Api(tags={"角色菜单"})
@RestController
@RequestMapping("/api/sys-role-menus")
@Slf4j
public class SysRoleMenuController extends BaseController {
	
	@Autowired
	public SysRoleMenuService sysRoleMenuServiceImpl;
	
	/**
	 * @explain 查询角色菜单对象  <swagger GET请求>
	 * @param   对象参数：id
	 * @return  sysRoleMenu
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "获取角色菜单信息", notes = "获取角色菜单信息[sysRoleMenu]，作者：lhy")
	@ApiImplicitParam(paramType="path", name = "id", value = "角色菜单id", required = true, dataType = "Long")
	@RequestMapping(value = "/{id}",method = RequestMethod.GET, produces = "application/json") 
	public ResponseInfo<SysRoleMenu> getSysRoleMenuById(@PathVariable("id")Long id){
		SysRoleMenu result=sysRoleMenuServiceImpl.findById(id);
		return new ResponseInfo<SysRoleMenu>(ResultInfo.Success, result);
	}
	
	/**
	 * @explain 添加角色菜单对象
	 * @param   对象参数：sysRoleMenu
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "添加角色菜单", notes = "添加角色菜单[sysRoleMenu],作者：lhy")
	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseInfo<SysRoleMenu> add(@RequestBody SysRoleMenu sysRoleMenu){
		sysRoleMenuServiceImpl.add(sysRoleMenu);
		return new ResponseInfo<SysRoleMenu>(ResultInfo.Success, sysRoleMenu);
	}
	
	/**
	 * @explain 删除角色菜单对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@RequestMapping(method = RequestMethod.DELETE, produces = "application/json")
	@ApiOperation(value = "删除角色菜单", notes = "删除角色菜单,作者：lhy")
	@ApiImplicitParam(paramType="query", name = "id", value = "角色菜单id", required = true, dataType = "Long")
	public ResponseInfo<String> deleteById(Long id){
		sysRoleMenuServiceImpl.deleteById(id);
		return new ResponseInfo<String>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 修改角色菜单对象
	 * @param   对象参数：sysRoleMenu
	 * @return  sysRoleMenu
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "修改角色菜单", notes = "修改角色菜单[sysRoleMenu],作者：lhy")
	@RequestMapping(method = RequestMethod.PUT, produces = "application/json")
	public ResponseInfo<SysRoleMenu> update(@RequestBody SysRoleMenu sysRoleMenu){
		sysRoleMenuServiceImpl.update(sysRoleMenu);
		return new ResponseInfo<SysRoleMenu>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 获取匹配角色菜单
	 * @param   对象参数：sysRoleMenu
	 * @return  List<SysRoleMenu>
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "条件查询角色菜单", notes = "条件查询[sysRoleMenu],作者：lhy")
	@PostMapping("/query")
	public ResponseInfo<List<SysRoleMenu>> querySysRoleMenuList(@RequestBody SysRoleMenu sysRoleMenu){
		List<SysRoleMenu> list = sysRoleMenuServiceImpl.querySysRoleMenuList(sysRoleMenu);
		return new ResponseInfo<List<SysRoleMenu>>(ResultInfo.Success, list);	
	}
	
	/**
	 * @explain 分页条件查询角色菜单   
	 * @param   对象参数：AppPage<SysRoleMenu>
	 * @return  PageDataVo<SysRoleMenu>
	 * @author  lhy
	 * @time    2019年5月5日
	 */

	@ApiOperation(value = "分页查询", notes = "分页查询返回对象[PageDataVo<SysRoleMenu>],作者：lhy")
	@ApiImplicitParams({
        @ApiImplicitParam(paramType="query", name = "page", value = "当前页", required = true, dataType = "int"),
        @ApiImplicitParam(paramType="query", name = "size", value = "页行数", required = true, dataType = "int")
    })
	@RequestMapping(value = "",method = RequestMethod.GET, produces = "application/json")
	public ResponseInfo<PageInfo<SysRoleMenu>> getSysRoleMenuBySearch(
		@RequestParam(value="page",defaultValue ="1")  Integer page,
		@RequestParam(value="size",defaultValue ="100")  Integer size){
		AppPage<SysRoleMenu> aPage =new AppPage<SysRoleMenu>();
		aPage.setPageNum(page);
		aPage.setPageSize(size);
		//其他参数
//		SysRoleMenu sysRoleMenu=new SysRoleMenu();
//		page.setParam(sysRoleMenu);
		//分页数据
		PageInfo<SysRoleMenu> pageInfo = sysRoleMenuServiceImpl.list(aPage);
		return new ResponseInfo<PageInfo<SysRoleMenu>>(ResultInfo.Success, pageInfo);	
	}
}