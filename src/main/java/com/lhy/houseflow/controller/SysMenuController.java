/**
 * @filename:SysMenuController 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.SysMenu;
import com.lhy.houseflow.entity.vo.MenuPermissionVo;
import com.lhy.houseflow.entity.vo.MenuRoute;
import com.lhy.houseflow.entity.vo.MenuVo;
import com.lhy.houseflow.entity.vo.SimpleTreeData;
import com.lhy.houseflow.service.SysMenuService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**   
 * 
 * @Description:  菜单资源接口层
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Api(tags={"菜单资源"})
@RestController
@RequestMapping("/api/menus")
@Slf4j
public class SysMenuController extends BaseController {
	
	@Autowired
	public SysMenuService sysMenuServiceImpl;
	
	/**
	 * @explain 查询菜单资源对象  <swagger GET请求>
	 * @param   对象参数：id
	 * @return  sysMenu
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "获取菜单资源信息", notes = "获取菜单资源信息[sysMenu]，作者：lhy")
	@ApiImplicitParam(paramType="path", name = "id", value = "菜单资源id", required = true, dataType = "Long")
	@RequestMapping(value = "/{id}",method = RequestMethod.GET, produces = "application/json") 
	public ResponseInfo<SysMenu> getSysMenuById(@PathVariable("id")Long id){
		SysMenu result=sysMenuServiceImpl.findById(id);
		return new ResponseInfo<SysMenu>(ResultInfo.Success, result);
	}
	
	/**
	 * @explain 添加菜单资源对象
	 * @param   对象参数：sysMenu
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "添加菜单资源", notes = "添加菜单资源[sysMenu],作者：lhy")
	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseInfo<SysMenu> add(@RequestBody SysMenu sysMenu){
		sysMenuServiceImpl.add(sysMenu);
		return new ResponseInfo<SysMenu>(ResultInfo.Success, sysMenu);
	}
	
	/**
	 * @explain 删除菜单资源对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@DeleteMapping(value = "/{id}")
	@ApiOperation(value = "删除菜单资源", notes = "删除菜单资源,作者：lhy")
	@ApiImplicitParam(paramType="query", name = "id", value = "菜单资源id", required = true, dataType = "Long")
	public ResponseInfo<String> deleteById(@PathVariable("id") Long id){
		sysMenuServiceImpl.deleteById(id);
		return new ResponseInfo<String>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 修改菜单资源对象
	 * @param   对象参数：sysMenu
	 * @return  sysMenu
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "修改菜单资源", notes = "修改菜单资源[sysMenu],作者：lhy")
	@RequestMapping(method = RequestMethod.PUT, produces = "application/json")
	public ResponseInfo<SysMenu> update(@RequestBody SysMenu sysMenu){
		sysMenuServiceImpl.update(sysMenu);
		return new ResponseInfo<SysMenu>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 获取匹配菜单资源
	 * @param   对象参数：sysMenu
	 * @return  List<SysMenu>
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "条件查询菜单资源", notes = "条件查询[sysMenu],作者：lhy")
	@PostMapping("/query")
	public ResponseInfo<List<SysMenu>> querySysMenuList(@RequestBody SysMenu sysMenu){
		List<SysMenu> list = sysMenuServiceImpl.querySysMenuList(sysMenu);
		return new ResponseInfo<List<SysMenu>>(ResultInfo.Success, list);	
	}
	
	/**
	 * @explain 分页条件查询菜单资源   
	 * @param   对象参数：AppPage<SysMenu>
	 * @return  PageDataVo<SysMenu>
	 * @author  lhy
	 * @time    2019年5月5日
	 */

	@ApiOperation(value = "分页查询", notes = "分页查询返回对象[PageDataVo<SysMenu>],作者：lhy")
	@ApiImplicitParams({
        @ApiImplicitParam(paramType="query", name = "page", value = "当前页", required = true, dataType = "int"),
        @ApiImplicitParam(paramType="query", name = "size", value = "页行数", required = true, dataType = "int")
    })
	@RequestMapping(value = "",method = RequestMethod.GET, produces = "application/json")
	public ResponseInfo<PageInfo<SysMenu>> getSysMenuBySearch(
		@RequestParam(value="page",defaultValue ="1")  Integer page,
		@RequestParam(value="size",defaultValue ="100")  Integer size){
		AppPage<SysMenu> aPage =new AppPage<SysMenu>();
		aPage.setPageNum(page);
		aPage.setPageSize(size);
		//其他参数
//		SysMenu sysMenu=new SysMenu();
//		page.setParam(sysMenu);
		//分页数据
		PageInfo<SysMenu> pageInfo = sysMenuServiceImpl.list(aPage);
		return new ResponseInfo<PageInfo<SysMenu>>(ResultInfo.Success, pageInfo);	
	}
	
    /**
     * @explain 构建菜单树
     * @return  MenuVo
     * @author  lhy
     * @time    2019年5月5日
     */
    /**
     * 
     * @return
     */
    
    @ApiOperation(value = "获取菜单资源信息", notes = "获取菜单资源信息[sysMenu]，作者：lhy")
    @GetMapping(value = "/tree") 
    public ResponseInfo<PageInfo<MenuVo>> buildTree(){
        PageInfo<SysMenu> page =  sysMenuServiceImpl.list(null);
        List<MenuVo> list=sysMenuServiceImpl.buildTreeBySysMenus(page.getList());
        PageInfo<MenuVo>  result = new PageInfo<MenuVo>(list);
        result.setTotal(page.getTotal());
        return new ResponseInfo<PageInfo<MenuVo>>(ResultInfo.Success, result);
    }
 
    /**
     * @explain 构建前端路由所需要的菜单
     * @return  MenuVo
     * @author  lhy
     * @time    2019年5月5日
     */
    /**
     * 
     * @return
     */
    
    @ApiOperation(value = "获取菜单路由", notes = "获取菜单资源信息[sysMenu]，作者：lhy")
    @GetMapping(value = "/route")     
    public ResponseInfo<List<MenuRoute>> buildMenuRoute() {
        PageInfo<SysMenu> page =  sysMenuServiceImpl.list(null);
        List<MenuVo> list=sysMenuServiceImpl.buildTreeBySysMenus(page.getList());        
        List<MenuRoute> result =  sysMenuServiceImpl.buildMenuRoute(list);
        return new ResponseInfo<List<MenuRoute>>(ResultInfo.Success, result);
    }
    
    @ApiOperation(value = "获取选择菜单", notes = "获取选择菜单[sysMenu]，作者：lhy")
    @GetMapping(value = "/selected-tree")     
    public ResponseInfo<List<SimpleTreeData>> getSimpleTreeDatas() {
        PageInfo<SysMenu> page =  sysMenuServiceImpl.list(null);
        List<MenuVo> list=sysMenuServiceImpl.buildTreeBySysMenus(page.getList());        
        List<SimpleTreeData> result =  sysMenuServiceImpl.getSimpleTreeData(list);
        return new ResponseInfo<List<SimpleTreeData>>(ResultInfo.Success, result);
    }   
    
    /**
     * @explain 构建菜单树
     * @return  MenuVo
     * @author  lhy
     * @time    2019年5月5日
     */
    /**
     * 
     * @return
     */
    
    @ApiOperation(value = "获取菜单权限信息", notes = "获取菜单资源信息[sysMenu]，作者：lhy")
    @GetMapping(value = "/permissions") 
    public ResponseInfo<List<MenuPermissionVo>> getMenuPermissions(){
        List<MenuPermissionVo> list=sysMenuServiceImpl.getMenuPermissions();
        return new ResponseInfo<List<MenuPermissionVo>>(ResultInfo.Success, list);
    }    

 
    	
}