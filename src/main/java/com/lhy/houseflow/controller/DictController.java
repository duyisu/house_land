/**
 * @filename:DictController 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.Dict;
import com.lhy.houseflow.service.DictService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**   
 * 
 * @Description:  字典类别接口层
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Api(tags={"字典类别"})
@RestController
@RequestMapping("/api/dicts")
@Slf4j
public class DictController extends BaseController {
	
	@Autowired
	public DictService dictServiceImpl;
	
	/**
	 * @explain 查询字典类别对象  <swagger GET请求>
	 * @param   对象参数：id
	 * @return  dict
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "获取字典类别信息", notes = "获取字典类别信息[dict]，作者：lhy")
	@ApiImplicitParam(paramType="path", name = "id", value = "字典类别id", required = true, dataType = "Long")
	@RequestMapping(value = "/{id}",method = RequestMethod.GET, produces = "application/json") 
	public ResponseInfo<Dict> getDictById(@PathVariable("id")Long id){
		Dict result=dictServiceImpl.findById(id);
		return new ResponseInfo<Dict>(ResultInfo.Success, result);
	}
	
	/**
	 * @explain 添加字典类别对象
	 * @param   对象参数：dict
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "添加字典类别", notes = "添加字典类别[dict],作者：lhy")
	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseInfo<Dict> add(@RequestBody Dict dict){
		dictServiceImpl.add(dict);
		return new ResponseInfo<Dict>(ResultInfo.Success, dict);
	}
	
	/**
	 * @explain 删除字典类别对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@RequestMapping(method = RequestMethod.DELETE, produces = "application/json")
	@ApiOperation(value = "删除字典类别", notes = "删除字典类别,作者：lhy")
	@ApiImplicitParam(paramType="query", name = "id", value = "字典类别id", required = true, dataType = "Long")
	public ResponseInfo<String> deleteById(Long id){
		dictServiceImpl.deleteById(id);
		return new ResponseInfo<String>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 修改字典类别对象
	 * @param   对象参数：dict
	 * @return  dict
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "修改字典类别", notes = "修改字典类别[dict],作者：lhy")
	@RequestMapping(method = RequestMethod.PUT, produces = "application/json")
	public ResponseInfo<Dict> update(@RequestBody Dict dict){
		dictServiceImpl.update(dict);
		return new ResponseInfo<Dict>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 获取匹配字典类别
	 * @param   对象参数：dict
	 * @return  List<Dict>
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "条件查询字典类别", notes = "条件查询[dict],作者：lhy")
	@PostMapping("/query")
	public ResponseInfo<List<Dict>> queryDictList(@RequestBody Dict dict){
		List<Dict> list = dictServiceImpl.queryDictList(dict);
		return new ResponseInfo<List<Dict>>(ResultInfo.Success, list);	
	}
	
	/**
	 * @explain 分页条件查询字典类别   
	 * @param   对象参数：AppPage<Dict>
	 * @return  PageInfo<Dict>
	 * @author  lhy
	 * @time    2019年5月5日
	 */

	@ApiOperation(value = "分页查询", notes = "分页查询返回对象[PageInfo<Dict>],作者：lhy")
	@ApiImplicitParams({
        @ApiImplicitParam(paramType="query", name = "page", value = "当前页", required = true, dataType = "int"),
        @ApiImplicitParam(paramType="query", name = "size", value = "页行数", required = true, dataType = "int")
    })
	@RequestMapping(value = "",method = RequestMethod.GET, produces = "application/json")
	public ResponseInfo<PageInfo<Dict>> getDictBySearch(
		@RequestParam(value="page",defaultValue ="1")  Integer page,
		@RequestParam(value="size",defaultValue ="100")  Integer size){
		AppPage<Dict> aPage =new AppPage<Dict>();
		aPage.setPageNum(page);
		aPage.setPageSize(size);
		//其他参数
//		Dict dict=new Dict();
//		page.setParam(dict);
		//分页数据
		PageInfo<Dict> pageInfo = dictServiceImpl.list(aPage);
		return new ResponseInfo<PageInfo<Dict>>(ResultInfo.Success, pageInfo);	
	}
}