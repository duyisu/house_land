package com.lhy.houseflow.controller;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.task.api.Task;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.alibaba.fastjson.JSON;
import com.alibaba.fastjson.JSONArray;
import com.alibaba.fastjson.JSONObject;
import com.lhy.houseflow.common.UploadUtil;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.constant.FlowConst;
import com.lhy.houseflow.entity.CaseDefendant;
import com.lhy.houseflow.entity.CaseRecord;
import com.lhy.houseflow.entity.CaseRecordFile;
import com.lhy.houseflow.entity.Department;
import com.lhy.houseflow.entity.SysUser;
import com.lhy.houseflow.entity.UploadFile;
import com.lhy.houseflow.entity.vo.CaseProcessWorkVo;
import com.lhy.houseflow.entity.vo.CaseRecordVo;
import com.lhy.houseflow.entity.vo.UserInfo;
import com.lhy.houseflow.service.CaseDefendantService;
import com.lhy.houseflow.service.CaseRecordFileService;
import com.lhy.houseflow.service.CaseRecordService;
import com.lhy.houseflow.service.DepartmentService;
import com.lhy.houseflow.service.IllegalBuildingLandService;
import com.lhy.houseflow.service.LoginService;
import com.lhy.houseflow.service.UploadFileService;
import com.lhy.houseflow.util.JwtUtil;


@RestController
public class AppController extends BaseController {

    @Autowired
    public LoginService loginServiceImpl;
    @Autowired
    public DepartmentService departmentServiceImpl;
    @Autowired
    public CaseRecordService caseRecordServiceImpl;
    @Autowired
    public CaseRecordFileService caseRecordFileServiceImpl;
    @Autowired
    public CaseDefendantService caseDefendantServiceImpl;
    @Autowired
    public UploadFileService uploadFileServiceImpl;
    @Autowired
    private IllegalBuildingLandService illegalBuildingLandService;
    
    
    /**
     * app端 登录
     */
    @PostMapping(value = "/applogin")
    public ResponseInfo<UserInfo> applogin(String paramStr) {
        JSONObject jsonParam = JSONObject.parseObject(paramStr);
        String userName = jsonParam.getString("userName");
        String password = jsonParam.getString("password");
        SysUser user = loginServiceImpl.login(userName, password);
        UserInfo info = new UserInfo();
        BeanUtils.copyProperties(user, info);
        info.setToken(JwtUtil.generateToken(info.getUserName()));
        return new ResponseInfo<UserInfo>(ResultInfo.Success, info);    
    }
    
    @GetMapping(value = "appGetDepById")
    public ResponseInfo<Department> getDepById(Long depId) {
        Department dep = departmentServiceImpl.findById(depId);
        return new ResponseInfo<Department>(ResultInfo.Success,dep);
    }
    
    @PostMapping(value="appCaseCreate")
    public ResponseInfo<CaseRecordVo> caseCreate(String caseRecordStr){
        JSONObject jsonParam = JSONObject.parseObject(caseRecordStr);
        CaseRecord caseRecord = new CaseRecord();
        caseRecord.setCaseDesc(jsonParam.getString("caseDesc"));
        caseRecord.setCaseType(jsonParam.getInteger("caseType"));
        caseRecord.setCaseSubtype(jsonParam.getInteger("caseSubtype"));
        caseRecord.setPosition(jsonParam.getString("position"));
        caseRecord.setLongitude(jsonParam.getBigDecimal("longitude"));
        caseRecord.setLatitude(jsonParam.getBigDecimal("latitude"));
        caseRecord.setAddress(jsonParam.getString("address"));
        caseRecord.setPurposes(jsonParam.getString("purposes"));
        caseRecord.setArea(jsonParam.getBigDecimal("area"));
        caseRecord.setRealArea(jsonParam.getBigDecimal("realArea"));
        caseRecord.setBuildKind(jsonParam.getInteger("buildKind"));
        caseRecord.setBuildStruct(jsonParam.getInteger("buildStruct"));
        caseRecord.setReportUserId(jsonParam.getLong("reportUserId"));
        caseRecord.setStatus(FlowConst.CASE_STATUS_PROCESSING);
        //caseRecord.setCaseNo("");
        caseRecord.setSource(7);
        //caseRecord.setEnebled(1);
        
        ///caseRecordServiceImpl.add(caseRecord);
        
        List<CaseDefendant> caseDefendantList = new ArrayList<CaseDefendant>();
        List<CaseRecordFile> caseRecordFileList = new ArrayList<CaseRecordFile>();
        CaseDefendant defendant = null;
        JSONArray defendantArray = jsonParam.getJSONArray("defendantArray");
        for (int i = 0; i < defendantArray.size(); i ++) {
            JSONObject defendantJson = defendantArray.getJSONObject(i);
            defendant = new CaseDefendant();
            //defendant.setCaseId(caseRecord.getId());
            defendant.setDefendantName(defendantJson.getString("defendantName"));
            defendant.setDefendantTel(defendantJson.getString("defendantTel"));
            defendant.setDefendantIscardno(defendantJson.getString("defendantIscardno"));
            //caseDefendantServiceImpl.add(defendant);
            caseDefendantList.add(defendant);
        }
        
        CaseRecordFile caseRecordFile = null;
        JSONArray caseRecordFileArray = jsonParam.getJSONArray("caseRecordFileArray");
        for (int i = 0; i < caseRecordFileArray.size(); i ++) {
            JSONObject fileObject = caseRecordFileArray.getJSONObject(i);
            caseRecordFile = new CaseRecordFile();
            //caseRecordFile.setCaseId(caseRecord.getId());
            caseRecordFile.setFileId(fileObject.getLong("fileId"));
            //caseRecordFileServiceImpl.add(caseRecordFile);
            caseRecordFileList.add(caseRecordFile);
        }
        caseRecord = illegalBuildingLandService.addCaseRecord(caseRecord, caseDefendantList, caseRecordFileList);
        //System.out.println(caseRecord.getId());
        //ProcessInstance inst = illegalBuildingLandService.createTask(currUser().getUsername(), caseRecord.getId());
        CaseRecordVo caseRecordVo = new CaseRecordVo();
        caseRecordVo.setCaseRecord(caseRecord);
        caseRecordVo.setCaseDefendantList(caseDefendantList);
        caseRecordVo.setCaseRecordFileList(caseRecordFileList);
        return new ResponseInfo<CaseRecordVo>(ResultInfo.Success, caseRecordVo);
    }
    
    @PostMapping(value = "/appUpload")
    @ResponseBody  
    public ResponseInfo<String> appUpload(HttpServletRequest request, List<MultipartFile> files) {
        if (files == null || files.size() == 0) {
            // 设置错误状态码
            return new ResponseInfo<String>(ResultInfo.UPLOAD_EMPTY_FILE);
        }
        JSONArray filePaths = new JSONArray();
        UploadFile uploadFile = null;
        for (MultipartFile file : files) {
            // 拿到文件名
            String filename = file.getOriginalFilename();
            // 存放上传图片的文件夹
            File fileDir = UploadUtil.getImgDirFile();
            // 输出文件夹绝对路径  -- 这里的绝对路径是相当于当前项目的路径而不是“容器”路径
            //System.out.println(fileDir.getAbsolutePath());
            try {
                // 构建真实的文件路径
                File newFile = new File(fileDir.getAbsolutePath() + File.separator + filename);
                String savePath = fileDir.getPath() + File.separator + filename;
                //System.out.println(newFile.getAbsolutePath());
                // 上传图片到 -》 “绝对路径”
                file.transferTo(newFile);
                uploadFile = new UploadFile();
                String fileType = "";
                String fileNameNoEx = "";
                if ((filename != null) && (filename.length() > 0)) { 
                    int dot = filename.lastIndexOf('.'); 
                    if ((dot >-1) && (dot < (filename.length() - 1))) { 
                        fileType = filename.substring(dot + 1); 
                        fileNameNoEx = filename.substring(0, dot);
                    } 
                } 
                uploadFile.setFilename(fileNameNoEx);
                uploadFile.setFilepath(savePath);
                uploadFile.setOriginalName(filename);
                uploadFile.setFileType(fileType);
                uploadFileServiceImpl.add(uploadFile);
                
                JSONObject imageJson = new JSONObject();
                imageJson.put("fileId", uploadFile.getId());
                filePaths.add(imageJson);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        return new ResponseInfo<String>(ResultInfo.Success,filePaths.toString());
    }
    
    @PostMapping(value = "/appTodoWork")
    @ResponseBody
    public ResponseInfo<String> appTodoWork(String userId) {
        /*
         * JSONArray resultArray = new JSONArray(); JSONObject demo1 = new JSONObject();
         * demo1.put("caseRecordNo", "00133212334"); demo1.put("caseDesc", "案件描述示例文本");
         * demo1.put("address", "广信区"); demo1.put("gmtCreate", "2019-08-11 14:02:01");
         * demo1.put("processInstanceId", "1"); demo1.put("caseStatus", "toSign");
         * resultArray.add(demo1);
         * 
         * JSONObject demo2 = new JSONObject(); demo2.put("caseRecordNo",
         * "00133212334"); demo2.put("caseDesc", "案件描述示例文本2"); demo2.put("address",
         * "广信区2"); demo2.put("gmtCreate", "2019-08-12 14:02:01");
         * demo2.put("processInstanceId", "2"); demo2.put("caseStatus", "toHandle");
         * resultArray.add(demo2);
         */
        List<CaseProcessWorkVo> myWorkList = illegalBuildingLandService.findAppTodo(new Long(userId).longValue());
        return new ResponseInfo<String>(ResultInfo.Success,JSON.toJSONString(myWorkList));
    }
    

}
