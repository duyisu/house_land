/**
 * @filename:BuildApplyFileController 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.BuildApplyFile;
import com.lhy.houseflow.entity.vo.TaskFileIds;
import com.lhy.houseflow.service.BuildApplyFileService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**   
 * 
 * @Description:  建房申请附件表接口层
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Api(tags={"建房申请附件表"})
@RestController
@RequestMapping("/api/build-apply-files")
@Slf4j
public class BuildApplyFileController extends BaseController {
	
	@Autowired
	public BuildApplyFileService buildApplyFileServiceImpl;
	
	/**
	 * @explain 查询建房申请附件表对象  <swagger GET请求>
	 * @param   对象参数：id
	 * @return  buildApplyFile
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "获取建房申请附件表信息", notes = "获取建房申请附件表信息[buildApplyFile]，作者：lhy")
	@ApiImplicitParam(paramType="path", name = "id", value = "建房申请附件表id", required = true, dataType = "Long")
	@RequestMapping(value = "/{id}",method = RequestMethod.GET, produces = "application/json") 
	public ResponseInfo<BuildApplyFile> getBuildApplyFileById(@PathVariable("id")Long id){
		BuildApplyFile result=buildApplyFileServiceImpl.findById(id);
		return new ResponseInfo<BuildApplyFile>(ResultInfo.Success, result);
	}
	
	/**
	 * @explain 添加建房申请附件表对象
	 * @param   对象参数：buildApplyFile
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "添加建房申请附件表", notes = "添加建房申请附件表[buildApplyFile],作者：lhy")
	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseInfo<String> add(@RequestBody TaskFileIds TaskFileIds){
		buildApplyFileServiceImpl.add(TaskFileIds);
		return new ResponseInfo<String>(ResultInfo.Success);
	}
	
	/**
	 * @explain 删除建房申请附件表对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@RequestMapping(method = RequestMethod.DELETE, produces = "application/json")
	@ApiOperation(value = "删除建房申请附件表", notes = "删除建房申请附件表,作者：lhy")
	@ApiImplicitParam(paramType="query", name = "id", value = "建房申请附件表id", required = true, dataType = "Long")
	public ResponseInfo<String> deleteById(Long id){
		buildApplyFileServiceImpl.deleteById(id);
		return new ResponseInfo<String>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 修改建房申请附件表对象
	 * @param   对象参数：buildApplyFile
	 * @return  buildApplyFile
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "修改建房申请附件表", notes = "修改建房申请附件表[buildApplyFile],作者：lhy")
	@RequestMapping(method = RequestMethod.PUT, produces = "application/json")
	public ResponseInfo<BuildApplyFile> update(@RequestBody BuildApplyFile buildApplyFile){
		buildApplyFileServiceImpl.update(buildApplyFile);
		return new ResponseInfo<BuildApplyFile>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 获取匹配建房申请附件表
	 * @param   对象参数：buildApplyFile
	 * @return  List<BuildApplyFile>
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "条件查询建房申请附件表", notes = "条件查询[buildApplyFile],作者：lhy")
	@PostMapping("/query")
	public ResponseInfo<List<BuildApplyFile>> queryBuildApplyFileList(@RequestBody BuildApplyFile buildApplyFile){
		List<BuildApplyFile> list = buildApplyFileServiceImpl.queryBuildApplyFileList(buildApplyFile);
		return new ResponseInfo<List<BuildApplyFile>>(ResultInfo.Success, list);	
	}
	
	/**
	 * @explain 分页条件查询建房申请附件表   
	 * @param   对象参数：AppPage<BuildApplyFile>
	 * @return  PageDataVo<BuildApplyFile>
	 * @author  lhy
	 * @time    2019年5月5日
	 */

	@ApiOperation(value = "分页查询", notes = "分页查询返回对象[PageDataVo<BuildApplyFile>],作者：lhy")
	@ApiImplicitParams({
        @ApiImplicitParam(paramType="query", name = "page", value = "当前页", required = true, dataType = "int"),
        @ApiImplicitParam(paramType="query", name = "size", value = "页行数", required = true, dataType = "int")
    })
	@RequestMapping(value = "",method = RequestMethod.GET, produces = "application/json")
	public ResponseInfo<PageInfo<BuildApplyFile>> getBuildApplyFileBySearch(
		@RequestParam(value="page",defaultValue ="1")  Integer page,
		@RequestParam(value="size",defaultValue ="100")  Integer size){
		AppPage<BuildApplyFile> apage =new AppPage<BuildApplyFile>();
		apage.setPageNum(page);
		apage.setPageSize(size);
		//其他参数
//		BuildApplyFile buildApplyFile=new BuildApplyFile();
//		page.setParam(buildApplyFile);
		//分页数据
		PageInfo<BuildApplyFile> pageInfo = buildApplyFileServiceImpl.list(apage);
		return new ResponseInfo<PageInfo<BuildApplyFile>>(ResultInfo.Success, pageInfo);	
	}
}