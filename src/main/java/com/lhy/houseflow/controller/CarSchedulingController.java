/**
 * @filename:CarSchedulingController 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.CarScheduling;
import com.lhy.houseflow.service.CarSchedulingService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**   
 * 
 * @Description:  车辆排班接口层
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Api(tags={"车辆排班"})
@RestController
@RequestMapping("/api/car-schedulings")
@Slf4j
public class CarSchedulingController extends BaseController {
	
	@Autowired
	public CarSchedulingService carSchedulingServiceImpl;
	
	/**
	 * @explain 查询车辆排班对象  <swagger GET请求>
	 * @param   对象参数：id
	 * @return  carScheduling
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "获取车辆排班信息", notes = "获取车辆排班信息[carScheduling]，作者：lhy")
	@ApiImplicitParam(paramType="path", name = "id", value = "车辆排班id", required = true, dataType = "Long")
	@RequestMapping(value = "/{id}",method = RequestMethod.GET, produces = "application/json") 
	public ResponseInfo<CarScheduling> getCarSchedulingById(@PathVariable("id")Long id){
		CarScheduling result=carSchedulingServiceImpl.findById(id);
		return new ResponseInfo<CarScheduling>(ResultInfo.Success, result);
	}
	
	/**
	 * @explain 添加车辆排班对象
	 * @param   对象参数：carScheduling
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "添加车辆排班", notes = "添加车辆排班[carScheduling],作者：lhy")
	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseInfo<CarScheduling> add(@RequestBody CarScheduling carScheduling){
		carSchedulingServiceImpl.add(carScheduling);
		return new ResponseInfo<CarScheduling>(ResultInfo.Success, carScheduling);
	}
	
	/**
	 * @explain 删除车辆排班对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@RequestMapping(method = RequestMethod.DELETE, produces = "application/json")
	@ApiOperation(value = "删除车辆排班", notes = "删除车辆排班,作者：lhy")
	@ApiImplicitParam(paramType="query", name = "id", value = "车辆排班id", required = true, dataType = "Long")
	public ResponseInfo<String> deleteById(Long id){
		carSchedulingServiceImpl.deleteById(id);
		return new ResponseInfo<String>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 修改车辆排班对象
	 * @param   对象参数：carScheduling
	 * @return  carScheduling
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "修改车辆排班", notes = "修改车辆排班[carScheduling],作者：lhy")
	@RequestMapping(method = RequestMethod.PUT, produces = "application/json")
	public ResponseInfo<CarScheduling> update(@RequestBody CarScheduling carScheduling){
		carSchedulingServiceImpl.update(carScheduling);
		return new ResponseInfo<CarScheduling>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 获取匹配车辆排班
	 * @param   对象参数：carScheduling
	 * @return  List<CarScheduling>
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "条件查询车辆排班", notes = "条件查询[carScheduling],作者：lhy")
	@PostMapping("/query")
	public ResponseInfo<List<CarScheduling>> queryCarSchedulingList(@RequestBody CarScheduling carScheduling){
		List<CarScheduling> list = carSchedulingServiceImpl.queryCarSchedulingList(carScheduling);
		return new ResponseInfo<List<CarScheduling>>(ResultInfo.Success, list);	
	}
	
	/**
	 * @explain 分页条件查询车辆排班   
	 * @param   对象参数：AppPage<CarScheduling>
	 * @return  PageDataVo<CarScheduling>
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@GetMapping("/")
	@ApiOperation(value = "分页查询", notes = "分页查询返回对象[PageDataVo<CarScheduling>],作者：lhy")
	@ApiImplicitParams({
        @ApiImplicitParam(paramType="query", name = "page", value = "当前页", required = true, dataType = "int"),
        @ApiImplicitParam(paramType="query", name = "size", value = "页行数", required = true, dataType = "int")
    })
	
	public ResponseInfo<PageInfo<CarScheduling>> getCarSchedulingBySearch(
		@RequestParam(value="page",defaultValue ="1")  Integer page,
		@RequestParam(value="size",defaultValue ="100")  Integer size){
		AppPage<CarScheduling> aPage =new AppPage<CarScheduling>();
		aPage.setPageNum(page);
		aPage.setPageSize(size);
		//其他参数
//		CarScheduling carScheduling=new CarScheduling();
//		page.setParam(carScheduling);
		//分页数据
		PageInfo<CarScheduling> pageInfo = carSchedulingServiceImpl.list(aPage);
		return new ResponseInfo<PageInfo<CarScheduling>>(ResultInfo.Success, pageInfo);	
	}
}