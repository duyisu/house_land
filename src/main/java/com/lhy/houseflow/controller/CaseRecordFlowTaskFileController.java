/**
 * @filename:CaseRecordFlowTaskFileController 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.CaseRecordFlowTaskFile;
import com.lhy.houseflow.service.CaseRecordFlowTaskFileService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**   
 * 
 * @Description:  两违流程附件表接口层
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
/*
@Api(tags={"两违流程附件表"})
@RestController
@RequestMapping("/api/case-record-flow-task-files")
*/
@Slf4j
public class CaseRecordFlowTaskFileController extends BaseController {
	
	@Autowired
	public CaseRecordFlowTaskFileService caseRecordFlowTaskFileServiceImpl;
	
	/**
	 * @explain 查询两违流程附件表对象  <swagger GET请求>
	 * @param   对象参数：id
	 * @return  caseRecordFlowTaskFile
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "获取两违流程附件表信息", notes = "获取两违流程附件表信息[caseRecordFlowTaskFile]，作者：lhy")
	@ApiImplicitParam(paramType="path", name = "id", value = "两违流程附件表id", required = true, dataType = "Long")
	@RequestMapping(value = "/{id}",method = RequestMethod.GET, produces = "application/json") 
	public ResponseInfo<CaseRecordFlowTaskFile> getCaseRecordFlowTaskFileById(@PathVariable("id")Long id){
		CaseRecordFlowTaskFile result=caseRecordFlowTaskFileServiceImpl.findById(id);
		return new ResponseInfo<CaseRecordFlowTaskFile>(ResultInfo.Success, result);
	}
	
	/**
	 * @explain 添加两违流程附件表对象
	 * @param   对象参数：caseRecordFlowTaskFile
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "添加两违流程附件表", notes = "添加两违流程附件表[caseRecordFlowTaskFile],作者：lhy")
	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseInfo<CaseRecordFlowTaskFile> add(@RequestBody CaseRecordFlowTaskFile caseRecordFlowTaskFile){
		caseRecordFlowTaskFileServiceImpl.add(caseRecordFlowTaskFile);
		return new ResponseInfo<CaseRecordFlowTaskFile>(ResultInfo.Success, caseRecordFlowTaskFile);
	}
	
	/**
	 * @explain 删除两违流程附件表对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@RequestMapping(method = RequestMethod.DELETE, produces = "application/json")
	@ApiOperation(value = "删除两违流程附件表", notes = "删除两违流程附件表,作者：lhy")
	@ApiImplicitParam(paramType="query", name = "id", value = "两违流程附件表id", required = true, dataType = "Long")
	public ResponseInfo<String> deleteById(Long id){
		caseRecordFlowTaskFileServiceImpl.deleteById(id);
		return new ResponseInfo<String>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 修改两违流程附件表对象
	 * @param   对象参数：caseRecordFlowTaskFile
	 * @return  caseRecordFlowTaskFile
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "修改两违流程附件表", notes = "修改两违流程附件表[caseRecordFlowTaskFile],作者：lhy")
	@RequestMapping(method = RequestMethod.PUT, produces = "application/json")
	public ResponseInfo<CaseRecordFlowTaskFile> update(@RequestBody CaseRecordFlowTaskFile caseRecordFlowTaskFile){
		caseRecordFlowTaskFileServiceImpl.update(caseRecordFlowTaskFile);
		return new ResponseInfo<CaseRecordFlowTaskFile>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 获取匹配两违流程附件表
	 * @param   对象参数：caseRecordFlowTaskFile
	 * @return  List<CaseRecordFlowTaskFile>
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "条件查询两违流程附件表", notes = "条件查询[caseRecordFlowTaskFile],作者：lhy")
	@PostMapping("/query")
	public ResponseInfo<List<CaseRecordFlowTaskFile>> queryCaseRecordFlowTaskFileList(@RequestBody CaseRecordFlowTaskFile caseRecordFlowTaskFile){
		List<CaseRecordFlowTaskFile> list = caseRecordFlowTaskFileServiceImpl.queryCaseRecordFlowTaskFileList(caseRecordFlowTaskFile);
		return new ResponseInfo<List<CaseRecordFlowTaskFile>>(ResultInfo.Success, list);	
	}
	
	/**
	 * @explain 分页条件查询两违流程附件表   
	 * @param   对象参数：AppPage<CaseRecordFlowTaskFile>
	 * @return  PageDataVo<CaseRecordFlowTaskFile>
	 * @author  lhy
	 * @time    2019年5月5日
	 */

	@ApiOperation(value = "分页查询", notes = "分页查询返回对象[PageDataVo<CaseRecordFlowTaskFile>],作者：lhy")
	@ApiImplicitParams({
        @ApiImplicitParam(paramType="query", name = "page", value = "当前页", required = true, dataType = "int"),
        @ApiImplicitParam(paramType="query", name = "size", value = "页行数", required = true, dataType = "int")
    })
	@RequestMapping(value = "",method = RequestMethod.GET, produces = "application/json")
	public ResponseInfo<PageInfo<CaseRecordFlowTaskFile>> getCaseRecordFlowTaskFileBySearch(
		@RequestParam(value="page",defaultValue ="1")  Integer page,
		@RequestParam(value="size",defaultValue ="100")  Integer size){
		AppPage<CaseRecordFlowTaskFile> aPage =new AppPage<CaseRecordFlowTaskFile>();
		aPage.setPageNum(page);
		aPage.setPageSize(size);
		//其他参数
//		CaseRecordFlowTaskFile caseRecordFlowTaskFile=new CaseRecordFlowTaskFile();
//		page.setParam(caseRecordFlowTaskFile);
		//分页数据
		PageInfo<CaseRecordFlowTaskFile> pageInfo = caseRecordFlowTaskFileServiceImpl.list(aPage);
		return new ResponseInfo<PageInfo<CaseRecordFlowTaskFile>>(ResultInfo.Success, pageInfo);	
	}
}