package com.lhy.houseflow.controller;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.lhy.houseflow.service.UploadFileService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Api(tags={"下载文件"})
//@RestController
@RequestMapping("/app-download")
public class FileDwonloadController {
    @Value("${upload.apppath}")
    private String filepath;
    private static final String PATH_DELIMITADOR = "/";   
    
   /**
     * @explain 文件下载 get
     * @param 文件ID：id
     * @return ResponseInfo
     * @author lhy
     * @time 2019年8月5日
     */
    @ApiOperation(value = "文件下载", notes = "文件下载，作者：lhy")
    @ApiImplicitParam(paramType = "path", name = "filename", value = "文件名", required = true, dataType = "String")
    @GetMapping(value = "/{filename}")
    public String download(@PathVariable("filename") String filename, HttpServletResponse response) {
        File file = new File(filepath + filename);

        if (file.exists()) {
            response.setContentType("application/force-download");
            response.addHeader("Content-Disposition", "attachment;fileName=" + filename);

            BufferedInputStream bi = null;
            try {
                byte[] buffer = new byte[1024];
                bi = new BufferedInputStream(new FileInputStream(file));
                ServletOutputStream outputStream = response.getOutputStream();
                int i = -1;
                while (-1 != (i = bi.read(buffer))) {
                    outputStream.write(buffer, 0, i);
                }

            } catch (Exception e) {
                log.error(e.getMessage());
            } finally {
                if (bi != null) {
                    try {
                        bi.close();
                    } catch (IOException e) {
                        log.error(e.getMessage());
                    }
                }
            }
        }
        return filename;
    } 

}
