/**
 * @filename:CarController 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.Car;
import com.lhy.houseflow.service.CarService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**   
 * 
 * @Description:  车辆接口层
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Api(tags={"车辆"})
@RestController
@RequestMapping("/api/cars")
@Slf4j
public class CarController extends BaseController {
	
	@Autowired
	public CarService carServiceImpl;
	
	/**
	 * @explain 查询车辆对象  <swagger GET请求>
	 * @param   对象参数：id
	 * @return  car
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "获取车辆信息", notes = "获取车辆信息[car]，作者：lhy")
	@ApiImplicitParam(paramType="path", name = "id", value = "车辆id", required = true, dataType = "Long")
	@RequestMapping(value = "/{id}",method = RequestMethod.GET, produces = "application/json") 
	public ResponseInfo<Car> getCarById(@PathVariable("id")Long id){
		Car result=carServiceImpl.findById(id);
		return new ResponseInfo<Car>(ResultInfo.Success, result);
	}
	
	/**
	 * @explain 添加车辆对象
	 * @param   对象参数：car
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "添加车辆", notes = "添加车辆[car],作者：lhy")
	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseInfo<Car> add(@RequestBody Car car){
		carServiceImpl.add(car);
		return new ResponseInfo<Car>(ResultInfo.Success, car);
	}
	
	/**
	 * @explain 删除车辆对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@RequestMapping(method = RequestMethod.DELETE, produces = "application/json")
	@ApiOperation(value = "删除车辆", notes = "删除车辆,作者：lhy")
	@ApiImplicitParam(paramType="query", name = "id", value = "车辆id", required = true, dataType = "Long")
	public ResponseInfo<String> deleteById(Long id){
		carServiceImpl.deleteById(id);
		return new ResponseInfo<String>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 修改车辆对象
	 * @param   对象参数：car
	 * @return  car
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "修改车辆", notes = "修改车辆[car],作者：lhy")
	@RequestMapping(method = RequestMethod.PUT, produces = "application/json")
	public ResponseInfo<Car> update(@RequestBody Car car){
		carServiceImpl.update(car);
		return new ResponseInfo<Car>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 获取匹配车辆
	 * @param   对象参数：car
	 * @return  List<Car>
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "条件查询车辆", notes = "条件查询[car],作者：lhy")
	@PostMapping("/query")
	public ResponseInfo<List<Car>> queryCarList(@RequestBody Car car){
		List<Car> list = carServiceImpl.queryCarList(car);
		return new ResponseInfo<List<Car>>(ResultInfo.Success, list);	
	}
	
	/**
	 * @explain 分页条件查询车辆   
	 * @param   对象参数：AppPage<Car>
	 * @return  PageDataVo<Car>
	 * @author  lhy
	 * @time    2019年5月5日
	 */

	@ApiOperation(value = "分页查询", notes = "分页查询返回对象[PageDataVo<Car>],作者：lhy")
	@ApiImplicitParams({
        @ApiImplicitParam(paramType="query", name = "page", value = "当前页", required = true, dataType = "int"),
        @ApiImplicitParam(paramType="query", name = "size", value = "页行数", required = true, dataType = "int")
    })
	@RequestMapping(value = "",method = RequestMethod.GET, produces = "application/json")
	public ResponseInfo<PageInfo<Car>> getCarBySearch(
		@RequestParam(value="page",defaultValue ="1")  Integer page,
		@RequestParam(value="size",defaultValue ="100")  Integer size){
		AppPage<Car> apage =new AppPage<Car>();
		apage.setPageNum(page);
		apage.setPageSize(size);
		//其他参数
//		Car car=new Car();
//		page.setParam(car);
		//分页数据
		PageInfo<Car> pageInfo = carServiceImpl.list(apage);
		return new ResponseInfo<PageInfo<Car>>(ResultInfo.Success, pageInfo);	
	}
}