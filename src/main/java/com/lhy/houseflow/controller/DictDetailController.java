/**
 * @filename:DictDetailController 2019年5月5日
 * @project HouseFlow V1.0 Copyright(c) 2018 lhy Co. Ltd. All right reserved.
 */
package com.lhy.houseflow.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.Dict;
import com.lhy.houseflow.entity.DictDetail;
import com.lhy.houseflow.service.DictDetailService;
import com.lhy.houseflow.service.DictService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @Description: 字典明细接口层
 * @Author: lhy
 * @CreateDate: 2019年5月5日
 * @Version: V1.0
 * 
 */
@Api(tags = {"字典明细"})
@RestController
@RequestMapping("/api/dict-details")
@Slf4j
public class DictDetailController extends BaseController {

    @Autowired
    public DictDetailService dictDetailServiceImpl;
    @Autowired
	public DictService dictServiceImpl;

    /**
     * @explain 查询字典明细对象 <swagger GET请求>
     * @param 对象参数：id
     * @return dictDetail
     * @author lhy
     * @time 2019年5月5日
     */
    @ApiOperation(value = "获取字典明细信息", notes = "获取字典明细信息[dictDetail]，作者：lhy")
    @ApiImplicitParam(paramType = "path", name = "id", value = "字典明细id", required = true, dataType = "Long")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
    public ResponseInfo<DictDetail> getDictDetailById(@PathVariable("id") Long id) {
        DictDetail result = dictDetailServiceImpl.findById(id);
        return new ResponseInfo<DictDetail>(ResultInfo.Success, result);
    }

    /**
     * @explain 添加字典明细对象
     * @param 对象参数：dictDetail
     * @return int
     * @author lhy
     * @time 2019年5月5日
     */
    @ApiOperation(value = "添加字典明细", notes = "添加字典明细[dictDetail],作者：lhy")
    @RequestMapping(method = RequestMethod.POST, produces = "application/json")
    public ResponseInfo<DictDetail> add(@RequestBody DictDetail dictDetail) {
        dictDetailServiceImpl.add(dictDetail);
        return new ResponseInfo<DictDetail>(ResultInfo.Success, dictDetail);
    }

    /**
     * @explain 删除字典明细对象
     * @param 对象参数：id
     * @return int
     * @author lhy
     * @time 2019年5月5日
     */
    @RequestMapping(method = RequestMethod.DELETE, produces = "application/json")
    @ApiOperation(value = "删除字典明细", notes = "删除字典明细,作者：lhy")
    @ApiImplicitParam(paramType = "query", name = "id", value = "字典明细id", required = true, dataType = "Long")
    public ResponseInfo<String> deleteById(Long id) {
        dictDetailServiceImpl.deleteById(id);
        return new ResponseInfo<String>(ResultInfo.Success, null);
    }

    /**
     * @explain 修改字典明细对象
     * @param 对象参数：dictDetail
     * @return dictDetail
     * @author lhy
     * @time 2019年5月5日
     */
    @ApiOperation(value = "修改字典明细", notes = "修改字典明细[dictDetail],作者：lhy")
    @RequestMapping(method = RequestMethod.PUT, produces = "application/json")
    public ResponseInfo<DictDetail> update(@RequestBody DictDetail dictDetail) {
        dictDetailServiceImpl.update(dictDetail);
        return new ResponseInfo<DictDetail>(ResultInfo.Success, null);
    }

    /**
     * @explain 获取匹配字典明细
     * @param 对象参数：dictDetail
     * @return List<DictDetail>
     * @author lhy
     * @time 2019年5月5日
     */
    @ApiOperation(value = "条件查询字典明细", notes = "条件查询[dictDetail],作者：lhy")
    @PostMapping("/query")
    public ResponseInfo<List<DictDetail>> queryDictDetailList(@RequestBody DictDetail dictDetail) {
        List<DictDetail> list = dictDetailServiceImpl.queryDictDetailList(dictDetail);
        return new ResponseInfo<List<DictDetail>>(ResultInfo.Success, list);
    }

    /**
     * @explain 获取匹配字典明细
     * @param 对象参数：dictDetail
     * @return List<DictDetail>
     */
    @ApiOperation(value = "根据字典类别的名称条件查询字典明细列表", notes = "条件查询[dictName]")
    @PostMapping("/queryByDict")
    public ResponseInfo<List<DictDetail>> queryDictDetailListByDictName(@RequestParam(value="dictName") String dictName){
    	Dict dict = new Dict();
    	dict.setName(dictName);
    	List<Dict> dictList = dictServiceImpl.queryDictList(dict);
    	List<DictDetail> list = new ArrayList<DictDetail>();
    	DictDetail dictDetail = null;
    	if(dictList != null && dictList.size() >0) {
    		for(Dict d : dictList) {
    			dictDetail = new DictDetail();
    			dictDetail.setDictId(d.getId());
    			List<DictDetail> tempList = dictDetailServiceImpl.queryDictDetailList(dictDetail);
    			list.addAll(tempList);
    		}
    	}
    	return new ResponseInfo<List<DictDetail>>(ResultInfo.Success,list);
    }
    /**
     * @explain 分页条件查询字典明细
     * @param 对象参数：AppPage<DictDetail>
     * @return PageInfo<DictDetail>
     * @author lhy
     * @time 2019年5月5日
     */

    @ApiOperation(value = "分页查询", notes = "分页查询返回对象[PageInfo<DictDetail>],作者：lhy")
    @ApiImplicitParams({
        @ApiImplicitParam(paramType = "query", name = "dictName", value = "字典名称", required = true, dataType = "String"),
        @ApiImplicitParam(paramType = "query", name = "page", value = "当前页", required = false, dataType = "int"),
        @ApiImplicitParam(paramType = "query", name = "size", value = "页行数", required = false, dataType = "int")})
    @RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
    public ResponseInfo<PageInfo<DictDetail>> getDictDetailBySearch(
        @RequestParam(value = "dictName", defaultValue = "100") String dictName,
        @RequestParam(value = "page", defaultValue = "1") Integer page,
        @RequestParam(value = "size", defaultValue = "100") Integer size) {
        AppPage<DictDetail> aPage = new AppPage<DictDetail>();
        aPage.setPageNum(page);
        aPage.setPageSize(size);

        PageInfo<DictDetail> pageInfo = dictDetailServiceImpl.queryByDictName(dictName, aPage);
        return new ResponseInfo<PageInfo<DictDetail>>(ResultInfo.Success, pageInfo);
    }

    @ApiOperation(value = "分页查询", notes = "分页查询返回对象[PageInfo<DictDetail>],作者：lhy")
    @ApiImplicitParams({
        @ApiImplicitParam(paramType = "query", name = "dictName", value = "字典名称", required = true, dataType = "String"),
        @ApiImplicitParam(paramType = "query", name = "page", value = "当前页", required = false, dataType = "int"),
        @ApiImplicitParam(paramType = "query", name = "size", value = "页行数", required = false, dataType = "int")})
    @RequestMapping(value = "/map", method = RequestMethod.GET, produces = "application/json")
    public ResponseInfo<Map<String, List<DictDetail>>> getDictDetailByDictNames(
        @RequestParam(value = "dictName", defaultValue = "100") String dictName,
        @RequestParam(value = "page", defaultValue = "1") Integer page,
        @RequestParam(value = "size", defaultValue = "100") Integer size) {
        AppPage<DictDetail> aPage = new AppPage<DictDetail>();
        aPage.setPageNum(page);
        aPage.setPageSize(size);
        Map<String, List<DictDetail>> map = new HashMap<String, List<DictDetail>>();
        String[] names = StringUtils.split(dictName, ",");
        if (names != null) {
            for (String name : names) {
                PageInfo<DictDetail> pageInfo = dictDetailServiceImpl.queryByDictName(name, aPage);
                map.put(name, pageInfo.getList());
            }
        }

        return new ResponseInfo<Map<String, List<DictDetail>>>(ResultInfo.Success, map);
    }

}