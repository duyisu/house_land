package com.lhy.houseflow.controller;

import java.util.List;

import org.flowable.engine.history.HistoricActivityInstance;
import org.flowable.engine.runtime.ProcessInstance;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.constant.FlowConst;
import com.lhy.houseflow.entity.CaseDefendant;
import com.lhy.houseflow.entity.CaseRecord;
import com.lhy.houseflow.entity.CaseRecordFile;
import com.lhy.houseflow.entity.CaseRecordFlowTask;
import com.lhy.houseflow.entity.EntityUtils;
import com.lhy.houseflow.entity.SysUser;
import com.lhy.houseflow.entity.TaskUserInfo;
import com.lhy.houseflow.entity.WfDefinitionStep;
import com.lhy.houseflow.entity.vo.CaseRecordBaseInfoVo;
import com.lhy.houseflow.entity.vo.CaseRecordFlowTaskVo;
import com.lhy.houseflow.entity.vo.CaseRecordInfoEditVo;
import com.lhy.houseflow.entity.vo.CaseRecordInfoVo;
import com.lhy.houseflow.entity.vo.TaskDoInfo;
import com.lhy.houseflow.entity.vo.TaskVo;
import com.lhy.houseflow.service.IllegalBuildingLandService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @Description: 两违管理接口层
 * @Author: lhy
 * @CreateDate: 2019年5月5日
 * @Version: V1.0
 * 
 */
@Api(tags = {"两违管理"})
@RestController
@RequestMapping("/api/illegal-two")
@Slf4j
public class IllegalBuildingLandController extends BaseController {

    @Autowired
    private IllegalBuildingLandService illegalBuildingLandService;

    /**
     * @explain 新建案件
     * @param 对象参数：CaseRecordInfoEditVo
     * @return int
     * @author lhy
     * @time 2019年5月5日
     */
    @ApiOperation(value = "新建案件", notes = "新建案件,此时可区分保存与提交按钮：保存即未草稿，保存status=01,提交设置status=02")
    @RequestMapping(value = "/case-records", method = RequestMethod.POST, produces = "application/json")
    public ResponseInfo<CaseRecord> addCaseRecord(@RequestBody CaseRecordInfoEditVo info) {
        CaseRecord record = EntityUtils.createCaseRecordInfoEditVo(info);
        List<CaseDefendant> defendants = EntityUtils.createCaseDefendants(info);
        List<CaseRecordFile> files = EntityUtils.createCaseRecordFiles(info);
        record.setReportUserId(currUser().getId());
        record = illegalBuildingLandService.addCaseRecord(record, defendants, files);
        return new ResponseInfo<CaseRecord>(ResultInfo.Success, record);
    }

    @ApiOperation(value = "修改案件", notes = "新建案件,作者：lhy")
    @RequestMapping(value = "/case-records", method = RequestMethod.PUT, produces = "application/json")
    public ResponseInfo<CaseRecord> updateCaseRecord(@RequestBody CaseRecordInfoEditVo info) {
        CaseRecord record = EntityUtils.createCaseRecordInfoEditVo(info);
        List<CaseDefendant> defendants = EntityUtils.createCaseDefendants(info);
        List<CaseRecordFile> files = EntityUtils.createCaseRecordFiles(info);
        record = illegalBuildingLandService.updateCaseRecord(record, defendants, files);

        return new ResponseInfo<CaseRecord>(ResultInfo.Success, record);
    }
    
    /**
     * @explain 新建两违流程
     * @param 对象参数：CaseRecord
     * @return int
     * @author lhy
     * @time 2019年5月5日
     */
    @ApiOperation(value = "案件明细", notes = "案件明细,作者：lhy")
    @ApiImplicitParams({
        @ApiImplicitParam(paramType = "path", name = "caseId", value = "案件ID", required = true, dataType = "Integer")})
    @RequestMapping(value = "/case-records/{caseId}", method = RequestMethod.GET, produces = "application/json")
    public ResponseInfo<CaseRecordInfoVo> findCaseRecordInfo(@PathVariable(value = "caseId", required = true) int caseId) {
        CaseRecordInfoVo inst = illegalBuildingLandService.findCaseRecordInfo(new Long(caseId));
        return new ResponseInfo<CaseRecordInfoVo>(ResultInfo.Success, inst);
    } 
    /**
     * @explain 新建两违流程
     * @param 对象参数：CaseRecord
     * @return int
     * @author lhy
     * @time 2019年5月5日
     */
    @ApiOperation(value = "新建", notes = "新建两违流程,作者：lhy")
    @ApiImplicitParams({
        @ApiImplicitParam(paramType = "path", name = "caseId", value = "案件ID", required = true, dataType = "Long")})
    @RequestMapping(value = "/case/{caseId}", method = RequestMethod.POST, produces = "application/json")
    public ResponseInfo<ProcessInstance> add(@PathVariable(value = "caseId", required = true) Long caseId) {
        ProcessInstance inst = illegalBuildingLandService.createTask(currUser().getUsername(), caseId);
        return new ResponseInfo<ProcessInstance>(ResultInfo.Success, inst);
    }
    /**
     * 我的任务
     */

    @ApiOperation(value = "我的任务（待签收）", notes = "我的任务（待签收）,作者：lhy")
    @ApiImplicitParams({
        @ApiImplicitParam(paramType = "query", name = "reportName", value = "举报人", required = false,
            dataType = "String"),
        @ApiImplicitParam(paramType = "query", name = "defendantName", value = "举报对象", required = false,
            dataType = "String"),
        @ApiImplicitParam(paramType = "query", name = "sources", value = "举报类型", required = false,
            dataType = "Integer"),
        @ApiImplicitParam(paramType = "query", name = "caseType", value = "两违类型", required = false,
            dataType = "Integer"),
        @ApiImplicitParam(paramType = "query", name = "caseSubtype", value = "二级类型", required = false,
            dataType = "Integer"),
        @ApiImplicitParam(paramType = "query", name = "areaId", value = "所属镇区", required = false, dataType = "Integer"),
        @ApiImplicitParam(paramType = "query", name = "addr", value = "两违地址", required = false, dataType = "String")})
    @RequestMapping(value = "", method = RequestMethod.GET, produces = "application/json")
    public ResponseInfo<List<TaskVo<CaseRecordBaseInfoVo>>> getMyTask(
        @RequestParam(value = "reportName") String reportName,
        @RequestParam(value = "defendantName") String defendantName, @RequestParam(value = "sources") Integer sources,
        @RequestParam(value = "caseType") Integer caseType, @RequestParam(value = "caseSubtype") Integer caseSubtype,
        @RequestParam(value = "areaId") Long areaId, @RequestParam(value = "addr") String addr) {

        List<TaskVo<CaseRecordBaseInfoVo>> tasks = illegalBuildingLandService.myTask(currUser(),
            reportName, defendantName, sources, caseType, caseSubtype, areaId, addr);
        return new ResponseInfo<List<TaskVo<CaseRecordBaseInfoVo>>>(ResultInfo.Success, tasks);
    }

    /**
     * 我的任务
     */

    @ApiOperation(value = "我的任务（已完成）", notes = "我的任务[HistoricActivityInstance],作者：lhy")
    @ApiImplicitParams({
        @ApiImplicitParam(paramType = "query", name = "page", value = "当前页", required = false, dataType = "int"),
        @ApiImplicitParam(paramType = "query", name = "size", value = "页行数", required = false, dataType = "int"),
        @ApiImplicitParam(paramType = "query", name = "reportName", value = "举报人", required = false,
            dataType = "String"),
        @ApiImplicitParam(paramType = "query", name = "defendantName", value = "举报对象", required = false,
            dataType = "String"),
        @ApiImplicitParam(paramType = "query", name = "sources", value = "举报类型", required = false,
            dataType = "Integer"),
        @ApiImplicitParam(paramType = "query", name = "caseType", value = "两违类型", required = false,
            dataType = "Integer"),
        @ApiImplicitParam(paramType = "query", name = "caseSubtype", value = "二级类型", required = false,
            dataType = "Integer"),
        @ApiImplicitParam(paramType = "query", name = "areaId", value = "所属镇区", required = false, dataType = "Integer"),
        @ApiImplicitParam(paramType = "query", name = "addr", value = "两违地址", required = false, dataType = "String")})

    @RequestMapping(value = "/history", method = RequestMethod.GET, produces = "application/json")
    public ResponseInfo<List<TaskVo<CaseRecordBaseInfoVo>>> getCarBySearch(
        @RequestParam(value = "reportName") String reportName,
        @RequestParam(value = "defendantName") String defendantName, @RequestParam(value = "sources") Integer sources,
        @RequestParam(value = "caseType") Integer caseType, @RequestParam(value = "caseSubtype") Integer caseSubtype,
        @RequestParam(value = "areaId") Long areaId, @RequestParam(value = "addr") String addr,
        @RequestParam(value = "type", defaultValue = "1") Integer type,
        @RequestParam(value = "page", defaultValue = "1") Integer page,
        @RequestParam(value = "size", defaultValue = "100") Integer size) {
        List<HistoricActivityInstance> info =
            illegalBuildingLandService.myHistory(currUser().getUsername(), page, size);
        return new ResponseInfo<List<TaskVo<CaseRecordBaseInfoVo>>>(ResultInfo.Success, null);
    }

    @ApiOperation(value = "签收", notes = "签收,作者：lhy")
    @ApiImplicitParams({
        @ApiImplicitParam(paramType = "path", name = "taskId", value = "任务ID", required = true, dataType = "String")})
    @RequestMapping(value = "/{taskId}/claim", method = RequestMethod.POST, produces = "application/json")
    public ResponseInfo<String> claimTask(@PathVariable(value = "taskId", required = true) String taskId) {
        illegalBuildingLandService.claimTask(currUser().getUsername(), taskId);
        return new ResponseInfo<String>(ResultInfo.Success);
    }

    @ApiOperation(value = "委派", notes = "委派,作者：lhy")
    @ApiImplicitParams({
    @ApiImplicitParam(paramType = "path", name = "taskId", value = "任务ID", required = true, dataType = "String")})
    @RequestMapping(value = "/{taskId}/delegate", method = RequestMethod.POST, produces = "application/json")
    public ResponseInfo<String> delegateTask(@PathVariable(value = "taskId", required = true) String taskId) {
        illegalBuildingLandService.delegateTask(currUser().getUsername(), taskId);
        return new ResponseInfo<String>(ResultInfo.Success);
    }

    @ApiOperation(value = "提交任务", notes = "提交任务,作者：lhy")
    @ApiImplicitParams({
        @ApiImplicitParam(paramType = "path", name = "taskId", value = "任务ID", required = true, dataType = "String")})
    @RequestMapping(value = "/{taskId}/complete", method = RequestMethod.POST, produces = "application/json")
    public ResponseInfo<String> complete(@PathVariable(value = "taskId", required = true) String taskId,
        CaseRecordFlowTaskVo info) {
        CaseRecordFlowTask flowTask = new CaseRecordFlowTask();
        BeanUtils.copyProperties(info, flowTask);
        illegalBuildingLandService.complete(info.getTaskId(), flowTask);
        return new ResponseInfo<String>(ResultInfo.Success);
    }

    @ApiOperation(value = "下一步用户任务信息", notes = "待提交任务用户信息,作者：lhy")
    @ApiImplicitParams({
        @ApiImplicitParam(paramType = "path", name = "taskId", value = "任务ID", required = true, dataType = "String")})
    @RequestMapping(value = "/{taskId}/next-task-user", method = RequestMethod.POST, produces = "application/json")
    public ResponseInfo<List<TaskUserInfo>>
        nextTaskUserInfos(@PathVariable(value = "taskId", required = true) String taskId) {
        List<TaskUserInfo> list = illegalBuildingLandService.findNextTaskUserInfosByTaskId(taskId);
        return new ResponseInfo<List<TaskUserInfo>>(ResultInfo.Success, list);
    }
    
    @ApiOperation(value = "流程下一步发送至用户", notes = "查看或处理时得到当前的stepId,根据stepId获取流程下一步流转人员，直接显示人员姓名在页面上")
    @RequestMapping(value = "/nextStepUser", method = RequestMethod.POST, produces = "application/json")
    public ResponseInfo<List<SysUser>> nextStepUser( @RequestParam(value = "stepId") int stepId ) {
        List<SysUser> list = illegalBuildingLandService.wfToUser(Long.valueOf(stepId));
        return new ResponseInfo<List<SysUser>>(ResultInfo.Success, list);
    }
    
	
	  @ApiOperation(value = "工作台任务", notes = "两违案件工作台,staus:草稿:'draft',待办：‘todo',已办:'hasDone'")
	  @ApiImplicitParams({
	        @ApiImplicitParam(paramType = "query", name = "reportName", value = "上报人", required = false,
	            dataType = "String"),
	        @ApiImplicitParam(paramType = "query", name = "complainant", value = "投诉人", required = false,
	            dataType = "String"),
	        @ApiImplicitParam(paramType = "query", name = "source", value = "举报类型", required = false,
	            dataType = "Integer"),
	        @ApiImplicitParam(paramType = "query", name = "caseType", value = "两违类型", required = false,
	            dataType = "Integer"),
	        @ApiImplicitParam(paramType = "query", name = "caseSubtype", value = "二级类型", required = false,
	            dataType = "Integer"),
	        @ApiImplicitParam(paramType = "query", name = "areaId", value = "所属镇区", required = false, dataType = "Integer"),
	        @ApiImplicitParam(paramType = "query", name = "addr", value = "两违地址", required = false, dataType = "String")})
	  @RequestMapping(value = "/caseRecordQryList", method = RequestMethod.POST,produces = "application/json") 
	  public ResponseInfo<PageInfo<CaseRecord>> caseRecordMyWorkList(
			@RequestParam(value = "reportName", required = false) String reportName,
	        @RequestParam(value = "complainant", required = false) String complainant, 
	        @RequestParam(value = "source", required = false) Integer source,
	        @RequestParam(value = "caseType", required = false) Integer caseType, 
	        @RequestParam(value = "caseSubtype", required = false) Integer caseSubtype,
	        @RequestParam(value = "areaId", required = false) Long areaId, 
	        @RequestParam(value = "addr", required = false) String addr,
	        @RequestParam(value = "page", defaultValue = "1") Integer page,
	        @RequestParam(value = "size", defaultValue = "100") Integer size) { 
		  PageInfo<CaseRecord> list =illegalBuildingLandService.findMyCaseRecordWork(currUser().getId(),reportName,
				  complainant,source,caseType,caseSubtype,areaId,addr,page,size); 
		  return new ResponseInfo<PageInfo<CaseRecord>>(ResultInfo.Success,list); 
	  }
	  
	@ApiOperation(value = "待办已办草稿列表", notes = "待办 已办 草稿")
	@RequestMapping(value = "/myWorkCaseRecordList", method = RequestMethod.GET,produces = "application/json")
    public ResponseInfo<PageInfo<CaseRecord>> myWorkCaseRecordList(@RequestParam(value = "status") String status){
	  PageInfo<CaseRecord> CaseRecordList = illegalBuildingLandService.myWorkCaseRecord(currUser().getId(), status);
    	return new ResponseInfo<PageInfo<CaseRecord>>(ResultInfo.Success,CaseRecordList);
    }
	/**
	 * 根据当前流程处理步骤 查询下一步骤 遇到多个 需要在页面上做出选择
	 * @param stepId
	 * @return
	 */
	@ApiOperation(value = "根据当前流程步骤获取下一步骤列表", notes = "查看案件后，根据案件的当前流程查询下一步可能的步骤，在页面上把该列表作为分支，选择的单选框即代表流程的走向")
	@ApiImplicitParams({
	    @ApiImplicitParam(paramType = "path", name = "stepId", value = "流程步骤ID", required = true, dataType = "Integer")})
	@RequestMapping(value = "/getNextHandle", method = RequestMethod.POST,produces = "application/json")
    public ResponseInfo<List<WfDefinitionStep>> getNextHandle(@RequestParam(value = "stepId") int stepId){
		List<WfDefinitionStep> defStepList = illegalBuildingLandService.findNextSteps(new Long(stepId));
    	return new ResponseInfo<List<WfDefinitionStep>>(ResultInfo.Success,defStepList);
    }
    
	@ApiOperation(value = "查看案件", notes = "根据insId查看案件 在页面中需要向后台加载案件信息，当事人列表、附件列表，已经处理的流程列表，当前的状态是否已签收，是否处于办结状态 等等")
	@ApiImplicitParams({
	    @ApiImplicitParam(paramType = "path", name = "insId", value = "流程实例ID", required = true, dataType = "Integer")})
	@RequestMapping(value = "/viewCaseRecord", method = RequestMethod.POST,produces = "application/json")
    public ResponseInfo<CaseRecordBaseInfoVo> viewCaseRecord(@RequestParam(value = "insId") int insId){
		CaseRecordBaseInfoVo caseRecordVo = illegalBuildingLandService.viewCaseRecord(new Long(insId));
    	return new ResponseInfo<CaseRecordBaseInfoVo>(ResultInfo.Success,caseRecordVo);
    }
    
	@ApiOperation(value = "签收案件", notes = "根据insId查看案件后，未签收的案件 进行签收，签收后继续查看案件详情，可维持下一步操作")
	@ApiImplicitParams({
	    @ApiImplicitParam(paramType = "path", name = "insId", value = "流程实例ID", required = true, dataType = "Integer")})
	@RequestMapping(value = "/signCaseRecord", method = RequestMethod.POST,produces = "application/json")
    public ResponseInfo<CaseRecordBaseInfoVo> signCaseRecord(@RequestParam(value = "insId") int insId){
		illegalBuildingLandService.signProcess(currUser().getId(), new Long(insId));
		CaseRecordBaseInfoVo caseRecordVo = illegalBuildingLandService.viewCaseRecord(new Long(insId));
    	return new ResponseInfo<CaseRecordBaseInfoVo>(ResultInfo.Success,caseRecordVo);
    }
	
	@ApiOperation(value = "处理案件", notes = "根据insId查看案件后，未签收的案件 进行签收，签收后继续查看案件详情，可继续处理，可维持下一步操作")
	@RequestMapping(value = "/handleCaseRecord", method = RequestMethod.POST,produces = "application/json")
    public ResponseInfo<CaseRecordBaseInfoVo> handleCaseRecord(@RequestBody TaskDoInfo info){
		illegalBuildingLandService.handleProcess(currUser().getId(), info.getInsId(), info.getCurrStepId(), 
		    info.getNextStepId(), info.getProcessOpinion());
		CaseRecordBaseInfoVo caseRecordVo = illegalBuildingLandService.viewCaseRecord(info.getInsId());
    	return new ResponseInfo<CaseRecordBaseInfoVo>(ResultInfo.Success,caseRecordVo);
    }
	
	@ApiOperation(value = "两违流程办结", notes = "根据insId办结流程")
    @RequestMapping(value = "/finishCaseRecord", method = RequestMethod.POST, produces = "application/json")
    public ResponseInfo<CaseRecordBaseInfoVo> finishBuildApply( @RequestParam(value = "insId") int insId,String processOpinion ) {
		illegalBuildingLandService.finishProcess(currUser().getId(), new Long(insId), processOpinion,FlowConst.CASE_STATUS_FINISH);
		CaseRecordBaseInfoVo caseRecordVo = illegalBuildingLandService.viewCaseRecord(new Long(insId));
        return new ResponseInfo<CaseRecordBaseInfoVo>(ResultInfo.Success, caseRecordVo);
    }
	
	@ApiOperation(value = "两违流程销案", notes = "根据insId销案流程")
    @RequestMapping(value = "/cancelBuildApply", method = RequestMethod.POST, produces = "application/json")
    public ResponseInfo<CaseRecordBaseInfoVo> cancelBuildApply( @RequestParam(value = "insId") int insId,String processOpinion ) {
		illegalBuildingLandService.finishProcess(currUser().getId(), new Long(insId), processOpinion,FlowConst.CASE_STATUS_ABORTED);
		CaseRecordBaseInfoVo caseRecordVo = illegalBuildingLandService.viewCaseRecord(new Long(insId));
        return new ResponseInfo<CaseRecordBaseInfoVo>(ResultInfo.Success, caseRecordVo);
    }
    /*   
    @ApiOperation(value = "保存流程附件", notes = "提交任务,作者：lhy")
    @ApiImplicitParams({
        @ApiImplicitParam(paramType = "path", name = "caseId", value = "任务ID", required = true, dataType = "String")})    
    
    @RequestMapping(value = "/files/case/{caseId}", method = RequestMethod.POST)
    public ResponseInfo<List<UploadFileResultVo>> uploadCAase(@RequestParam("file") MultipartFile[] files) {
        List<UploadFileResultVo> result = uploadFiles(null, files);
        return new ResponseInfo<List<UploadFileResultVo>>(ResultInfo.Success, result);
    }  
    
    @ApiOperation(value = "保存流程附件", notes = "提交任务,作者：lhy")
    @ApiImplicitParams({
        @ApiImplicitParam(paramType = "path", name = "caseId", value = "任务ID", required = true, dataType = "String")})    
    
    @RequestMapping(value = "/files/task/{taskId}", method = RequestMethod.POST)
    public ResponseInfo<List<UploadFileResultVo>> uploadTask(@RequestParam("file") MultipartFile[] files) {
      //  List<UploadFileResultVo> result = uploadFiles(null, files);
        return new ResponseInfo<List<UploadFileResultVo>>(ResultInfo.Success, result);
    }    
    */

}
