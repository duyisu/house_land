/**
 * @filename:AreaController 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.Area;
import com.lhy.houseflow.service.AreaService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**   
 * 
 * @Description:  区域接口层
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
/*
@Api(tags={"区域"})
@RestController
@RequestMapping("/api/areas")
*/
@Slf4j
public class AreaController extends BaseController {
	
	@Autowired
	public AreaService areaServiceImpl;
	
	/**
	 * @explain 查询区域对象  <swagger GET请求>
	 * @param   对象参数：id
	 * @return  area
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "获取区域信息", notes = "获取区域信息[area]，作者：lhy")
	@ApiImplicitParam(paramType="path", name = "id", value = "区域id", required = true, dataType = "Long")
	@RequestMapping(value = "/{id}",method = RequestMethod.GET, produces = "application/json") 
	public ResponseInfo<Area> getAreaById(@PathVariable("id")Long id){
		Area result=areaServiceImpl.findById(id);
		return new ResponseInfo<Area>(ResultInfo.Success, result);
	}
	
	/**
	 * @explain 添加区域对象
	 * @param   对象参数：area
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "添加区域", notes = "添加区域[area],作者：lhy")
	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseInfo<Area> add(@RequestBody Area area){
		areaServiceImpl.add(area);
		return new ResponseInfo<Area>(ResultInfo.Success, area);
	}
	
	/**
	 * @explain 删除区域对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@RequestMapping(method = RequestMethod.DELETE, produces = "application/json")
	@ApiOperation(value = "删除区域", notes = "删除区域,作者：lhy")
	@ApiImplicitParam(paramType="query", name = "id", value = "区域id", required = true, dataType = "Long")
	public ResponseInfo<String> deleteById(Long id){
		areaServiceImpl.deleteById(id);
		return new ResponseInfo<String>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 修改区域对象
	 * @param   对象参数：area
	 * @return  area
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "修改区域", notes = "修改区域[area],作者：lhy")
	@RequestMapping(method = RequestMethod.PUT, produces = "application/json")
	public ResponseInfo<Area> update(@RequestBody Area area){
		areaServiceImpl.update(area);
		return new ResponseInfo<Area>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 获取匹配区域
	 * @param   对象参数：area
	 * @return  List<Area>
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "条件查询区域", notes = "条件查询[area],作者：lhy")
	@PostMapping("/query")
	public ResponseInfo<List<Area>> queryAreaList(@RequestBody Area area){
		List<Area> list = areaServiceImpl.queryAreaList(area);
		return new ResponseInfo<List<Area>>(ResultInfo.Success, list);	
	}
	
	/**
	 * @explain 分页条件查询区域   
	 * @param   对象参数：AppPage<Area>
	 * @return  PageDataVo<Area>
	 * @author  lhy
	 * @time    2019年5月5日
	 */

	@ApiOperation(value = "分页查询", notes = "分页查询返回对象[PageDataVo<Area>],作者：lhy")
	@ApiImplicitParams({
        @ApiImplicitParam(paramType="query", name = "page", value = "当前页", required = true, dataType = "int"),
        @ApiImplicitParam(paramType="query", name = "size", value = "页行数", required = true, dataType = "int")
    })
	@RequestMapping(value = "",method = RequestMethod.GET, produces = "application/json")
	public ResponseInfo<PageInfo<Area>> getAreaBySearch(
		@RequestParam(value="page",defaultValue ="1")  Integer page,
		@RequestParam(value="size",defaultValue ="100")  Integer size){
		AppPage<Area> apage =new AppPage<Area>();
		apage.setPageNum(page);
		apage.setPageSize(size);
		//其他参数
//		Area area=new Area();
//		page.setParam(area);
		//分页数据
		PageInfo<Area> pageInfo = areaServiceImpl.list(apage);
		return new ResponseInfo<PageInfo<Area>>(ResultInfo.Success, pageInfo);	
	}
}