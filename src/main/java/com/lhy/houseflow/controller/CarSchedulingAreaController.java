/**
 * @filename:CarSchedulingAreaController 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.CarSchedulingArea;
import com.lhy.houseflow.service.CarSchedulingAreaService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**   
 * 
 * @Description:  车辆排班接口层
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Api(tags={"车辆排班"})
@RestController
@RequestMapping("/api/car-scheduling-areas")
@Slf4j
public class CarSchedulingAreaController extends BaseController {
	
	@Autowired
	public CarSchedulingAreaService carSchedulingAreaServiceImpl;
	
	/**
	 * @explain 查询车辆排班对象  <swagger GET请求>
	 * @param   对象参数：id
	 * @return  carSchedulingArea
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "获取车辆排班信息", notes = "获取车辆排班信息[carSchedulingArea]，作者：lhy")
	@ApiImplicitParam(paramType="path", name = "id", value = "车辆排班id", required = true, dataType = "Long")
	@RequestMapping(value = "/{id}",method = RequestMethod.GET, produces = "application/json") 
	public ResponseInfo<CarSchedulingArea> getCarSchedulingAreaById(@PathVariable("id")Long id){
		CarSchedulingArea result=carSchedulingAreaServiceImpl.findById(id);
		return new ResponseInfo<CarSchedulingArea>(ResultInfo.Success, result);
	}
	
	/**
	 * @explain 添加车辆排班对象
	 * @param   对象参数：carSchedulingArea
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "添加车辆排班", notes = "添加车辆排班[carSchedulingArea],作者：lhy")
	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseInfo<CarSchedulingArea> add(@RequestBody CarSchedulingArea carSchedulingArea){
		carSchedulingAreaServiceImpl.add(carSchedulingArea);
		return new ResponseInfo<CarSchedulingArea>(ResultInfo.Success, carSchedulingArea);
	}
	
	/**
	 * @explain 删除车辆排班对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@RequestMapping(method = RequestMethod.DELETE, produces = "application/json")
	@ApiOperation(value = "删除车辆排班", notes = "删除车辆排班,作者：lhy")
	@ApiImplicitParam(paramType="query", name = "id", value = "车辆排班id", required = true, dataType = "Long")
	public ResponseInfo<String> deleteById(Long id){
		carSchedulingAreaServiceImpl.deleteById(id);
		return new ResponseInfo<String>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 修改车辆排班对象
	 * @param   对象参数：carSchedulingArea
	 * @return  carSchedulingArea
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "修改车辆排班", notes = "修改车辆排班[carSchedulingArea],作者：lhy")
	@RequestMapping(method = RequestMethod.PUT, produces = "application/json")
	public ResponseInfo<CarSchedulingArea> update(@RequestBody CarSchedulingArea carSchedulingArea){
		carSchedulingAreaServiceImpl.update(carSchedulingArea);
		return new ResponseInfo<CarSchedulingArea>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 获取匹配车辆排班
	 * @param   对象参数：carSchedulingArea
	 * @return  List<CarSchedulingArea>
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "条件查询车辆排班", notes = "条件查询[carSchedulingArea],作者：lhy")
	@PostMapping("/query")
	public ResponseInfo<List<CarSchedulingArea>> queryCarSchedulingAreaList(@RequestBody CarSchedulingArea carSchedulingArea){
		List<CarSchedulingArea> list = carSchedulingAreaServiceImpl.queryCarSchedulingAreaList(carSchedulingArea);
		return new ResponseInfo<List<CarSchedulingArea>>(ResultInfo.Success, list);	
	}
	
	/**
	 * @explain 分页条件查询车辆排班   
	 * @param   对象参数：AppPage<CarSchedulingArea>
	 * @return  PageDataVo<CarSchedulingArea>
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@GetMapping("/")
	@ApiOperation(value = "分页查询", notes = "分页查询返回对象[PageDataVo<CarSchedulingArea>],作者：lhy")
	@ApiImplicitParams({
        @ApiImplicitParam(paramType="query", name = "page", value = "当前页", required = true, dataType = "int"),
        @ApiImplicitParam(paramType="query", name = "size", value = "页行数", required = true, dataType = "int")
    })
	
	public ResponseInfo<PageInfo<CarSchedulingArea>> getCarSchedulingAreaBySearch(
		@RequestParam(value="page",defaultValue ="1")  Integer page,
		@RequestParam(value="size",defaultValue ="100")  Integer size){
		AppPage<CarSchedulingArea> aPage =new AppPage<CarSchedulingArea>();
		aPage.setPageNum(page);
		aPage.setPageSize(size);
		//其他参数
//		CarSchedulingArea carSchedulingArea=new CarSchedulingArea();
//		page.setParam(carSchedulingArea);
		//分页数据
		PageInfo<CarSchedulingArea> pageInfo = carSchedulingAreaServiceImpl.list(aPage);
		return new ResponseInfo<PageInfo<CarSchedulingArea>>(ResultInfo.Success, pageInfo);	
	}
}