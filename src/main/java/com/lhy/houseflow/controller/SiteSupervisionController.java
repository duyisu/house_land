/**
 * @filename:SiteSupervisionController 2019年9月4日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.SiteSupervision;
import com.lhy.houseflow.service.SiteSupervisionService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**   
 * 
 * @Description:  现场监督接口层
 * @Author:       lhy   
 * @CreateDate:   2019年9月4日
 * @Version:      V1.0
 *    
 */
@Api(tags={"现场监督"})
@RestController
@RequestMapping("/api/site-supervisions")
@Slf4j
public class SiteSupervisionController extends BaseController {
	
	@Autowired
	public SiteSupervisionService siteSupervisionServiceImpl;
	
	/**
	 * @explain 查询现场监督对象  <swagger GET请求>
	 * @param   对象参数：id
	 * @return  siteSupervision
	 * @author  lhy
	 * @time    2019年9月4日
	 */
	@ApiOperation(value = "获取现场监督信息", notes = "获取现场监督信息[siteSupervision]，作者：lhy")
	@ApiImplicitParam(paramType="path", name = "id", value = "现场监督id", required = true, dataType = "Long")
	@RequestMapping(value = "/{id}",method = RequestMethod.GET, produces = "application/json") 
	public ResponseInfo<SiteSupervision> getSiteSupervisionById(@PathVariable("id")Long id){
		SiteSupervision result=siteSupervisionServiceImpl.findById(id);
		return new ResponseInfo<SiteSupervision>(ResultInfo.Success, result);
	}
	
	/**
	 * @explain 添加现场监督对象
	 * @param   对象参数：siteSupervision
	 * @return  int
	 * @author  lhy
	 * @time    2019年9月4日
	 */
	@ApiOperation(value = "添加现场监督", notes = "添加现场监督[siteSupervision],作者：lhy")
	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseInfo<SiteSupervision> add(@RequestBody SiteSupervision siteSupervision){
		siteSupervisionServiceImpl.add(siteSupervision);
		return new ResponseInfo<SiteSupervision>(ResultInfo.Success, siteSupervision);
	}
	
	/**
	 * @explain 删除现场监督对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 * @time    2019年9月4日
	 */
	@RequestMapping(method = RequestMethod.DELETE, produces = "application/json")
	@ApiOperation(value = "删除现场监督", notes = "删除现场监督,作者：lhy")
	@ApiImplicitParam(paramType="query", name = "id", value = "现场监督id", required = true, dataType = "Long")
	public ResponseInfo<String> deleteById(Long id){
		siteSupervisionServiceImpl.deleteById(id);
		return new ResponseInfo<String>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 修改现场监督对象
	 * @param   对象参数：siteSupervision
	 * @return  siteSupervision
	 * @author  lhy
	 * @time    2019年9月4日
	 */
	@ApiOperation(value = "修改现场监督", notes = "修改现场监督[siteSupervision],作者：lhy")
	@RequestMapping(method = RequestMethod.PUT, produces = "application/json")
	public ResponseInfo<SiteSupervision> update(@RequestBody SiteSupervision siteSupervision){
		siteSupervisionServiceImpl.update(siteSupervision);
		return new ResponseInfo<SiteSupervision>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 获取匹配现场监督
	 * @param   对象参数：siteSupervision
	 * @return  List<SiteSupervision>
	 * @author  lhy
	 * @time    2019年9月4日
	 */
	@ApiOperation(value = "条件查询现场监督", notes = "条件查询[siteSupervision],作者：lhy")
	@PostMapping("/query")
	public ResponseInfo<List<SiteSupervision>> querySiteSupervisionList(@RequestBody SiteSupervision siteSupervision){
		List<SiteSupervision> list = siteSupervisionServiceImpl.querySiteSupervisionList(siteSupervision);
		return new ResponseInfo<List<SiteSupervision>>(ResultInfo.Success, list);	
	}
	
	/**
	 * @explain 分页条件查询现场监督   
	 * @param   对象参数：AppPage<SiteSupervision>
	 * @return  PageInfo<SiteSupervision>
	 * @author  lhy
	 * @time    2019年9月4日
	 */

	@ApiOperation(value = "分页查询", notes = "分页查询返回对象[PageInfo<SiteSupervision>],作者：lhy")
	@ApiImplicitParams({
        @ApiImplicitParam(paramType="query", name = "page", value = "当前页", required = true, dataType = "int"),
        @ApiImplicitParam(paramType="query", name = "size", value = "页行数", required = true, dataType = "int")
    })
	@RequestMapping(value = "",method = RequestMethod.GET, produces = "application/json")
	public ResponseInfo<PageInfo<SiteSupervision>> getSiteSupervisionBySearch(
		@RequestParam(value="page",defaultValue ="1")  Integer page,
		@RequestParam(value="size",defaultValue ="100")  Integer size){
		AppPage<SiteSupervision> aPage =new AppPage<SiteSupervision>();
		aPage.setPageNum(page);
		aPage.setPageSize(size);
		//其他参数
//		SiteSupervision siteSupervision=new SiteSupervision();
//		page.setParam(siteSupervision);
		//分页数据
		PageInfo<SiteSupervision> pageInfo = siteSupervisionServiceImpl.list(aPage);
		return new ResponseInfo<PageInfo<SiteSupervision>>(ResultInfo.Success, pageInfo);	
	}
}