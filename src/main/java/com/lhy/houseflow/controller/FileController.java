package com.lhy.houseflow.controller;

import java.util.ArrayList;
import java.util.List;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.dao.UploadFileDao;
import com.lhy.houseflow.entity.UploadFile;
import com.lhy.houseflow.entity.vo.FileInfoVo;
import com.lhy.houseflow.entity.vo.UploadFileResultVo;
import com.lhy.houseflow.service.FileService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @Description: 文件服务接口层
 * @Author: lhy
 * @CreateDate: 2019年8月5日
 * @Version: V1.0
 * 
 */
@Api(tags = {"文件服务"})
@RestController
@RequestMapping("/api/files")
@Slf4j
public class FileController extends BaseController {

    @Autowired
    private FileService fileService;
    @Autowired
    private UploadFileDao uploadFileDao;
    

    /**
     * 多文件上传
     * 
     * @return
     * @throws Exception
     */
    // @RequestMapping(value = "/upload", produces = "application/json;charset=UTF-8", method = RequestMethod.POST)
    @RequestMapping(value = "/upload", method = RequestMethod.POST)
    public ResponseInfo<List<UploadFileResultVo>> upload(@RequestParam("file") MultipartFile[] files) {
        List<UploadFileResultVo> result = uploadFiles(0, 0L, 0, files);
        return new ResponseInfo<List<UploadFileResultVo>>(ResultInfo.Success, result);
    }
    

    @ApiOperation(value = "附件上传", notes = "附件上传")
    @ApiImplicitParams({
        @ApiImplicitParam(paramType = "query", name = "kind", value = "附件类别(0:未分类;1:立卷立案;2:两违流程;3:两违任务;4:建房审批;5审批任务)", required = true, dataType = "int"),
        @ApiImplicitParam(paramType = "query", name = "id", value = "流程任务ID", required = true, dataType = "long"),
        @ApiImplicitParam(paramType = "query", name = "subkind", value = "附件分类", required = false, dataType = "int"),
        @ApiImplicitParam(paramType = "form", name = "file", value = "", required = true, dataType = "file[]")
        })    
    @RequestMapping(value = "/upload-flow", method = RequestMethod.POST)
    public ResponseInfo<List<UploadFileResultVo>> uploadIllegalTwoFlow(
        @RequestParam(value = "kind", required = true)Integer kind, 
        @RequestParam(value = "id", required = true)Long id, 
        @RequestParam(value = "subkind", required = true)Integer subkind, 
        @RequestParam("file") MultipartFile[] files) {
        List<UploadFileResultVo> result = uploadFiles(kind, id, subkind, files);
        return new ResponseInfo<List<UploadFileResultVo>>(ResultInfo.Success, result);
    }    

    @ApiOperation(value = "查询附件上传", notes = "查询附件上传")
    @ApiImplicitParams({
        @ApiImplicitParam(paramType = "query", name = "businessKind", value = "附件类别(0:未分类;1:立卷立案;2:两违流程;3:两违任务;4:建房审批;5审批任务)", required = true, dataType = "int"),
        @ApiImplicitParam(paramType = "query", name = "businessId", value = "流程任务ID", required = true, dataType = "long")
        })    
    @RequestMapping(value = "/upload-flow", method = RequestMethod.GET)
    public ResponseInfo<List<FileInfoVo>> getFileInfo(
        @RequestParam(value = "businessKind", required = true)Integer businessKind, 
        @RequestParam(value = "businessId", required = true)Long businessId) {
        List<FileInfoVo> result = uploadFileDao.findCaseRecordFileInfoByCaseId(businessId, businessKind);
        return new ResponseInfo<List<FileInfoVo>>(ResultInfo.Success, result);
    }

    /**
     * @explain 文件下载 get
     * @param 文件ID：id
     * @return ResponseInfo
     * @author lhy
     * @time 2019年8月5日
     */
    @ApiOperation(value = "文件下载", notes = "文件下载，作者：lhy")
    @ApiImplicitParam(paramType = "path", name = "id", value = "文件id", required = true, dataType = "Long")
    @RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = "application/json")
    public ResponseInfo<String> download(@PathVariable("id") Long id, HttpServletResponse response) {
        UploadFile fileInfo = uploadFileDao.findById(id);
        fileService.downLoadFile(fileInfo, response);

        return new ResponseInfo<String>(ResultInfo.Success);
    }
/*
 *     @ApiModelProperty(name = "businessId" , value = "业务ID")
    private Long businessId;
    @ApiModelProperty(name = "businessKind" , value = "文件所属类别(0:未分类;1:两违流程;2:两违任务;3:建房审批;4审批任务)")
    private Integer businessKind;
    @ApiModelProperty(name = "businessSubName" , value = "业务文件分类")
    private String businessSubName;

 */
    
    private List<UploadFileResultVo> uploadFiles(Integer businessKind, Long businessId, Integer businessSubKind, MultipartFile[] files) {

        List<UploadFileResultVo> list = new ArrayList<UploadFileResultVo>();
        if (files == null || files.length == 0) {
            log.info("uploadFiles file count = 0");
            return list;
        }
        log.info("uploadFiles file count:{}", files.length);
        for (MultipartFile file : files) {
            UploadFile info;
            try {
                info = fileService.fileUpload(businessKind, businessId, businessSubKind, file);
                list.add(createSuccess(info));
            } catch (Exception e) {
                list.add(createFailt(file, e.getMessage()));
                log.error(e.getMessage());
                e.printStackTrace();
            }
        }
        return list;
    }

    private UploadFileResultVo createSuccess(UploadFile info) {
        UploadFileResultVo result = new UploadFileResultVo();
        result.setId(info.getId());
        result.setFilename(info.getFilename());
        result.setSuccess(true);
        return result;
    }

    private UploadFileResultVo createFailt(MultipartFile file, String msg) {
        UploadFileResultVo result = new UploadFileResultVo();
        result.setId(null);
        result.setFilename(file.getOriginalFilename());
        result.setSuccess(false);
        result.setMsg(msg);
        return result;
    }
}
