package com.lhy.houseflow.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;

import com.lhy.houseflow.dao.SysPermissionDao;
import com.lhy.houseflow.dao.SysUserDao;
import com.lhy.houseflow.entity.JwtUser;

public class BaseController {

    @Autowired 
    protected SysPermissionDao permissionDao;
    
    @Autowired 
    protected SysUserDao userDao;
    
    public BaseController() {
        super();
    }
    
    protected JwtUser currUser(){
        Object obj = SecurityContextHolder.getContext().getAuthentication().getPrincipal();
        if (obj == null || (!(obj instanceof JwtUser))) return null; 
        return  (JwtUser)obj;
    }

}