/**
 * @filename:CaseDefendantController 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.bind.annotation.RequestParam;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.CaseDefendant;
import com.lhy.houseflow.service.CaseDefendantService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**   
 * 
 * @Description:  立案登记当事人接口层
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Api(tags={"立案登记当事人"})
@RestController
@RequestMapping("/api/case-defendants")
@Slf4j
public class CaseDefendantController extends BaseController {
	
	@Autowired
	public CaseDefendantService caseDefendantServiceImpl;
	
	/**
	 * @explain 查询立案登记当事人对象  <swagger GET请求>
	 * @param   对象参数：id
	 * @return  caseDefendant
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "获取立案登记当事人信息", notes = "获取立案登记当事人信息[caseDefendant]，作者：lhy")
	@ApiImplicitParam(paramType="path", name = "id", value = "立案登记当事人id", required = true, dataType = "Long")
	@RequestMapping(value = "/{id}",method = RequestMethod.GET, produces = "application/json") 
	public ResponseInfo<CaseDefendant> getCaseDefendantById(@PathVariable("id")Long id){
		CaseDefendant result=caseDefendantServiceImpl.findById(id);
		return new ResponseInfo<CaseDefendant>(ResultInfo.Success, result);
	}
	
	/**
	 * @explain 添加立案登记当事人对象
	 * @param   对象参数：caseDefendant
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "添加立案登记当事人", notes = "添加立案登记当事人[caseDefendant],作者：lhy")
	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseInfo<CaseDefendant> add(@RequestBody CaseDefendant caseDefendant){
		caseDefendantServiceImpl.add(caseDefendant);
		return new ResponseInfo<CaseDefendant>(ResultInfo.Success, caseDefendant);
	}
	
	/**
	 * @explain 删除立案登记当事人对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@RequestMapping(method = RequestMethod.DELETE, produces = "application/json")
	@ApiOperation(value = "删除立案登记当事人", notes = "删除立案登记当事人,作者：lhy")
	@ApiImplicitParam(paramType="query", name = "id", value = "立案登记当事人id", required = true, dataType = "Long")
	public ResponseInfo<String> deleteById(Long id){
		caseDefendantServiceImpl.deleteById(id);
		return new ResponseInfo<String>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 修改立案登记当事人对象
	 * @param   对象参数：caseDefendant
	 * @return  caseDefendant
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "修改立案登记当事人", notes = "修改立案登记当事人[caseDefendant],作者：lhy")
	@RequestMapping(method = RequestMethod.PUT, produces = "application/json")
	public ResponseInfo<CaseDefendant> update(@RequestBody CaseDefendant caseDefendant){
		caseDefendantServiceImpl.update(caseDefendant);
		return new ResponseInfo<CaseDefendant>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 获取匹配立案登记当事人
	 * @param   对象参数：caseDefendant
	 * @return  List<CaseDefendant>
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "条件查询立案登记当事人", notes = "条件查询[caseDefendant],作者：lhy")
	@PostMapping("/query")
	public ResponseInfo<List<CaseDefendant>> queryCaseDefendantList(@RequestBody CaseDefendant caseDefendant){
		List<CaseDefendant> list = caseDefendantServiceImpl.queryCaseDefendantList(caseDefendant);
		return new ResponseInfo<List<CaseDefendant>>(ResultInfo.Success, list);	
	}
	
	/**
	 * @explain 分页条件查询立案登记当事人   
	 * @param   对象参数：AppPage<CaseDefendant>
	 * @return  PageDataVo<CaseDefendant>
	 * @author  lhy
	 * @time    2019年5月5日
	 */

	@ApiOperation(value = "分页查询", notes = "分页查询返回对象[PageDataVo<CaseDefendant>],作者：lhy")
	@ApiImplicitParams({
        @ApiImplicitParam(paramType="query", name = "page", value = "当前页", required = true, dataType = "int"),
        @ApiImplicitParam(paramType="query", name = "size", value = "页行数", required = true, dataType = "int")
    })
	@RequestMapping(value = "",method = RequestMethod.GET, produces = "application/json")
	public ResponseInfo<PageInfo<CaseDefendant>> getCaseDefendantBySearch(
		@RequestParam(value="page",defaultValue ="1")  Integer page,
		@RequestParam(value="size",defaultValue ="100")  Integer size){
		AppPage<CaseDefendant> aPage =new AppPage<CaseDefendant>();
		aPage.setPageNum(page);
		aPage.setPageSize(size);
		//其他参数
//		CaseDefendant caseDefendant=new CaseDefendant();
//		page.setParam(caseDefendant);
		//分页数据
		PageInfo<CaseDefendant> pageInfo = caseDefendantServiceImpl.list(aPage);
		return new ResponseInfo<PageInfo<CaseDefendant>>(ResultInfo.Success, pageInfo);	
	}
}