/**
 * @filename:CarRouteController 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.CarRoute;
import com.lhy.houseflow.service.CarRouteService;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;

/**   
 * 
 * @Description:  车辆轨迹表接口层
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Api(tags={"车辆轨迹表"})
@RestController
@RequestMapping("/carRoutes")
@Slf4j
public class CarRouteController {
	
	@Autowired
	public CarRouteService carRouteServiceImpl;
	
	/**
	 * @explain 查询车辆轨迹表对象  <swagger GET请求>
	 * @param   对象参数：id
	 * @return  carRoute
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "获取车辆轨迹表信息", notes = "获取车辆轨迹表信息[carRoute]，作者：lhy")
	@ApiImplicitParam(paramType="path", name = "id", value = "车辆轨迹表id", required = true, dataType = "Long")
	@RequestMapping(value = "/{id}",method = RequestMethod.GET, produces = "application/json") 
	public ResponseInfo<CarRoute> getCarRouteById(@PathVariable("id")Long id){
		CarRoute result=carRouteServiceImpl.findById(id);
		return new ResponseInfo<CarRoute>(ResultInfo.Success, result);
	}
	
	/**
	 * @explain 添加车辆轨迹表对象
	 * @param   对象参数：carRoute
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "添加车辆轨迹表", notes = "添加车辆轨迹表[carRoute],作者：lhy")
	@RequestMapping(method = RequestMethod.POST, produces = "application/json")
	public ResponseInfo<CarRoute> add(@RequestBody CarRoute carRoute){
		carRouteServiceImpl.add(carRoute);
		return new ResponseInfo<CarRoute>(ResultInfo.Success, carRoute);
	}
	
	/**
	 * @explain 删除车辆轨迹表对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@RequestMapping(method = RequestMethod.DELETE, produces = "application/json")
	@ApiOperation(value = "删除车辆轨迹表", notes = "删除车辆轨迹表,作者：lhy")
	@ApiImplicitParam(paramType="query", name = "id", value = "车辆轨迹表id", required = true, dataType = "Long")
	public ResponseInfo<String> deleteById(Long id){
		carRouteServiceImpl.deleteById(id);
		return new ResponseInfo<String>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 修改车辆轨迹表对象
	 * @param   对象参数：carRoute
	 * @return  carRoute
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "修改车辆轨迹表", notes = "修改车辆轨迹表[carRoute],作者：lhy")
	@RequestMapping(method = RequestMethod.PUT, produces = "application/json")
	public ResponseInfo<CarRoute> update(@RequestBody CarRoute carRoute){
		carRouteServiceImpl.update(carRoute);
		return new ResponseInfo<CarRoute>(ResultInfo.Success, null);
	}
	
	/**
	 * @explain 获取匹配车辆轨迹表
	 * @param   对象参数：carRoute
	 * @return  List<CarRoute>
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@ApiOperation(value = "条件查询车辆轨迹表", notes = "条件查询[carRoute],作者：lhy")
	@PostMapping("/queryCarRouteList")
	public ResponseInfo<List<CarRoute>> queryCarRouteList(@RequestBody CarRoute carRoute){
		List<CarRoute> list = carRouteServiceImpl.queryCarRouteList(carRoute);
		return new ResponseInfo<List<CarRoute>>(ResultInfo.Success, list);	
	}
	
	/**
	 * @explain 分页条件查询车辆轨迹表   
	 * @param   对象参数：AppPage<CarRoute>
	 * @return  PageInfo<CarRoute>
	 * @author  lhy
	 * @time    2019年5月5日
	 */
	@GetMapping("/getPageCarRoute")
	@ApiOperation(value = "分页查询", notes = "分页查询返回对象[PageInfo<CarRoute>],作者：lhy")
	@ApiImplicitParams({
        @ApiImplicitParam(paramType="query", name = "pageNum", value = "当前页", required = true, dataType = "int"),
        @ApiImplicitParam(paramType="query", name = "pageSize", value = "页行数", required = true, dataType = "int")
    })
	public ResponseInfo<PageInfo<CarRoute>> getCarRouteBySearch(Integer pageNum,Integer pageSize){
		AppPage<CarRoute> page =new AppPage<CarRoute>();
		page.setPageNum(pageNum);
		page.setPageSize(pageSize);
		//其他参数
		CarRoute carRoute=new CarRoute();
		page.setParam(carRoute);
		//分页数据
		PageInfo<CarRoute> pageInfo = carRouteServiceImpl.getCarRouteBySearch(page);
		return new ResponseInfo<PageInfo<CarRoute>>(ResultInfo.Success, pageInfo);	
	}
}