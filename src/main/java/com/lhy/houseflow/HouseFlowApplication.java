package com.lhy.houseflow;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

import lombok.extern.slf4j.Slf4j;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

//@ComponentScan(basePackages = "com.wt.gjf.sentencecalc.repository")
//@EnableCaching
//@SpringBootApplication(exclude = {DataSourceAutoConfiguration.class})
@SpringBootApplication
@EnableSwagger2
@Slf4j
// @EnableCaching
//@ComponentScan(basePackages = {"com.lhy.houseflow"})
public class HouseFlowApplication {

	public static void main(String[] args) {
		SpringApplication.run(HouseFlowApplication.class, args);
	}
/*
	@Bean
	public CommandLineRunner init(final RepositoryService repositoryService, final RuntimeService runtimeService,
			final TaskService taskService) {

		return new CommandLineRunner() {
			@Override
			public void run(String... strings) throws Exception {
				log.info(
						"Number of process definitions : {}",repositoryService.createProcessDefinitionQuery().count());
				log.info("Number of tasks : {}", taskService.createTaskQuery().count());
				runtimeService.startProcessInstanceByKey("oneTaskProcess");
				log.info("Number of tasks after process start: {}", taskService.createTaskQuery().count());
			}
		};
	}
	*/
}
// */

/*
 * public class HouseFlowApplication extends SpringBootServletInitializer {
 * 
 * @Override protected SpringApplicationBuilder
 * configure(SpringApplicationBuilder application) { return
 * application.sources(HouseFlowApplication.class); }
 * 
 * public static void main(String[] args) {
 * SpringApplication.run(HouseFlowApplication.class, args); } }
 */
/*
 * @Component
 * 
 * @Order(1) class RunnerLoadCrimes implements CommandLineRunner {
 * 
 * @Override public void run(String... args) throws Exception {
 * System.out.println("The OrderRunner2 start to initialize ..."); } }
 * 
 * @Component
 * 
 * @Order(2) class RunnerLoadPlots implements CommandLineRunner {
 * 
 * @Override public void run(String... args) throws Exception {
 * System.out.println("The OrderRunner2 start to initialize ..."); } }
 */
