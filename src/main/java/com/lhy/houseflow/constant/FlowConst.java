package com.lhy.houseflow.constant;

public class FlowConst {
    private FlowConst(){
        
    }
    
    public static final String FLOW_OPR_TYPE ="oprType";
    public static final String FLOW_OPR_NAME ="oprName";
  //两违案件来源
    public static final String DICT_CASE_SOURCE ="caseSource";
    //两违案件类型
    public static final String DICT_CASE_TYPE ="caseType";
    //两违二级案件类型
    public static final String DICT_CASE_SUB_TYPE ="caseSubType";
    //两违违建性质
    public static final String DICT_CASE_BUILD_KIND ="caseBuildKind";
    //两违违建结构
    public static final String DICT_CASE_BUILD_STRUCT ="caseBuildStruct";
    
    public static int CASE_STATUS_DRAFT = 1; //案件状态 草稿
	public static int CASE_STATUS_PROCESSING = 2; //处理中
	public static int CASE_STATUS_PAUSE = 3; //挂起
	public static int CASE_STATUS_WITHDRAW = 4; //回退
	public static int CASE_STATUS_FINISH = 9;  //完结
	public static int CASE_STATUS_ABORTED = 99; //销案
	
	public static String MY_WORK_DRAFT = "draft";
	public static String MY_WORK_TODO = "wait";//todo
	public static String MY_WORK_HAS_DONE = "hasDone";
	
	public static String PROCESS_STATUS_NotOpen = "01"; //未签收
	public static String PROCESS_STATUS_Processing = "02"; //处理中
	public static String PROCESS_STATUS_Finished = "03"; //已完成
	public static String PROCESS_STATUS_Backward = "04"; 
	public static String PROCESS_STATUS_Abandoned = "99"; //废弃
	
	//PROCESSTYPE
	public static String PROCESS_TYPE_Process = "01"; //处理流程
	public static String PROCESS_TYPE_Urgency = "02"; //督办
	public static String PROCESS_TYPE_Forward = "03"; //查阅
	//StepType
	public static String STEP_TYPE_StartUpStepType = "01";	//启动步骤
	public static String STEP_TYPE_ProcessStepType = "02";	//中间步骤
	public static String STEP_TYPE_FinishStepType = "09";		//结束步骤
	public static String STEP_TYPE_FileStepType = "03";		//出具文件 不需要实际操作，可继续执行下一步
	
	//MODE
	public static String STEP_MODE_NoSignature = "01"; //无需签收
	public static String STEP_MODE_Signature = "02";	//需要签收
	
	public static String SHOW_TRUE = "True";
	public static String SHOW_FALSE = "False";
	
	public final static String INS_StartUp = "00"; //草稿
	public final static String INS_WaitingForSignature = "01"; //提交去下一步 等待下一步的人签收
	public final static String INS_Processing = "02";
	public final static String INS_HoldOn = "03";
	public final static String INS_Finished = "09";
	public final static String INS_Aborted = "99";
	
	public static String DEF_STEP_TYPE_START = "01";
	public static String DEF_STEP_TYPE_PROCESSING = "02";
	public static String DEF_STEP_TYPE_READ = "03";
	public static String DEF_STEP_TYPE_FINISH = "09";
	
	public static String FLOW_SEL_USER_MODE_ROLE_USER = "03"; //按角色选人
	
	public static Long DEF_ILLEGA_TWO = (long) 2;
	public static Long DEF_BUILD_APPLY = (long) 1;
	public static Long DEF_CHECK = (long) 3;
	
	

}
