package com.lhy.houseflow.flowable;

import org.flowable.idm.engine.IdmEngineConfiguration;

import lombok.extern.slf4j.Slf4j;


/*
@Configuration
@Import({IdmEngineServicesAutoConfiguration.class})  
@AutoConfigureAfter({ IdmEngineServicesAutoConfiguration.class, })
*/
@Slf4j

public class CustomIdmEngineConfiguration extends IdmEngineConfiguration {
    public CustomIdmEngineConfiguration() {
        idmIdentityService = new CustomIdentityServiceImpl();
    }

}
/*

public class CustomIdmEngineConfiguration extends SpringProcessEngineConfiguration{

 @Bean
 public IdmIdentityService idmIdentityService(){
     log.info("CustomIdmEngineConfiguration CustomIdentityServiceImpl");
     IdmEngineServicesAutoConfiguration c;
     return new CustomIdentityServiceImpl();
 }


}*/
