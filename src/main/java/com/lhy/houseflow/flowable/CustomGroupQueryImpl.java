package com.lhy.houseflow.flowable;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Predicate;

import org.flowable.common.engine.impl.interceptor.CommandContext;
import org.flowable.idm.api.Group;
import org.flowable.idm.engine.impl.GroupQueryImpl;
import org.flowable.idm.engine.impl.persistence.entity.GroupEntity;
import org.flowable.idm.engine.impl.persistence.entity.GroupEntityImpl;
import org.springframework.beans.factory.annotation.Autowired;

import com.lhy.houseflow.dao.SysPermissionDao;
import com.lhy.houseflow.entity.SysPermission;

public class CustomGroupQueryImpl extends GroupQueryImpl {
    /**
     * 
     */
    private static final long serialVersionUID = 5237409092407399175L;

    @Autowired
    protected SysPermissionDao sysPermissionDao;

    @Override
    public long executeCount(CommandContext commandContext) {
        return executeQuery().size();
    }

    @Override
    public List<Group> executeList(CommandContext commandContext) {
        return executeQuery();
    }

    // groupID 用SysPermission.ocde
    protected List<Group> executeQuery() {
        if (getUserId() != null) {
            return findGroupsByUser(getUserId());
        } else if (getUserIds() != null) {
            return findGroupsByUserIds(getUserIds());
        } else if (getId() != null) {
            return findGroupsById(getId());
        } else if (getIds() != null) {
            return findGroupsByIds(getIds());
        } else {
            return findAllGroups();
        }

    }

    private List<Group> findGroupsByIds(List<String> ids) {
        List<SysPermission> list = sysPermissionDao.list();
        list.removeIf(new Predicate<SysPermission>() {
            @Override
            public boolean test(SysPermission p) {                
                return !ids.contains(p.getCode());
            }
        });
        return getListBySysPermission(list);
    }

    private List<Group> findGroupsByUserIds(List<String> userIds) {
        // TODO Auto-generated method stub
        return null;
    }

    private List<Group> findAllGroups() {
        return getListBySysPermission(sysPermissionDao.list());
    }

    private List<Group> findGroupsById(String id) {
        SysPermission sysPermission = sysPermissionDao.findByCode(id);
        if (sysPermission == null)
            return null;
        List<SysPermission> list = new ArrayList<SysPermission>();
        list.add(sysPermission);
        return getListBySysPermission(list);
    }

    private List<Group> findGroupsByUser(String userId) {
        return getListBySysPermission(sysPermissionDao.findByUsername(userId));
    }

    private GroupEntity createBySysPermission(SysPermission sysPermission) {
        GroupEntity group = new GroupEntityImpl();
        group.setId(sysPermission.getCode());
        group.setName(sysPermission.getName());
//        group.setType(sysPermission.getPermissionMethod());

        return group;
    }

    private List<Group> getListBySysPermission(List<SysPermission> sysPermissions) {
        if (sysPermissions == null)
            return null;
        List<Group> list = new ArrayList<Group>();
        for (SysPermission sysPermission : sysPermissions) {
            list.add(createBySysPermission(sysPermission));
        }
        return list;
    }

}
