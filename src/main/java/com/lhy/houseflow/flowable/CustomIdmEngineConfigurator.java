package com.lhy.houseflow.flowable;

import org.flowable.common.engine.impl.AbstractEngineConfiguration;
import org.flowable.common.engine.impl.interceptor.EngineConfigurationConstants;
import org.flowable.idm.engine.configurator.IdmEngineConfigurator;

public class CustomIdmEngineConfigurator extends IdmEngineConfigurator {
    @Override
    public void beforeInit(AbstractEngineConfiguration engineConfiguration) {
        // Nothing to do
    }

    @Override
    public void configure(AbstractEngineConfiguration engineConfiguration) {
        
        this.idmEngineConfiguration = new CustomIdmEngineConfiguration();        
        super.configure(engineConfiguration);

    }
  

}
