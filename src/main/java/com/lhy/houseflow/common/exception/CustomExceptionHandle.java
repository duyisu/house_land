package com.lhy.houseflow.common.exception;

import org.apache.commons.lang3.StringUtils;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;

import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;

import lombok.extern.slf4j.Slf4j;

@ControllerAdvice
@Slf4j
public class CustomExceptionHandle<T> {
	@ExceptionHandler(value = Exception.class)
	@ResponseBody
	public ResponseInfo<T> handle(Exception e) {
		if (e instanceof CustomException) {
			CustomException exception = (CustomException) e;
			return createResponseInfo(exception.getResultInfo(), e.getMessage());
		} else {
//		    NoHandlerFoundException
			e.printStackTrace();
			log.error("error:{}", e.getMessage());
		}
		return createResponseInfo(ResultInfo.Unhandle_error, e.getMessage());
	}

	private ResponseInfo<T> createResponseInfo(final ResultInfo info, final String msg) {
		if (StringUtils.isBlank(msg)){
			return new ResponseInfo<T>(info.getCode(), info.getMessage(), null);
		}
		return new ResponseInfo<T>(info.getCode(), msg, null);
	}

}
