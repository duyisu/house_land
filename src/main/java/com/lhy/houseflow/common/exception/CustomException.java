package com.lhy.houseflow.common.exception;

import com.lhy.houseflow.common.model.ResultInfo;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class CustomException extends RuntimeException {

	private static final long serialVersionUID = 3997947665529198941L;
	private ResultInfo resultInfo;

	public ResultInfo getResultInfo() {
		return resultInfo;
	}

	public CustomException(ResultInfo resultInfo) {
		this.resultInfo = resultInfo;	
		log.error(resultInfo.getMessage());
	}
	
	public CustomException(ResultInfo resultInfo, String message) {		
		super(message);
		this.resultInfo = resultInfo;
		log.error(getMessage());
	}
}
