package com.lhy.houseflow.common.model.tree;

import java.util.List;


public class TreeNode <T extends NodeData>{
    private T data;
    private List<T> children;

    public List<T> getChildren() {
        return children;
    }

    public void setChildren(List<T> children) {
        this.children = children;
    }

    public T getData() {
        return data;
    }

    public void setData(T data) {
        this.data = data;
    }

    
}
