package com.lhy.houseflow.common.model.tree;

public class NodeData {
    private Long id;
    private String name;
    private  Long pid;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Long getPid() {
        return pid;
    }

    public void setPid(Long pid) {
        this.pid = pid;
    }

    public String getLable() {
        return name;
    }

    public void setLable(String lable) {
        this.name = lable;
    }
}
