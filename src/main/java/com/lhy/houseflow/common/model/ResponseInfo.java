package com.lhy.houseflow.common.model;

public class ResponseInfo<T> {
	private int code;
	private String msg;
	private T data;

	public ResponseInfo(int code, String msg, T data) {
		this.code = code;
		this.msg = msg;
		this.data = data;
	}

	public ResponseInfo(ResultInfo info, T data) {
		this(info.getCode(), info.getMessage(), data);
	}

	public ResponseInfo(ResultInfo info) {
		this(info.getCode(), info.getMessage(), null);
	}
	
	public ResponseInfo(T data) {
		this(ResultInfo.Success, data);
	}	


	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMsg() {
		return msg;
	}

	public void setMsg(String msg) {
		this.msg = msg;
	}

	public T getData() {
		return data;
	}

	public void setData(T data) {
		this.data = data;
	}

}
