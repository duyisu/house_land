package com.lhy.houseflow.common.model;

public enum ResultInfo {

	Success(0, "Success"), 
	Auth_User_Not_Exist(1000, "用户不存在"),
	Auth_Login_Erorr(1001, "用户密码认证失败"), 
	Auth_Token_Issued_Erorr(1002, "秘钥签发失败"), 
	Auth_JWT_Issued_Erorr(1003, "JWT 签发失败"),
	Auth_JWT_Expired(1004, "JWT token 过期"),
	Auth_JWT_Refresh_Expired(1005, "jwt_refresh_token过期"),
	Auth_JWT_Error(1006, "JWT 认证失败"),
	Auth_JWT_No_Permission(1007, "无权限访问此资源"),	
	Auth_UNAUTHORIZED(1401, "未授权没有登陆"),
	Auth_Forbidden(1403, "无权限访问此资源"),
	
	Task_id_not_exist(2001, "没有流程任务"),
	

	Unhandle_error(9700, "服务器内部错误"), 
	DataBase_error(9800, "数据库错误"), 
	Network_error(9900,	"网络错误"),
	Excel_Workbook_NotExist(8000,"Excel 不存在"),
	Excel_Sheet_NotExist(8001,"Excel Sheet 不存在"),
	EXCEL_LAYOUT_ERROR(8002,"Excel 格式不正确"), 
	
	UPLOAD_EMPTY_FILE(4001,"上传失败，未选择文件"),
	UPLOAD_FAIL_EXCEPTION(4002,"上传失败");
/*
 * //认证授权相关  
1000: 动态秘钥签发成功----issued tokenKey success
1001：动态秘钥签发失败----issued tokenKey fail
1002：用户密码认证失败----login fail
1003: 用户密码认证成功,JWT 签发成功,返回jwt-----issued jwt success
1004：JWT 签发失败----issued jwt fail
1005: jwt_real_token过期,jwt_refresh_token还未过期,服务器返回新的jwt,客户端需携带新返回的jwt对这次请求重新发起----new jwt  
1006: jwt_real_token过期,jwt_refresh_token过期(通知客户端重新登录)-----expired jwt
1007: jwt_token认证失败无效(包括用户认证失败或者jwt令牌错误无效或者用户未登录)----error jwt
1008: jwt token无权限访问此资源----no permission
...  
  
// 用户相关  
2001: 注册账户信息不完善----lack account info  
2002: 注册用户成功----register success
2003: 用户不存在  
  
// 业务1  
3001: 业务1XXX  
3002: 业务1XXX  
 */

	private int code;

	public int getCode() {
		return code;
	}

	public void setCode(int code) {
		this.code = code;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

	private String message;

	ResultInfo(int code, String message) {
		this.code = code;
		this.message = message;
	}

}
