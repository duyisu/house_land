package com.lhy.houseflow.common.model;

public class AppPage<T> {

	public AppPage() {
		// TODO Auto-generated constructor stub
	}
	
	private int page;
	private int size;
	private T param;

	public int getPageNum() {
		return page;
	}

	public void setPageNum(int pageNum) {
		this.page = pageNum;
	}

	public int getPageSize() {
		return size;
	}

	public void setPageSize(int pageSize) {
		this.size = pageSize;
	}

	public T getParam() {
		return param;
	}

	public void setParam(T param) {
		this.param = param;
	}
	

}
