/**
 * @filename:Menu 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

/**   
 *  
 * @Description:  菜单资源表
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Menu implements Serializable {

	private static final long serialVersionUID = 1565314588347L;
	
	@ApiModelProperty(name = "id" , value = "id")
	private Long id;
	@ApiModelProperty(name = "menuName" , value = "菜单名称")
	private String menuName;
	@ApiModelProperty(name = "menuDesc" , value = "菜单描述")
	private String menuDesc;
	@ApiModelProperty(name = "menuUrl" , value = "菜单地址")
	private String menuUrl;
	@ApiModelProperty(name = "menuAction" , value = "菜单动作")
	private String menuAction;
	@ApiModelProperty(name = "menuOrder" , value = "菜单序号")
	private Integer menuOrder;
	@ApiModelProperty(name = "parentId" , value = "上级菜单")
	private Long parentId;
	@ApiModelProperty(name = "enebled" , value = "是否有效（1：有效0：无效）")
	private Integer enebled;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@ApiModelProperty(name = "gmtCreate" , value = "创建时间")
	private Date gmtCreate;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@ApiModelProperty(name = "gmtModified" , value = "修改时间")
	private Date gmtModified;
}
