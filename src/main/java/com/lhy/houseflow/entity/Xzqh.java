/**
 * @filename:Xzqh 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

/**   
 *  
 * @Description:  地区
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Xzqh implements Serializable {

	private static final long serialVersionUID = 1567291919412L;
	
	@ApiModelProperty(name = "id" , value = "")
	private Integer id;
	@ApiModelProperty(name = "province" , value = "省")
	private String province;
	@ApiModelProperty(name = "city" , value = "市")
	private String city;
	@ApiModelProperty(name = "district" , value = "区 县")
	private String district;
	@ApiModelProperty(name = "town" , value = "镇")
	private String town;
	@ApiModelProperty(name = "village" , value = "村")
	private String village;
	@ApiModelProperty(name = "xzqhCode" , value = "行政区划编码")
	private String xzqhCode;
}
