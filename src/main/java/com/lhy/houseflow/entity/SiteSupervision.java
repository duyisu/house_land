/**
 * @filename:SiteSupervision 2019年9月4日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

/**   
 *  
 * @Description:  现场监督
 * @Author:       lhy   
 * @CreateDate:   2019年9月4日
 * @Version:      V1.0
 *    
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SiteSupervision implements Serializable {

	private static final long serialVersionUID = 1567562981214L;
	
	@ApiModelProperty(name = "id" , value = "id")
	private Long id;
	@ApiModelProperty(name = "applyId" , value = "申请ID")
	private Long applyId;
	@ApiModelProperty(name = "insid" , value = "流程实例ID")
	private Long insid;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@ApiModelProperty(name = "gmtCreate" , value = "创建时间")
	private Date gmtCreate;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@ApiModelProperty(name = "gmtModified" , value = "修改时间")
	private Date gmtModified;
}
