/**
 * @filename:WfDefinitionSteprela 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

/**   
 *  
 * @Description:  步骤关系
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WfDefinitionSteprela implements Serializable {

	private static final long serialVersionUID = 1567273611858L;
	
	@ApiModelProperty(name = "priorstepId" , value = "")
	private Long priorstepId;
	@ApiModelProperty(name = "nextstepId" , value = "")
	private Long nextstepId;
	@ApiModelProperty(name = "steprelaId" , value = "")
	private Long steprelaId;
	@ApiModelProperty(name = "version" , value = "")
	private Integer version;
	@ApiModelProperty(name = "tips" , value = "")
	private String tips;
}
