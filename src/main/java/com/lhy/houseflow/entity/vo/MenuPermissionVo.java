package com.lhy.houseflow.entity.vo;

import java.util.List;

import com.lhy.houseflow.entity.SysPermission;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;
@Data
@EqualsAndHashCode(callSuper=false)
@AllArgsConstructor
@NoArgsConstructor
public class MenuPermissionVo extends MenuVo {
    /**
     * 
     */
    private static final long serialVersionUID = 7359457670422950580L;
    private List<SysPermission> permissions;

}
