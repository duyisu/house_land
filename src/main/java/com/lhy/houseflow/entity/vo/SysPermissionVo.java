package com.lhy.houseflow.entity.vo;

import java.util.Date;

import com.lhy.houseflow.entity.SysPermission;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysPermissionVo {
    @ApiModelProperty(name = "id" , value = "id")
    private Long id;
    @ApiModelProperty(name = "code" , value = "权限代码")
    private String code;
    @ApiModelProperty(name = "name" , value = "权限名称")
    private String name;
    @ApiModelProperty(name = "menuId" , value = "菜单ID")
    private Long menuId;
    @ApiModelProperty(name = "sort" , value = "序号")
    private Integer sort;
    @ApiModelProperty(name = "enabled" , value = "是否有效（1：有效0：无效）")
    private Integer enabled;
    @ApiModelProperty(name = "menu" , value = "所属菜单")
    private MenuVo menu;
}
