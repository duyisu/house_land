package com.lhy.houseflow.entity.vo;

import java.util.Date;

import org.flowable.task.api.Task;

import com.lhy.houseflow.entity.TaskUserInfo;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;
@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class TaskVo<T> {
    private T data;
    private Task task;
    private String name;
    private String id;
    private String assignee;
    private Date dueDate;
    private Date claimTime;
    private Date createTime;
    
    private TaskUserInfo nextTaskUserInfo;
    public TaskVo(Task task){
        this.task = task;
    }
}
