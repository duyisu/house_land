package com.lhy.houseflow.entity.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class NameValue {
    @ApiModelProperty(name = "id" , value = "id")
    private Long id;    
    @ApiModelProperty(name = "name" , value = "标签名称")
    private String name;
    @ApiModelProperty(name = "value" , value = "值")
    private Integer value;
}
