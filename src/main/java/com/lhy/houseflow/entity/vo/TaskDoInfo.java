package com.lhy.houseflow.entity.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TaskDoInfo {
    @ApiModelProperty(name = "insId" , value = "流程实例ID")
    private Long insId;
    @ApiModelProperty(name = "currStepId" , value = "当前处理流程ID")
    private Long currStepId;
    @ApiModelProperty(name = "nextStepId" , value = "下一步处理流程ID")
    private Long nextStepId;
    @ApiModelProperty(name = "processOpinion" , value = "处理意见")
    private String processOpinion;
    @ApiModelProperty(name = "processOpinionDesc" , value = "处理意见描述")
    private String processOpinionDesc;
}

