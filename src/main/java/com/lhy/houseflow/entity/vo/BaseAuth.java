package com.lhy.houseflow.entity.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BaseAuth implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 4080986761964200051L;
	private String userName;
	private String password;

}
