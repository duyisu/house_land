package com.lhy.houseflow.entity.vo;

import java.io.Serializable;
import java.math.BigDecimal;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


@Data
@AllArgsConstructor
@NoArgsConstructor
public class CaseRecordFlowTaskVo  implements Serializable {

    private static final long serialVersionUID = -5925309997666625121L;
    @ApiModelProperty(name = "oprMethod" , value = "处理措施")
    private String oprMethod;
    @ApiModelProperty(name = "oprType" , value = "处理意见")
    private String oprType;
    @ApiModelProperty(name = "oprDesc" , value = "处理意见描述")
    private String oprDesc;
    @ApiModelProperty(name = "selfArea" , value = "自行拆除面积")
    private BigDecimal selfArea;
    @ApiModelProperty(name = "otherArea" , value = "协助拆除面积面积")
    private BigDecimal otherArea;
    @ApiModelProperty(name = "newArea" , value = "即建即拆面积")  
    private BigDecimal newArea;    
    
    @ApiModelProperty(name = "taskId" , value = "任务ID")
    private String taskId;    
    @ApiModelProperty(name = "toAssignee" , value = "下一任务执行人")
    private String toAssignee;    

}
