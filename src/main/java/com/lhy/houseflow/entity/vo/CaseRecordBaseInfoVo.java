package com.lhy.houseflow.entity.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CaseRecordBaseInfoVo implements Serializable {

    private static final long serialVersionUID = 8918737910648059943L;
    @ApiModelProperty(name = "id" , value = "id")
    private Long id;
    @ApiModelProperty(name = "position" , value = "位置")
    private String position;
    @ApiModelProperty(name = "longitude" , value = "经度")
    private BigDecimal longitude;
    @ApiModelProperty(name = "latitude" , value = "纬度")
    private BigDecimal latitude;
    @ApiModelProperty(name = "purposes" , value = "用途")
    private String purposes;
    @ApiModelProperty(name = "area" , value = "占地面积")
    private BigDecimal area;
    @ApiModelProperty(name = "realArea" , value = "建筑占地面积")
    private BigDecimal realArea;
    @ApiModelProperty(name = "buildKind" , value = "违建性质")
    private Integer buildKind;
    @ApiModelProperty(name = "buildStruct" , value = "建筑结构")
    private Integer buildStruct;
    @ApiModelProperty(name = "caseNo" , value = "案件编号")
    private String caseNo;
    @ApiModelProperty(name = "caseRecordNo" , value = "立案编号")
    private String caseRecordNo;
    @ApiModelProperty(name = "caseDesc" , value = "案件描述")
    private String caseDesc;
    @ApiModelProperty(name = "processInstanceId" , value = "流程实例ID")
    private Long processInstanceId;
    @ApiModelProperty(name = "source" , value = "来源")
    private NameValue source;
    @ApiModelProperty(name = "caseType" , value = "两违类型")
    private NameValue caseType;
    @ApiModelProperty(name = "caseSubtype" , value = "二级类型")
    private NameValue caseSubtype;   
    @ApiModelProperty(name = "reportUserId" , value = "上报人")
    private String reportUserId;
    @ApiModelProperty(name = "reportName" , value = "上报人姓名")
    private String reportName;
    @ApiModelProperty(name = "reportTel" , value = "上报电话")
    private String reportTel;
    @ApiModelProperty(name = "complainant" , value = "投诉人")
    private String complainant;
    @ApiModelProperty(name = "complainantTel" , value = "投诉电话") 
    private String complainantTel;
    @ApiModelProperty(name = "areaId" , value = "镇区ID")
    private Long areaId;
    @ApiModelProperty(name = "address" , value = "地址")
    private String address;
    @ApiModelProperty(name = "building" , value = "建筑信息")
    private CaseRecordBuildingVo building;    
    @ApiModelProperty(name = "defendants" , value = "当事人") 
    private List<CaseDefendantVo> defendants;
    
    @ApiModelProperty(name = "currTaskName" , value = "当前状态")
    private String currTaskName; 
    
    @ApiModelProperty(name = "files" , value = "附件") 
    private List<FileInfoVo> files;
    @ApiModelProperty(name = "hasFinishProcessList" , value = "处理过程列表") 
    private List<FlowProcessVo> hasFinishProcessVoList;
    @ApiModelProperty(name = "curProcess" , value = "当前处理环节") 
    private FlowProcessVo curProcess;
    
    @ApiModelProperty(name = "isShowSignBtn" , value = "是否显示签收按钮 True或False") 
    private String isShowSignBtn;
    @ApiModelProperty(name = "isShowHandleBtn" , value = "是否显示处理用的提交按钮 True或False") 
    private String isShowHandleBtn;
    @ApiModelProperty(name = "isShowFinishBtn" , value = "是否显示办结按钮 True或False") 
    private String isShowFinishBtn;
    


}