package com.lhy.houseflow.entity.vo;

import java.util.ArrayList;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class MenuVo {
    @ApiModelProperty(name = "id" , value = "id")
    private Long id;
    @ApiModelProperty(name = "component" , value = "菜单组件")
    private String component;
    @ApiModelProperty(name = "name" , value = "菜单名称")
    private String name;
    @ApiModelProperty(name = "path" , value = "菜单路径")
    private String path;
    @ApiModelProperty(name = "pid" , value = "上级菜单")
    private Long pid;
    @ApiModelProperty(name = "ico" , value = "菜单图标")
    private String ico;
    @ApiModelProperty(name = "sort" , value = "菜单序号")
    private Integer sort;
    @ApiModelProperty(name = "type" , value = "类型(0:菜单1：功能)")
    private Integer type;
    @ApiModelProperty(name = "iframe" , value = "外链标志（1：内部0：外链）")
    private Integer iframe;
    @ApiModelProperty(name = "children", value = "下级菜单")
    private List<MenuVo> children = new ArrayList<MenuVo>();
    
    public boolean isRoot(){
        return pid == null || pid.equals(0L);
    }
}
