package com.lhy.houseflow.entity.vo;

import java.util.ArrayList;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class DepartmentVo {
    
    @ApiModelProperty(name = "id" , value = "id")
    private Long id;
    @ApiModelProperty(name = "name" , value = "部门名称")
    private String name;
    @ApiModelProperty(name = "departDesc" , value = "部门描述")
    private String departDesc;
    @ApiModelProperty(name = "pid" , value = "上级部门")
    private Long pid;
    @ApiModelProperty(name = "enabled" , value = "是否有效（1：有效0：无效）")
    private Integer enabled;
    
    @ApiModelProperty(name = "children", value = "下级菜单")
    private List<DepartmentVo> children = new ArrayList<DepartmentVo>();
    
    public boolean isRoot(){
        return pid == null || pid.equals(0L);
    }   
    
    public String getLable(){
        return name;
    }
}
