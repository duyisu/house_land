package com.lhy.houseflow.entity.vo;

import java.util.List;

import com.lhy.houseflow.entity.CaseDefendant;
import com.lhy.houseflow.entity.CaseRecord;
import com.lhy.houseflow.entity.CaseRecordFile;

public class CaseRecordVo {
	private CaseRecord caseRecord;
	private List<CaseDefendant> caseDefendantList;
	private List<CaseRecordFile> caseRecordFileList;
	
	public CaseRecord getCaseRecord() {
		return caseRecord;
	}
	public void setCaseRecord(CaseRecord caseRecord) {
		this.caseRecord = caseRecord;
	}
	public List<CaseDefendant> getCaseDefendantList() {
		return caseDefendantList;
	}
	public void setCaseDefendantList(List<CaseDefendant> caseDefendantList) {
		this.caseDefendantList = caseDefendantList;
	}
	public List<CaseRecordFile> getCaseRecordFileList() {
		return caseRecordFileList;
	}
	public void setCaseRecordFileList(List<CaseRecordFile> caseRecordFileList) {
		this.caseRecordFileList = caseRecordFileList;
	}
	
	
}
