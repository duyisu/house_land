package com.lhy.houseflow.entity.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysUserVo {
    
    @ApiModelProperty(name = "id" , value = "id")
    private Long id;
    @ApiModelProperty(name = "userName" , value = "用户名称")
    private String userName;
    @ApiModelProperty(name = "departId" , value = "部门ID")
    private Long departId;
    @ApiModelProperty(name = "dept" , value = "部门ID")
    private DepartmentVo dept;    
    @ApiModelProperty(name = "realName" , value = "真实姓名")
    private String realName;
    @ApiModelProperty(name = "employeeNo" , value = "工号")
    private String employeeNo;
    @ApiModelProperty(name = "userTel" , value = "手机号码")
    private String userTel;
    @ApiModelProperty(name = "idCardNo" , value = "身份证号码")
    private String idCardNo;
    @ApiModelProperty(name = "userSex" , value = "性别")
    private Integer userSex;
    @ApiModelProperty(name = "userPassword" , value = "用户密码")
    private String userPassword;
    @ApiModelProperty(name = "salt" , value = "salt")
    private String salt;
    @ApiModelProperty(name = "userDesc" , value = "备注")
    private String userDesc;
    @ApiModelProperty(name = "enabled" , value = "是否有效（1：启用 0：停用）")
    private Integer enabled;

}
