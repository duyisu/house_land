package com.lhy.houseflow.entity.vo;

import java.util.HashMap;
import java.util.Map;

public class NameValueMap {
    
    private Map<Integer, NameValue> map = new HashMap<Integer, NameValue>();
    public void put(NameValue item){
        map.put(item.getValue(), item);
    }
    
    public NameValue get(Integer value){
        if (value == null) return null;
        return map.get(value);
    }

}
