package com.lhy.houseflow.entity.vo;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class UploadFileResultVo implements Serializable {

    private static final long serialVersionUID = -878357190073281624L;
    @ApiModelProperty(name = "id" , value = "id")
    private Long id;
    @ApiModelProperty(name = "filename" , value = "文件名称")
    private String filename;
    
    @ApiModelProperty(name = "msg" , value = "错误信息")
    private String msg;    
    @ApiModelProperty(name = "success" , value = "上传成功")
    private Boolean success;      
}
