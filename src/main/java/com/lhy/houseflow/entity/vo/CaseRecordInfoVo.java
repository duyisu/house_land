package com.lhy.houseflow.entity.vo;

import java.io.Serializable;
import java.util.List;

import com.lhy.houseflow.entity.UploadFile;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CaseRecordInfoVo extends CaseRecordBaseInfoVo implements Serializable {

    private static final long serialVersionUID = -1613933261608922972L;

    @ApiModelProperty(name = "files" , value = "附件")
    private List<UploadFile> uploadFiles;


}