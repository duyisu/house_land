package com.lhy.houseflow.entity.vo;

import java.io.Serializable;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class FlowProcessVo implements Serializable{/**
	 * 
	 */
	private static final long serialVersionUID = 1L;
	 @ApiModelProperty(name = "insId" , value = "流程实例id")
     private Long insId;
	 @ApiModelProperty(name = "stepId" , value = "流程步骤id")
	 private Long stepId;
	 @ApiModelProperty(name = "stepName" , value = "流程步骤名称")
	 private String stepName;
	 @ApiModelProperty(name = "fromUserId" , value = "派发任务人员id")
	 private Long fromUserId;
	 @ApiModelProperty(name = "fromUserName" , value = "派发任务人员名称")
	 private String fromUserName;
	 @ApiModelProperty(name = "toUserId" , value = "接收任务人员id")
	 private Long toUserId;
	 @ApiModelProperty(name = "toUserName" , value = "接收任务人员姓名")
	 private String toUserName;
	 @ApiModelProperty(name = "fromTimeStr" , value = "派发任务时间")
	 private String fromTimeStr;
	 @ApiModelProperty(name = "toTimeStr" , value = "签收任务时间")
	 private String toTimeStr;
	 @ApiModelProperty(name = "finishTimeStr" , value = "完成任务时间")
	 private String finishTimeStr;
	 @ApiModelProperty(name = "processStatus" , value = "该流程步骤状态")
	 private String processStatus;
	 @ApiModelProperty(name = "processOpinion" , value = "该流程步骤意见")
	 private String processOpinion;
	 @ApiModelProperty(name = "processFileList" , value = "该流程上传附件")
	 private List<FileInfoVo> processFileList;
	
	 
	
}
