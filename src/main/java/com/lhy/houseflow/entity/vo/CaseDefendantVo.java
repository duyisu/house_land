package com.lhy.houseflow.entity.vo;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CaseDefendantVo implements Serializable {

    private static final long serialVersionUID = 7043320926516668021L;
    @ApiModelProperty(name = "id" , value = "id")
    private Long id;
    @ApiModelProperty(name = "defendantName" , value = "被投诉人")
    private String defendantName;
    @ApiModelProperty(name = "defendantTel" , value = "被投诉人电话")
    private String defendantTel;
    @ApiModelProperty(name = "defendantIscardno" , value = "被投诉人身份证号码")
    private String defendantIscardno;
}
