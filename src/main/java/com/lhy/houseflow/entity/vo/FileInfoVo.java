package com.lhy.houseflow.entity.vo;

import java.io.Serializable;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class FileInfoVo implements Serializable {
        
        /**
     * 
     */
    private static final long serialVersionUID = -3341769356528583841L;
    
    @ApiModelProperty(name = "id" , value = "id")
    private Long id;
    @ApiModelProperty(name = "filename" , value = "文件名称")
    private String filename;
    @ApiModelProperty(name = "businessSubKind" , value = "业务文件分类")
    private Integer businessSubKind;     
    }