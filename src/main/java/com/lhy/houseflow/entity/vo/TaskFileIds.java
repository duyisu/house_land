package com.lhy.houseflow.entity.vo;

import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TaskFileIds {

    @ApiModelProperty(name = "id", value = "业务id")
    private Long id;

    @ApiModelProperty(name = "fileIds", value = "文件ID列表")
    private List<Long> fileIds;
    
    @ApiModelProperty(name = "isFlowFile", value = "是否流程文件")
    private boolean isFlowFile;    

}
