package com.lhy.houseflow.entity.vo;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class TaskUser implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = -7421922761396314108L;
    private String taskId;
    private String userName;

}
