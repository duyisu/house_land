package com.lhy.houseflow.entity.vo;

import java.io.Serializable;
import java.math.BigDecimal;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CaseRecordBuildingVo implements Serializable {

    private static final long serialVersionUID = 1542106993057172001L;
    @ApiModelProperty(name = "id" , value = "id")
    private Long id;
    @ApiModelProperty(name = "caseType" , value = "两违类型")
    private Integer caseType;
    @ApiModelProperty(name = "caseSubtype" , value = "二级类型")
    private Integer caseSubtype;
    @ApiModelProperty(name = "areaId" , value = "镇区ID")
    private Long areaId;
    @ApiModelProperty(name = "position" , value = "位置")
    private String position;
    @ApiModelProperty(name = "longitude" , value = "经度")
    private BigDecimal longitude;
    @ApiModelProperty(name = "latitude" , value = "纬度")
    private BigDecimal latitude;
    @ApiModelProperty(name = "address" , value = "地址")
    private String address;
    @ApiModelProperty(name = "purposes" , value = "用途")
    private String purposes;
    @ApiModelProperty(name = "area" , value = "占地面积")
    private BigDecimal area;
    @ApiModelProperty(name = "realArea" , value = "建筑占地面积")
    private BigDecimal realArea;
    @ApiModelProperty(name = "buildKind" , value = "违建性质")
    private Integer buildKind;
    @ApiModelProperty(name = "buildStruct" , value = "建筑结构")
    private Integer buildStruct;

}
