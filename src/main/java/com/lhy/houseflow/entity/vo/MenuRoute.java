package com.lhy.houseflow.entity.vo;

import java.io.Serializable;
import java.util.List;

import lombok.Data;

@Data
public class MenuRoute  implements Serializable {

        private String name;

        private String path;

        private String redirect;

        private String component;

        private Boolean alwaysShow;

        private MenuMetaVo meta;

        private List<MenuRoute> children;
    }