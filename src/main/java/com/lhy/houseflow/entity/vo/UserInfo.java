package com.lhy.houseflow.entity.vo;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collection;
import java.util.stream.Collectors;

import org.springframework.security.core.GrantedAuthority;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserInfo implements Serializable {

    private static final long serialVersionUID = -5557084795646426584L;
    @ApiModelProperty(name = "id" , value = "id")
    private Long id;
    @ApiModelProperty(name = "userName" , value = "用户名称")
    private String userName;
    @ApiModelProperty(name = "departId" , value = "部门")
    private Long departId;
    @ApiModelProperty(name = "realName" , value = "真实姓名")
    private String realName;
    @ApiModelProperty(name = "employeeNo" , value = "工号")
    private String employeeNo;
    @ApiModelProperty(name = "userTel" , value = "手机号码")
    private String userTel;
    @ApiModelProperty(name = "idCardNo" , value = "身份证号码")
    private String idCardNo;
    @ApiModelProperty(name = "userSex" , value = "性别")
    private Integer userSex;
    @ApiModelProperty(name = "userDesc" , value = "备注")
    private String userDesc;
    @ApiModelProperty(name = "enabled" , value = "是否有效（1：启用 0：停用）")
    private Integer enabled;
    @ApiModelProperty(name = "token" , value = "token")
    private String token;
    private Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority> ();
    
    public Collection getRoles() {
        return authorities.stream().map(GrantedAuthority::getAuthority).collect(Collectors.toSet());
    }
}
