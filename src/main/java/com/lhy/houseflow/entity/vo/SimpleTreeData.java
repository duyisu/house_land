package com.lhy.houseflow.entity.vo;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class SimpleTreeData {
    private Long id;
    private String label;
    private List<SimpleTreeData> children;

}
