package com.lhy.houseflow.entity.vo;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class PeopleVo {
    @ApiModelProperty(name = "name" , value = "姓名")
    private String name;
    @ApiModelProperty(name = "tel" , value = "电话")
    private String tel;

}
