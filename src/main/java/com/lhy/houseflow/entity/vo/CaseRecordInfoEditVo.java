package com.lhy.houseflow.entity.vo;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.List;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class CaseRecordInfoEditVo  implements Serializable {

    private static final long serialVersionUID = -1613933261608922972L;

    @ApiModelProperty(name = "id" , value = "id")
    private Long id;
    @ApiModelProperty(name = "caseNo" , value = "案件编号")
    private String caseNo;
    @ApiModelProperty(name = "caseRecordNo" , value = "立案编号")
    private String caseRecordNo;
    @ApiModelProperty(name = "caseDesc" , value = "案件描述")
    private String caseDesc;
    @ApiModelProperty(name = "processInstanceId" , value = "流程实例ID")
    private Long processInstanceId;
    @ApiModelProperty(name = "source" , value = "来源")
    private Integer source;
    @ApiModelProperty(name = "caseType" , value = "两违类型")
    private Integer caseType;
    @ApiModelProperty(name = "caseSubtype" , value = "二级类型")
    private Integer caseSubtype;
    @ApiModelProperty(name = "complainant" , value = "投诉人")
    private String complainant;
    @ApiModelProperty(name = "complainantTel" , value = "投诉电话")
    private String complainantTel;
    @ApiModelProperty(name = "areaId" , value = "镇区ID")
    private Long areaId;
    @ApiModelProperty(name = "position" , value = "位置")
    private String position;
    @ApiModelProperty(name = "longitude" , value = "经度")
    private BigDecimal longitude;
    @ApiModelProperty(name = "latitude" , value = "纬度")
    private BigDecimal latitude;
    @ApiModelProperty(name = "address" , value = "地址")
    private String address;
    @ApiModelProperty(name = "purposes" , value = "用途")
    private String purposes;
    @ApiModelProperty(name = "area" , value = "占地面积")
    private BigDecimal area;
    @ApiModelProperty(name = "realArea" , value = "建筑占地面积")
    private BigDecimal realArea;
    @ApiModelProperty(name = "buildKind" , value = "违建性质")
    private Integer buildKind;
    @ApiModelProperty(name = "buildStruct" , value = "建筑结构")
    private Integer buildStruct;
    @ApiModelProperty(name = "reportName" , value = "上报人")
    private String reportName;
    @ApiModelProperty(name = "reportTel" , value = "上报电话")
    private String reportTel;
    @ApiModelProperty(name = "enabled" , value = "是否有效（1：有效 0：无效）")
    private Integer enabled; 
    @ApiModelProperty(name = "status" , value = "状态（1：草稿 2：处理中）")
    private Integer status;   
    @ApiModelProperty(name = "defendants" , value = "被投诉人") 
    private List<CaseDefendantVo> defendants;
    @ApiModelProperty(name = "files" , value = "附件")
    private List<Long> uploadFileIds;


}