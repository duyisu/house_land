package com.lhy.houseflow.entity;

import java.util.ArrayList;
import java.util.List;

import lombok.Data;
@Data
public class BaseTreeData {
    private Long id;
    private String name;
    private  Long pid;
    private List<? extends BaseTreeData> children = new ArrayList<BaseTreeData>();
    
    public boolean isRoot(){
        return pid == null || pid.equals(0L);
    }   
    
    public static List<? extends BaseTreeData>  buildTree(List<? extends BaseTreeData> listSortById){
        
        List<? extends BaseTreeData> tree = new ArrayList<BaseTreeData>();
        return listSortById;
        
    }
    
    private static BaseTreeData bsearchWithoutRecursion(List<?extends BaseTreeData> list, Long parentId) {
        int low = 0;
        int high = list.size() - 1;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            int p = Long.compare(list.get(mid).getId(), parentId);
            if (p > 0)
                high = mid - 1;
            else if (p < 0)
                low = mid + 1;
            else
                return list.get(mid);
        }
        return null;
    }
}
