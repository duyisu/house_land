/**
 * @filename:BuildApply 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

/**   
 *  
 * @Description:  建房申请
 * @Author:       lhy   
 * @CreateDate:   2019年9月2日
 * @Version:      V1.0
 *    
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BuildApply implements Serializable {

	private static final long serialVersionUID = 1567396850318L;
	
	@ApiModelProperty(name = "id" , value = "id")
	private Long id;
	@ApiModelProperty(name = "applyNo" , value = "申请编号")
	private String applyNo;
	@ApiModelProperty(name = "processInstanceId" , value = "流程实例ID")
	private String processInstanceId;
	@ApiModelProperty(name = "applyName" , value = "申请人")
	private String applyName;
	@ApiModelProperty(name = "applyTel" , value = "申请电话")
	private String applyTel;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@ApiModelProperty(name = "applyTime" , value = "申请时间")
	private Date applyTime;
	@ApiModelProperty(name = "applyAddr" , value = "申请地址")
	private Long applyAddr;
	@ApiModelProperty(name = "applyKind" , value = "申请类型")
	private Integer applyKind;
	@ApiModelProperty(name = "longitude" , value = "经度")
	private BigDecimal longitude;
	@ApiModelProperty(name = "latitude" , value = "纬度")
	private BigDecimal latitude;
	@ApiModelProperty(name = "holderIdcardno" , value = "户主身份证号码")
	private String holderIdcardno;
	@ApiModelProperty(name = "holderName" , value = "户主姓名")
	private String holderName;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
	@ApiModelProperty(name = "holderBirthday" , value = "户主生日")
	private Date holderBirthday;
	@ApiModelProperty(name = "holderSex" , value = "户主性别")
	private Integer holderSex;
	@ApiModelProperty(name = "married" , value = "户主婚否")
	private Integer married;
	@ApiModelProperty(name = "familySize" , value = "家庭人数")
	private Integer familySize;
	@ApiModelProperty(name = "spouseName" , value = "配偶姓名")
	private String spouseName;
	@ApiModelProperty(name = "spouseIdcardno" , value = "配偶身份证号码")
	private String spouseIdcardno;
	@ApiModelProperty(name = "address" , value = "户口所在地")
	private String address;
	@ApiModelProperty(name = "houseAddr" , value = "现住址")
	private String houseAddr;
	@ApiModelProperty(name = "houseNum" , value = "住房数量")
	private Integer houseNum;
	@ApiModelProperty(name = "area" , value = "占地面积")
	private BigDecimal area;
	@ApiModelProperty(name = "realArea" , value = "建筑占地面积")
	private BigDecimal realArea;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
	@ApiModelProperty(name = "houseTime" , value = "建筑时间")
	private Date houseTime;
	@ApiModelProperty(name = "floors" , value = "建筑层数")
	private Integer floors;
	@ApiModelProperty(name = "houseDesc" , value = "房屋说明")
	private String houseDesc;
	@ApiModelProperty(name = "applyReason" , value = "申请理由")
	private Integer applyReason;
	@ApiModelProperty(name = "sumarea" , value = "建筑总面积")
	private BigDecimal sumarea;
	@ApiModelProperty(name = "applyArea" , value = "申请占地面积")
	private BigDecimal applyArea;
	@ApiModelProperty(name = "applyRealArea" , value = "申请建筑面积")
	private BigDecimal applyRealArea;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
	@ApiModelProperty(name = "applyHouseTime" , value = "申请建筑时间")
	private Date applyHouseTime;
	@ApiModelProperty(name = "applyFloors" , value = "申请建筑层数")
	private Integer applyFloors;
	@ApiModelProperty(name = "eaveHeight" , value = "申请檐口高度")
	private BigDecimal eaveHeight;
	@ApiModelProperty(name = "applyHouseAddr" , value = "申请建房地点")
	private String applyHouseAddr;
	@ApiModelProperty(name = "landKind" , value = "土地性质")
	private Integer landKind;
	@ApiModelProperty(name = "buildPaperNo" , value = "建筑图集号")
	private String buildPaperNo;
	@ApiModelProperty(name = "eastName" , value = "东面姓名")
	private String eastName;
	@ApiModelProperty(name = "southName" , value = "南面姓名")
	private String southName;
	@ApiModelProperty(name = "westName" , value = "西面姓名")
	private String westName;
	@ApiModelProperty(name = "northName" , value = "北面姓名")
	private String northName;
	@ApiModelProperty(name = "eastApply" , value = "东面意见")
	private Integer eastApply;
	@ApiModelProperty(name = "southApply" , value = "南面意见")
	private Integer southApply;
	@ApiModelProperty(name = "westApply" , value = "西面意见")
	private Integer westApply;
	@ApiModelProperty(name = "northApply" , value = "北面意见")
	private Integer northApply;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@ApiModelProperty(name = "eastTime" , value = "东面时间")
	private Date eastTime;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@ApiModelProperty(name = "southTime" , value = "南面时间")
	private Date southTime;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@ApiModelProperty(name = "westTime" , value = "西面时间")
	private Date westTime;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@ApiModelProperty(name = "northTime" , value = "北面时间")
	private Date northTime;
	@ApiModelProperty(name = "rule2" , value = "符合规则2")
	private Integer rule2;
	@ApiModelProperty(name = "rule3" , value = "符合规则3")
	private Integer rule3;
	@ApiModelProperty(name = "rule1" , value = "符合规则1")
	private Integer rule1;
	@ApiModelProperty(name = "enabled" , value = "")
	private Integer enabled;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@ApiModelProperty(name = "gmtCreate" , value = "创建时间")
	private Date gmtCreate;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@ApiModelProperty(name = "gmtModified" , value = "修改时间")
	private Date gmtModified;
	@ApiModelProperty(name = "status" , value = "")
	private Integer status;
	@ApiModelProperty(name = "insid" , value = "")
	private Long insid;
    @ApiModelProperty(name = "createUserId" , value = "")
    private Long createUserId;	
}
