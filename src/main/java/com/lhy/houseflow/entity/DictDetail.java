/**
 * @filename:DictDetail 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

/**   
 *  
 * @Description:  字典明细
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class DictDetail implements Serializable {

	private static final long serialVersionUID = 1566808463745L;
	
	@ApiModelProperty(name = "id" , value = "id")
	private Long id;
	@ApiModelProperty(name = "name" , value = "标签名称")
	private String name;
	@ApiModelProperty(name = "value" , value = "值")
	private Integer value;
	@ApiModelProperty(name = "sort" , value = "排序")
	private Integer sort;
	@ApiModelProperty(name = "dictId" , value = "字典ID")
	private Long dictId;
}
