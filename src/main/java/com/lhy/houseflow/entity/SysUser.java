/**
 * @filename:SysUser 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

/**   
 *  
 * @Description:  用户
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysUser implements Serializable {

	private static final long serialVersionUID = 1566618094866L;
	
	@ApiModelProperty(name = "id" , value = "id")
	private Long id;
	@ApiModelProperty(name = "userName" , value = "用户名称")
	private String userName;
	@ApiModelProperty(name = "departId" , value = "部门")
	private Long departId;
	@ApiModelProperty(name = "realName" , value = "真实姓名")
	private String realName;
	@ApiModelProperty(name = "employeeNo" , value = "工号")
	private String employeeNo;
	@ApiModelProperty(name = "userTel" , value = "手机号码")
	private String userTel;
	@ApiModelProperty(name = "idCardNo" , value = "身份证号码")
	private String idCardNo;
	@ApiModelProperty(name = "userSex" , value = "性别")
	private Integer userSex;
	@ApiModelProperty(name = "userPassword" , value = "用户密码")
	private String userPassword;
	@ApiModelProperty(name = "salt" , value = "salt")
	private String salt;
	@ApiModelProperty(name = "userDesc" , value = "备注")
	private String userDesc;
	@ApiModelProperty(name = "enabled" , value = "是否有效（1：启用 0：停用）")
	private Integer enabled;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@ApiModelProperty(name = "gmtCreate" , value = "创建时间")
	private Date gmtCreate;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@ApiModelProperty(name = "gmtModified" , value = "修改时间")
	private Date gmtModified;
}
