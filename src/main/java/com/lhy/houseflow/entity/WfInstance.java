/**
 * @filename:WfInstance 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

/**   
 *  
 * @Description:  工作流实例表
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WfInstance implements Serializable {

	private static final long serialVersionUID = 1567273611858L;
	
	@ApiModelProperty(name = "insId" , value = "")
	private Long insId;
	@ApiModelProperty(name = "defId" , value = "")
	private Long defId;
	@ApiModelProperty(name = "title" , value = "")
	private String title;
	@ApiModelProperty(name = "status" , value = "")
	private String status;
	@ApiModelProperty(name = "stepId" , value = "")
	private Long stepId;
	@ApiModelProperty(name = "fromuser" , value = "")
	private Integer fromuser;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
	@ApiModelProperty(name = "fromtime" , value = "")
	private Date fromtime;
	@ApiModelProperty(name = "touser" , value = "")
	private Integer touser;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
	@ApiModelProperty(name = "totime" , value = "")
	private Date totime;
	@ApiModelProperty(name = "creator" , value = "")
	private Integer creator;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
	@ApiModelProperty(name = "createtime" , value = "")
	private Date createtime;
	@ApiModelProperty(name = "modifier" , value = "")
	private Integer modifier;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
	@ApiModelProperty(name = "modifitime" , value = "")
	private Date modifitime;
	@ApiModelProperty(name = "version" , value = "")
	private Integer version;
	@ApiModelProperty(name = "ecclassname" , value = "")
	private String ecclassname;
	@ApiModelProperty(name = "fromuserdept" , value = "")
	private Integer fromuserdept;
	@ApiModelProperty(name = "touserdept" , value = "")
	private Integer touserdept;
	@ApiModelProperty(name = "creatordept" , value = "")
	private Integer creatordept;
}
