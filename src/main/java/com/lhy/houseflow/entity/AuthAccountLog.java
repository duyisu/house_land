package com.lhy.houseflow.entity;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuthAccountLog implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 6871031134087496022L;

    private Integer id;

    private String logName;

    private String userId;

    private String ip;

    private Short succeed;

    private String message;

    private Date createTime;

}
