package com.lhy.houseflow.entity;

import java.io.Serializable;
import java.util.Set;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoleMenusVo implements Serializable {

    @ApiModelProperty(name = "id", value = "id")
    private Long id;
    @ApiModelProperty(name = "name", value = "角色名称")
    private String name;
    @ApiModelProperty(name = "roleDesc", value = "角色描述")
    private String roleDesc;
    @ApiModelProperty(name = "roleKind", value = "角色级别（0：超级管理员1：管理员）")
    private Integer roleKind;
    @ApiModelProperty(name = "enabled", value = "是否有效（1：有效0：无效）")
    private Integer enabled;
    @ApiModelProperty(name = "menuIds", value = "菜单IDs")
    private Set<String> menuIds;
}
