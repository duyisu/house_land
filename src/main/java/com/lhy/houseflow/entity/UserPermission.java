package com.lhy.houseflow.entity;

import java.util.List;

public class UserPermission extends SysUser{
    /**
     * 
     */
    private static final long serialVersionUID = -7744420865650096826L;
    private List<String> permissions;

    public List<String> getPermissions() {
        return permissions;
    }

    public void setPermissions(List<String> permissions) {
        this.permissions = permissions;
    }

}
