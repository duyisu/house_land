/**
 * @filename:WfDefinitionStep 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

/**   
 *  
 * @Description:  工作流步骤定义表
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WfDefinitionStep implements Serializable {

	private static final long serialVersionUID = 1567273611858L;
	
	@ApiModelProperty(name = "stepId" , value = "")
	private Long stepId;
	@ApiModelProperty(name = "stepName" , value = "")
	private String stepName;
	@ApiModelProperty(name = "stepType" , value = "")
	private String stepType;
	@ApiModelProperty(name = "defId" , value = "")
	private Long defId;
	@ApiModelProperty(name = "dynamiccond" , value = "")
	private String dynamiccond;
	@ApiModelProperty(name = "iseditable" , value = "")
	private String iseditable;
	@ApiModelProperty(name = "signmode" , value = "")
	private String signmode;
	@ApiModelProperty(name = "selectusermode" , value = "")
	private String selectusermode;
	@ApiModelProperty(name = "selection" , value = "")
	private Integer selection;
	@ApiModelProperty(name = "days1" , value = "")
	private Integer days1;
	@ApiModelProperty(name = "days2" , value = "")
	private Integer days2;
	@ApiModelProperty(name = "version" , value = "")
	private Integer version;
	@ApiModelProperty(name = "selectuseraction" , value = "")
	private String selectuseraction;
	@ApiModelProperty(name = "sendaction" , value = "")
	private String sendaction;
	@ApiModelProperty(name = "terminateaction" , value = "")
	private String terminateaction;
	@ApiModelProperty(name = "isphone" , value = "")
	private String isphone;
	@ApiModelProperty(name = "isuploadfile" , value = "")
	private String isuploadfile;
	@ApiModelProperty(name = "sortCode" , value = "")
	private Integer sortCode;
	@ApiModelProperty(name = "isdelay" , value = "")
	private String isdelay;
	@ApiModelProperty(name = "envSelection" , value = "")
	private Integer envSelection;
}
