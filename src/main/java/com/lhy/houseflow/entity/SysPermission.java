/**
 * @filename:SysPermission 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

/**   
 *  
 * @Description:  权限
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysPermission implements Serializable {
    /**
     * 
     */
    private static final long serialVersionUID = 6796932758173799197L;
    @ApiModelProperty(name = "id" , value = "id")
    private Long id;
    @ApiModelProperty(name = "code" , value = "权限代码")
    private String code;
    @ApiModelProperty(name = "name" , value = "权限名称")
    private String name;
    @ApiModelProperty(name = "menuId" , value = "菜单ID")
    private Long menuId;
    @ApiModelProperty(name = "sort" , value = "序号")
    private Integer sort;
    @ApiModelProperty(name = "enabled" , value = "是否有效（1：有效0：无效）")
    private Integer enabled;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(name = "gmtCreate" , value = "创建时间")
    private Date gmtCreate;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(name = "gmtModified" , value = "修改时间")
    private Date gmtModified;
}
