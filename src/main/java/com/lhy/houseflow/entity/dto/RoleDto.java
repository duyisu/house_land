package com.lhy.houseflow.entity.dto;

import java.util.Set;

import com.lhy.houseflow.entity.SysRole;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class RoleDto extends SysRole {
    @ApiModelProperty(name = "menuIds", value = "菜单IDs")
    private Set<Long> menuIds;
    @ApiModelProperty(name = "permissionIds", value = "权限IDs")
    private Set<Long> permissionIds;

}
