package com.lhy.houseflow.entity.dto;

import java.util.Set;

import com.lhy.houseflow.entity.SysMenu;
import com.lhy.houseflow.entity.SysPermission;

import io.swagger.annotations.ApiModelProperty;

public class SysMenuDto extends SysMenu{
    @ApiModelProperty(name = "permissions", value = "角色IDs")
    private Set<SysPermission> permissions;
}
