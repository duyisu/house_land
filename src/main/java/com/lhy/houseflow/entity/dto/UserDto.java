package com.lhy.houseflow.entity.dto;

import java.util.Set;

import com.lhy.houseflow.entity.SysUser;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@EqualsAndHashCode(callSuper = false)
public class UserDto extends SysUser {
    @ApiModelProperty(name = "roleIdString", value = "角色IDs")
    private Set<Long> roleIds;
//    @JsonIgnore
//    private String roleIdString;
    
    @ApiModelProperty(name = "departName" , value = "部门名称")
    private String departName;
/*
    public Set<Long> getRoleIds() {
        return IdMapUtils.parseLong(roleIdString);
    }

    public void setRoldIds(Set<Long> idSet) {
        roleIdString = IdMapUtils.toString(idSet);
    }
    */

}
