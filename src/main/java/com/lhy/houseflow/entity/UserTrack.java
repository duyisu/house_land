/**
 * @filename:UserTrack 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

/**   
 *  
 * @Description:  人员轨迹
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UserTrack implements Serializable {

	private static final long serialVersionUID = 1566368181391L;
	
	@ApiModelProperty(name = "id" , value = "id")
	private Long id;
	@ApiModelProperty(name = "userId" , value = "人员ID")
	private Long userId;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@ApiModelProperty(name = "trackTime" , value = "时间")
	private Date trackTime;
	@ApiModelProperty(name = "locationx" , value = "经度")
	private BigDecimal locationx;
	@ApiModelProperty(name = "locationy" , value = "纬度")
	private BigDecimal locationy;
	@ApiModelProperty(name = "locationGrid" , value = "定位网格")
	private Long locationGrid;
	@ApiModelProperty(name = "radius" , value = "半径")
	private BigDecimal radius;
	@ApiModelProperty(name = "loctype" , value = "LOCTYPE")
	private String loctype;
	@ApiModelProperty(name = "satelliteNumber" , value = "卫星数量")
	private Integer satelliteNumber;
	@ApiModelProperty(name = "filtered" , value = "是否过滤（1：过滤0：无）")
	private Integer filtered;
	@ApiModelProperty(name = "trackGroup" , value = "轨迹分组")
	private String trackGroup;
	@ApiModelProperty(name = "trackFlag" , value = "轨迹标志")
	private Integer trackFlag;
	@ApiModelProperty(name = "valid" , value = "是否有效（1：有效0：无效）")
	private Integer valid;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@ApiModelProperty(name = "gmtCreate" , value = "创建时间")
	private Date gmtCreate;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@ApiModelProperty(name = "gmtModified" , value = "修改时间")
	private Date gmtModified;
}
