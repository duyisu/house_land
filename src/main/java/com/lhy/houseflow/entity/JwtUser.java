package com.lhy.houseflow.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.userdetails.UserDetails;

public class JwtUser implements UserDetails {

    private static final long serialVersionUID = -661843458165783452L;
    private Long id;
    private String username;
    private String password;
    private String tel;
    private Collection<GrantedAuthority> authorities = new ArrayList<GrantedAuthority> ();

    public JwtUser() {

    }

    public JwtUser(Long id, String username, String password, String tel,
        Collection<GrantedAuthority> authorities) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.tel = tel;
        this.authorities = authorities;
    }

    // 返回分配给用户的角色列表
    @Override
    public Collection<? extends GrantedAuthority> getAuthorities() {
        return authorities;
    }

    public Long getId() {
        return id;
    }

    @Override
    public String getPassword() {
        return password;
    }

    @Override
    public String getUsername() {
        return username;
    }

    // 账户是否未过期
    @Override
    public boolean isAccountNonExpired() {
        return true;
    }

    // 账户是否未锁定
    @Override
    public boolean isAccountNonLocked() {
        return true;
    }

    // 密码是否未过期
    @Override
    public boolean isCredentialsNonExpired() {
        return true;
    }

    // 账户是否激活
    @Override
    public boolean isEnabled() {
        return true;
    }

    public String getTel() {
        return tel;
    }
    
    public void addAuthority(GrantedAuthority authority){
        authorities.add(authority);
        
    }

    @Override
    public String toString() {
        StringBuffer sb = new StringBuffer();
        sb.append("id:").append(id).append("username:").append(username).append("tel:").append(tel).append("password:")
            .append(password);
        if (authorities != null) {
            sb.append("authorities:");
            for (GrantedAuthority g : authorities) {
                sb.append(g.toString()).append(",");
            }
        }
        return sb.toString();
    }
    
    public List<String> getPermissions(){
        Set<String> set = new HashSet<String>();
        if (authorities!= null){
            for (GrantedAuthority g : authorities) {
                set.add(g.getAuthority());
            }           
        }
        return new ArrayList<String>(set);
        
    }
}
