/**
 * @filename:WfDefinition 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

/**   
 *  
 * @Description:  工作流定义表
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WfDefinition implements Serializable {

	private static final long serialVersionUID = 1567273611858L;
	
	@ApiModelProperty(name = "defId" , value = "")
	private Long defId;
	@ApiModelProperty(name = "defName" , value = "")
	private String defName;
	@ApiModelProperty(name = "formtable" , value = "")
	private String formtable;
	@ApiModelProperty(name = "formobject" , value = "")
	private String formobject;
	@ApiModelProperty(name = "note" , value = "")
	private String note;
	@ApiModelProperty(name = "version" , value = "")
	private Integer version;
	@ApiModelProperty(name = "defLevel" , value = "")
	private String defLevel;
}
