package com.lhy.houseflow.entity;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

public final class JwtUserFactory {
    private JwtUserFactory() {}

    public static JwtUser create(SysUser user) {
        // FilterInvocation fi;
        return new JwtUser(user.getId(), user.getUserName(), user.getUserPassword(), user.getUserTel(), null);
    }

    public static JwtUser create(SysUser user, List<SysPermission> permission) {
        JwtUser jwtUser = create(user);
        if (permission != null) {
            for (SysPermission p : permission) {
                jwtUser.addAuthority(createAuthority(p));
            }
        }
        return jwtUser;
    }

    public static Collection<GrantedAuthority> createAuthorities(Collection<SysPermission> permissions) {
        List<GrantedAuthority> list = new ArrayList<GrantedAuthority>();
        for (SysPermission p : permissions)
            list.add(createAuthority(p));
        list.add(new SimpleGrantedAuthority("ADMIN"));
        return list;

    }

    public static GrantedAuthority createAuthority(SysPermission permission) {

        SimpleGrantedAuthority authority = new SimpleGrantedAuthority(permission.getCode());
        // new RestGrantedAuthority(permission.getPermissionUrl(), permission.getPermissionMethod(),
        // permission.getPermissionCode());
        return authority;
    }

}