/**
 * @filename:SysMenu 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.entity;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**   
 *  
 * @Description:  菜单资源
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysMenu implements Serializable {

	private static final long serialVersionUID = 1566704995977L;
	
	@ApiModelProperty(name = "id" , value = "id")
	private Long id;
	@ApiModelProperty(name = "component" , value = "菜单组件")
	private String component;
	@ApiModelProperty(name = "name" , value = "菜单名称")
	private String name;
	@ApiModelProperty(name = "path" , value = "菜单路径")
	private String path;
	@ApiModelProperty(name = "pid" , value = "上级菜单")
	private Long pid;
	@ApiModelProperty(name = "ico" , value = "菜单图标")
	private String ico;
	@ApiModelProperty(name = "sort" , value = "菜单序号")
	private Integer sort;
	@ApiModelProperty(name = "type" , value = "类型(0:菜单1：功能)")
	private Integer type;
	@ApiModelProperty(name = "iframe" , value = "外链标志（1：内部0：外链）")
	private Integer iframe;
	@ApiModelProperty(name = "enabled" , value = "是否有效（1：有效0：无效）")
	private Integer enabled;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@ApiModelProperty(name = "gmtCreate" , value = "创建时间")
	private Date gmtCreate;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@ApiModelProperty(name = "gmtModified" , value = "修改时间")
	private Date gmtModified;
	@ApiModelProperty(name = "sysPermissions" , value = "权限列表")
	private List<SysPermission> sysPermissions;
}
