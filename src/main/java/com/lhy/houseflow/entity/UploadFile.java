/**
 * @filename:UploadFile 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.entity;

import java.io.Serializable;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**   
 *  
 * @Description:  上传文件
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class UploadFile implements Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = 3706762762264615003L;
    
    @ApiModelProperty(name = "id" , value = "id")
    private Long id;
    @ApiModelProperty(name = "filename" , value = "文件名称")
    private String filename;
    @ApiModelProperty(name = "fileType" , value = "文件类型--扩展名")
    private String fileType;
    @ApiModelProperty(name = "originalName" , value = "原始文件名称")
    private String originalName;
    @ApiModelProperty(name = "filepath" , value = "文件路径")
    private String filepath;
    @ApiModelProperty(name = "fileUrl" , value = "文件URL")
    private String fileUrl;
    @ApiModelProperty(name = "businessId" , value = "业务ID")
    private Long businessId;
    @ApiModelProperty(name = "businessKind" , value = "文件类别(0:未分类;1:立卷立案;2:两违流程;3:两违任务;4:建房审批;5审批任务)")
    private Integer businessKind;
    @ApiModelProperty(name = "businessSubKind" , value = "业务文件分类")
    private Integer businessSubKind;
    @ApiModelProperty(name = "enabled" , value = "")
    private Integer enabled;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(name = "gmtCreate" , value = "创建时间")
    private Date gmtCreate;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(name = "gmtModified" , value = "修改时间")
    private Date gmtModified;
}