package com.lhy.houseflow.entity;

public enum FlowApplyType {
    Agree(0,"同意"),
    Reject (1,"拒绝");
    
    
    private int code;
    private String name;
    public int getCode() {
        return code;
    }
    public void setCode(int code) {
        this.code = code;
    }
    public String getName() {
        return name;
    }
    public void setName(String name) {
        this.name = name;
    }
    
    FlowApplyType(int code, String name) {
        this.code = code;
        this.name = name;
    }    

}
