/**
 * @filename:BuildApplyFile 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

/**   
 *  
 * @Description:  建房申请附件
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class BuildApplyFile implements Serializable {

	private static final long serialVersionUID = 1567308450765L;
	
	@ApiModelProperty(name = "id" , value = "id")
	private Long id;
	@ApiModelProperty(name = "applyId" , value = "申请ID")
	private Long applyId;
	@ApiModelProperty(name = "fileId" , value = "文件ID")
	private Long fileId;
	@ApiModelProperty(name = "enabled" , value = "")
	private Integer enabled;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@ApiModelProperty(name = "gmtCreate" , value = "创建时间")
	private Date gmtCreate;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@ApiModelProperty(name = "gmtModified" , value = "修改时间")
	private Date gmtModified;
	@ApiModelProperty(name = "flowFile" , value = "鏂囦欢鎵€灞炵被鍒紙0锛氫换鍔℃枃浠讹紝1锛氭祦绋嬫枃浠讹級")
	private Integer flowFile;
}
