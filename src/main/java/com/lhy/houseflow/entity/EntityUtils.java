package com.lhy.houseflow.entity;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;

import com.lhy.houseflow.entity.dto.UserDto;
import com.lhy.houseflow.entity.vo.CaseDefendantVo;
import com.lhy.houseflow.entity.vo.CaseRecordBaseInfoVo;
import com.lhy.houseflow.entity.vo.CaseRecordBuildingVo;
import com.lhy.houseflow.entity.vo.CaseRecordInfoEditVo;
import com.lhy.houseflow.entity.vo.CaseRecordInfoVo;
import com.lhy.houseflow.entity.vo.NameValue;
import com.lhy.houseflow.entity.vo.NameValueMap;
import com.lhy.houseflow.entity.vo.PeopleVo;

public class EntityUtils {
    public static SimpleUser createSimpleUser(UserDto userDto) {
        SimpleUser user = new SimpleUser();
        BeanUtils.copyProperties(userDto, user);
        return user;

    }

    public static PeopleVo createPeopleVo(CaseDefendant entity) {
        if (entity == null)
            return null;
        PeopleVo item = new PeopleVo();
        item.setName(entity.getDefendantName());
        item.setTel(entity.getDefendantTel());
        return item;

    }

    public static NameValue createNameValueVo(DictDetail entity) {
        NameValue item = new NameValue();
        BeanUtils.copyProperties(entity, item);
        return item;

    }
    /*
    @ApiModelProperty(name = "source" , value = "来源")
    @ApiModelProperty(name = "caseType" , value = "两违类型")
    @ApiModelProperty(name = "caseSubtype" , value = "二级类型")
     */

    public static CaseRecordBaseInfoVo createCaseRecordBaseInfoVo(CaseRecord entity, NameValueMap sources,
        NameValueMap caseTypes, NameValueMap caseSubtypes, List<CaseDefendant> defendants) {
        if (entity == null)
            return null;
        CaseRecordBaseInfoVo item = new CaseRecordBaseInfoVo();
        copyCaseRecordBaseInfoVoProperties(entity, sources, caseTypes, caseSubtypes, defendants, item);
        return item;

    }
    
    public static CaseRecordInfoVo createCaseRecordInfoVo(CaseRecord entity, NameValueMap sources,
        NameValueMap caseTypes, NameValueMap caseSubtypes, List<CaseDefendant> defendants) {
        if (entity == null)
            return null;
        CaseRecordInfoVo item = new CaseRecordInfoVo();
        copyCaseRecordBaseInfoVoProperties(entity, sources, caseTypes, caseSubtypes, defendants, item);
        return item;

    }  

    private static void copyCaseRecordBaseInfoVoProperties(CaseRecord entity, NameValueMap sources, NameValueMap caseTypes,
        NameValueMap caseSubtypes, List<CaseDefendant> defendants, CaseRecordBaseInfoVo item) {
        BeanUtils.copyProperties(entity, item);
        CaseRecordBuildingVo building= new CaseRecordBuildingVo();        
        BeanUtils.copyProperties(entity, building);        
        item.setBuilding(building); 

        item.setSource(sources.get(entity.getSource()));
        item.setCaseType(caseTypes.get(entity.getCaseType()));
        item.setCaseSubtype(caseSubtypes.get(entity.getCaseSubtype()));
        if (defendants != null && !defendants.isEmpty()) {
            List<CaseDefendantVo> peoples = new ArrayList<CaseDefendantVo>();
            for (CaseDefendant defendant : defendants) {
                CaseDefendantVo p = new CaseDefendantVo();
                BeanUtils.copyProperties(defendant, p);
                peoples.add(p);
            }
            item.setDefendants(peoples);
        }
    }

    public static CaseRecord createCaseRecordBaseInfoVo(CaseRecordBaseInfoVo vo) {
        if (vo == null)
            return null;
        CaseRecord item = new CaseRecord();
        BeanUtils.copyProperties(vo.getBuilding(), item);
        BeanUtils.copyProperties(vo, item);
        if (vo.getSource() != null) {
            item.setSource(vo.getSource().getValue());
        }
    
      

        if (vo.getCaseType() != null) {
            item.setCaseType(vo.getCaseType().getValue());
        }

        if (vo.getCaseSubtype() != null) {
            item.setCaseSubtype(vo.getCaseSubtype().getValue());
        }

        return item;
    }

    public static List<CaseDefendant> createCaseDefendants(CaseRecordInfoEditVo info) {
        List<CaseDefendant> defendants = new ArrayList<CaseDefendant>();
        if (info.getDefendants() != null) {
            for (CaseDefendantVo defendantSource : info.getDefendants()) {
                CaseDefendant defendant = new CaseDefendant();
                BeanUtils.copyProperties(defendantSource, defendant);
                defendants.add(defendant);
            }
        }
        return defendants;
    }

    public static List<CaseRecordFile> createCaseRecordFiles(CaseRecordInfoEditVo info) {
        List<CaseRecordFile> files = new ArrayList<CaseRecordFile>();
        if (info.getUploadFileIds()!= null) {
            for (Long fileId : info.getUploadFileIds()) {
                CaseRecordFile file = new CaseRecordFile();
                file.setFileId(fileId);
                files.add(file);
            }
        }
        return files;
    }

    public static CaseRecord createCaseRecordInfoEditVo(CaseRecordInfoEditVo vo) {
        if (vo == null)
            return null;
        CaseRecord item = new CaseRecord();
        BeanUtils.copyProperties(vo, item);
        return item;
    }

}
