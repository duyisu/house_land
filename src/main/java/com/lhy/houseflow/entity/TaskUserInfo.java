package com.lhy.houseflow.entity;

import java.util.List;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class TaskUserInfo {
    protected String name;
    protected String documentation;
    private List<SimpleUser> users;
    private boolean isEndTask;
}
