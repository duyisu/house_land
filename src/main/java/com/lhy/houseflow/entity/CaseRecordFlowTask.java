/**
 * @filename:CaseRecordFlowTask 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

/**   
 *  
 * @Description:  两违流程表
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class CaseRecordFlowTask implements Serializable {

	private static final long serialVersionUID = 1566463124448L;
	
	@ApiModelProperty(name = "id" , value = "id")
	private Long id;
	@ApiModelProperty(name = "caseId" , value = "案件Id")
	private Long caseId;
	@ApiModelProperty(name = "taskId" , value = "任务ID")
	private String taskId;
	@ApiModelProperty(name = "processInstanceId" , value = "流程实例ID")
	private String processInstanceId;
	@ApiModelProperty(name = "oprUserId" , value = "处理人员ID")
	private Long oprUserId;
	@ApiModelProperty(name = "oprMethod" , value = "处理措施")
	private String oprMethod;
	@ApiModelProperty(name = "oprType" , value = "处理意见")
	private String oprType;
	@ApiModelProperty(name = "oprDesc" , value = "处理意见描述")
	private String oprDesc;
	@ApiModelProperty(name = "selfArea" , value = "自行拆除面积")
	private BigDecimal selfArea;
	@ApiModelProperty(name = "otherArea" , value = "协助拆除面积面积")
	private BigDecimal otherArea;
	@ApiModelProperty(name = "newArea" , value = "即建即拆面积")
	private BigDecimal newArea;
	@ApiModelProperty(name = "enabled" , value = "是否有效（1：有效 0：无效）")
	private Integer enabled;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@ApiModelProperty(name = "gmtCreate" , value = "创建时间")
	private Date gmtCreate;
	@DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	@JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
	@ApiModelProperty(name = "gmtModified" , value = "修改时间")
	private Date gmtModified;
}
