/**
 * @filename:WfProcess 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

/**   
 *  
 * @Description:  实例处理任务表
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class WfProcess implements Serializable {

	private static final long serialVersionUID = 1567273611858L;
	
	@ApiModelProperty(name = "processId" , value = "")
	private Long processId;
	@ApiModelProperty(name = "lastprocessId" , value = "")
	private Long lastprocessId;
	@ApiModelProperty(name = "insId" , value = "")
	private Long insId;
	@ApiModelProperty(name = "stepId" , value = "")
	private Long stepId;
	@ApiModelProperty(name = "processstatus" , value = "")
	private String processstatus;
	@ApiModelProperty(name = "processtype" , value = "")
	private String processtype;
	@ApiModelProperty(name = "fromuser" , value = "")
	private Integer fromuser;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
	@ApiModelProperty(name = "fromtime" , value = "")
	private Date fromtime;
	@ApiModelProperty(name = "touser" , value = "")
	private Integer touser;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
	@ApiModelProperty(name = "totime" , value = "")
	private Date totime;
	@ApiModelProperty(name = "opinion" , value = "")
	private String opinion;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
	@ApiModelProperty(name = "finishtime" , value = "")
	private Date finishtime;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
	@ApiModelProperty(name = "worktime1" , value = "")
	private Date worktime1;
	@DateTimeFormat(pattern = "yyyy-MM-dd")
	@JsonFormat(pattern="yyyy-MM-dd",timezone = "GMT+8")
	@ApiModelProperty(name = "worktime2" , value = "")
	private Date worktime2;
	@ApiModelProperty(name = "version" , value = "")
	private Integer version;
	@ApiModelProperty(name = "maliciousRollback" , value = "")
	private String maliciousRollback;
	@ApiModelProperty(name = "fromuserdept" , value = "")
	private Integer fromuserdept;
	@ApiModelProperty(name = "touserdept" , value = "")
	private Integer touserdept;
	@ApiModelProperty(name = "handleStatus" , value = "")
	private String handleStatus;
}
