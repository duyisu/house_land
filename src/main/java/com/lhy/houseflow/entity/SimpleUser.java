package com.lhy.houseflow.entity;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.ToString;

@Data
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class SimpleUser {
    @ApiModelProperty(name = "id" , value = "id")
    private Long id;
    @ApiModelProperty(name = "userName" , value = "用户名称")
    private String userName;
    @ApiModelProperty(name = "departId" , value = "部门")
    private Long departId;
    @ApiModelProperty(name = "realName" , value = "真实姓名")
    private String realName;
    @ApiModelProperty(name = "departName" , value = "部门名称")
    private String departName;
}
