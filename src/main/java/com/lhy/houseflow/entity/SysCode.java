/**
 * @filename:SysCode 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.entity;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;
import org.springframework.format.annotation.DateTimeFormat;
import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.Data;
import lombok.NoArgsConstructor;
import lombok.AllArgsConstructor;

/**   
 *  
 * @Description:  系统编号
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysCode implements Serializable {

	private static final long serialVersionUID = 1566368181391L;
	
	@ApiModelProperty(name = "id" , value = "id")
	private Long id;
	@ApiModelProperty(name = "codeKey" , value = "代码类型")
	private String codeKey;
	@ApiModelProperty(name = "codeDate" , value = "前缀日期")
	private Long codeDate;
	@ApiModelProperty(name = "currCode" , value = "当前编号")
	private Long currCode;
	@ApiModelProperty(name = "codeLength" , value = "编码长度")
	private Integer codeLength;
}
