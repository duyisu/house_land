package com.lhy.houseflow.entity;

import java.io.Serializable;
import java.util.Date;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class AuthOperationLog implements Serializable {
    /**
	 * 
	 */
	private static final long serialVersionUID = 8356179982645881772L;

	private Integer id;

    private String logName;

    private String userId;

    private String api;

    private String method;

    private Short succeed;

    private String message;

    private Date createTime;	
}
