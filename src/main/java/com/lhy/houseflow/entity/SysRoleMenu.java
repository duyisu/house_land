/**
 * @filename:SysRoleMenu 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.entity;

import java.io.Serializable;
import java.util.Date;

import org.springframework.format.annotation.DateTimeFormat;

import com.fasterxml.jackson.annotation.JsonFormat;

import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

/**   
 *  
 * @Description:  角色菜单
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Data
@AllArgsConstructor
@NoArgsConstructor
public class SysRoleMenu implements Serializable {
    
    /**
     * 
     */
    private static final long serialVersionUID = 610865170668735924L;
    @ApiModelProperty(name = "id" , value = "id")
    private Long id;
    @ApiModelProperty(name = "roleId" , value = "角色ID")
    private Long roleId;
    @ApiModelProperty(name = "menuId" , value = "权限ID")
    private Long menuId;
    @ApiModelProperty(name = "permissionId" , value = "权限ID")
    private Long permissionId;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(name = "gmtCreate" , value = "创建时间")
    private Date gmtCreate;
    @DateTimeFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    @JsonFormat(pattern="yyyy-MM-dd HH:mm:ss",timezone = "GMT+8")
    @ApiModelProperty(name = "gmtModified" , value = "修改时间")
    private Date gmtModified;
}
