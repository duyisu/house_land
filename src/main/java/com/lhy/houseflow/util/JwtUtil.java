package com.lhy.houseflow.util;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang3.RegExUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import io.jsonwebtoken.Claims;
import io.jsonwebtoken.Jwts;
import io.jsonwebtoken.SignatureAlgorithm;
import lombok.extern.slf4j.Slf4j;

/**
 * 
 */

@Slf4j
@Component
public class JwtUtil {
    
    private static final int Munite_Millis = 60*1000;

    @Value("${jwt.login-path}")
    public void setLoginPath(String value) {
        JwtUtil.LOGIN_PATH = value;
    }

    private static String LOGIN_PATH;

    @Value("${jwt.header}")
    public void setHeader(String value) {
        JwtUtil.HEADER = value;
    }

    private static String HEADER;// = "Authorization";

    @Value("${jwt.token-head}")
    public void setTokenHeader(String value) {
        JwtUtil.TOKENH_EADER = value;
    }

    private static String TOKENH_EADER;// = "Bearer ";

    @Value("${jwt.secret}")
    public void setSecret(String value) {
        JwtUtil.secret = value;
    }

    private static String secret;

    @Value("${jwt.expiration}")
    public void setExpiration(Integer value) {
        JwtUtil.EXPIRATION = value * Munite_Millis;
    }

    private static Integer EXPIRATION;

    public static String parseUserName(Claims claims) {
        if (claims == null)
            return null;
        return claims.getSubject();

    }

    public static String generateToken(String userName) {
        String token = Jwts.builder().setSubject(userName).setIssuedAt(new Date())
            .setExpiration(new Date(System.currentTimeMillis() + EXPIRATION)).signWith(SignatureAlgorithm.HS256, secret)
            .compact();
        return token;
    }

    public static String generateHeadToken(String userName) {
        String token = Jwts.builder().setSubject(userName).setIssuedAt(new Date())
            .setExpiration(new Date(System.currentTimeMillis() + EXPIRATION)).signWith(SignatureAlgorithm.HS256, secret)
            .compact();
        return TOKENH_EADER + token;
    }

    public static boolean isNotExpiration(Claims claims) {
        if (claims == null)
            return false;
        return claims.getExpiration().after(new Date());
    }

    public static String getTokenFromRequest(HttpServletRequest request) {
        String token = request.getHeader(HEADER);
        token = RegExUtils.removeFirst(token, JwtUtil.TOKENH_EADER);
        if (StringUtils.equalsAnyIgnoreCase(token, "undefined")){
            token = null;
        }
        return token;
    }

    public static Claims parseClaims(String token) {
        Claims claims = null;
        try {
            claims = Jwts.parser().setSigningKey(secret).parseClaimsJws(token).getBody();
            log.info("claims:{}", claims.toString());
        } catch (Exception e) {
            log.error(e.getMessage());
        }
        return claims;
    }

}