package com.lhy.houseflow.util;

import java.util.HashSet;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;

public class IdMapUtils {

    private static final String SEPARATOR_CHAR = ",";

    public static Set<Long> parseLong(String idStrings) {
        return parseLong(idStrings, SEPARATOR_CHAR);
    }

    public static String toString(Set<Long> ids) {
        return toString(ids, SEPARATOR_CHAR);
    }

    public static Set<Long> parseLong(String idStrings, final String separatorChars) {
        Set<Long> idSet = new HashSet<Long>();
        String[] arrayIds = StringUtils.split(idStrings, separatorChars);
        if (arrayIds != null) {
            for (String id : arrayIds) {
                idSet.add(Long.parseLong(id));
            }
        }
        return idSet;
    }

    public static String toString(Set<Long> ids, String separatorChars) {
        if (ids == null || ids.isEmpty())
            return null;
        return StringUtils.join(ids, separatorChars);

    }

}
