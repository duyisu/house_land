/**
 * @filename:UploadFileService 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.entity.UploadFile;
/**   
 *  
 * @Description:  上传文件——SERVICE
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
public interface UploadFileService {
	
	/**
	 * @explain 查询上传文件对象
	 * @param   对象参数：id
	 * @return  UploadFile
	 * @author  lhy
	 */
	public UploadFile findById(Long id);
	
	/**
	 * @explain 删除上传文件对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 */
	public int deleteById(Long id);
	
	/**
	 * @explain 添加上传文件对象
	 * @param   对象参数：UploadFile
	 * @return  int
	 * @author  lhy
	 */
	public int add(UploadFile uploadFile);
	
	/**
	 * @explain 修改上传文件对象
	 * @param   对象参数：UploadFile
	 * @return  int
	 * @author  lhy
	 */
	public int update(UploadFile uploadFile);
	
	/**
	 * @explain 查询上传文件集合
	 * @param   对象参数：UploadFile
	 * @return  List<UploadFile>
	 * @author  lhy
	 */
	public List<UploadFile> queryUploadFileList(UploadFile uploadFile);
	
	/**
	 * @explain 分页查询上传文件
	 * @param   对象参数：UploadFile
	 * @return  PageDataVo<UploadFile>
	 * @author  lhy
	 */
	public PageInfo<UploadFile> list(AppPage<UploadFile> page);
}