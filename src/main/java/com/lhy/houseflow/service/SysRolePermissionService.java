/**
 * @filename:SysRolePermissionService 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.entity.SysRolePermission;
/**   
 *  
 * @Description:  角色权限——SERVICE
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
public interface SysRolePermissionService {
	
	/**
	 * @explain 查询角色权限对象
	 * @param   对象参数：id
	 * @return  SysRolePermission
	 * @author  lhy
	 */
	public SysRolePermission findById(Long id);
	
	/**
	 * @explain 删除角色权限对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 */
	public int deleteById(Long id);
	
	/**
	 * @explain 添加角色权限对象
	 * @param   对象参数：SysRolePermission
	 * @return  int
	 * @author  lhy
	 */
	public int add(SysRolePermission sysRolePermission);
	
	/**
	 * @explain 修改角色权限对象
	 * @param   对象参数：SysRolePermission
	 * @return  int
	 * @author  lhy
	 */
	public int update(SysRolePermission sysRolePermission);
	
	/**
	 * @explain 查询角色权限集合
	 * @param   对象参数：SysRolePermission
	 * @return  List<SysRolePermission>
	 * @author  lhy
	 */
	public List<SysRolePermission> querySysRolePermissionList(SysRolePermission sysRolePermission);
	
	/**
	 * @explain 分页查询角色权限
	 * @param   对象参数：SysRolePermission
	 * @return  PageDataVo<SysRolePermission>
	 * @author  lhy
	 */
	public PageInfo<SysRolePermission> list(AppPage<SysRolePermission> page);
}