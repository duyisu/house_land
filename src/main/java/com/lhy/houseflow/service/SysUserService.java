/**
 * @filename:SysUserService 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.exception.CustomException;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.entity.SysUser;
import com.lhy.houseflow.entity.dto.UserDto;
/**   
 *  
 * @Description:  用户——SERVICE
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
public interface SysUserService {
	
	/**
	 * @explain 查询用户对象
	 * @param   对象参数：id
	 * @return  SysUser
	 * @author  lhy
	 */
	public SysUser findById(Long id);
	
	/**
	 * @explain 删除用户对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 */
	public int deleteById(Long id);
	
	/**
	 * @explain 添加用户对象
	 * @param   对象参数：SysUser
	 * @return  int
	 * @author  lhy
	 */
	public int add(UserDto sysUser);
	
	/**
	 * @explain 修改用户对象
	 * @param   对象参数：SysUser
	 * @return  int
	 * @author  lhy
	 */
	public int update(UserDto sysUser);
	
	/**
	 * @explain 查询用户集合
	 * @param   对象参数：SysUser
	 * @return  List<SysUser>
	 * @author  lhy
	 */
	List<UserDto> querySysUserList(SysUser sysUser);
	
	/**
	 * @explain 分页查询用户
	 * @param   对象参数：SysUser
	 * @return  PageDataVo<SysUser>
	 * @author  lhy
	 */
	PageInfo<UserDto> list(AppPage<SysUser> page);

    SysUser findByUsername(String userName);

    List<String> loadPermissionByUsername(String username) throws CustomException;
}