/**
 * @filename:AreaService 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.entity.Area;
/**   
 *  
 * @Description:  区域——SERVICE
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
public interface AreaService {
	
	/**
	 * @explain 查询区域对象
	 * @param   对象参数：id
	 * @return  Area
	 * @author  lhy
	 */
	public Area findById(Long id);
	
	/**
	 * @explain 删除区域对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 */
	public int deleteById(Long id);
	
	/**
	 * @explain 添加区域对象
	 * @param   对象参数：Area
	 * @return  int
	 * @author  lhy
	 */
	public int add(Area area);
	
	/**
	 * @explain 修改区域对象
	 * @param   对象参数：Area
	 * @return  int
	 * @author  lhy
	 */
	public int update(Area area);
	
	/**
	 * @explain 查询区域集合
	 * @param   对象参数：Area
	 * @return  List<Area>
	 * @author  lhy
	 */
	public List<Area> queryAreaList(Area area);
	
	/**
	 * @explain 分页查询区域
	 * @param   对象参数：Area
	 * @return  PageDataVo<Area>
	 * @author  lhy
	 */
	public PageInfo<Area> list(AppPage<Area> page);
}