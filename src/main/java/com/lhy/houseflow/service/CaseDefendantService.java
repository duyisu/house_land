/**
 * @filename:CaseDefendantService 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.entity.CaseDefendant;
/**   
 *  
 * @Description:  立案登记当事人——SERVICE
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
public interface CaseDefendantService {
	
	/**
	 * @explain 查询立案登记当事人对象
	 * @param   对象参数：id
	 * @return  CaseDefendant
	 * @author  lhy
	 */
	public CaseDefendant findById(Long id);
	
	/**
	 * @explain 删除立案登记当事人对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 */
	public int deleteById(Long id);
	
	/**
	 * @explain 添加立案登记当事人对象
	 * @param   对象参数：CaseDefendant
	 * @return  int
	 * @author  lhy
	 */
	public int add(CaseDefendant caseDefendant);
	
	/**
	 * @explain 修改立案登记当事人对象
	 * @param   对象参数：CaseDefendant
	 * @return  int
	 * @author  lhy
	 */
	public int update(CaseDefendant caseDefendant);
	
	/**
	 * @explain 查询立案登记当事人集合
	 * @param   对象参数：CaseDefendant
	 * @return  List<CaseDefendant>
	 * @author  lhy
	 */
	public List<CaseDefendant> queryCaseDefendantList(CaseDefendant caseDefendant);
	
	/**
	 * @explain 分页查询立案登记当事人
	 * @param   对象参数：CaseDefendant
	 * @return  PageDataVo<CaseDefendant>
	 * @author  lhy
	 */
	public PageInfo<CaseDefendant> list(AppPage<CaseDefendant> page);
}