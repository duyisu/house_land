/**
 * @filename:CaseRecordFlowTaskService 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.entity.CaseRecordFlowTask;
/**   
 *  
 * @Description:  两违流程表——SERVICE
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
public interface CaseRecordFlowTaskService {
	
	/**
	 * @explain 查询两违流程表对象
	 * @param   对象参数：id
	 * @return  CaseRecordFlowTask
	 * @author  lhy
	 */
	public CaseRecordFlowTask findById(Long id);
	
	/**
	 * @explain 删除两违流程表对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 */
	public int deleteById(Long id);
	
	/**
	 * @explain 添加两违流程表对象
	 * @param   对象参数：CaseRecordFlowTask
	 * @return  int
	 * @author  lhy
	 */
	public int add(CaseRecordFlowTask caseRecordFlowTask);
	
	/**
	 * @explain 修改两违流程表对象
	 * @param   对象参数：CaseRecordFlowTask
	 * @return  int
	 * @author  lhy
	 */
	public int update(CaseRecordFlowTask caseRecordFlowTask);
	
	/**
	 * @explain 查询两违流程表集合
	 * @param   对象参数：CaseRecordFlowTask
	 * @return  List<CaseRecordFlowTask>
	 * @author  lhy
	 */
	public List<CaseRecordFlowTask> queryCaseRecordFlowTaskList(CaseRecordFlowTask caseRecordFlowTask);
	
	/**
	 * @explain 分页查询两违流程表
	 * @param   对象参数：CaseRecordFlowTask
	 * @return  PageDataVo<CaseRecordFlowTask>
	 * @author  lhy
	 */
	public PageInfo<CaseRecordFlowTask> list(AppPage<CaseRecordFlowTask> page);
}