/**
 * @filename:SysUserRoleService 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.entity.SysUserRole;
/**   
 *  
 * @Description:  用户角色——SERVICE
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
public interface SysUserRoleService {
	
	/**
	 * @explain 查询用户角色对象
	 * @param   对象参数：id
	 * @return  SysUserRole
	 * @author  lhy
	 */
	public SysUserRole findById(Long id);
	
	/**
	 * @explain 删除用户角色对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 */
	public int deleteById(Long id);
	
	/**
	 * @explain 添加用户角色对象
	 * @param   对象参数：SysUserRole
	 * @return  int
	 * @author  lhy
	 */
	public int add(SysUserRole sysUserRole);
	
	/**
	 * @explain 修改用户角色对象
	 * @param   对象参数：SysUserRole
	 * @return  int
	 * @author  lhy
	 */
	public int update(SysUserRole sysUserRole);
	
	/**
	 * @explain 查询用户角色集合
	 * @param   对象参数：SysUserRole
	 * @return  List<SysUserRole>
	 * @author  lhy
	 */
	public List<SysUserRole> querySysUserRoleList(SysUserRole sysUserRole);
	
	/**
	 * @explain 分页查询用户角色
	 * @param   对象参数：SysUserRole
	 * @return  PageDataVo<SysUserRole>
	 * @author  lhy
	 */
	public PageInfo<SysUserRole> list(AppPage<SysUserRole> page);
}