/**
 * @filename:CarService 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.entity.Car;
/**   
 *  
 * @Description:  车辆——SERVICE
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
public interface CarService {
	
	/**
	 * @explain 查询车辆对象
	 * @param   对象参数：id
	 * @return  Car
	 * @author  lhy
	 */
	public Car findById(Long id);
	
	/**
	 * @explain 删除车辆对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 */
	public int deleteById(Long id);
	
	/**
	 * @explain 添加车辆对象
	 * @param   对象参数：Car
	 * @return  int
	 * @author  lhy
	 */
	public int add(Car car);
	
	/**
	 * @explain 修改车辆对象
	 * @param   对象参数：Car
	 * @return  int
	 * @author  lhy
	 */
	public int update(Car car);
	
	/**
	 * @explain 查询车辆集合
	 * @param   对象参数：Car
	 * @return  List<Car>
	 * @author  lhy
	 */
	public List<Car> queryCarList(Car car);
	
	/**
	 * @explain 分页查询车辆
	 * @param   对象参数：Car
	 * @return  PageDataVo<Car>
	 * @author  lhy
	 */
	public PageInfo<Car> list(AppPage<Car> page);
}