/**
 * @filename:CarTrackService 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.entity.CarTrack;
/**   
 *  
 * @Description:  车辆轨迹——SERVICE
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
public interface CarTrackService {
	
	/**
	 * @explain 查询车辆轨迹对象
	 * @param   对象参数：id
	 * @return  CarTrack
	 * @author  lhy
	 */
	public CarTrack findById(Long id);
	
	/**
	 * @explain 删除车辆轨迹对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 */
	public int deleteById(Long id);
	
	/**
	 * @explain 添加车辆轨迹对象
	 * @param   对象参数：CarTrack
	 * @return  int
	 * @author  lhy
	 */
	public int add(CarTrack carTrack);
	
	/**
	 * @explain 修改车辆轨迹对象
	 * @param   对象参数：CarTrack
	 * @return  int
	 * @author  lhy
	 */
	public int update(CarTrack carTrack);
	
	/**
	 * @explain 查询车辆轨迹集合
	 * @param   对象参数：CarTrack
	 * @return  List<CarTrack>
	 * @author  lhy
	 */
	public List<CarTrack> queryCarTrackList(CarTrack carTrack);
	
	/**
	 * @explain 分页查询车辆轨迹
	 * @param   对象参数：CarTrack
	 * @return  PageDataVo<CarTrack>
	 * @author  lhy
	 */
	public PageInfo<CarTrack> list(AppPage<CarTrack> page);
}