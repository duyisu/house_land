/**
 * @filename:DictService 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.entity.Dict;
/**   
 *  
 * @Description:  字典类别——SERVICE
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
public interface DictService {
	
	/**
	 * @explain 查询字典类别对象
	 * @param   对象参数：id
	 * @return  Dict
	 * @author  lhy
	 */
	public Dict findById(Long id);
	
	/**
	 * @explain 删除字典类别对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 */
	public int deleteById(Long id);
	
	/**
	 * @explain 添加字典类别对象
	 * @param   对象参数：Dict
	 * @return  int
	 * @author  lhy
	 */
	public int add(Dict dict);
	
	/**
	 * @explain 修改字典类别对象
	 * @param   对象参数：Dict
	 * @return  int
	 * @author  lhy
	 */
	public int update(Dict dict);
	
	/**
	 * @explain 查询字典类别集合
	 * @param   对象参数：Dict
	 * @return  List<Dict>
	 * @author  lhy
	 */
	public List<Dict> queryDictList(Dict dict);
	
	/**
	 * @explain 分页查询字典类别
	 * @param   对象参数：Dict
	 * @return  PageInfo<Dict>
	 * @author  lhy
	 */
	public PageInfo<Dict> list(AppPage<Dict> page);
}