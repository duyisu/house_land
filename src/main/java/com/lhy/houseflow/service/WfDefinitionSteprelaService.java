/**
 * @filename:WfDefinitionSteprelaService 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.entity.WfDefinitionSteprela;
/**   
 *  
 * @Description:  步骤关系——SERVICE
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
public interface WfDefinitionSteprelaService {
	
	/**
	 * @explain 查询步骤关系对象
	 * @param   对象参数：id
	 * @return  WfDefinitionSteprela
	 * @author  lhy
	 */
	public WfDefinitionSteprela findById(Long id);
	
	/**
	 * @explain 删除步骤关系对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 */
	public int deleteById(Long id);
	
	/**
	 * @explain 添加步骤关系对象
	 * @param   对象参数：WfDefinitionSteprela
	 * @return  int
	 * @author  lhy
	 */
	public int add(WfDefinitionSteprela wfDefinitionSteprela);
	
	/**
	 * @explain 修改步骤关系对象
	 * @param   对象参数：WfDefinitionSteprela
	 * @return  int
	 * @author  lhy
	 */
	public int update(WfDefinitionSteprela wfDefinitionSteprela);
	
	/**
	 * @explain 查询步骤关系集合
	 * @param   对象参数：WfDefinitionSteprela
	 * @return  List<WfDefinitionSteprela>
	 * @author  lhy
	 */
	public List<WfDefinitionSteprela> queryWfDefinitionSteprelaList(WfDefinitionSteprela wfDefinitionSteprela);
	
	/**
	 * @explain 分页查询步骤关系
	 * @param   对象参数：WfDefinitionSteprela
	 * @return  PageInfo<WfDefinitionSteprela>
	 * @author  lhy
	 */
	public PageInfo<WfDefinitionSteprela> list(AppPage<WfDefinitionSteprela> page);
}