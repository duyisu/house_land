/**
 * @filename:WfDefinitionStepService 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.entity.WfDefinitionStep;
/**   
 *  
 * @Description:  工作流步骤定义表——SERVICE
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
public interface WfDefinitionStepService {
	
	/**
	 * @explain 查询工作流步骤定义表对象
	 * @param   对象参数：id
	 * @return  WfDefinitionStep
	 * @author  lhy
	 */
	public WfDefinitionStep findById(Long id);
	
	/**
	 * @explain 删除工作流步骤定义表对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 */
	public int deleteById(Long id);
	
	/**
	 * @explain 添加工作流步骤定义表对象
	 * @param   对象参数：WfDefinitionStep
	 * @return  int
	 * @author  lhy
	 */
	public int add(WfDefinitionStep wfDefinitionStep);
	
	/**
	 * @explain 修改工作流步骤定义表对象
	 * @param   对象参数：WfDefinitionStep
	 * @return  int
	 * @author  lhy
	 */
	public int update(WfDefinitionStep wfDefinitionStep);
	
	/**
	 * @explain 查询工作流步骤定义表集合
	 * @param   对象参数：WfDefinitionStep
	 * @return  List<WfDefinitionStep>
	 * @author  lhy
	 */
	public List<WfDefinitionStep> queryWfDefinitionStepList(WfDefinitionStep wfDefinitionStep);
	
	/**
	 * @explain 分页查询工作流步骤定义表
	 * @param   对象参数：WfDefinitionStep
	 * @return  PageInfo<WfDefinitionStep>
	 * @author  lhy
	 */
	public PageInfo<WfDefinitionStep> list(AppPage<WfDefinitionStep> page);
}