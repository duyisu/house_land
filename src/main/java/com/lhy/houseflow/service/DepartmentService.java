/**
 * @filename:DepartmentService 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.entity.Department;
import com.lhy.houseflow.entity.vo.DepartmentVo;
import com.lhy.houseflow.entity.vo.SimpleTreeData;
/**   
 *  
 * @Description:  部门——SERVICE
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
public interface DepartmentService {
	
	/**
	 * @explain 查询部门对象
	 * @param   对象参数：id
	 * @return  Department
	 * @author  lhy
	 */
	public Department findById(Long id);
	
	/**
	 * @explain 删除部门对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 */
	public int deleteById(Long id);
	
	/**
	 * @explain 添加部门对象
	 * @param   对象参数：Department
	 * @return  int
	 * @author  lhy
	 */
	public int add(Department department);
	
	/**
	 * @explain 修改部门对象
	 * @param   对象参数：Department
	 * @return  int
	 * @author  lhy
	 */
	public int update(Department department);
	
	/**
	 * @explain 查询部门集合
	 * @param   对象参数：Department
	 * @return  List<Department>
	 * @author  lhy
	 */
	public List<Department> queryDepartmentList(Department department);
	
	/**
	 * @explain 分页查询部门
	 * @param   对象参数：Department
	 * @return  PageDataVo<Department>
	 * @author  lhy
	 */
	public PageInfo<Department> list(AppPage<Department> page);

    List<DepartmentVo> buildTree(List<Department> depts);

    List<SimpleTreeData> getSimpleTreeData(List<DepartmentVo> tree);
}