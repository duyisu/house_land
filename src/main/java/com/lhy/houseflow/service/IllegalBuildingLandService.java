package com.lhy.houseflow.service;

import java.util.List;

import org.flowable.engine.history.HistoricActivityInstance;
import org.flowable.engine.runtime.ProcessInstance;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.entity.CaseDefendant;
import com.lhy.houseflow.entity.CaseRecord;
import com.lhy.houseflow.entity.CaseRecordFile;
import com.lhy.houseflow.entity.CaseRecordFlowTask;
import com.lhy.houseflow.entity.JwtUser;
import com.lhy.houseflow.entity.SysUser;
import com.lhy.houseflow.entity.TaskUserInfo;
import com.lhy.houseflow.entity.WfDefinitionStep;
import com.lhy.houseflow.entity.WfProcess;
import com.lhy.houseflow.entity.vo.CaseProcessWorkVo;
import com.lhy.houseflow.entity.vo.CaseRecordBaseInfoVo;
import com.lhy.houseflow.entity.vo.CaseRecordInfoVo;
import com.lhy.houseflow.entity.vo.CaseRecordVo;
import com.lhy.houseflow.entity.vo.FlowProcessVo;
import com.lhy.houseflow.entity.vo.TaskVo;

public interface IllegalBuildingLandService {

    ProcessInstance createTask(String userName, Long caseId);

    /**
     * 我的待办
     */

    List<TaskVo<CaseRecordBaseInfoVo>> myTask(JwtUser user, String reportName, String defendantName, Integer sources,
        Integer caseType, Integer caseSubtype, Long areaId, String addr);

    /**
     * 我的办理
     */

    List<HistoricActivityInstance> myHistory(String userName, int firstIdx, int pageSize);

    /**
     * 签收任务
     *
     * @param userName
     * @param taskId
     */
    void claimTask(String userName, String taskId);

    /**
     * 委派任务
     *
     * @param userName
     * @param taskId
     */
    void delegateTask(String userName, String taskId);
    
    /**
     * 转办任务
     *
     * @param userName
     * @param taskId
     */
    void turnTask(String userName, String taskId, String trunUserName);    

    /**
     * 完成
     */
    void complete(String taskId, CaseRecordFlowTask info);
    
    /**
     *退回
     */
    void backTask(String taskId, String backTaskId, CaseRecordFlowTask info);

    CaseRecord addCaseRecord(CaseRecord info, List<CaseDefendant> defendants, List<CaseRecordFile> files); 
    
    CaseRecord updateCaseRecord(CaseRecord info, List<CaseDefendant> defendants, List<CaseRecordFile> files);
    
    List<TaskUserInfo> findNextTaskUserInfosByTaskId(String taskId);

    CaseRecordInfoVo findCaseRecordInfo(Long caseId);
    
    WfDefinitionStep findStartUpStep(Long defId);
    
    List<WfDefinitionStep> findNextSteps (Long stepId);
    
    List<SysUser> wfToUser(Long stepId);
    
    void signProcess(Long userId,Long insId);
    void handleProcess(Long userId,Long insId,Long currStepId,Long nextStepId,String processOpinion);
    List<WfProcess> hasFinishProcess(Long insId);
    List<WfProcess> curToHandleProcess(Long insId);
    List<WfProcess> myToHandleProcess(Long userId);
    List<WfProcess> myHandledProcess(long userId);
    CaseRecordBaseInfoVo viewCaseRecord(Long insId);
    public FlowProcessVo copyProcessToVo (WfProcess p);

    /**
     * 查询工作台
     * @param currUserId
     * @param status
     * @param reportName
     * @param defendantName
     * @param sources
     * @param caseType
     * @param caseSubtype
     * @param areaId
     * @param addr
     * @param page
     * @param size
     * @return
     */
    PageInfo<CaseRecord> findMyCaseRecordWork(Long currUserId, String reportName, String complainant,
			Integer sources, Integer caseType, Integer caseSubtype, Long areaId, String addr, Integer page,
			Integer size);//String defendantName
    List<CaseProcessWorkVo> findAppTodo(Long userId);
    
    public PageInfo<CaseRecord> myWorkCaseRecord(Long currUserId,String status);

	void finishProcess(Long id, Long insId, String processOpinion, int caseStaus);

}