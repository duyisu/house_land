/**
 * @filename:SiteSupervisionService 2019年9月4日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.entity.SiteSupervision;
/**   
 *  
 * @Description:  现场监督——SERVICE
 * @Author:       lhy   
 * @CreateDate:   2019年9月4日
 * @Version:      V1.0
 *    
 */
public interface SiteSupervisionService {
	
	/**
	 * @explain 查询现场监督对象
	 * @param   对象参数：id
	 * @return  SiteSupervision
	 * @author  lhy
	 */
	public SiteSupervision findById(Long id);
	
	/**
	 * @explain 删除现场监督对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 */
	public int deleteById(Long id);
	
	/**
	 * @explain 添加现场监督对象
	 * @param   对象参数：SiteSupervision
	 * @return  int
	 * @author  lhy
	 */
	public int add(SiteSupervision siteSupervision);
	
	/**
	 * @explain 修改现场监督对象
	 * @param   对象参数：SiteSupervision
	 * @return  int
	 * @author  lhy
	 */
	public int update(SiteSupervision siteSupervision);
	
	/**
	 * @explain 查询现场监督集合
	 * @param   对象参数：SiteSupervision
	 * @return  List<SiteSupervision>
	 * @author  lhy
	 */
	public List<SiteSupervision> querySiteSupervisionList(SiteSupervision siteSupervision);
	
	/**
	 * @explain 分页查询现场监督
	 * @param   对象参数：SiteSupervision
	 * @return  PageInfo<SiteSupervision>
	 * @author  lhy
	 */
	public PageInfo<SiteSupervision> list(AppPage<SiteSupervision> page);
}