/**
 * @filename:BuildApplyFileService 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.entity.BuildApplyFile;
import com.lhy.houseflow.entity.vo.TaskFileIds;
/**   
 *  
 * @Description:  建房申请附件表——SERVICE
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
public interface BuildApplyFileService {
	
	/**
	 * @explain 查询建房申请附件表对象
	 * @param   对象参数：id
	 * @return  BuildApplyFile
	 * @author  lhy
	 */
	public BuildApplyFile findById(Long id);
	
	/**
	 * @explain 删除建房申请附件表对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 */
	public int deleteById(Long id);
	
	/**
	 * @explain 添加建房申请附件表对象
	 * @param   对象参数：BuildApplyFile
	 * @return  int
	 * @author  lhy
	 */
	public int add(TaskFileIds taskFileIds);
	
	/**
	 * @explain 修改建房申请附件表对象
	 * @param   对象参数：BuildApplyFile
	 * @return  int
	 * @author  lhy
	 */
	public int update(BuildApplyFile buildApplyFile);
	
	/**
	 * @explain 查询建房申请附件表集合
	 * @param   对象参数：BuildApplyFile
	 * @return  List<BuildApplyFile>
	 * @author  lhy
	 */
	public List<BuildApplyFile> queryBuildApplyFileList(BuildApplyFile buildApplyFile);
	
	/**
	 * @explain 分页查询建房申请附件表
	 * @param   对象参数：BuildApplyFile
	 * @return  PageDataVo<BuildApplyFile>
	 * @author  lhy
	 */
	public PageInfo<BuildApplyFile> list(AppPage<BuildApplyFile> page);
}