/**
 * @filename:CaseRecordFlowTaskFileService 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.entity.CaseRecordFlowTaskFile;
/**   
 *  
 * @Description:  两违流程附件表——SERVICE
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
public interface CaseRecordFlowTaskFileService {
    
	
	/**
	 * @explain 查询两违流程附件表对象
	 * @param   对象参数：id
	 * @return  CaseRecordFlowTaskFile
	 * @author  lhy
	 */
	public CaseRecordFlowTaskFile findById(Long id);
	
	/**
	 * @explain 删除两违流程附件表对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 */
	public int deleteById(Long id);
	
	/**
	 * @explain 添加两违流程附件表对象
	 * @param   对象参数：CaseRecordFlowTaskFile
	 * @return  int
	 * @author  lhy
	 */
	public int add(CaseRecordFlowTaskFile caseRecordFlowTaskFile);
	
	/**
	 * @explain 修改两违流程附件表对象
	 * @param   对象参数：CaseRecordFlowTaskFile
	 * @return  int
	 * @author  lhy
	 */
	public int update(CaseRecordFlowTaskFile caseRecordFlowTaskFile);
	
	/**
	 * @explain 查询两违流程附件表集合
	 * @param   对象参数：CaseRecordFlowTaskFile
	 * @return  List<CaseRecordFlowTaskFile>
	 * @author  lhy
	 */
	public List<CaseRecordFlowTaskFile> queryCaseRecordFlowTaskFileList(CaseRecordFlowTaskFile caseRecordFlowTaskFile);
	
	/**
	 * @explain 分页查询两违流程附件表
	 * @param   对象参数：CaseRecordFlowTaskFile
	 * @return  PageDataVo<CaseRecordFlowTaskFile>
	 * @author  lhy
	 */
	public PageInfo<CaseRecordFlowTaskFile> list(AppPage<CaseRecordFlowTaskFile> page);
}