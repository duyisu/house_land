/**
 * @filename:WfDefinitionService 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.entity.WfDefinition;
/**   
 *  
 * @Description:  工作流定义表——SERVICE
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
public interface WfDefinitionService {
	
	/**
	 * @explain 查询工作流定义表对象
	 * @param   对象参数：id
	 * @return  WfDefinition
	 * @author  lhy
	 */
	public WfDefinition findById(Long id);
	
	/**
	 * @explain 删除工作流定义表对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 */
	public int deleteById(Long id);
	
	/**
	 * @explain 添加工作流定义表对象
	 * @param   对象参数：WfDefinition
	 * @return  int
	 * @author  lhy
	 */
	public int add(WfDefinition wfDefinition);
	
	/**
	 * @explain 修改工作流定义表对象
	 * @param   对象参数：WfDefinition
	 * @return  int
	 * @author  lhy
	 */
	public int update(WfDefinition wfDefinition);
	
	/**
	 * @explain 查询工作流定义表集合
	 * @param   对象参数：WfDefinition
	 * @return  List<WfDefinition>
	 * @author  lhy
	 */
	public List<WfDefinition> queryWfDefinitionList(WfDefinition wfDefinition);
	
	/**
	 * @explain 分页查询工作流定义表
	 * @param   对象参数：WfDefinition
	 * @return  PageInfo<WfDefinition>
	 * @author  lhy
	 */
	public PageInfo<WfDefinition> list(AppPage<WfDefinition> page);
}