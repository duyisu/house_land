/**
 * @filename:CarSchedulingAreaService 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.entity.CarSchedulingArea;
/**   
 *  
 * @Description:  车辆排班——SERVICE
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
public interface CarSchedulingAreaService {
	
	/**
	 * @explain 查询车辆排班对象
	 * @param   对象参数：id
	 * @return  CarSchedulingArea
	 * @author  lhy
	 */
	public CarSchedulingArea findById(Long id);
	
	/**
	 * @explain 删除车辆排班对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 */
	public int deleteById(Long id);
	
	/**
	 * @explain 添加车辆排班对象
	 * @param   对象参数：CarSchedulingArea
	 * @return  int
	 * @author  lhy
	 */
	public int add(CarSchedulingArea carSchedulingArea);
	
	/**
	 * @explain 修改车辆排班对象
	 * @param   对象参数：CarSchedulingArea
	 * @return  int
	 * @author  lhy
	 */
	public int update(CarSchedulingArea carSchedulingArea);
	
	/**
	 * @explain 查询车辆排班集合
	 * @param   对象参数：CarSchedulingArea
	 * @return  List<CarSchedulingArea>
	 * @author  lhy
	 */
	public List<CarSchedulingArea> queryCarSchedulingAreaList(CarSchedulingArea carSchedulingArea);
	
	/**
	 * @explain 分页查询车辆排班
	 * @param   对象参数：CarSchedulingArea
	 * @return  PageDataVo<CarSchedulingArea>
	 * @author  lhy
	 */
	public PageInfo<CarSchedulingArea> list(AppPage<CarSchedulingArea> page);
}