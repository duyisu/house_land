/**
 * @filename:CarSchedulingService 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.entity.CarScheduling;
/**   
 *  
 * @Description:  车辆排班——SERVICE
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
public interface CarSchedulingService {
	
	/**
	 * @explain 查询车辆排班对象
	 * @param   对象参数：id
	 * @return  CarScheduling
	 * @author  lhy
	 */
	public CarScheduling findById(Long id);
	
	/**
	 * @explain 删除车辆排班对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 */
	public int deleteById(Long id);
	
	/**
	 * @explain 添加车辆排班对象
	 * @param   对象参数：CarScheduling
	 * @return  int
	 * @author  lhy
	 */
	public int add(CarScheduling carScheduling);
	
	/**
	 * @explain 修改车辆排班对象
	 * @param   对象参数：CarScheduling
	 * @return  int
	 * @author  lhy
	 */
	public int update(CarScheduling carScheduling);
	
	/**
	 * @explain 查询车辆排班集合
	 * @param   对象参数：CarScheduling
	 * @return  List<CarScheduling>
	 * @author  lhy
	 */
	public List<CarScheduling> queryCarSchedulingList(CarScheduling carScheduling);
	
	/**
	 * @explain 分页查询车辆排班
	 * @param   对象参数：CarScheduling
	 * @return  PageDataVo<CarScheduling>
	 * @author  lhy
	 */
	public PageInfo<CarScheduling> list(AppPage<CarScheduling> page);
}