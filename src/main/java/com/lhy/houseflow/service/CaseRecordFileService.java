/**
 * @filename:CaseRecordFileService 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.entity.CaseRecordFile;
/**   
 *  
 * @Description:  立案登记附件表——SERVICE
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
public interface CaseRecordFileService {
	
	/**
	 * @explain 查询立案登记附件表对象
	 * @param   对象参数：id
	 * @return  CaseRecordFile
	 * @author  lhy
	 */
	public CaseRecordFile findById(Long id);
	
	/**
	 * @explain 删除立案登记附件表对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 */
	public int deleteById(Long id);
	
	/**
	 * @explain 添加立案登记附件表对象
	 * @param   对象参数：CaseRecordFile
	 * @return  int
	 * @author  lhy
	 */
	public int add(CaseRecordFile caseRecordFile);
	
	/**
	 * @explain 修改立案登记附件表对象
	 * @param   对象参数：CaseRecordFile
	 * @return  int
	 * @author  lhy
	 */
	public int update(CaseRecordFile caseRecordFile);
	
	/**
	 * @explain 查询立案登记附件表集合
	 * @param   对象参数：CaseRecordFile
	 * @return  List<CaseRecordFile>
	 * @author  lhy
	 */
	public List<CaseRecordFile> queryCaseRecordFileList(CaseRecordFile caseRecordFile);
	
	/**
	 * @explain 分页查询立案登记附件表
	 * @param   对象参数：CaseRecordFile
	 * @return  PageDataVo<CaseRecordFile>
	 * @author  lhy
	 */
	public PageInfo<CaseRecordFile> list(AppPage<CaseRecordFile> page);
}