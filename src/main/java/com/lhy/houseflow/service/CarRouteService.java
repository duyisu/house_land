/**
 * @filename:CarRouteService 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.entity.CarRoute;
/**   
 *  
 * @Description:  车辆轨迹表——SERVICE
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
public interface CarRouteService {
	
	/**
	 * @explain 查询车辆轨迹表对象
	 * @param   对象参数：id
	 * @return  CarRoute
	 * @author  lhy
	 */
	public CarRoute findById(Long id);
	
	/**
	 * @explain 删除车辆轨迹表对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 */
	public int deleteById(Long id);
	
	/**
	 * @explain 添加车辆轨迹表对象
	 * @param   对象参数：CarRoute
	 * @return  int
	 * @author  lhy
	 */
	public int add(CarRoute carRoute);
	
	/**
	 * @explain 修改车辆轨迹表对象
	 * @param   对象参数：CarRoute
	 * @return  int
	 * @author  lhy
	 */
	public int update(CarRoute carRoute);
	
	/**
	 * @explain 查询车辆轨迹表集合
	 * @param   对象参数：CarRoute
	 * @return  List<CarRoute>
	 * @author  lhy
	 */
	public List<CarRoute> queryCarRouteList(CarRoute carRoute);
	
	/**
	 * @explain 分页查询车辆轨迹表
	 * @param   对象参数：CarRoute
	 * @return  PageInfo<CarRoute>
	 * @author  lhy
	 */
	public PageInfo<CarRoute> getCarRouteBySearch(AppPage<CarRoute> page);
}