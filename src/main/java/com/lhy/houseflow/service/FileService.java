package com.lhy.houseflow.service;

import javax.servlet.http.HttpServletResponse;

import org.springframework.web.multipart.MultipartFile;

import com.lhy.houseflow.entity.UploadFile;

public interface FileService {

//    UploadFile fileUpload(String subPath,MultipartFile file) throws Exception;

    void downLoadFile(UploadFile fileinfo, HttpServletResponse response);

    UploadFile fileUpload(Integer businessKind, Long businessId, Integer businessSubKind, MultipartFile file)
        throws Exception;

}