/**
 * @filename:SysPermissionService 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.entity.SysPermission;
/**   
 *  
 * @Description:  权限——SERVICE
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
public interface SysPermissionService {
	
	/**
	 * @explain 查询权限对象
	 * @param   对象参数：id
	 * @return  SysPermission
	 * @author  lhy
	 */
	public SysPermission findById(Long id);
	
	/**
	 * @explain 删除权限对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 */
	public int deleteById(Long id);
	
	/**
	 * @explain 添加权限对象
	 * @param   对象参数：SysPermission
	 * @return  int
	 * @author  lhy
	 */
	public int add(SysPermission sysPermission);
	
	/**
	 * @explain 修改权限对象
	 * @param   对象参数：SysPermission
	 * @return  int
	 * @author  lhy
	 */
	public int update(SysPermission sysPermission);
	
	/**
	 * @explain 查询权限集合
	 * @param   对象参数：SysPermission
	 * @return  List<SysPermission>
	 * @author  lhy
	 */
	public List<SysPermission> querySysPermissionList(SysPermission sysPermission);
	
	/**
	 * @explain 分页查询权限
	 * @param   对象参数：SysPermission
	 * @return  PageDataVo<SysPermission>
	 * @author  lhy
	 */
	public PageInfo<SysPermission> list(AppPage<SysPermission> page);

    List<SysPermission> findByUsername(String username);

}