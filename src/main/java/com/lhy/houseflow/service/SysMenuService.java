/**
 * @filename:SysMenuService 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.entity.SysMenu;
import com.lhy.houseflow.entity.vo.MenuPermissionVo;
import com.lhy.houseflow.entity.vo.MenuRoute;
import com.lhy.houseflow.entity.vo.MenuVo;
import com.lhy.houseflow.entity.vo.SimpleTreeData;
/**   
 *  
 * @Description:  菜单资源——SERVICE
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
public interface SysMenuService {
	
	/**
	 * @explain 查询菜单资源对象
	 * @param   对象参数：id
	 * @return  SysMenu
	 * @author  lhy
	 */
	public SysMenu findById(Long id);
	
	/**
	 * @explain 删除菜单资源对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 */
	public int deleteById(Long id);
	
	/**
	 * @explain 添加菜单资源对象
	 * @param   对象参数：SysMenu
	 * @return  int
	 * @author  lhy
	 */
	public int add(SysMenu sysMenu);
	
	/**
	 * @explain 修改菜单资源对象
	 * @param   对象参数：SysMenu
	 * @return  int
	 * @author  lhy
	 */
	public int update(SysMenu sysMenu);
	
	/**
	 * @explain 查询菜单资源集合
	 * @param   对象参数：SysMenu
	 * @return  List<SysMenu>
	 * @author  lhy
	 */
	public List<SysMenu> querySysMenuList(SysMenu sysMenu);
	
	/**
	 * @explain 分页查询菜单资源
	 * @param   对象参数：SysMenu
	 * @return  PageDataVo<SysMenu>
	 * @author  lhy
	 */
	public PageInfo<SysMenu> list(AppPage<SysMenu> page);

    List<MenuVo> buildTreeBySysMenus(List<SysMenu> list);

    List<MenuRoute> buildMenuRoute(List<MenuVo> tree);

    List<SimpleTreeData> getSimpleTreeData(List<MenuVo> tree);
    
    List<MenuPermissionVo> getMenuPermissions();
}