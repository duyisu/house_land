/**
 * @filename:CaseRecordService 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.entity.CaseRecord;
/**   
 *  
 * @Description:  立案登记——SERVICE
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
public interface CaseRecordService {
	
	/**
	 * @explain 查询立案登记对象
	 * @param   对象参数：id
	 * @return  CaseRecord
	 * @author  lhy
	 */
	public CaseRecord findById(Long id);
	
	/**
	 * @explain 删除立案登记对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 */
	public int deleteById(Long id);
	
	/**
	 * @explain 添加立案登记对象
	 * @param   对象参数：CaseRecord
	 * @return  int
	 * @author  lhy
	 */
	public int add(CaseRecord caseRecord);
	
	/**
	 * @explain 修改立案登记对象
	 * @param   对象参数：CaseRecord
	 * @return  int
	 * @author  lhy
	 */
	public int update(CaseRecord caseRecord);
	
	/**
	 * @explain 查询立案登记集合
	 * @param   对象参数：CaseRecord
	 * @return  List<CaseRecord>
	 * @author  lhy
	 */
	public List<CaseRecord> queryCaseRecordList(CaseRecord caseRecord);
	
	/**
	 * @explain 分页查询立案登记
	 * @param   对象参数：CaseRecord
	 * @return  PageDataVo<CaseRecord>
	 * @author  lhy
	 */
	public PageInfo<CaseRecord> list(AppPage<CaseRecord> page);
}