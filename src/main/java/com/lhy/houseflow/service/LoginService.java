package com.lhy.houseflow.service;

import com.lhy.houseflow.entity.SysUser;

public interface LoginService {

	SysUser login(String userName, String password);

}