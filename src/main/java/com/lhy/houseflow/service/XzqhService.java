/**
 * @filename:XzqhService 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.entity.Xzqh;
/**   
 *  
 * @Description:  地区——SERVICE
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
public interface XzqhService {
	
	/**
	 * @explain 查询地区对象
	 * @param   对象参数：id
	 * @return  Xzqh
	 * @author  lhy
	 */
	public Xzqh findById(Long id);
	
	/**
	 * @explain 删除地区对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 */
	public int deleteById(Long id);
	
	/**
	 * @explain 添加地区对象
	 * @param   对象参数：Xzqh
	 * @return  int
	 * @author  lhy
	 */
	public int add(Xzqh xzqh);
	
	/**
	 * @explain 修改地区对象
	 * @param   对象参数：Xzqh
	 * @return  int
	 * @author  lhy
	 */
	public int update(Xzqh xzqh);
	
	/**
	 * @explain 查询地区集合
	 * @param   对象参数：Xzqh
	 * @return  List<Xzqh>
	 * @author  lhy
	 */
	public List<Xzqh> queryXzqhList(Xzqh xzqh);
	
	/**
	 * @explain 分页查询地区
	 * @param   对象参数：Xzqh
	 * @return  PageInfo<Xzqh>
	 * @author  lhy
	 */
	public PageInfo<Xzqh> list(AppPage<Xzqh> page);
}