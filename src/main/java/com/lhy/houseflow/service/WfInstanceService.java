/**
 * @filename:WfInstanceService 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.entity.WfInstance;
/**   
 *  
 * @Description:  工作流实例表——SERVICE
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
public interface WfInstanceService {
	
	/**
	 * @explain 查询工作流实例表对象
	 * @param   对象参数：id
	 * @return  WfInstance
	 * @author  lhy
	 */
	public WfInstance findById(Long id);
	
	/**
	 * @explain 删除工作流实例表对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 */
	public int deleteById(Long id);
	
	/**
	 * @explain 添加工作流实例表对象
	 * @param   对象参数：WfInstance
	 * @return  int
	 * @author  lhy
	 */
	public int add(WfInstance wfInstance);
	
	/**
	 * @explain 修改工作流实例表对象
	 * @param   对象参数：WfInstance
	 * @return  int
	 * @author  lhy
	 */
	public int update(WfInstance wfInstance);
	
	/**
	 * @explain 查询工作流实例表集合
	 * @param   对象参数：WfInstance
	 * @return  List<WfInstance>
	 * @author  lhy
	 */
	public List<WfInstance> queryWfInstanceList(WfInstance wfInstance);
	
	/**
	 * @explain 分页查询工作流实例表
	 * @param   对象参数：WfInstance
	 * @return  PageInfo<WfInstance>
	 * @author  lhy
	 */
	public PageInfo<WfInstance> list(AppPage<WfInstance> page);
}