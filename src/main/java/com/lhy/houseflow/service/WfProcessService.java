/**
 * @filename:WfProcessService 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.entity.WfProcess;
/**   
 *  
 * @Description:  实例处理任务表——SERVICE
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
public interface WfProcessService {
	
	/**
	 * @explain 查询实例处理任务表对象
	 * @param   对象参数：id
	 * @return  WfProcess
	 * @author  lhy
	 */
	public WfProcess findById(Long id);
	
	/**
	 * @explain 删除实例处理任务表对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 */
	public int deleteById(Long id);
	
	/**
	 * @explain 添加实例处理任务表对象
	 * @param   对象参数：WfProcess
	 * @return  int
	 * @author  lhy
	 */
	public int add(WfProcess wfProcess);
	
	/**
	 * @explain 修改实例处理任务表对象
	 * @param   对象参数：WfProcess
	 * @return  int
	 * @author  lhy
	 */
	public int update(WfProcess wfProcess);
	
	/**
	 * @explain 查询实例处理任务表集合
	 * @param   对象参数：WfProcess
	 * @return  List<WfProcess>
	 * @author  lhy
	 */
	public List<WfProcess> queryWfProcessList(WfProcess wfProcess);
	
	/**
	 * @explain 分页查询实例处理任务表
	 * @param   对象参数：WfProcess
	 * @return  PageInfo<WfProcess>
	 * @author  lhy
	 */
	public PageInfo<WfProcess> list(AppPage<WfProcess> page);
}