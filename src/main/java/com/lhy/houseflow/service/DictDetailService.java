/**
 * @filename:DictDetailService 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service;

import java.util.List;
import java.util.Map;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.entity.DictDetail;
import com.lhy.houseflow.entity.vo.NameValue;
import com.lhy.houseflow.entity.vo.NameValueMap;
/**   
 *  
 * @Description:  字典明细——SERVICE
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
public interface DictDetailService {
	
	/**
	 * @explain 查询字典明细对象
	 * @param   对象参数：id
	 * @return  DictDetail
	 * @author  lhy
	 */
	public DictDetail findById(Long id);
	
	/**
	 * @explain 删除字典明细对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 */
	public int deleteById(Long id);
	
	/**
	 * @explain 添加字典明细对象
	 * @param   对象参数：DictDetail
	 * @return  int
	 * @author  lhy
	 */
	public int add(DictDetail dictDetail);
	
	/**
	 * @explain 修改字典明细对象
	 * @param   对象参数：DictDetail
	 * @return  int
	 * @author  lhy
	 */
	public int update(DictDetail dictDetail);
	
	/**
	 * @explain 查询字典明细集合
	 * @param   对象参数：DictDetail
	 * @return  List<DictDetail>
	 * @author  lhy
	 */
	public List<DictDetail> queryDictDetailList(DictDetail dictDetail);
	
	/**
	 * @explain 分页查询字典明细
	 * @param   对象参数：DictDetail
	 * @return  PageInfo<DictDetail>
	 * @author  lhy
	 */
	public PageInfo<DictDetail> list(AppPage<DictDetail> page);
	
    PageInfo<DictDetail> queryByDictName(String dictName, AppPage<DictDetail> page);

    NameValueMap queryMapByDictName(String dictName);
}