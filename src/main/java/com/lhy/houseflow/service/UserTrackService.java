/**
 * @filename:UserTrackService 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.entity.UserTrack;
/**   
 *  
 * @Description:  人员轨迹——SERVICE
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
public interface UserTrackService {
	
	/**
	 * @explain 查询人员轨迹对象
	 * @param   对象参数：id
	 * @return  UserTrack
	 * @author  lhy
	 */
	public UserTrack findById(Long id);
	
	/**
	 * @explain 删除人员轨迹对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 */
	public int deleteById(Long id);
	
	/**
	 * @explain 添加人员轨迹对象
	 * @param   对象参数：UserTrack
	 * @return  int
	 * @author  lhy
	 */
	public int add(UserTrack userTrack);
	
	/**
	 * @explain 修改人员轨迹对象
	 * @param   对象参数：UserTrack
	 * @return  int
	 * @author  lhy
	 */
	public int update(UserTrack userTrack);
	
	/**
	 * @explain 查询人员轨迹集合
	 * @param   对象参数：UserTrack
	 * @return  List<UserTrack>
	 * @author  lhy
	 */
	public List<UserTrack> queryUserTrackList(UserTrack userTrack);
	
	/**
	 * @explain 分页查询人员轨迹
	 * @param   对象参数：UserTrack
	 * @return  PageDataVo<UserTrack>
	 * @author  lhy
	 */
	public PageInfo<UserTrack> list(AppPage<UserTrack> page);
}