/**
 * @filename:MenuService 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.entity.Menu;
/**   
 *  
 * @Description:  菜单资源表——SERVICE
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
public interface MenuService {
	
	/**
	 * @explain 查询菜单资源表对象
	 * @param   对象参数：id
	 * @return  Menu
	 * @author  lhy
	 */
	public Menu findById(Long id);
	
	/**
	 * @explain 删除菜单资源表对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 */
	public int deleteById(Long id);
	
	/**
	 * @explain 添加菜单资源表对象
	 * @param   对象参数：Menu
	 * @return  int
	 * @author  lhy
	 */
	public int add(Menu menu);
	
	/**
	 * @explain 修改菜单资源表对象
	 * @param   对象参数：Menu
	 * @return  int
	 * @author  lhy
	 */
	public int update(Menu menu);
	
	/**
	 * @explain 查询菜单资源表集合
	 * @param   对象参数：Menu
	 * @return  List<Menu>
	 * @author  lhy
	 */
	public List<Menu> queryMenuList(Menu menu);
	
	/**
	 * @explain 分页查询菜单资源表
	 * @param   对象参数：Menu
	 * @return  PageInfo<Menu>
	 * @author  lhy
	 */
	public PageInfo<Menu> getMenuBySearch(AppPage<Menu> page);
}