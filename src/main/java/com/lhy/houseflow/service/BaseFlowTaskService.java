package com.lhy.houseflow.service;

import java.util.List;
import java.util.Map;

import org.flowable.bpmn.model.UserTask;
import org.flowable.engine.history.HistoricActivityInstance;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.engine.task.Comment;
import org.flowable.task.api.Task;

import com.lhy.houseflow.entity.JwtUser;
import com.lhy.houseflow.entity.SysUser;
import com.lhy.houseflow.entity.TaskUserInfo;

public interface BaseFlowTaskService {
    
    String getBusinessKey(Long businessId);

    ProcessInstance createTask(String userName, String processDefinitionKey, Long businessId);

    /**
     * 待处理，未签收或者已分配
     */
    List<Task> getNeedClaimTaskByUser(String processDefinitionKey, String taskDefinitionKey, JwtUser user);

    /**
     * 我的办理
     */
    List<HistoricActivityInstance> getHistoryTaskByUser(String processDefinitionKey, String taskDefinitionKey,
        String userName, int firstIdx, int pageSize);

    /**
     * 签收任务
     *
     * @param userName
     * @param taskId
     */
    void claimTask(String userName, String taskId);

    /**
     * 委派任务
     *
     * @param userName
     * @param taskId
     */
    void delegateTask(String userName, String taskId);
    
    /**
     * 转办任务
     *
     * @param userName
     * @param taskId
     */
    void turnTask(String userName, String taskId, String trunUserName);        

    /**
     * 完成任务
     */
    void complete(String taskId, Map<String, Object> variables);
    
    /**
     *退回
     */
    void backTask(String taskId, String backTaskId, Map<String, Object> variables);      

    boolean isAssigneeOrCandidate(SysUser user, String taskId);

    Task checkTaskFromTaskId(String taskId);
    
    Comment addComment(String taskId, String processInstanceId, String type, String message);
    
    /** Update a comment to a task and/or process instance. */
    void saveComment(Comment comment);
     
    /**
     * Returns an individual comment with the given id. Returns null if no comment exists with the given id.
     */
    Comment getComment(String commentId);
     
    /** Removes all comments from the provided task and/or process instance */
    void deleteComments(String taskId, String processInstanceId);

    List<TaskUserInfo> findNextTaskUserInfosByTaskId(String taskId);

    Task taskId(String taskId);

    List<UserTask> getUserTaskFormTypes(String processDefinitionId);

}