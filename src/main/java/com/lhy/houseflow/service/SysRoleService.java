/**
 * @filename:SysRoleService 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.entity.SysRole;
import com.lhy.houseflow.entity.dto.RoleDto;
/**   
 *  
 * @Description:  角色——SERVICE
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
public interface SysRoleService {
	
	/**
	 * @explain 查询角色对象
	 * @param   对象参数：id
	 * @return  SysRole
	 * @author  lhy
	 */
	public SysRole findById(Long id);
	
	/**
	 * @explain 删除角色对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 */
	public int deleteById(Long id);
	
	/**
	 * @explain 添加角色对象
	 * @param   对象参数：SysRole
	 * @return  int
	 * @author  lhy
	 */
	public int add(RoleDto sysRole);
	
	/**
	 * @explain 修改角色对象
	 * @param   对象参数：SysRole
	 * @return  int
	 * @author  lhy
	 */
	public int update(RoleDto sysRole);
	
	/**
	 * @explain 查询角色集合
	 * @param   对象参数：SysRole
	 * @return  List<SysRole>
	 * @author  lhy
	 */
	public List<SysRole> querySysRoleList(SysRole sysRole);
	
	/**
	 * @explain 分页查询角色
	 * @param   对象参数：SysRole
	 * @return  PageDataVo<SysRole>
	 * @author  lhy
	 */
	public PageInfo<SysRole> list(AppPage<SysRole> page);

    PageInfo<RoleDto> getRoles(AppPage<String> page);
    
    RoleDto findRoleDtoById(Long id);
}