/**
 * @filename:CarServiceImpl 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.Car;
import com.lhy.houseflow.dao.CarDao;
import com.lhy.houseflow.service.CarService;

/**   
 *  
 * @Description:  车辆——SERVICEIMPL
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Service
@Transactional(rollbackFor=Exception.class)
@Slf4j
public class CarServiceImpl implements CarService {
	
	@Autowired
	public CarDao carDao;
	
	//查询对象
	@Override
	public Car findById(Long id) {
		return carDao.findById(id);
	}
	
	//删除对象
	@Override
	public int deleteById(Long id) {
		return carDao.deleteById(id);
	}
	
	//添加对象
	@Override
	public int add(Car car) {
		return carDao.add(car);
	}
	
	//修改对象
	@Override
	public int update(Car car) {
		return carDao.update(car);
	}
	
	//查询集合
	@Override
	public List<Car> queryCarList(Car car) {
		return carDao.queryCarList(car);
	}
	
	//分页查询
	@Override
	public PageInfo<Car> list(AppPage<Car> page) {
		PageHelper.startPage(page.getPageNum(),page.getPageSize());
		List<Car> list = null;
		if (page.getParam() == null)
			list=carDao.list();		
		else
			list=carDao.queryCarList(page.getParam());
		PageInfo<Car> pageInfo = new PageInfo<Car>(list);
		return pageInfo;
	}
}