/**
 * @filename:AreaServiceImpl 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.Area;
import com.lhy.houseflow.dao.AreaDao;
import com.lhy.houseflow.service.AreaService;

/**   
 *  
 * @Description:  区域——SERVICEIMPL
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Service
@Transactional(rollbackFor=Exception.class)
@Slf4j
public class AreaServiceImpl implements AreaService {
	
	@Autowired
	public AreaDao areaDao;
	
	//查询对象
	@Override
	public Area findById(Long id) {
		return areaDao.findById(id);
	}
	
	//删除对象
	@Override
	public int deleteById(Long id) {
		return areaDao.deleteById(id);
	}
	
	//添加对象
	@Override
	public int add(Area area) {
		return areaDao.add(area);
	}
	
	//修改对象
	@Override
	public int update(Area area) {
		return areaDao.update(area);
	}
	
	//查询集合
	@Override
	public List<Area> queryAreaList(Area area) {
		return areaDao.queryAreaList(area);
	}
	
	//分页查询
	@Override
	public PageInfo<Area> list(AppPage<Area> page) {
		PageHelper.startPage(page.getPageNum(),page.getPageSize());
		List<Area> list = null;
		if (page.getParam() == null)
			list=areaDao.list();		
		else
			list=areaDao.queryAreaList(page.getParam());
		PageInfo<Area> pageInfo = new PageInfo<Area>(list);
		return pageInfo;
	}
}