/**
 * @filename:SiteSupervisionServiceImpl 2019年9月4日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.SiteSupervision;
import com.lhy.houseflow.dao.SiteSupervisionDao;
import com.lhy.houseflow.service.SiteSupervisionService;

/**   
 *  
 * @Description:  现场监督——SERVICEIMPL
 * @Author:       lhy   
 * @CreateDate:   2019年9月4日
 * @Version:      V1.0
 *    
 */
@Service
@Transactional(rollbackFor=Exception.class)
@Slf4j
public class SiteSupervisionServiceImpl implements SiteSupervisionService {
	
	@Autowired
	public SiteSupervisionDao siteSupervisionDao;
	
	//查询对象
	@Override
	public SiteSupervision findById(Long id) {
		return siteSupervisionDao.findById(id);
	}
	
	//删除对象
	@Override
	public int deleteById(Long id) {
		return siteSupervisionDao.deleteById(id);
	}
	
	//添加对象
	@Override
	public int add(SiteSupervision siteSupervision) {
		return siteSupervisionDao.add(siteSupervision);
	}
	
	//修改对象
	@Override
	public int update(SiteSupervision siteSupervision) {
		return siteSupervisionDao.update(siteSupervision);
	}
	
	//查询集合
	@Override
	public List<SiteSupervision> querySiteSupervisionList(SiteSupervision siteSupervision) {
		return siteSupervisionDao.querySiteSupervisionList(siteSupervision);
	}
	
	//分页查询
	@Override
	public PageInfo<SiteSupervision> list(AppPage<SiteSupervision> page) {
		PageHelper.startPage(page.getPageNum(),page.getPageSize());
		List<SiteSupervision> list = null;
		if (page.getParam() == null)
			list=siteSupervisionDao.list();		
		else
			list=siteSupervisionDao.querySiteSupervisionList(page.getParam());
		PageInfo<SiteSupervision> pageInfo = new PageInfo<SiteSupervision>(list);
		return pageInfo;
	}
}