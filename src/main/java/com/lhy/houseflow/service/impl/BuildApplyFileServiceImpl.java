/**
 * @filename:BuildApplyFileServiceImpl 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.dao.BuildApplyFileDao;
import com.lhy.houseflow.entity.BuildApplyFile;
import com.lhy.houseflow.entity.vo.TaskFileIds;
import com.lhy.houseflow.service.BuildApplyFileService;

import lombok.extern.slf4j.Slf4j;

/**   
 *  
 * @Description:  建房申请附件表——SERVICEIMPL
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Service
@Transactional(rollbackFor=Exception.class)
@Slf4j
public class BuildApplyFileServiceImpl implements BuildApplyFileService {
	
	@Autowired
	public BuildApplyFileDao buildApplyFileDao;
	
	//查询对象
	@Override
	public BuildApplyFile findById(Long id) {
		return buildApplyFileDao.findById(id);
	}
	
	//删除对象
	@Override
	public int deleteById(Long id) {
		return buildApplyFileDao.deleteById(id);
	}
	
	//添加对象
	@Override
	public int add(TaskFileIds taskFileIds) {
	    if (taskFileIds == null || taskFileIds.getFileIds()==null || taskFileIds.getFileIds().isEmpty())
	        return 0;
	    for (Long fileId: taskFileIds.getFileIds()){
	        BuildApplyFile info = new BuildApplyFile();
	        info.setApplyId(taskFileIds.getId());
	        info.setEnabled(1);
	        info.setFileId(fileId);
	        info.setFlowFile(taskFileIds.isFlowFile()? 1: 0);
	        buildApplyFileDao.add(info);
	    }
		return taskFileIds.getFileIds().size();
	}
	
	//修改对象
	@Override
	public int update(BuildApplyFile buildApplyFile) {
		return buildApplyFileDao.update(buildApplyFile);
	}
	
	//查询集合
	@Override
	public List<BuildApplyFile> queryBuildApplyFileList(BuildApplyFile buildApplyFile) {
		return buildApplyFileDao.queryBuildApplyFileList(buildApplyFile);
	}
	
	//分页查询
	@Override
	public PageInfo<BuildApplyFile> list(AppPage<BuildApplyFile> page) {
		PageHelper.startPage(page.getPageNum(),page.getPageSize());
		List<BuildApplyFile> list = null;
		if (page.getParam() == null)
			list=buildApplyFileDao.list();		
		else
			list=buildApplyFileDao.queryBuildApplyFileList(page.getParam());
		PageInfo<BuildApplyFile> pageInfo = new PageInfo<BuildApplyFile>(list);
		return pageInfo;
	}
}