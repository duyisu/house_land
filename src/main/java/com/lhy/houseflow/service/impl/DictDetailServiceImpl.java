/**
 * @filename:DictDetailServiceImpl 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.dao.DictDetailDao;
import com.lhy.houseflow.entity.DictDetail;
import com.lhy.houseflow.entity.EntityUtils;
import com.lhy.houseflow.entity.vo.NameValueMap;
import com.lhy.houseflow.service.DictDetailService;

import lombok.extern.slf4j.Slf4j;

/**   
 *  
 * @Description:  字典明细——SERVICEIMPL
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Service
@Transactional(rollbackFor=Exception.class)
@Slf4j
public class DictDetailServiceImpl implements DictDetailService {
	
	@Autowired
	public DictDetailDao dictDetailDao;
	
	//查询对象
	@Override
	public DictDetail findById(Long id) {
		return dictDetailDao.findById(id);
	}
	
	//删除对象
	@Override
	public int deleteById(Long id) {
		return dictDetailDao.deleteById(id);
	}
	
	//添加对象
	@Override
	public int add(DictDetail dictDetail) {
		return dictDetailDao.add(dictDetail);
	}
	
	//修改对象
	@Override
	public int update(DictDetail dictDetail) {
		return dictDetailDao.update(dictDetail);
	}
	
	//查询集合
	@Override
	public List<DictDetail> queryDictDetailList(DictDetail dictDetail) {
		return dictDetailDao.queryDictDetailList(dictDetail);
	}
	
	//分页查询
	@Override
	public PageInfo<DictDetail> list(AppPage<DictDetail> page) {
		PageHelper.startPage(page.getPageNum(),page.getPageSize());
		List<DictDetail> list = null;
		if (page.getParam() == null)
			list=dictDetailDao.list();		
		else
			list=dictDetailDao.queryDictDetailList(page.getParam());
		PageInfo<DictDetail> pageInfo = new PageInfo<DictDetail>(list);
		return pageInfo;
	}

    @Override
    public PageInfo<DictDetail> queryByDictName(String dictName, AppPage<DictDetail> page) {
        PageHelper.startPage(page.getPageNum(),page.getPageSize());
        List<DictDetail> list=dictDetailDao.queryByDictName(dictName);
        PageInfo<DictDetail> pageInfo = new PageInfo<DictDetail>(list);
        return pageInfo;
    }
    
    @Override
    public NameValueMap queryMapByDictName(String dictName){
        NameValueMap map = new NameValueMap();
        List<DictDetail> list=dictDetailDao.queryByDictName(dictName);
        for (DictDetail item: list){
            map.put(EntityUtils.createNameValueVo(item));
        }
        return map;
    }
}