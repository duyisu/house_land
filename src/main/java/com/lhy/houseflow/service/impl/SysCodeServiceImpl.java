/**
 * @filename:SysCodeServiceImpl 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service.impl;

import java.util.Calendar;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lhy.houseflow.dao.SysCodeDao;
import com.lhy.houseflow.entity.SysCode;
import com.lhy.houseflow.service.SysCodeService;

import lombok.extern.slf4j.Slf4j;

/**   
 *  
 * @Description:  系统编号——SERVICEIMPL
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Service
@Transactional(rollbackFor=Exception.class)
@Slf4j
public class SysCodeServiceImpl implements SysCodeService {

    private static final String KEY_CASE = "CASE_SN";
    private static final String KEY_CASE_RECORD = "CASE_RECORD_SN";
    private static final int SN_LENGTH=1000000;

    @Autowired
    public SysCodeDao sysCodeDao;

    private String getCodeStringByKey(String key) {
        Calendar cal = Calendar.getInstance();
        int year = cal.get(Calendar.YEAR);
        int month = cal.get(Calendar.MONTH);
        int day = cal.get(Calendar.DATE);
        long lday = year * 10000 + month * 100 + day;
        long sn = lday * SN_LENGTH;
        SysCode sysCode = sysCodeDao.findByKeyAndDate(key, Long.valueOf(lday));
        if (sysCode == null) {
            sysCode = new SysCode();
            sysCode.setCodeDate(lday);
            sysCode.setCodeKey(key);
            sysCode.setCurrCode(1L);
            sysCodeDao.add(sysCode);
        } else {
            sysCodeDao.updateAddCode(key, lday);
            sn = sn + sysCode.getCurrCode();
        }
        sn++;
        return Long.toString(sn);
    }

    @Override
    public String getCaseSn() {
        return getCodeStringByKey(KEY_CASE);
    }

    @Override
    public String getCaseRecordSn() {
        return getCodeStringByKey(KEY_CASE_RECORD);
    }
}