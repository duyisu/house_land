/**
 * @filename:WfDefinitionServiceImpl 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.WfDefinition;
import com.lhy.houseflow.dao.WfDefinitionDao;
import com.lhy.houseflow.service.WfDefinitionService;

/**   
 *  
 * @Description:  工作流定义表——SERVICEIMPL
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Service
@Transactional(rollbackFor=Exception.class)
@Slf4j
public class WfDefinitionServiceImpl implements WfDefinitionService {
	
	@Autowired
	public WfDefinitionDao wfDefinitionDao;
	
	//查询对象
	@Override
	public WfDefinition findById(Long id) {
		return wfDefinitionDao.findById(id);
	}
	
	//删除对象
	@Override
	public int deleteById(Long id) {
		return wfDefinitionDao.deleteById(id);
	}
	
	//添加对象
	@Override
	public int add(WfDefinition wfDefinition) {
		return wfDefinitionDao.add(wfDefinition);
	}
	
	//修改对象
	@Override
	public int update(WfDefinition wfDefinition) {
		return wfDefinitionDao.update(wfDefinition);
	}
	
	//查询集合
	@Override
	public List<WfDefinition> queryWfDefinitionList(WfDefinition wfDefinition) {
		return wfDefinitionDao.queryWfDefinitionList(wfDefinition);
	}
	
	//分页查询
	@Override
	public PageInfo<WfDefinition> list(AppPage<WfDefinition> page) {
		PageHelper.startPage(page.getPageNum(),page.getPageSize());
		List<WfDefinition> list = null;
		if (page.getParam() == null)
			list=wfDefinitionDao.list();		
		else
			list=wfDefinitionDao.queryWfDefinitionList(page.getParam());
		PageInfo<WfDefinition> pageInfo = new PageInfo<WfDefinition>(list);
		return pageInfo;
	}
}