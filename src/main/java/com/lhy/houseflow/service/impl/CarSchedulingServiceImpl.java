/**
 * @filename:CarSchedulingServiceImpl 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.CarScheduling;
import com.lhy.houseflow.dao.CarSchedulingDao;
import com.lhy.houseflow.service.CarSchedulingService;

/**   
 *  
 * @Description:  车辆排班——SERVICEIMPL
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Service
@Transactional(rollbackFor=Exception.class)
@Slf4j
public class CarSchedulingServiceImpl implements CarSchedulingService {
	
	@Autowired
	public CarSchedulingDao carSchedulingDao;
	
	//查询对象
	@Override
	public CarScheduling findById(Long id) {
		return carSchedulingDao.findById(id);
	}
	
	//删除对象
	@Override
	public int deleteById(Long id) {
		return carSchedulingDao.deleteById(id);
	}
	
	//添加对象
	@Override
	public int add(CarScheduling carScheduling) {
		return carSchedulingDao.add(carScheduling);
	}
	
	//修改对象
	@Override
	public int update(CarScheduling carScheduling) {
		return carSchedulingDao.update(carScheduling);
	}
	
	//查询集合
	@Override
	public List<CarScheduling> queryCarSchedulingList(CarScheduling carScheduling) {
		return carSchedulingDao.queryCarSchedulingList(carScheduling);
	}
	
	//分页查询
	@Override
	public PageInfo<CarScheduling> list(AppPage<CarScheduling> page) {
		PageHelper.startPage(page.getPageNum(),page.getPageSize());
		List<CarScheduling> list = null;
		if (page.getParam() == null)
			list=carSchedulingDao.list();		
		else
			list=carSchedulingDao.queryCarSchedulingList(page.getParam());
		PageInfo<CarScheduling> pageInfo = new PageInfo<CarScheduling>(list);
		return pageInfo;
	}
}