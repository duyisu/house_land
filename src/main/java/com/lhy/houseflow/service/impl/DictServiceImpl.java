/**
 * @filename:DictServiceImpl 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.Dict;
import com.lhy.houseflow.dao.DictDao;
import com.lhy.houseflow.service.DictService;

/**   
 *  
 * @Description:  字典类别——SERVICEIMPL
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Service
@Transactional(rollbackFor=Exception.class)
@Slf4j
public class DictServiceImpl implements DictService {
	
	@Autowired
	public DictDao dictDao;
	
	//查询对象
	@Override
	public Dict findById(Long id) {
		return dictDao.findById(id);
	}
	
	//删除对象
	@Override
	public int deleteById(Long id) {
		return dictDao.deleteById(id);
	}
	
	//添加对象
	@Override
	public int add(Dict dict) {
		return dictDao.add(dict);
	}
	
	//修改对象
	@Override
	public int update(Dict dict) {
		return dictDao.update(dict);
	}
	
	//查询集合
	@Override
	public List<Dict> queryDictList(Dict dict) {
		return dictDao.queryDictList(dict);
	}
	
	//分页查询
	@Override
	public PageInfo<Dict> list(AppPage<Dict> page) {
		PageHelper.startPage(page.getPageNum(),page.getPageSize());
		List<Dict> list = null;
		if (page.getParam() == null)
			list=dictDao.list();		
		else
			list=dictDao.queryDictList(page.getParam());
		PageInfo<Dict> pageInfo = new PageInfo<Dict>(list);
		return pageInfo;
	}
}