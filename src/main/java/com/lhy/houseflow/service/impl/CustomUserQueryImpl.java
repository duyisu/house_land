package com.lhy.houseflow.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.flowable.common.engine.impl.interceptor.CommandContext;
import org.flowable.idm.api.User;
import org.flowable.idm.engine.impl.UserQueryImpl;
import org.flowable.idm.engine.impl.persistence.entity.UserEntity;
import org.flowable.idm.engine.impl.persistence.entity.UserEntityImpl;
import org.springframework.beans.factory.annotation.Autowired;

import com.lhy.houseflow.dao.SysUserDao;
import com.lhy.houseflow.entity.SysUser;

public class CustomUserQueryImpl extends UserQueryImpl {
    /**
     * 
     */
    private static final long serialVersionUID = -2792208976517367164L;
    
    @Autowired SysUserDao sysUserDao;

    @Override
    public long executeCount(CommandContext commandContext) {
        return executeQuery().size();
    }

    @Override
    public List<User> executeList(CommandContext commandContext) {
        return executeQuery();
    }

    protected List<User> executeQuery() {
        if (getId() != null) {
            List<User> result = new ArrayList<User>();
            result.add(findById(getId()));
            return result;
        } else if (getIdIgnoreCase() != null) {
            List<User> result = new ArrayList<User>();
            result.add(findById(getIdIgnoreCase()));
            return result;
        } else if (getFullNameLike() != null) {
            return executeNameQuery(getFullNameLike());
        } else if (getFullNameLikeIgnoreCase() != null) {
            return executeNameQuery(getFullNameLikeIgnoreCase());
        } else {
            return executeAllUserQuery();
        }
    }

    private List<User> executeAllUserQuery() {
        return createUsersFromSysUsers(sysUserDao.list());
    }

    private List<User> executeNameQuery(String fullNameLike) {
         List<SysUser> sysUsers = sysUserDao.list();
        List<User> users = new ArrayList<User>();
        for (SysUser sysUser: sysUsers){
            if (StringUtils.containsIgnoreCase(sysUser.getRealName(), fullNameLike))
            users.add(createUserFromSysUser(sysUser));
        }
        
        return users;
    }

    private User findById(String id) {
        SysUser sysUser = sysUserDao.findByUsername(id);
        return createUserFromSysUser(sysUser);
    }
    
    private UserEntity createUserFromSysUser(SysUser sysUser){
        if (sysUser==null) return null;
        UserEntity user = new UserEntityImpl();
        user.setId(sysUser.getUserName());
        user.setDisplayName(sysUser.getRealName());
        user.setFirstName("");
        user.setLastName(sysUser.getRealName());
        
        return user;
    }
    
    private List<User> createUsersFromSysUsers(List<SysUser> sysUsers){
        if (sysUsers==null) return null;
        List<User> users = new ArrayList<User>();
        for (SysUser sysUser: sysUsers){
            users.add(createUserFromSysUser(sysUser));
        }
        
        return users;
    }    

}
