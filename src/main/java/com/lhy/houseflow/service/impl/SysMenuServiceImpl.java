/**
 * @filename:SysMenuServiceImpl 2019年5月5日
 * @project HouseFlow V1.0 Copyright(c) 2018 lhy Co. Ltd. All right reserved.
 */
package com.lhy.houseflow.service.impl;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.dao.SysMenuDao;
import com.lhy.houseflow.dao.SysPermissionDao;
import com.lhy.houseflow.entity.SysMenu;
import com.lhy.houseflow.entity.SysPermission;
import com.lhy.houseflow.entity.vo.MenuMetaVo;
import com.lhy.houseflow.entity.vo.MenuPermissionVo;
import com.lhy.houseflow.entity.vo.MenuRoute;
import com.lhy.houseflow.entity.vo.MenuVo;
import com.lhy.houseflow.entity.vo.SimpleTreeData;
import com.lhy.houseflow.service.SysMenuService;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @Description: 菜单资源——SERVICEIMPL
 * @Author: lhy
 * @CreateDate: 2019年5月5日
 * @Version: V1.0
 * 
 */

@Service
@Transactional(rollbackFor = Exception.class)
@Slf4j
public class SysMenuServiceImpl implements SysMenuService {
    private static final String LAYOUT = "Layout";
    private static final String INDEX = "index";

    @Autowired
    public SysMenuDao sysMenuDao;
    @Autowired
    public SysPermissionDao sysPermisssionDao;

    // 查询对象
    @Override
    public SysMenu findById(Long id) {
        return sysMenuDao.findById(id);
    }

    // 删除对象
    @Override
    public int deleteById(Long id) {
        return sysMenuDao.deleteById(id);
    }

    // 添加对象
    @Override
    public int add(SysMenu sysMenu) {
        return sysMenuDao.add(sysMenu);
    }

    // 修改对象
    @Override
    public int update(SysMenu sysMenu) {
        return sysMenuDao.update(sysMenu);
    }

    // 查询集合
    @Override
    public List<SysMenu> querySysMenuList(SysMenu sysMenu) {
        return sysMenuDao.querySysMenuList(sysMenu);
    }

    // 分页查询
    @Override
    public PageInfo<SysMenu> list(AppPage<SysMenu> page) {
        List<SysMenu> list = null;
        if (page == null || page.getParam() == null) {
            list = sysMenuDao.list();
        } else {
            PageHelper.startPage(page.getPageNum(), page.getPageSize());

            list = sysMenuDao.querySysMenuList(page.getParam());
        }
        PageInfo<SysMenu> pageInfo = new PageInfo<SysMenu>(list);
        return pageInfo;
    }

    @Override
    public List<SimpleTreeData> getSimpleTreeData(List<MenuVo> tree) {
        List<SimpleTreeData> list = new ArrayList<SimpleTreeData>();
        for (MenuVo item : tree) {
            SimpleTreeData data = new SimpleTreeData(item.getId(), item.getName(), null);
            if (item.getChildren() != null && !item.getChildren().isEmpty()) {
                data.setChildren(getSimpleTreeData(item.getChildren()));
            }
            list.add(data);
        }
        return list;
    }

    @Override
    public List<MenuVo> buildTreeBySysMenus(List<SysMenu> list) {
        List<MenuVo> treeList = new ArrayList<MenuVo>();
        for (SysMenu menu : list) {
            MenuVo item = new MenuVo();
            BeanUtils.copyProperties(menu, item);
            treeList.add(item);
        }
        return buildTreeByNullChildren(treeList);
    }

    private List<MenuVo> buildTreeByNullChildren(List<MenuVo> treeList) {
        List<MenuVo> tree = new ArrayList<MenuVo>();
        for (MenuVo item : treeList) {
            if (item.isRoot()) {
                tree.add(item);
            } else {
                MenuVo parent = bsearchWithoutRecursion(treeList, item.getPid());
                if (parent != null) {
                    parent.getChildren().add(item);
                }
            }
        }
        sortTree(tree);
        return tree;
    }

    @Override
    public List<MenuRoute> buildMenuRoute(List<MenuVo> tree) {
        List<MenuRoute> list = new LinkedList<MenuRoute>();

        for (MenuVo item : tree) {
            MenuRoute menuRoute = new MenuRoute();
            String path = item.getPath();
            if (path == null)
                path = "";

            menuRoute.setName(item.getName());
            menuRoute.setPath(path);
            menuRoute.setMeta(new MenuMetaVo(item.getName(), item.getIco()));
            if (item.getPid() == null || item.getPid().equals(0L)) {
                menuRoute.setPath("/" + path);

                menuRoute.setComponent(StringUtils.isEmpty(item.getComponent()) ? LAYOUT : item.getComponent());
            } else if (StringUtils.isNoneEmpty(item.getComponent())) {
                menuRoute.setComponent(item.getComponent());
            }

            List<MenuVo> childs = item.getChildren();
            if (childs != null && childs.size() > 0) {
                menuRoute.setAlwaysShow(true);
                menuRoute.setRedirect("noredirect");
                menuRoute.setChildren(buildMenuRoute(childs));
            } else if (item.getPid() == null || item.getPid().equals(0L)) {
                MenuRoute menuRoute1 = new MenuRoute();
                menuRoute1.setMeta(menuRoute.getMeta());
                menuRoute1.setPath(INDEX);
                menuRoute1.setName(menuRoute.getName());
                menuRoute1.setComponent(menuRoute.getComponent());

                menuRoute.setName(null);
                menuRoute.setMeta(null);
                menuRoute.setComponent(LAYOUT);
                List<MenuRoute> list1 = new ArrayList<MenuRoute>();
                list1.add(menuRoute1);
                menuRoute.setChildren(list1);
            }

            list.add(menuRoute);

        }

        return list;
    }

    private void sortTree(List<MenuVo> list) {
        if (list == null || list.isEmpty())
            return;
        for (MenuVo item : list) {
            sortTree(item.getChildren());
        }
        list.sort((a, b) -> a.getSort() - b.getSort());

    }

    private MenuVo bsearchWithoutRecursion(List<MenuVo> list, Long parentId) {
        int low = 0;
        int high = list.size() - 1;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            int p = Long.compare(list.get(mid).getId(), parentId);
            if (p > 0)
                high = mid - 1;
            else if (p < 0)
                low = mid + 1;
            else
                return list.get(mid);
        }
        return null;
    }

    @Override
    public List<MenuPermissionVo> getMenuPermissions() {
        List<SysMenu> sysMenus = sysMenuDao.list();
        sysMenus.sort((a, b) -> a.getSort() - b.getSort());
        List<SysPermission> permissions = sysPermisssionDao.list();
        Map<Long, List<SysPermission>> menuIdPermissionMap = new HashMap<Long,  List<SysPermission>>();
        for (SysPermission p: permissions){
            List<SysPermission> list = menuIdPermissionMap.get(p.getMenuId());
            if (list == null){
                list = new ArrayList<SysPermission>();
                menuIdPermissionMap.put(p.getMenuId(), list);
            }
            list.add(p);
        }
        
        List<MenuVo> menuVos = new ArrayList<MenuVo>();
        for (SysMenu sysMenu: sysMenus){
            MenuPermissionVo menuPermission = new MenuPermissionVo();
            BeanUtils.copyProperties(sysMenu, menuPermission);
            menuPermission.setPermissions(menuIdPermissionMap.get(sysMenu.getId()));            
            menuVos.add(menuPermission);
        }        
        
        menuVos = this.buildTreeByNullChildren(menuVos);
        List<MenuPermissionVo> result= new ArrayList<MenuPermissionVo>();
        for (MenuVo menuVo: menuVos){
            result.add((MenuPermissionVo)menuVo)  ;
        }
        

        return result;
    }
}