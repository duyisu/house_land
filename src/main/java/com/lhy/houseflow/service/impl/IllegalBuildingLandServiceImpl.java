package com.lhy.houseflow.service.impl;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.flowable.engine.history.HistoricActivityInstance;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.task.api.Task;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.constant.FlowConst;
import com.lhy.houseflow.dao.CaseDefendantDao;
import com.lhy.houseflow.dao.CaseRecordDao;
import com.lhy.houseflow.dao.CaseRecordFileDao;
import com.lhy.houseflow.dao.CaseRecordFlowTaskDao;
import com.lhy.houseflow.dao.SysUserDao;
import com.lhy.houseflow.dao.SysUserRoleDao;
import com.lhy.houseflow.dao.WfDefinitionDao;
import com.lhy.houseflow.dao.WfDefinitionStepDao;
import com.lhy.houseflow.dao.WfDefinitionSteprelaDao;
import com.lhy.houseflow.dao.WfInstanceDao;
import com.lhy.houseflow.dao.WfProcessDao;
import com.lhy.houseflow.entity.BuildApply;
import com.lhy.houseflow.entity.CaseDefendant;
import com.lhy.houseflow.entity.CaseRecord;
import com.lhy.houseflow.entity.CaseRecordFile;
import com.lhy.houseflow.entity.CaseRecordFlowTask;
import com.lhy.houseflow.entity.EntityUtils;
import com.lhy.houseflow.entity.JwtUser;
import com.lhy.houseflow.entity.SysRoleMenu;
import com.lhy.houseflow.entity.SysUser;
import com.lhy.houseflow.entity.SysUserRole;
import com.lhy.houseflow.entity.TaskUserInfo;
import com.lhy.houseflow.entity.WfDefinitionStep;
import com.lhy.houseflow.entity.WfDefinitionSteprela;
import com.lhy.houseflow.entity.WfInstance;
import com.lhy.houseflow.entity.WfProcess;
import com.lhy.houseflow.entity.vo.CaseProcessWorkVo;
import com.lhy.houseflow.entity.vo.CaseRecordBaseInfoVo;
import com.lhy.houseflow.entity.vo.CaseRecordInfoVo;
import com.lhy.houseflow.entity.vo.CaseRecordVo;
import com.lhy.houseflow.entity.vo.FlowProcessVo;
import com.lhy.houseflow.entity.vo.NameValueMap;
import com.lhy.houseflow.entity.vo.TaskVo;
import com.lhy.houseflow.service.BaseFlowTaskService;
import com.lhy.houseflow.service.DictDetailService;
import com.lhy.houseflow.service.IllegalBuildingLandService;
import com.lhy.houseflow.service.SysCodeService;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @Description: 两违流程——SERVICEIMPL
 * @Author: lhy
 * @CreateDate: 2019年8月13日
 * @Version: V1.0
 * 
 */
@Service
@Transactional(rollbackFor = Exception.class)
@Slf4j
public class IllegalBuildingLandServiceImpl implements IllegalBuildingLandService {
    private static final String PROCESS_KEY = "IllegalBuildingLand";
    @Autowired
    private CaseRecordDao caseRecordDao;
    @Autowired
    private SysCodeService sysCodeService;
    @Autowired
    private CaseDefendantDao caseDefendantDao;
    @Autowired
    CaseRecordFlowTaskDao caseRecordFlowTaskDao;
    @Autowired
    CaseRecordFileDao caseRecordFileDao;
    @Autowired
    private DictDetailService dictDetailDao;

    @Autowired
    private BaseFlowTaskService baseFlowTaskService;
    
    @Autowired
    private WfDefinitionSteprelaDao wfDefRelaDao;
    @Autowired
    private WfDefinitionStepDao wfDefStepDao;
    @Autowired
    private SysUserRoleDao userRoleDao;
    @Autowired
    private SysUserDao userDao;
    @Autowired
    private WfInstanceDao insDao;
    @Autowired
    private WfProcessDao processDao;
    

    /* (non-Javadoc)
     * @see com.lhy.houseflow.service.impl.IllegalBuildingLandService#createTask(java.lang.String, com.lhy.houseflow.entity.CaseRecord, java.util.List)
     */

    @Override
    public CaseRecord addCaseRecord(CaseRecord info, List<CaseDefendant> defendants, List<CaseRecordFile> files) {
        WfDefinitionStep startupStep = this.findStartUpStep(FlowConst.DEF_ILLEGA_TWO);
        Date curDate = new Date();
    	WfInstance ins = new WfInstance();
        ins.setDefId(FlowConst.DEF_ILLEGA_TWO);
        ins.setStatus(FlowConst.INS_StartUp); //草稿
        if(info.getStatus() != 0 && info.getStatus() != null && info.getStatus() == FlowConst.CASE_STATUS_PROCESSING) {
        	ins.setStatus(FlowConst.INS_WaitingForSignature);
        }
        ins.setStepId(startupStep.getStepId());
        ins.setCreator(new Long(info.getReportUserId()).intValue());
        ins.setCreatetime(curDate);
        ins.setFromuser(new Long(info.getReportUserId()).intValue());
        ins.setFromtime(curDate);
        insDao.add(ins);
        
        Long insId = ins.getInsId();
        
        WfProcess process = new WfProcess();
        process.setInsId(insId);
        process.setStepId(startupStep.getStepId());
        process.setFromuser(new Long(info.getReportUserId()).intValue());
        process.setFromtime(curDate);
        process.setProcessstatus(FlowConst.PROCESS_STATUS_NotOpen);
        process.setProcesstype(FlowConst.PROCESS_TYPE_Process);
        if(info.getStatus() != null && info.getStatus() == FlowConst.CASE_STATUS_PROCESSING) {
        	process.setTouser(new Long(info.getReportUserId()).intValue());
            process.setTotime(curDate);
            process.setProcessstatus(FlowConst.PROCESS_STATUS_Finished);
            process.setFinishtime(curDate);
        }
        processDao.add(process);
        
        if(info.getStatus() != null && info.getStatus() == FlowConst.CASE_STATUS_PROCESSING) {
        	List<WfDefinitionStep> nextStep = this.findNextSteps(startupStep.getStepId()) ;
        	if(nextStep.size()>0) {
        		for(WfDefinitionStep s: nextStep) {
        			Long nextStepId = s.getStepId();
        			List<SysUser> toUserList = this.wfToUser(nextStepId);
    				for(SysUser u: toUserList) {
    					WfProcess nextProcess = new WfProcess();
    					nextProcess.setStepId(nextStepId);
    					nextProcess.setLastprocessId(process.getProcessId());
    					nextProcess.setFromtime(new Date());
    					nextProcess.setInsId(process.getInsId());
    					nextProcess.setFromuser(process.getTouser());
    					nextProcess.setProcessstatus(FlowConst.PROCESS_STATUS_NotOpen);
    					nextProcess.setProcesstype(process.getProcesstype());
    					nextProcess.setTouser(new Long(u.getId()).intValue());
    					processDao.add(nextProcess);
    				}
        		}
        		
        	}
        }
        info.setProcessInstanceId(insId);
    	String sn = sysCodeService.getCaseSn();
        info.setCaseNo(sn);
        sn = sysCodeService.getCaseRecordSn();
        info.setCaseRecordNo(sn);
        //info.setStatus(FlowConst.CASE_STATUS_DRAFT);
        caseRecordDao.add(info);
        if (defendants != null) {
            for (CaseDefendant caseDefendant : defendants) {
                caseDefendant.setCaseId(info.getId());
                caseDefendantDao.add(caseDefendant);
            }
        }
        if (files != null) {
            for (CaseRecordFile file : files) {
                file.setCaseId(info.getId());
                caseRecordFileDao.add(file);
            }
        }

        return info;
    }

    @Override
    public CaseRecord updateCaseRecord(CaseRecord info, List<CaseDefendant> defendants, List<CaseRecordFile> files) {
        caseRecordDao.update(info);
        if (defendants != null) {
            for (CaseDefendant caseDefendant : defendants) {
                if (caseDefendant.getId() == null || caseDefendant.getId() == 0) {
                    caseDefendantDao.add(caseDefendant);
                } else
                    caseDefendantDao.update(caseDefendant);
            }
        }
        if (files != null) {
            for (CaseRecordFile file : files) {
                CaseRecordFile aFile = caseRecordFileDao.findByCaseIdAndFileId(info.getId(), file.getFileId());
                if (aFile == null) {
                    caseRecordFileDao.add(file);
                } else {
                    caseRecordFileDao.update(file);
                }
            }
        }

        return info;
    }

    @Override
    public ProcessInstance createTask(String userName, Long caseId) {
        ProcessInstance processInstance = baseFlowTaskService.createTask(userName, PROCESS_KEY, caseId);
        caseRecordDao.updateProcessInstanceId(processInstance.getId(), caseId);
        return processInstance;
    }

    /* (non-Javadoc)
     * @see com.lhy.houseflow.service.impl.IllegalBuildingLandService#myTask(java.lang.String)
     */

    @Override
    public List<TaskVo<CaseRecordBaseInfoVo>> myTask(JwtUser user, String reportName, String defendantName,
        Integer sources, Integer caseType, Integer caseSubtype, Long areaId, String addr) {
        List<Task> tasks = baseFlowTaskService.getNeedClaimTaskByUser(PROCESS_KEY, null, user);
        return createTaskVos(tasks, reportName, defendantName, sources, caseType, caseSubtype, areaId, addr);
    }

    /* (non-Javadoc)
     * @see com.lhy.houseflow.service.impl.IllegalBuildingLandService#myHistory(java.lang.String, int, int)
     */

    @Override
    public List<HistoricActivityInstance> myHistory(String userName, int firstIdx, int pageSize) {
        return baseFlowTaskService.getHistoryTaskByUser(PROCESS_KEY, null, userName, firstIdx, pageSize);
    }

    /* (non-Javadoc)
     * @see com.lhy.houseflow.service.impl.IllegalBuildingLandService#claimTask(java.lang.String, java.lang.String)
     */
    @Override
    public void claimTask(String userName, String taskId) {
        baseFlowTaskService.claimTask(userName, taskId);
    }

    /* (non-Javadoc)
     * @see com.lhy.houseflow.service.impl.IllegalBuildingLandService#delegateTask(java.lang.String, java.lang.String)
     */
    @Override
    public void delegateTask(String userName, String taskId) {
        baseFlowTaskService.delegateTask(userName, taskId);
    }

    /* (non-Javadoc)
     * @see com.lhy.houseflow.service.impl.IllegalBuildingLandService#complete(java.lang.String, com.lhy.houseflow.entity.CaseRecordFlowTask)
     */
    @Override
    public void complete(String taskId, CaseRecordFlowTask info) {
        caseRecordFlowTaskDao.add(info);
        HashMap<String, Object> variables = getVariables(info);

        baseFlowTaskService.complete(taskId, variables);
    }

    private HashMap<String, Object> getVariables(CaseRecordFlowTask info) {
        HashMap<String, Object> variables = new HashMap<String, Object>();
        if (info == null)
            return variables;

        if (info.getOprType() != null) {
            variables.put(FlowConst.FLOW_OPR_TYPE, info.getOprType());
        }

        if (StringUtils.isNoneBlank(info.getOprDesc())) {
            variables.put(FlowConst.FLOW_OPR_NAME, info.getOprDesc());
        }

        return variables;

    }

    @Override
    public void turnTask(String userName, String taskId, String trunUserName) {
        baseFlowTaskService.turnTask(userName, taskId, trunUserName);

    }

    @Override
    public void backTask(String taskId, String backTaskId, CaseRecordFlowTask info) {
        baseFlowTaskService.backTask(taskId, backTaskId, null);

    }

    @Override
    public List<TaskUserInfo> findNextTaskUserInfosByTaskId(String taskId) {
        return baseFlowTaskService.findNextTaskUserInfosByTaskId(taskId);
    }

    private List<TaskVo<CaseRecordBaseInfoVo>> createTaskVos(List<Task> tasks, String reportName, String defendantName,
        Integer sources, Integer caseType, Integer caseSubtype, Long areaId, String addr) {
        List<TaskVo<CaseRecordBaseInfoVo>> list = new ArrayList<TaskVo<CaseRecordBaseInfoVo>>();
        if (tasks == null || tasks.isEmpty())
            return list;
        for (Task task : tasks) {
            TaskVo<CaseRecordBaseInfoVo> vo = createTaskVo(task);
            list.add(vo);
        }
        return list;
    }

    private TaskVo<CaseRecordBaseInfoVo> createTaskVo(Task task) {
        TaskVo<CaseRecordBaseInfoVo> vo = new TaskVo<CaseRecordBaseInfoVo>(task);
        CaseRecord caseRecord = caseRecordDao.findByTaskId(task.getId());
        NameValueMap sources = dictDetailDao.queryMapByDictName(FlowConst.DICT_CASE_SOURCE);
        NameValueMap caseTypes = dictDetailDao.queryMapByDictName(FlowConst.DICT_CASE_TYPE);
        NameValueMap caseSubtypes = dictDetailDao.queryMapByDictName(FlowConst.DICT_CASE_SUB_TYPE);
        List<CaseDefendant> defendants = caseDefendantDao.findByCaseId(caseRecord.getId());
        CaseRecordBaseInfoVo data =
            EntityUtils.createCaseRecordBaseInfoVo(caseRecord, sources, caseTypes, caseSubtypes, defendants);
        vo.setData(data);
        return vo;
    }

    @Override
    public CaseRecordInfoVo findCaseRecordInfo(Long caseId) {
        CaseRecord caseRecord = caseRecordDao.findById(caseId);
        NameValueMap sources = dictDetailDao.queryMapByDictName(FlowConst.DICT_CASE_SOURCE);
        NameValueMap caseTypes = dictDetailDao.queryMapByDictName(FlowConst.DICT_CASE_TYPE);
        NameValueMap caseSubtypes = dictDetailDao.queryMapByDictName(FlowConst.DICT_CASE_SUB_TYPE);
        List<CaseDefendant> defendants = caseDefendantDao.findByCaseId(caseRecord.getId());
        CaseRecordInfoVo data =   EntityUtils.createCaseRecordInfoVo(caseRecord, sources, caseTypes, caseSubtypes, defendants);
        return data;
    }

	@Override
	public WfDefinitionStep findStartUpStep(Long defId) {
		WfDefinitionStep step = new WfDefinitionStep();
		step.setDefId(defId);
		step.setStepType(FlowConst.DEF_STEP_TYPE_START);
		List<WfDefinitionStep> startupStepList = wfDefStepDao.queryWfDefinitionStepList(step);
		WfDefinitionStep startupStep = null;
		if (startupStepList.size() > 0) {
			startupStep = startupStepList.get(0);
		}
		return startupStep;
	}

	/**
	 * 查询下一步骤
	 */
	@Override
	public List<WfDefinitionStep> findNextSteps(Long stepId) {
		WfDefinitionSteprela wfDefinitionSteprela = new WfDefinitionSteprela();
		wfDefinitionSteprela.setPriorstepId(stepId);
		List<WfDefinitionSteprela> stepRelaList = wfDefRelaDao.queryWfDefinitionSteprelaList(wfDefinitionSteprela);
		List<WfDefinitionStep> stepList = new ArrayList<WfDefinitionStep>();
		if(stepRelaList.size() >0) {
			for(WfDefinitionSteprela r : stepRelaList) {
				stepList.add(wfDefStepDao.findById(r.getNextstepId()));
			}
		}
		return stepList;
	}

	/**
	 * 下一步人员列表
	 */
	@Override
	public List<SysUser> wfToUser(Long stepId) {
		List<SysUser> wfToUserList = new ArrayList<SysUser>();
		WfDefinitionStep step = wfDefStepDao.findById(stepId);
		if(step.getSelectusermode().equals(FlowConst.FLOW_SEL_USER_MODE_ROLE_USER)) { 
			SysUserRole sysUserRole = new SysUserRole();
			sysUserRole.setRoleId(Long.valueOf(step.getSelection()));
			List<SysUserRole> userRoleList = userRoleDao.querySysUserRoleList(sysUserRole);
			if(userRoleList.size() > 0) {
				for(SysUserRole userRole : userRoleList) {
					wfToUserList.add(userDao.findById(userRole.getUserId()));
				}
			}
		}
		return wfToUserList;
	}

	/**
	 * 签收
	 */
	@Override
	public void signProcess(Long userId, Long insId) {
		WfInstance toSignIns = insDao.findById(insId);
		toSignIns.setStatus(FlowConst.INS_Processing);
		insDao.update(toSignIns);
		WfProcess wfProcess = new WfProcess();
		wfProcess.setInsId(insId);
		wfProcess.setProcessstatus(FlowConst.PROCESS_STATUS_NotOpen);
		List<WfProcess> processList = processDao.queryWfProcessList(wfProcess);
		if(processList.size() > 0) {
			for (WfProcess p: processList) {
				if(p.getTouser().longValue() == userId) {
					p.setTotime(new Date());
					p.setProcessstatus(FlowConst.PROCESS_STATUS_Processing);
				} else {
					p.setProcessstatus(FlowConst.PROCESS_STATUS_Abandoned);
				}
				processDao.update(p);
			}
		}
	}

	/**
	 * 处理流程
	 */
	@Override
	public void handleProcess(Long userId, Long insId,Long currStepId,Long nextStepId,String processOpinion) {
		//WfDefinitionStep nextStep = wfDefStepDao.findById(nextStepId);
		WfInstance toHandleIns = insDao.findById(insId);
		if(toHandleIns.getStatus().equals(FlowConst.INS_StartUp)) {
			toHandleIns.setStatus(FlowConst.INS_Processing);
			WfProcess qryProcess = new WfProcess();
			qryProcess.setInsId(insId);
			List<WfProcess> wfProcessList = processDao.queryWfProcessList(qryProcess);
			for(WfProcess p: wfProcessList) {
				p.setTouser(new Long(userId).intValue());
				processDao.update(p);
			}
		}
		if(nextStepId == null || nextStepId == 0l) {
			List<WfDefinitionStep> nextStepList = this.findNextSteps(currStepId);
			if(nextStepList.size() > 0) {
				nextStepId = nextStepList.get(0).getStepId();
			}
		}
		toHandleIns.setStepId(nextStepId);
		insDao.update(toHandleIns);
		
		CaseRecord caseRecord = new CaseRecord();
		caseRecord.setProcessInstanceId(insId);
		List<CaseRecord> caseRecordList = caseRecordDao.queryCaseRecordList(caseRecord);
		if(caseRecordList.size() > 0) {
			for(CaseRecord c:caseRecordList) {
				c.setStatus(FlowConst.CASE_STATUS_PROCESSING);
				caseRecordDao.update(c);
			}
		}
		WfProcess qryProcess = new WfProcess();
		qryProcess.setInsId(insId);
		qryProcess.setTouser(new Long(userId).intValue());
		List<WfProcess> wfProcessList = processDao.queryWfProcessList(qryProcess);
		List<SysUser> userList = this.wfToUser(nextStepId);
		if(wfProcessList.size() > 0) {
			for(WfProcess p : wfProcessList) {
				p.setProcessstatus(FlowConst.PROCESS_STATUS_Finished);
				p.setOpinion(processOpinion);
				p.setFinishtime(new Date());
				processDao.update(p);
				for(SysUser u: userList) {
					WfProcess nextProcess = new WfProcess();
					nextProcess.setStepId(nextStepId);
					nextProcess.setLastprocessId(p.getProcessId());
					nextProcess.setFromtime(new Date());
					nextProcess.setInsId(p.getInsId());
					nextProcess.setFromuser(p.getTouser());
					nextProcess.setProcessstatus(FlowConst.PROCESS_STATUS_NotOpen);
					nextProcess.setProcesstype(p.getProcesstype());
					nextProcess.setTouser(new Long(u.getId()).intValue());
					processDao.add(nextProcess);
				}
			}
		}
		
	}

	/**
	 * 已完成的流程列表
	 */
	@Override
	public List<WfProcess> hasFinishProcess(Long insId) {
		WfProcess qryProcess = new WfProcess();
		qryProcess.setInsId(insId);
		qryProcess.setProcessstatus(FlowConst.PROCESS_STATUS_Finished);
		List<WfProcess> wfProcessList = processDao.queryWfProcessList(qryProcess);
		return wfProcessList;
	}

	/**
	 * 当前需要处理的流程
	 */
	@Override
	public List<WfProcess> curToHandleProcess(Long insId) {
		return processDao.unHandleProcessList(insId);
	}

	/**
	 * 我的待办
	 */
	@Override
	public List<WfProcess> myToHandleProcess(Long userId) {
		return processDao.findMyTodoProcess(userId);
	}

	/**
	 * 我的已办
	 */
	@Override
	public List<WfProcess> myHandledProcess(long userId) {
		return processDao.findMyDoneProcess(userId);
	}

	@Override
	public CaseRecordBaseInfoVo viewCaseRecord(Long insId) {
		CaseRecord item = new CaseRecord();
		item.setProcessInstanceId(insId);
		List<CaseRecord> caseRecordList = caseRecordDao.queryCaseRecordList(item);
		CaseRecordBaseInfoVo caseRecordInfo = new CaseRecordBaseInfoVo();
		if(caseRecordList.size() > 0) {
			BeanUtils.copyProperties(caseRecordList.get(0), caseRecordInfo);
		}
		//TODO 附件
		
		List<WfProcess> hasFinishProcessList = this.hasFinishProcess(insId);
		List<FlowProcessVo> hasFinishProcessVoList = new ArrayList<FlowProcessVo>();
		if(hasFinishProcessList.size() > 0) {
			for(WfProcess p: hasFinishProcessList) {
				hasFinishProcessVoList.add(copyProcessToVo(p));
			}
		}
		caseRecordInfo.setHasFinishProcessVoList(hasFinishProcessVoList);
		WfProcess curProcess = null;
		List<WfProcess> curProcessList = this.curToHandleProcess(insId);
		if(curProcessList.size() > 0) {
			curProcess = curProcessList.get(0);
			caseRecordInfo.setCurProcess(copyProcessToVo(curProcess));
			//是否显示签收 办结 等按钮
			if(curProcess.getProcessstatus().equals(FlowConst.PROCESS_STATUS_NotOpen)) {
				caseRecordInfo.setIsShowSignBtn(FlowConst.SHOW_TRUE);
				caseRecordInfo.setIsShowHandleBtn(FlowConst.SHOW_FALSE);
			}
			if(curProcess.getProcessstatus().equals(FlowConst.PROCESS_STATUS_Processing)) {
				caseRecordInfo.setIsShowSignBtn(FlowConst.SHOW_FALSE);
				caseRecordInfo.setIsShowHandleBtn(FlowConst.SHOW_TRUE);
			}
			WfDefinitionStep curStep = wfDefStepDao.findById(curProcess.getStepId());
			if(curStep.getStepType().equals(FlowConst.STEP_TYPE_FinishStepType)) {
				caseRecordInfo.setIsShowFinishBtn(FlowConst.SHOW_TRUE);
			}else {
				caseRecordInfo.setIsShowFinishBtn(FlowConst.SHOW_FALSE);
			}
		}
		
		return caseRecordInfo;
	}
	
	public FlowProcessVo copyProcessToVo (WfProcess p) {
		String strDateFormat = "yyyy-MM-dd HH:mm:ss";
        SimpleDateFormat sdf = new SimpleDateFormat(strDateFormat);
		FlowProcessVo processVo = new FlowProcessVo();
		if (p.getFinishtime() != null) {
			processVo.setFinishTimeStr(sdf.format(p.getFinishtime()));
		}
		if (p.getFromtime() != null) {
			processVo.setFromTimeStr(sdf.format(p.getFromtime()));
		}
		processVo.setFromUserId(new Long(p.getFromuser()));
		processVo.setFromUserName(userDao.findById(Long.valueOf(p.getFromuser())).getRealName());
		processVo.setInsId(p.getInsId());
		processVo.setProcessOpinion(p.getOpinion());
		String processStatus = p.getProcessstatus();
		String status = "";
		if (processStatus.equals(FlowConst.PROCESS_STATUS_NotOpen)){
			status = "待签收";
		}
		if (processStatus.equals(FlowConst.PROCESS_STATUS_Processing)){
			status = "处理中";
		}
		if (processStatus.equals(FlowConst.PROCESS_STATUS_Finished)){
			status = "已完成";
		}
		processVo.setProcessStatus(status);
		processVo.setStepId(p.getStepId());
		processVo.setStepName(wfDefStepDao.findById(p.getStepId()).getStepName());
		if(p.getTotime() != null) {
            processVo.setToTimeStr(sdf.format(p.getTotime()));
		}
		Long  userId = null;
		if (p.getTouser() != null){
		    userId = new Long(p.getTouser());
		    processVo.setToUserId(userId);
		    SysUser user = userDao.findById(userId);
		    if (user !=null)
		    processVo.setToUserName(user.getRealName());
		}
		
		return processVo;
	}

	@Override
	public PageInfo<CaseRecord> findMyCaseRecordWork(Long currUserId, String reportName, String complainant,
			Integer sources, Integer caseType, Integer caseSubtype, Long areaId, String addr, Integer page,
			Integer size) { //String defendantName
		PageHelper.startPage(page,size);
		CaseRecord caseRecord = new CaseRecord();
		/*
		 * List<CaseDefendant> defendants = null; if(defendantName != null) {
		 * CaseDefendant defendant = new CaseDefendant();
		 * defendant.setDefendantName(defendantName); defendants =
		 * caseDefendantDao.queryCaseDefendantList(defendant); } List<CaseRecord>
		 * defendantCaseList = new ArrayList<CaseRecord>();//根据当事人姓名查询相关案件 if(defendants
		 * != null && defendants.size() >0) { for(CaseDefendant d : defendants) {
		 * defendantCaseList.add(caseRecordDao.findById(d.getCaseId())); } }
		 */
		if(caseType != null && caseType != 0) {
			caseRecord.setCaseType(caseType);
		}
		if(caseSubtype != null && caseSubtype != 0) {
			caseRecord.setCaseSubtype(caseSubtype);
		}
		if(sources != null && sources != 0) {
			caseRecord.setSource(sources);
		}
		if(complainant != null) {
			caseRecord.setComplainant(complainant);
		}
		if(areaId != null && areaId != 0) {
			caseRecord.setAreaId(areaId);
		}
		if(addr != null) {
			caseRecord.setAddress(addr);
		}
		
		List<CaseRecord> list = caseRecordDao.queryCaseRecordList(caseRecord);
		/*
		 * if (defendantCaseList.size() > 0) { list.retainAll(defendantCaseList); //
		 * 取两个集合的交集 }
		 */
		PageInfo<CaseRecord> pageInfo = new PageInfo<CaseRecord>(list);
		return pageInfo;
	}
	
	public PageInfo<CaseRecord> myWorkCaseRecord(Long currUserId,String status) {
		CaseRecord caseRecord = new CaseRecord();
		List<CaseRecord> myCaseRecordList = new ArrayList<CaseRecord>(); 
		if(status.equals(FlowConst.MY_WORK_DRAFT)) { //草稿
			caseRecord.setStatus(FlowConst.CASE_STATUS_DRAFT);
			caseRecord.setReportUserId(currUserId);
			myCaseRecordList = caseRecordDao.queryCaseRecordList(caseRecord);
		}
		else if(status.equals(FlowConst.MY_WORK_TODO)) { //待办
			myCaseRecordList = this.findMyTodoCase(currUserId);
		}
		else if(status.equals(FlowConst.MY_WORK_HAS_DONE)) {
			myCaseRecordList = this.findMyHasDoneCase(currUserId);
		}
		PageInfo<CaseRecord> pageInfo = new PageInfo<CaseRecord>(myCaseRecordList);
		return pageInfo;
	}

 	public List<CaseProcessWorkVo> findAppTodo(Long userId) {
 		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
 		CaseProcessWorkVo caseVo = null;
 		List<CaseProcessWorkVo> myWorkTodoList = new ArrayList<CaseProcessWorkVo>();
 		List<WfProcess> processList = processDao.findMyTodoProcess(userId);
 		if(processList.size() > 0) {
 			for (WfProcess p :processList) {
 				String status = p.getProcessstatus();
 				Long stepId = p.getStepId();
 				CaseRecord c = new CaseRecord();
 				c.setProcessInstanceId(p.getInsId());
 				List<CaseRecord> cList = caseRecordDao.queryCaseRecordList(c);
 				if(cList.size() > 0) {
 					for(CaseRecord caseRecord : cList) {
 						caseVo = new CaseProcessWorkVo();
 						caseVo.setGmtCreate(sdf.format(caseRecord.getGmtCreate()));
 						caseVo.setAddress(caseRecord.getAddress());
 						caseVo.setCaseDesc(caseRecord.getCaseDesc());
 						caseVo.setCaseId(String.valueOf(caseRecord.getId()));
 						caseVo.setCaseRecordNo(caseRecord.getCaseRecordNo());
 						caseVo.setProcessId(String.valueOf(p.getProcessId()));
 						if(status.equals(FlowConst.PROCESS_STATUS_NotOpen)) {
 							caseVo.setCaseStatus("toSign");
 						}
 						if(status.equals(FlowConst.PROCESS_STATUS_Processing)) {
 							caseVo.setCaseStatus("toHandle");
 						}
 						caseVo.setProcessInstanceId(String.valueOf(caseRecord.getProcessInstanceId()));
 						caseVo.setStepId(String.valueOf(stepId));
 						myWorkTodoList.add(caseVo);
 					}
 				}
 			}
 		}
 		return myWorkTodoList;
 	}
 	
 	private List<CaseRecord> findMyTodoCase(Long userId) {
 		Set<CaseRecord> caseRecordSet = new HashSet<CaseRecord>();
 		List<WfProcess> processList = processDao.findMyTodoProcess(userId);
 		if(processList.size() > 0) {
 			for (WfProcess p :processList) {
 				CaseRecord c = new CaseRecord();
 				c.setProcessInstanceId(p.getInsId());
 				List<CaseRecord> cList = caseRecordDao.queryCaseRecordList(c);
 				caseRecordSet.addAll(cList);
 				//caseRecordList.addAll(cList);
 			}
 		}
 		List<CaseRecord> caseRecordList = new ArrayList<CaseRecord>();
 		caseRecordList.addAll(caseRecordSet);
 		return caseRecordList;
 	}
 	private List<CaseRecord> findMyHasDoneCase(Long userId) {
 		Set<CaseRecord> caseRecordSet = new HashSet<CaseRecord>();
 		List<WfProcess> processList = processDao.findMyDoneProcess(userId);
 		if(processList.size() > 0) {
 			for (WfProcess p :processList) {
 				CaseRecord c = new CaseRecord();
 				c.setProcessInstanceId(p.getInsId());
 				List<CaseRecord> cList = caseRecordDao.queryCaseRecordList(c);
 				caseRecordSet.addAll(cList);
 				//caseRecordList.addAll(cList);
 			}
 		}
 		List<CaseRecord> caseRecordList = new ArrayList<CaseRecord>();
 		caseRecordList.addAll(caseRecordSet);
 		return caseRecordList;
 	}

	@Override
	public void finishProcess(Long currUserId, Long insId, String processOpinion, int caseStatus) {
		CaseRecord b = new CaseRecord();
		b.setProcessInstanceId(insId);
		List<CaseRecord> caseRecordList = caseRecordDao.queryCaseRecordList(b);
		if(caseRecordList.size() > 0) {
			CaseRecord caseRecord = caseRecordList.get(0);
			caseRecord.setStatus(caseStatus);
			caseRecordDao.update(caseRecord);
		}
		WfInstance wfIns = insDao.findById(insId);
		if(caseStatus == FlowConst.CASE_STATUS_FINISH) {
			wfIns.setStatus(FlowConst.INS_Finished);
		}
		if(caseStatus == FlowConst.CASE_STATUS_ABORTED) {
			wfIns.setStatus(FlowConst.INS_Aborted);
		}
		insDao.update(wfIns);
		WfProcess wfProcess = new WfProcess();
		wfProcess.setInsId(insId);
		List<WfProcess> processList = processDao.unHandleProcessList(insId);
		if(processList.size() > 0) {
			for(WfProcess p : processList) {
				if(p.getTouser() == new Long(currUserId).intValue()) {
					p.setProcessstatus(FlowConst.PROCESS_STATUS_Finished);
					p.setOpinion(processOpinion);
					p.setFinishtime(new Date());
				}else {
					p.setProcessstatus(FlowConst.PROCESS_STATUS_Abandoned);
				}
				processDao.update(p);
			}
		}
		
	}
 	
}
