package com.lhy.houseflow.service.impl;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang3.StringUtils;
import org.flowable.bpmn.model.BpmnModel;
import org.flowable.bpmn.model.EndEvent;
import org.flowable.bpmn.model.FlowElement;
import org.flowable.bpmn.model.FlowNode;
import org.flowable.bpmn.model.SequenceFlow;
import org.flowable.bpmn.model.UserTask;
import org.flowable.engine.HistoryService;
import org.flowable.engine.IdentityService;
import org.flowable.engine.ProcessEngine;
import org.flowable.engine.RepositoryService;
import org.flowable.engine.RuntimeService;
import org.flowable.engine.TaskService;
import org.flowable.engine.history.HistoricActivityInstance;
import org.flowable.engine.impl.persistence.entity.ExecutionEntity;
import org.flowable.engine.runtime.ProcessInstance;
import org.flowable.engine.task.Comment;
import org.flowable.task.api.Task;
import org.flowable.task.api.TaskQuery;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.lhy.houseflow.common.exception.CustomException;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.dao.SysPermissionDao;
import com.lhy.houseflow.entity.JwtUser;
import com.lhy.houseflow.entity.SimpleUser;
import com.lhy.houseflow.entity.SysUser;
import com.lhy.houseflow.entity.TaskUserInfo;
import com.lhy.houseflow.service.BaseFlowTaskService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class BaseFlowTaskServiceImpl implements BaseFlowTaskService {

    @Autowired
    protected RuntimeService runtimeService;
    @Autowired
    protected TaskService taskService;
    @Autowired
    protected RepositoryService repositoryService;
    @Autowired
    protected ProcessEngine processEngine;
    @Autowired
    protected IdentityService identityService;
    @Autowired
    protected HistoryService historyService;
    @Autowired SysPermissionDao sysPermissionDao;

    public BaseFlowTaskServiceImpl() {
        super();
    }
    
    @Override
    public List<UserTask> getUserTaskFormTypes(String processDefinitionId){
        BpmnModel bpmnModel = repositoryService.getBpmnModel(processDefinitionId);
        org.flowable.bpmn.model.Process process = bpmnModel.getMainProcess();

        List<UserTask> tasks = process.findFlowElementsOfType(UserTask.class);
        tasks.sort(new Comparator<UserTask>(){

            @Override
            public int compare(UserTask o1, UserTask o2) {
                return StringUtils.compare(o1.getId(), o2.getId());
            }
            
        });
        return tasks;
    }
    /* (non-Javadoc)
     * @see com.lhy.houseflow.service.impl.BaseFlowTaskService#createTask(java.lang.Long, java.lang.String, java.lang.String)
     */
    @Override
    public ProcessInstance createTask(String userId, String processDefinitionKey, Long businessId) {
        identityService.setAuthenticatedUserId(userId);
        ProcessInstance processInstance =
            runtimeService.startProcessInstanceByKey(processDefinitionKey, getBusinessKey(businessId));
         identityService.setAuthenticatedUserId(null);
        return processInstance;
    }

    /* (non-Javadoc)
     * @see com.lhy.houseflow.service.impl.BaseFlowTaskService#getNeedClaimTaskByUser(java.lang.String, java.lang.String, java.lang.String)
     */
    @Override
    public List<Task> getNeedClaimTaskByUser(String processDefinitionKey, String taskDefinitionKey, JwtUser user) {
        TaskQuery query = taskService.createTaskQuery();
        if (StringUtils.isNotBlank(processDefinitionKey))
            query = query.processDefinitionKey(processDefinitionKey);
        if (StringUtils.isNotBlank(processDefinitionKey))
            query = query.taskDefinitionKey(taskDefinitionKey);
        List<Task> tasks = query.taskCandidateOrAssigned(user.getUsername()).taskCandidateUser(user.getUsername()).or()
            .taskCandidateGroupIn(user.getPermissions()).orderByTaskCreateTime().desc().list();        
 //       List<Task> tasks = query.taskCandidateOrAssigned(user.getUsername()).taskCandidateUser(user.getUsername()).or()
 //           .taskCandidateGroupIn(user.getPermissions()).orderByTaskCreateTime().desc().list();
        // taskService.getIdentityLinksForTask(taskId)
        return tasks;
    }

    /* (non-Javadoc)
     * @see com.lhy.houseflow.service.impl.BaseFlowTaskService#getHistoryTaskByUser(java.lang.String, java.lang.String, java.lang.String, int, int)
     */
    @Override
    public List<HistoricActivityInstance> getHistoryTaskByUser(String processDefinitionKey, String taskDefinitionKey,
        String userName, int firstIdx, int pageSize) {
        List<HistoricActivityInstance> tasks = historyService.createHistoricActivityInstanceQuery()
            // 我审批的
            .taskAssignee(userName)
            // 按照结束时间倒序
            .orderByHistoricActivityInstanceEndTime().desc()
            // 已结束的（其实就是判断有没有结束时间）
            .finished()
            // 分页
            .listPage(firstIdx, pageSize);

        return tasks;
    }

    /* (non-Javadoc)
     * @see com.lhy.houseflow.service.impl.BaseFlowTaskService#claimTask(java.lang.String, java.lang.String)
     */
    @Override
    public void claimTask(String userName, String taskId) {
        checkTaskFromTaskId(taskId);
        taskService.claim(taskId, userName);

    }

    /* (non-Javadoc)
     * @see com.lhy.houseflow.service.impl.BaseFlowTaskService#delegateTask(java.lang.String, java.lang.String)
     */
    @Override
    public void delegateTask(String userName, String taskId) {
        checkTaskFromTaskId(taskId);
        taskService.delegateTask(taskId, userName);
    }

    /* (non-Javadoc)
     * @see com.lhy.houseflow.service.impl.BaseFlowTaskService#complete(java.lang.String)
     */
    @Override
    public void complete(String taskId, Map<String, Object> variables) {
        taskService.complete(taskId, variables);

    }

    /* (non-Javadoc)
     * @see com.lhy.houseflow.service.impl.BaseFlowTaskService#isAssigneeOrCandidate(com.lhy.houseflow.entity.SysUser, java.lang.String)
     */
    @Override
    public boolean isAssigneeOrCandidate(SysUser user, String taskId) {
        long count = taskService.createTaskQuery().taskId(taskId).or().taskAssignee(user.getUserName())
            .taskCandidateUser(user.getUserName())
            // .taskCandidateGroup(user.getGroup())
            .endOr().count();
        // TODO 增加 usergroup
        return count > 0;
    }

    /* (non-Javadoc)
     * @see com.lhy.houseflow.service.impl.BaseFlowTaskService#checkTaskFromTaskId(java.lang.String)
     */
    @Override
    public Task checkTaskFromTaskId(String taskId) {
        Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
        if (task == null) {
            throw new CustomException(ResultInfo.Task_id_not_exist);
        }

        return task;
    }

    @Override
    public String getBusinessKey(Long businessId) {
        return businessId.toString();
    }

    @Override
    public void saveComment(Comment comment) {
        taskService.saveComment(comment);
    }

    @Override
    public Comment getComment(String commentId) {

        return taskService.getComment(commentId);
    }

    @Override
    public void deleteComments(String taskId, String processInstanceId) {
        taskService.delegateTask(taskId, processInstanceId);
    }

    @Override
    public Comment addComment(String taskId, String processInstanceId, String type, String message) {
        return taskService.addComment(taskId, processInstanceId, type, message);
    }

    @Override
    public void turnTask(String userName, String taskId, String trunUserName) {
        taskService.setOwner(taskId, userName);
        taskService.setAssignee(taskId, trunUserName);

    }
    /*
    @Override
    public void backTask(String taskId, String backTaskId, Map<String, Object> variables) {
        runtimeService.createChangeActivityStateBuilder()
        .moveExecutionToActivityId(taskId, "firstTask")
        .changeState();
        Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
        String processInstanceId = task.getProcessInstanceId();
        FlowElement distActivity = processDefinitionUtils.findFlowElementById(task.getProcessDefinitionId(), backVo.getDistFlowElementId());
        //1. 保存任务信息
        task.setAssignee(backVo.getUserCode());
        taskService.saveTask(task);
        //2. 如果上一个节点是提交者的话要处理一下
        if (FlowConstant.FLOW_SUBMITTER.equals(distActivity.getName())) {
            //查找发起人 设置到变量中，以便驳回到提起人的时候能留在提交人这个节点
            ExtendProcinst extendProcinst = this.extendProcinstService.findExtendProcinstByProcessInstanceId(processInstanceId);
            String creator = null;
            if (extendProcinst != null) {
                creator = extendProcinst.getCreator();
                if (StringUtils.isBlank(creator)) {
                    creator = extendProcinst.getCurrentUserCode();
                }
            } else {
                ExtendHisprocinst extendHisprocinst = extendHisprocinstService.getExtendHisprocinstByProcessInstanceId(processInstanceId);
                creator = extendHisprocinst.getCreator();
                if (StringUtils.isBlank(creator)) {
                    creator = extendHisprocinst.getCurrentUserCode();
                }
            }
            if (StringUtils.isNotBlank(creator)) {
                runtimeService.setVariable(processInstanceId, FlowConstant.FLOW_SUBMITTER_VAR, creator);
            }
        }
        List<Task> tasks = taskService.createTaskQuery().processInstanceId(processInstanceId).list();
        List<String> currentActivityIds = new ArrayList<>();
        tasks.forEach(t -> currentActivityIds.add(t.getTaskDefinitionKey()));
        //3. 删除节点信息
        if (!(distActivity instanceof EndEvent)) {
            this.deleteHisActivities((Activity) distActivity, processInstanceId);
        }
        //4. 添加审批意见和修改流程状态
        this.addCommentAndUpdateProcessStatus(backVo, processInstanceId);
        //5.执行驳回操作
        runtimeService.createChangeActivityStateBuilder()
                .processInstanceId(processInstanceId)
                .moveActivityIdsToSingleActivityId(currentActivityIds, backVo.getDistFlowElementId())
                .changeState();
        
    }
    */

    @Override
    public void backTask(String taskId, String backTaskId, Map<String, Object> variables) {
        // TODO Auto-generated method stub

    }
    
    /**
     * 获取下一任务用户，操作
     *
     * @param node
     *            查询节点选择
     * @param taskId
     *            任务id
     */
    @Override
    public Task taskId(String taskId) {
        Task task = taskService.createTaskQuery().taskId(taskId).singleResult();
        return task;
    }

    /**
     * 获取下一任务用户，操作
     *
     * @param node
     *            查询节点选择
     * @param taskId
     *            任务id
     */
    @Override
    public List<TaskUserInfo> findNextTaskUserInfosByTaskId(String taskId) {
        List<TaskUserInfo> list = new ArrayList<TaskUserInfo>();
        Task task = taskService.createTaskQuery().taskId(taskId).singleResult();

        ExecutionEntity ee =
            (ExecutionEntity)runtimeService.createExecutionQuery().executionId(task.getExecutionId()).singleResult();

        // 当前审批节点
        String crruentActivityId = ee.getActivityId();
        BpmnModel bpmnModel = repositoryService.getBpmnModel(task.getProcessDefinitionId());
        FlowNode flowNode = (FlowNode)bpmnModel.getFlowElement(crruentActivityId);
        //bpmnModel.getUserTaskFormTypes()
        // 输出连线
         List<SequenceFlow> outFlows = flowNode.getOutgoingFlows();
        for (SequenceFlow sequenceFlow : outFlows) {
            // 下一个审批节点            
            FlowElement targetFlow = sequenceFlow.getTargetFlowElement();
            if (targetFlow instanceof UserTask) {
                TaskUserInfo info = new TaskUserInfo();
                info.setName(targetFlow.getName());
                info.setDocumentation(targetFlow.getDocumentation());
                info.setEndTask(false);
                info.setUsers(findUsers((UserTask)targetFlow));
                list.add(info);
                log.info("下一节点: id={} ,name={}", targetFlow.getId(), targetFlow.getName());
            }
            // 如果下个审批节点为结束节点
            if (targetFlow instanceof EndEvent) {
                TaskUserInfo info = new TaskUserInfo();
                info.setName(targetFlow.getName());                
                info.setDocumentation(targetFlow.getDocumentation());  
                info.setEndTask(true);
                list.add(info);
                log.info("下一节点为结束节点: id={} ,name={}", targetFlow.getId(), targetFlow.getName());
            }
        }
        return list;
    }
    
    private List<SimpleUser> findUsers(UserTask userTask){
        return null;
        
    }

}