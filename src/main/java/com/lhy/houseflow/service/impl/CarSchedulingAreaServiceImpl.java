/**
 * @filename:CarSchedulingAreaServiceImpl 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.CarSchedulingArea;
import com.lhy.houseflow.dao.CarSchedulingAreaDao;
import com.lhy.houseflow.service.CarSchedulingAreaService;

/**   
 *  
 * @Description:  车辆排班——SERVICEIMPL
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Service
@Transactional(rollbackFor=Exception.class)
@Slf4j
public class CarSchedulingAreaServiceImpl implements CarSchedulingAreaService {
	
	@Autowired
	public CarSchedulingAreaDao carSchedulingAreaDao;
	
	//查询对象
	@Override
	public CarSchedulingArea findById(Long id) {
		return carSchedulingAreaDao.findById(id);
	}
	
	//删除对象
	@Override
	public int deleteById(Long id) {
		return carSchedulingAreaDao.deleteById(id);
	}
	
	//添加对象
	@Override
	public int add(CarSchedulingArea carSchedulingArea) {
		return carSchedulingAreaDao.add(carSchedulingArea);
	}
	
	//修改对象
	@Override
	public int update(CarSchedulingArea carSchedulingArea) {
		return carSchedulingAreaDao.update(carSchedulingArea);
	}
	
	//查询集合
	@Override
	public List<CarSchedulingArea> queryCarSchedulingAreaList(CarSchedulingArea carSchedulingArea) {
		return carSchedulingAreaDao.queryCarSchedulingAreaList(carSchedulingArea);
	}
	
	//分页查询
	@Override
	public PageInfo<CarSchedulingArea> list(AppPage<CarSchedulingArea> page) {
		PageHelper.startPage(page.getPageNum(),page.getPageSize());
		List<CarSchedulingArea> list = null;
		if (page.getParam() == null)
			list=carSchedulingAreaDao.list();		
		else
			list=carSchedulingAreaDao.queryCarSchedulingAreaList(page.getParam());
		PageInfo<CarSchedulingArea> pageInfo = new PageInfo<CarSchedulingArea>(list);
		return pageInfo;
	}
}