/**
 * @filename:SysRoleMenuServiceImpl 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.SysRoleMenu;
import com.lhy.houseflow.dao.SysRoleMenuDao;
import com.lhy.houseflow.service.SysRoleMenuService;

/**   
 *  
 * @Description:  角色菜单——SERVICEIMPL
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Service
@Transactional(rollbackFor=Exception.class)
@Slf4j
public class SysRoleMenuServiceImpl implements SysRoleMenuService {
	
	@Autowired
	public SysRoleMenuDao sysRoleMenuDao;
	
	//查询对象
	@Override
	public SysRoleMenu findById(Long id) {
		return sysRoleMenuDao.findById(id);
	}
	
	//删除对象
	@Override
	public int deleteById(Long id) {
		return sysRoleMenuDao.deleteById(id);
	}
	
	//添加对象
	@Override
	public int add(SysRoleMenu sysRoleMenu) {
		return sysRoleMenuDao.add(sysRoleMenu);
	}
	
	//修改对象
	@Override
	public int update(SysRoleMenu sysRoleMenu) {
		return sysRoleMenuDao.update(sysRoleMenu);
	}
	
	//查询集合
	@Override
	public List<SysRoleMenu> querySysRoleMenuList(SysRoleMenu sysRoleMenu) {
		return sysRoleMenuDao.querySysRoleMenuList(sysRoleMenu);
	}
	
	//分页查询
	@Override
	public PageInfo<SysRoleMenu> list(AppPage<SysRoleMenu> page) {
		PageHelper.startPage(page.getPageNum(),page.getPageSize());
		List<SysRoleMenu> list = null;
		if (page.getParam() == null)
			list=sysRoleMenuDao.list();		
		else
			list=sysRoleMenuDao.querySysRoleMenuList(page.getParam());
		PageInfo<SysRoleMenu> pageInfo = new PageInfo<SysRoleMenu>(list);
		return pageInfo;
	}
}