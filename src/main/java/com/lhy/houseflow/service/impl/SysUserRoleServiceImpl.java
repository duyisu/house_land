/**
 * @filename:SysUserRoleServiceImpl 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.SysUserRole;
import com.lhy.houseflow.dao.SysUserRoleDao;
import com.lhy.houseflow.service.SysUserRoleService;

/**   
 *  
 * @Description:  用户角色——SERVICEIMPL
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Service
@Transactional(rollbackFor=Exception.class)
@Slf4j
public class SysUserRoleServiceImpl implements SysUserRoleService {
	
	@Autowired
	public SysUserRoleDao sysUserRoleDao;
	
	//查询对象
	@Override
	public SysUserRole findById(Long id) {
		return sysUserRoleDao.findById(id);
	}
	
	//删除对象
	@Override
	public int deleteById(Long id) {
		return sysUserRoleDao.deleteById(id);
	}
	
	//添加对象
	@Override
	public int add(SysUserRole sysUserRole) {
		return sysUserRoleDao.add(sysUserRole);
	}
	
	//修改对象
	@Override
	public int update(SysUserRole sysUserRole) {
		return sysUserRoleDao.update(sysUserRole);
	}
	
	//查询集合
	@Override
	public List<SysUserRole> querySysUserRoleList(SysUserRole sysUserRole) {
		return sysUserRoleDao.querySysUserRoleList(sysUserRole);
	}
	
	//分页查询
	@Override
	public PageInfo<SysUserRole> list(AppPage<SysUserRole> page) {
		PageHelper.startPage(page.getPageNum(),page.getPageSize());
		List<SysUserRole> list = null;
		if (page.getParam() == null)
			list=sysUserRoleDao.list();		
		else
			list=sysUserRoleDao.querySysUserRoleList(page.getParam());
		PageInfo<SysUserRole> pageInfo = new PageInfo<SysUserRole>(list);
		return pageInfo;
	}
}