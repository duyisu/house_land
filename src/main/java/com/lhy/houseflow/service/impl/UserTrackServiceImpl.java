/**
 * @filename:UserTrackServiceImpl 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.UserTrack;
import com.lhy.houseflow.dao.UserTrackDao;
import com.lhy.houseflow.service.UserTrackService;

/**   
 *  
 * @Description:  人员轨迹——SERVICEIMPL
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Service
@Transactional(rollbackFor=Exception.class)
@Slf4j
public class UserTrackServiceImpl implements UserTrackService {
	
	@Autowired
	public UserTrackDao userTrackDao;
	
	//查询对象
	@Override
	public UserTrack findById(Long id) {
		return userTrackDao.findById(id);
	}
	
	//删除对象
	@Override
	public int deleteById(Long id) {
		return userTrackDao.deleteById(id);
	}
	
	//添加对象
	@Override
	public int add(UserTrack userTrack) {
		return userTrackDao.add(userTrack);
	}
	
	//修改对象
	@Override
	public int update(UserTrack userTrack) {
		return userTrackDao.update(userTrack);
	}
	
	//查询集合
	@Override
	public List<UserTrack> queryUserTrackList(UserTrack userTrack) {
		return userTrackDao.queryUserTrackList(userTrack);
	}
	
	//分页查询
	@Override
	public PageInfo<UserTrack> list(AppPage<UserTrack> page) {
		PageHelper.startPage(page.getPageNum(),page.getPageSize());
		List<UserTrack> list = null;
		if (page.getParam() == null)
			list=userTrackDao.list();		
		else
			list=userTrackDao.queryUserTrackList(page.getParam());
		PageInfo<UserTrack> pageInfo = new PageInfo<UserTrack>(list);
		return pageInfo;
	}
}