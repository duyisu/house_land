/**
 * @filename:MenuServiceImpl 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.Menu;
import com.lhy.houseflow.dao.MenuDao;
import com.lhy.houseflow.service.MenuService;

/**   
 *  
 * @Description:  菜单资源表——SERVICEIMPL
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Service
@Transactional(rollbackFor=Exception.class)
@Slf4j
public class MenuServiceImpl implements MenuService {
	
	@Autowired
	public MenuDao menuDao;
	
	//查询对象
	@Override
	public Menu findById(Long id) {
		return menuDao.findById(id);
	}
	
	//删除对象
	@Override
	public int deleteById(Long id) {
		return menuDao.deleteById(id);
	}
	
	//添加对象
	@Override
	public int add(Menu menu) {
		return menuDao.add(menu);
	}
	
	//修改对象
	@Override
	public int update(Menu menu) {
		return menuDao.update(menu);
	}
	
	//查询集合
	@Override
	public List<Menu> queryMenuList(Menu menu) {
		return menuDao.queryMenuList(menu);
	}
	
	//分页查询
	@Override
	public PageInfo<Menu> getMenuBySearch(AppPage<Menu> page) {
		PageHelper.startPage(page.getPageNum(),page.getPageSize());
		List<Menu> list=menuDao.queryMenuList(page.getParam());
		PageInfo<Menu> pageInfo = new PageInfo<Menu>(list);
		return pageInfo;
	}
}