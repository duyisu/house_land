/**
 * @filename:SysPermissionServiceImpl 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.dao.SysPermissionDao;
import com.lhy.houseflow.entity.SysPermission;
import com.lhy.houseflow.service.SysPermissionService;

import lombok.extern.slf4j.Slf4j;

/**   
 *  
 * @Description:  权限——SERVICEIMPL
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Service
@Transactional(rollbackFor=Exception.class)
@Slf4j
public class SysPermissionServiceImpl implements SysPermissionService {
	
	@Autowired
	public SysPermissionDao sysPermissionDao;
	
	//查询对象
	@Override
	public SysPermission findById(Long id) {
		return sysPermissionDao.findById(id);
	}
	
	//删除对象
	@Override
	public int deleteById(Long id) {
		return sysPermissionDao.deleteById(id);
	}
	
	//添加对象
	@Override
	public int add(SysPermission sysPermission) {
		return sysPermissionDao.add(sysPermission);
	}
	
	//修改对象
	@Override
	public int update(SysPermission sysPermission) {
		return sysPermissionDao.update(sysPermission);
	}
	
	//查询集合
	@Override
	public List<SysPermission> querySysPermissionList(SysPermission sysPermission) {
		return sysPermissionDao.querySysPermissionList(sysPermission);
	}
	
	//分页查询
	@Override
	public PageInfo<SysPermission> list(AppPage<SysPermission> page) {		
		List<SysPermission> list = null;
		if (page.getParam() == null)
			list=sysPermissionDao.list();		
		else {
		    PageHelper.startPage(page.getPageNum(),page.getPageSize());
			list=sysPermissionDao.querySysPermissionList(page.getParam());
		}
		PageInfo<SysPermission> pageInfo = new PageInfo<SysPermission>(list);
		return pageInfo;
	}
	@Override
    public List<SysPermission> findByUsername(String username){
	    return sysPermissionDao.findByUsername(username);
	}
	
	
}