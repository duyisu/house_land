/**
 * @filename:WfDefinitionSteprelaServiceImpl 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.WfDefinitionSteprela;
import com.lhy.houseflow.dao.WfDefinitionSteprelaDao;
import com.lhy.houseflow.service.WfDefinitionSteprelaService;

/**   
 *  
 * @Description:  步骤关系——SERVICEIMPL
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Service
@Transactional(rollbackFor=Exception.class)
@Slf4j
public class WfDefinitionSteprelaServiceImpl implements WfDefinitionSteprelaService {
	
	@Autowired
	public WfDefinitionSteprelaDao wfDefinitionSteprelaDao;
	
	//查询对象
	@Override
	public WfDefinitionSteprela findById(Long id) {
		return wfDefinitionSteprelaDao.findById(id);
	}
	
	//删除对象
	@Override
	public int deleteById(Long id) {
		return wfDefinitionSteprelaDao.deleteById(id);
	}
	
	//添加对象
	@Override
	public int add(WfDefinitionSteprela wfDefinitionSteprela) {
		return wfDefinitionSteprelaDao.add(wfDefinitionSteprela);
	}
	
	//修改对象
	@Override
	public int update(WfDefinitionSteprela wfDefinitionSteprela) {
		return wfDefinitionSteprelaDao.update(wfDefinitionSteprela);
	}
	
	//查询集合
	@Override
	public List<WfDefinitionSteprela> queryWfDefinitionSteprelaList(WfDefinitionSteprela wfDefinitionSteprela) {
		return wfDefinitionSteprelaDao.queryWfDefinitionSteprelaList(wfDefinitionSteprela);
	}
	
	//分页查询
	@Override
	public PageInfo<WfDefinitionSteprela> list(AppPage<WfDefinitionSteprela> page) {
		PageHelper.startPage(page.getPageNum(),page.getPageSize());
		List<WfDefinitionSteprela> list = null;
		if (page.getParam() == null)
			list=wfDefinitionSteprelaDao.list();		
		else
			list=wfDefinitionSteprelaDao.queryWfDefinitionSteprelaList(page.getParam());
		PageInfo<WfDefinitionSteprela> pageInfo = new PageInfo<WfDefinitionSteprela>(list);
		return pageInfo;
	}
}