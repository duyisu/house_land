/**
 * @filename:CaseRecordFlowTaskFileServiceImpl 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.CaseRecordFlowTaskFile;
import com.lhy.houseflow.dao.CaseRecordFlowTaskFileDao;
import com.lhy.houseflow.service.CaseRecordFlowTaskFileService;

/**   
 *  
 * @Description:  两违流程附件表——SERVICEIMPL
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Service
@Transactional(rollbackFor=Exception.class)
@Slf4j
public class CaseRecordFlowTaskFileServiceImpl implements CaseRecordFlowTaskFileService {
	
	@Autowired
	public CaseRecordFlowTaskFileDao caseRecordFlowTaskFileDao;
	
	//查询对象
	@Override
	public CaseRecordFlowTaskFile findById(Long id) {
		return caseRecordFlowTaskFileDao.findById(id);
	}
	
	//删除对象
	@Override
	public int deleteById(Long id) {
		return caseRecordFlowTaskFileDao.deleteById(id);
	}
	
	//添加对象
	@Override
	public int add(CaseRecordFlowTaskFile caseRecordFlowTaskFile) {
		return caseRecordFlowTaskFileDao.add(caseRecordFlowTaskFile);
	}
	
	//修改对象
	@Override
	public int update(CaseRecordFlowTaskFile caseRecordFlowTaskFile) {
		return caseRecordFlowTaskFileDao.update(caseRecordFlowTaskFile);
	}
	
	//查询集合
	@Override
	public List<CaseRecordFlowTaskFile> queryCaseRecordFlowTaskFileList(CaseRecordFlowTaskFile caseRecordFlowTaskFile) {
		return caseRecordFlowTaskFileDao.queryCaseRecordFlowTaskFileList(caseRecordFlowTaskFile);
	}
	
	//分页查询
	@Override
	public PageInfo<CaseRecordFlowTaskFile> list(AppPage<CaseRecordFlowTaskFile> page) {
		PageHelper.startPage(page.getPageNum(),page.getPageSize());
		List<CaseRecordFlowTaskFile> list = null;
		if (page.getParam() == null)
			list=caseRecordFlowTaskFileDao.list();		
		else
			list=caseRecordFlowTaskFileDao.queryCaseRecordFlowTaskFileList(page.getParam());
		PageInfo<CaseRecordFlowTaskFile> pageInfo = new PageInfo<CaseRecordFlowTaskFile>(list);
		return pageInfo;
	}
}