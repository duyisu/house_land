/**
 * @filename:SysUserServiceImpl 2019年5月5日
 * @project HouseFlow V1.0 Copyright(c) 2018 lhy Co. Ltd. All right reserved.
 */
package com.lhy.houseflow.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.exception.CustomException;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.dao.SysPermissionDao;
import com.lhy.houseflow.dao.SysUserDao;
import com.lhy.houseflow.dao.SysUserRoleDao;
import com.lhy.houseflow.entity.SysPermission;
import com.lhy.houseflow.entity.SysUser;
import com.lhy.houseflow.entity.SysUserRole;
import com.lhy.houseflow.entity.dto.UserDto;
import com.lhy.houseflow.service.SysUserService;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @Description: 用户——SERVICEIMPL
 * @Author: lhy
 * @CreateDate: 2019年5月5日
 * @Version: V1.0
 * 
 */
@Service
@Transactional(rollbackFor = Exception.class)
@Slf4j
public class SysUserServiceImpl implements SysUserService {

    @Autowired
    public SysUserDao sysUserDao;
    @Autowired
    public SysUserRoleDao sysUserRoleDao;
    @Autowired
    public SysPermissionDao sysPermissionDao;
   // 查询对象
    @Override
    public SysUser findById(Long id) {
        return sysUserDao.findById(id);
    }

    // 删除对象
    @Override
    public int deleteById(Long id) {
        sysUserRoleDao.deleteByUserId(id);
        return sysUserDao.deleteById(id);
    }

    // 添加对象
    @Override
    public int add(UserDto user) {
        int cnt = sysUserDao.add(user);
        saveRole(user);
        return cnt;
    }
  

    private void saveRole(UserDto user) {
        for (Long roleId : user.getRoleIds()) {
            SysUserRole userRole = new SysUserRole();
            userRole.setRoleId(roleId);
            userRole.setUserId(user.getId());
            sysUserRoleDao.add(userRole);
        }
    }

    // 修改对象
    @Override
    public int update(UserDto user) {
        int count = sysUserDao.update(user);
        sysUserRoleDao.deleteByUserId(user.getId());
        saveRole(user);
        return count;
    }

    // 查询集合
    @Override
    public List<UserDto> querySysUserList(SysUser user) {
        List<UserDto> list = queryUserByParam(user);
        return list;
    }

    private List<UserDto> queryUserByParam(SysUser user) {
        List<UserDto> list = null;
        if (user == null)
            list = sysUserDao.findUsers(null, null, null, null);
        else
            list = sysUserDao.findUsers(user.getDepartId(), user.getUserName(), user.getUserTel(),
                user.getEnabled());
        return list;
    }

    // 分页查询
    @Override
    public PageInfo<UserDto> list(AppPage<SysUser> page) {
        PageHelper.startPage(page.getPageNum(), page.getPageSize());
        List<UserDto> list = queryUserByParam(page.getParam());
        PageInfo<UserDto> pageInfo = new PageInfo<UserDto>(list);
        return pageInfo;
    }

    @Override
    public SysUser findByUsername(String userName) {
        return sysUserDao.findByUsername(userName);
    }
    
    
    @Override
    public List<String> loadPermissionByUsername(String username) throws CustomException {
        
        SysUser user = sysUserDao.findByUsername(username);
        if (user == null) {
            throw new CustomException(ResultInfo.Auth_User_Not_Exist);
        } else {
            List<SysPermission> list = sysPermissionDao.findByUsername(username);
            Set<String> set = new HashSet<String>();
            if (list != null){
                for (SysPermission p: list){
                    set.add(p.getCode());
                }
            }
            return new ArrayList<String>(set);
        }
    }    
}