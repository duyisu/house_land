/**
 * @filename:CaseRecordServiceImpl 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.CaseRecord;
import com.lhy.houseflow.dao.CaseRecordDao;
import com.lhy.houseflow.service.CaseRecordService;
import com.lhy.houseflow.service.SysCodeService;

/**   
 *  
 * @Description:  立案登记——SERVICEIMPL
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Service
@Transactional(rollbackFor=Exception.class)
@Slf4j
public class CaseRecordServiceImpl implements CaseRecordService {
	
	@Autowired
	public CaseRecordDao caseRecordDao;
    @Autowired
    private SysCodeService sysCodeService;	
	
	//查询对象
	@Override
	public CaseRecord findById(Long id) {
		return caseRecordDao.findById(id);
	}
	
	//删除对象
	@Override
	public int deleteById(Long id) {
		return caseRecordDao.deleteById(id);
	}
	
	//添加对象
	@Override
	public int add(CaseRecord caseRecord) {
        String sn = sysCodeService.getCaseSn();
        caseRecord.setCaseNo(sn);
        sn = sysCodeService.getCaseRecordSn();
        caseRecord.setCaseRecordNo(sn); 	    
		return caseRecordDao.add(caseRecord);
	}
	
	//修改对象
	@Override
	public int update(CaseRecord caseRecord) {
		return caseRecordDao.update(caseRecord);
	}
	
	//查询集合
	@Override
	public List<CaseRecord> queryCaseRecordList(CaseRecord caseRecord) {
		return caseRecordDao.queryCaseRecordList(caseRecord);
	}
	
	//分页查询
	@Override
	public PageInfo<CaseRecord> list(AppPage<CaseRecord> page) {
		PageHelper.startPage(page.getPageNum(),page.getPageSize());
		List<CaseRecord> list = null;
		if (page.getParam() == null)
			list=caseRecordDao.list();		
		else
			list=caseRecordDao.queryCaseRecordList(page.getParam());
		PageInfo<CaseRecord> pageInfo = new PageInfo<CaseRecord>(list);
		return pageInfo;
	}
}