/**
 * @filename:CaseDefendantServiceImpl 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.CaseDefendant;
import com.lhy.houseflow.dao.CaseDefendantDao;
import com.lhy.houseflow.service.CaseDefendantService;

/**   
 *  
 * @Description:  立案登记当事人——SERVICEIMPL
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Service
@Transactional(rollbackFor=Exception.class)
@Slf4j
public class CaseDefendantServiceImpl implements CaseDefendantService {
	
	@Autowired
	public CaseDefendantDao caseDefendantDao;
	
	//查询对象
	@Override
	public CaseDefendant findById(Long id) {
		return caseDefendantDao.findById(id);
	}
	
	//删除对象
	@Override
	public int deleteById(Long id) {
		return caseDefendantDao.deleteById(id);
	}
	
	//添加对象
	@Override
	public int add(CaseDefendant caseDefendant) {
		return caseDefendantDao.add(caseDefendant);
	}
	
	//修改对象
	@Override
	public int update(CaseDefendant caseDefendant) {
		return caseDefendantDao.update(caseDefendant);
	}
	
	//查询集合
	@Override
	public List<CaseDefendant> queryCaseDefendantList(CaseDefendant caseDefendant) {
		return caseDefendantDao.queryCaseDefendantList(caseDefendant);
	}
	
	//分页查询
	@Override
	public PageInfo<CaseDefendant> list(AppPage<CaseDefendant> page) {
		PageHelper.startPage(page.getPageNum(),page.getPageSize());
		List<CaseDefendant> list = null;
		if (page.getParam() == null)
			list=caseDefendantDao.list();		
		else
			list=caseDefendantDao.queryCaseDefendantList(page.getParam());
		PageInfo<CaseDefendant> pageInfo = new PageInfo<CaseDefendant>(list);
		return pageInfo;
	}
}