/**
 * @filename:CarTrackServiceImpl 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.CarTrack;
import com.lhy.houseflow.dao.CarTrackDao;
import com.lhy.houseflow.service.CarTrackService;

/**   
 *  
 * @Description:  车辆轨迹——SERVICEIMPL
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Service
@Transactional(rollbackFor=Exception.class)
@Slf4j
public class CarTrackServiceImpl implements CarTrackService {
	
	@Autowired
	public CarTrackDao carTrackDao;
	
	//查询对象
	@Override
	public CarTrack findById(Long id) {
		return carTrackDao.findById(id);
	}
	
	//删除对象
	@Override
	public int deleteById(Long id) {
		return carTrackDao.deleteById(id);
	}
	
	//添加对象
	@Override
	public int add(CarTrack carTrack) {
		return carTrackDao.add(carTrack);
	}
	
	//修改对象
	@Override
	public int update(CarTrack carTrack) {
		return carTrackDao.update(carTrack);
	}
	
	//查询集合
	@Override
	public List<CarTrack> queryCarTrackList(CarTrack carTrack) {
		return carTrackDao.queryCarTrackList(carTrack);
	}
	
	//分页查询
	@Override
	public PageInfo<CarTrack> list(AppPage<CarTrack> page) {
		PageHelper.startPage(page.getPageNum(),page.getPageSize());
		List<CarTrack> list = null;
		if (page.getParam() == null)
			list=carTrackDao.list();		
		else
			list=carTrackDao.queryCarTrackList(page.getParam());
		PageInfo<CarTrack> pageInfo = new PageInfo<CarTrack>(list);
		return pageInfo;
	}
}