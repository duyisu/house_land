package com.lhy.houseflow.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lhy.houseflow.common.exception.CustomException;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.dao.SysUserDao;
import com.lhy.houseflow.entity.SysUser;
import com.lhy.houseflow.service.LoginService;

import lombok.extern.slf4j.Slf4j;

@Service
@Transactional(rollbackFor=Exception.class)
@Slf4j
public class LoginServiceImpl implements LoginService {

	@Autowired
	public SysUserDao sysUserDao;
	
	/* (non-Javadoc)
	 * @see com.lhy.houseflow.service.impl.LoginService#login(java.lang.String, java.lang.String)
	 */
	@Override
	public SysUser login(String userName, String password){
	   
		SysUser user = sysUserDao.findByUsername(userName);
		
		if (user == null){
			throw new CustomException(ResultInfo.Auth_User_Not_Exist);
		}
		
        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        // 加密
        String encodedPassword = passwordEncoder.encode(password.trim());	
        log.info("password：{}", password);
        log.info("encoded Password：{}", encodedPassword);
        log.info("DB      password：{}", user.getUserPassword());
        if (!passwordEncoder.matches(password, user.getUserPassword())){
            throw new CustomException(ResultInfo.Auth_Login_Erorr);
        }
/*		if (!StringUtils.equals(encodedPassword, user.getUserPassword())){
			throw new CustomException(ResultInfo.Auth_Login_Erorr);
		}
		*/
		
		return user;
		
	}

}
