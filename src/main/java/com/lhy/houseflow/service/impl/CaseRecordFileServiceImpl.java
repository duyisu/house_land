/**
 * @filename:CaseRecordFileServiceImpl 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.CaseRecordFile;
import com.lhy.houseflow.dao.CaseRecordFileDao;
import com.lhy.houseflow.service.CaseRecordFileService;

/**   
 *  
 * @Description:  立案登记附件表——SERVICEIMPL
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Service
@Transactional(rollbackFor=Exception.class)
@Slf4j
public class CaseRecordFileServiceImpl implements CaseRecordFileService {
	
	@Autowired
	public CaseRecordFileDao caseRecordFileDao;
	
	//查询对象
	@Override
	public CaseRecordFile findById(Long id) {
		return caseRecordFileDao.findById(id);
	}
	
	//删除对象
	@Override
	public int deleteById(Long id) {
		return caseRecordFileDao.deleteById(id);
	}
	
	//添加对象
	@Override
	public int add(CaseRecordFile caseRecordFile) {
		return caseRecordFileDao.add(caseRecordFile);
	}
	
	//修改对象
	@Override
	public int update(CaseRecordFile caseRecordFile) {
		return caseRecordFileDao.update(caseRecordFile);
	}
	
	//查询集合
	@Override
	public List<CaseRecordFile> queryCaseRecordFileList(CaseRecordFile caseRecordFile) {
		return caseRecordFileDao.queryCaseRecordFileList(caseRecordFile);
	}
	
	//分页查询
	@Override
	public PageInfo<CaseRecordFile> list(AppPage<CaseRecordFile> page) {
		PageHelper.startPage(page.getPageNum(),page.getPageSize());
		List<CaseRecordFile> list = null;
		if (page.getParam() == null)
			list=caseRecordFileDao.list();		
		else
			list=caseRecordFileDao.queryCaseRecordFileList(page.getParam());
		PageInfo<CaseRecordFile> pageInfo = new PageInfo<CaseRecordFile>(list);
		return pageInfo;
	}
}