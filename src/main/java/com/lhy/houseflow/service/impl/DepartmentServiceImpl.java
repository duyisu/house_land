/**
 * @filename:DepartmentServiceImpl 2019年5月5日
 * @project HouseFlow V1.0 Copyright(c) 2018 lhy Co. Ltd. All right reserved.
 */
package com.lhy.houseflow.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.dao.DepartmentDao;
import com.lhy.houseflow.entity.Department;
import com.lhy.houseflow.entity.vo.DepartmentVo;
import com.lhy.houseflow.entity.vo.SimpleTreeData;
import com.lhy.houseflow.service.DepartmentService;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @Description: 部门——SERVICEIMPL
 * @Author: lhy
 * @CreateDate: 2019年5月5日
 * @Version: V1.0
 * 
 */
@Service
@Transactional(rollbackFor = Exception.class)
@Slf4j
public class DepartmentServiceImpl implements DepartmentService {

    @Autowired
    public DepartmentDao departmentDao;

    // 查询对象
    @Override
    public Department findById(Long id) {
        return departmentDao.findById(id);
    }

    // 删除对象
    @Override
    public int deleteById(Long id) {
        return departmentDao.deleteById(id);
    }

    // 添加对象
    @Override
    public int add(Department department) {
        return departmentDao.add(department);
    }

    // 修改对象
    @Override
    public int update(Department department) {
        return departmentDao.update(department);
    }

    // 查询集合
    @Override
    public List<Department> queryDepartmentList(Department department) {
        return departmentDao.queryDepartmentList(department);
    }

    // 分页查询
    @Override
    public PageInfo<Department> list(AppPage<Department> page) {        
        List<Department> list = null;
        if (page == null || page.getParam() == null)
            list = departmentDao.list();
        else
        {
            PageHelper.startPage(page.getPageNum(), page.getPageSize());
            list = departmentDao.queryDepartmentList(page.getParam());
        }
        PageInfo<Department> pageInfo = new PageInfo<Department>(list);
        return pageInfo;
    }
    
    @Override
    public List<SimpleTreeData> getSimpleTreeData(List<DepartmentVo> tree) {
        List<SimpleTreeData> list = new ArrayList<SimpleTreeData>();
        for (DepartmentVo item : tree) {
            SimpleTreeData data = new SimpleTreeData(item.getId(), item.getName(), null);
            if (item.getChildren() != null && !item.getChildren().isEmpty()) {
                data.setChildren(getSimpleTreeData(item.getChildren()));
            }
            list.add(data);
        }
        return list;
    }    
    @Override
    public List<DepartmentVo> buildTree(List<Department> depts) {
        List<DepartmentVo> treeList = new ArrayList<DepartmentVo>();
        for (Department menu : depts) {
            DepartmentVo item = new DepartmentVo();
            BeanUtils.copyProperties(menu, item);
            treeList.add(item);
        }

        List<DepartmentVo> tree = new ArrayList<DepartmentVo>();
        for (DepartmentVo item : treeList) {
            if (item.isRoot()) {
                tree.add(item);
            } else {
                DepartmentVo parent = bsearchWithoutRecursion(treeList, item.getPid());
                if (parent != null) {
                    parent.getChildren().add(item);
                }
            }
        }
        sortTree(tree);
        return tree;
    }

    private void sortTree(List<DepartmentVo> list) {
        if (list == null || list.isEmpty())
            return;
        for (DepartmentVo item : list) {
            sortTree(item.getChildren());
        }
        list.sort((a, b) -> StringUtils.compare(a.getName(), b.getName()));

    }

    private DepartmentVo bsearchWithoutRecursion(List<DepartmentVo> list, Long parentId) {
        int low = 0;
        int high = list.size() - 1;
        while (low <= high) {
            int mid = low + (high - low) / 2;
            int p = Long.compare(list.get(mid).getId(), parentId);
            if (p > 0)
                high = mid - 1;
            else if (p < 0)
                low = mid + 1;
            else
                return list.get(mid);
        }
        return null;
    }
}