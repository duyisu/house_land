/**
 * @filename:SysRolePermissionServiceImpl 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.SysRolePermission;
import com.lhy.houseflow.dao.SysRolePermissionDao;
import com.lhy.houseflow.service.SysRolePermissionService;

/**   
 *  
 * @Description:  角色权限——SERVICEIMPL
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Service
@Transactional(rollbackFor=Exception.class)
@Slf4j
public class SysRolePermissionServiceImpl implements SysRolePermissionService {
	
	@Autowired
	public SysRolePermissionDao sysRolePermissionDao;
	
	//查询对象
	@Override
	public SysRolePermission findById(Long id) {
		return sysRolePermissionDao.findById(id);
	}
	
	//删除对象
	@Override
	public int deleteById(Long id) {
		return sysRolePermissionDao.deleteById(id);
	}
	
	//添加对象
	@Override
	public int add(SysRolePermission sysRolePermission) {
		return sysRolePermissionDao.add(sysRolePermission);
	}
	
	//修改对象
	@Override
	public int update(SysRolePermission sysRolePermission) {
		return sysRolePermissionDao.update(sysRolePermission);
	}
	
	//查询集合
	@Override
	public List<SysRolePermission> querySysRolePermissionList(SysRolePermission sysRolePermission) {
		return sysRolePermissionDao.querySysRolePermissionList(sysRolePermission);
	}
	
	//分页查询
	@Override
	public PageInfo<SysRolePermission> list(AppPage<SysRolePermission> page) {
		PageHelper.startPage(page.getPageNum(),page.getPageSize());
		List<SysRolePermission> list = null;
		if (page.getParam() == null)
			list=sysRolePermissionDao.list();		
		else
			list=sysRolePermissionDao.querySysRolePermissionList(page.getParam());
		PageInfo<SysRolePermission> pageInfo = new PageInfo<SysRolePermission>(list);
		return pageInfo;
	}
}