/**
 * @filename:SysRoleServiceImpl 2019年5月5日
 * @project HouseFlow V1.0 Copyright(c) 2018 lhy Co. Ltd. All right reserved.
 */
package com.lhy.houseflow.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.alibaba.druid.util.StringUtils;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.dao.SysMenuDao;
import com.lhy.houseflow.dao.SysPermissionDao;
import com.lhy.houseflow.dao.SysRoleDao;
import com.lhy.houseflow.dao.SysRoleMenuDao;
import com.lhy.houseflow.entity.SysPermission;
import com.lhy.houseflow.entity.SysRole;
import com.lhy.houseflow.entity.SysRoleMenu;
import com.lhy.houseflow.entity.dto.RoleDto;
import com.lhy.houseflow.service.SysRoleService;

import lombok.extern.slf4j.Slf4j;

/**
 * 
 * @Description: 角色——SERVICEIMPL
 * @Author: lhy
 * @CreateDate: 2019年5月5日
 * @Version: V1.0
 * 
 */
@Service
@Transactional(rollbackFor = Exception.class)
@Slf4j
public class SysRoleServiceImpl implements SysRoleService {

    @Autowired
    public SysRoleDao sysRoleDao;
    @Autowired
    public SysMenuDao sysMenuDao;
    @Autowired
    public SysPermissionDao sysPermissionDao; 
    @Autowired
    private SysRoleMenuDao sysRoleMenuDao;

    // 查询对象
    @Override
    public SysRole findById(Long id) {
        return sysRoleDao.findById(id);
    }

    // 删除对象
    @Override
    public int deleteById(Long id) {
        sysRoleMenuDao.deleteByRoleId(id);
        return sysRoleDao.deleteById(id);
    }

    // 添加对象
    @Override
    public int add(RoleDto sysRole) {
        int count = sysRoleDao.add(sysRole);
        addRoleMenu(sysRole);
        return count;
    }

    private void addRoleMenu(RoleDto sysRole) {
        if (sysRole.getPermissionIds()!= null && !sysRole.getPermissionIds().isEmpty()){
            for (Long permissionId: sysRole.getPermissionIds()){
               SysPermission  p = sysPermissionDao.findById(permissionId);
               SysRoleMenu sysRoleMenu = new SysRoleMenu();
               sysRoleMenu.setMenuId(p.getMenuId());
               sysRoleMenu.setPermissionId(permissionId);
               sysRoleMenu.setRoleId(sysRole.getId());
               sysRoleMenuDao.add(sysRoleMenu);
            }            
        }
    }

    // 修改对象
    @Override
    public int update(RoleDto sysRole) {
        int count= sysRoleDao.update(sysRole);
        sysRoleMenuDao.deleteByRoleId(sysRole.getId());
        addRoleMenu(sysRole);
        return count;
    }

    // 查询集合
    @Override
    public List<SysRole> querySysRoleList(SysRole sysRole) {
        return sysRoleDao.querySysRoleList(sysRole);
    }

    // 分页查询
    @Override
    public PageInfo<SysRole> list(AppPage<SysRole> page) {
        PageHelper.startPage(page.getPageNum(), page.getPageSize());
        List<SysRole> list = null;
        if (page.getParam() == null)
            list = sysRoleDao.list();
        else
            list = sysRoleDao.querySysRoleList(page.getParam());
        PageInfo<SysRole> pageInfo = new PageInfo<SysRole>(list);
        return pageInfo;
    }

    // 分页查询
    @Override
    public PageInfo<RoleDto> getRoles(AppPage<String> page) {
        PageHelper.startPage(page.getPageNum(), page.getPageSize());
        List<RoleDto> list = null;
        if (StringUtils.isEmpty(page.getParam()))
            list = sysRoleDao.FindRoles(null);
        else
            list = sysRoleDao.FindRoles(page.getParam());
        PageInfo<RoleDto> pageInfo = new PageInfo<RoleDto>(list);
        return pageInfo;
    }

    @Override
    public RoleDto findRoleDtoById(Long id) {
        // TODO Auto-generated method stub
        return sysRoleDao.findRoleDtoById(id);
    }

}