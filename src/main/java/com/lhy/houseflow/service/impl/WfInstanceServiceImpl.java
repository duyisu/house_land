/**
 * @filename:WfInstanceServiceImpl 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.WfInstance;
import com.lhy.houseflow.dao.WfInstanceDao;
import com.lhy.houseflow.service.WfInstanceService;

/**   
 *  
 * @Description:  工作流实例表——SERVICEIMPL
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Service
@Transactional(rollbackFor=Exception.class)
@Slf4j
public class WfInstanceServiceImpl implements WfInstanceService {
	
	@Autowired
	public WfInstanceDao wfInstanceDao;
	
	//查询对象
	@Override
	public WfInstance findById(Long id) {
		return wfInstanceDao.findById(id);
	}
	
	//删除对象
	@Override
	public int deleteById(Long id) {
		return wfInstanceDao.deleteById(id);
	}
	
	//添加对象
	@Override
	public int add(WfInstance wfInstance) {
		return wfInstanceDao.add(wfInstance);
	}
	
	//修改对象
	@Override
	public int update(WfInstance wfInstance) {
		return wfInstanceDao.update(wfInstance);
	}
	
	//查询集合
	@Override
	public List<WfInstance> queryWfInstanceList(WfInstance wfInstance) {
		return wfInstanceDao.queryWfInstanceList(wfInstance);
	}
	
	//分页查询
	@Override
	public PageInfo<WfInstance> list(AppPage<WfInstance> page) {
		PageHelper.startPage(page.getPageNum(),page.getPageSize());
		List<WfInstance> list = null;
		if (page.getParam() == null)
			list=wfInstanceDao.list();		
		else
			list=wfInstanceDao.queryWfInstanceList(page.getParam());
		PageInfo<WfInstance> pageInfo = new PageInfo<WfInstance>(list);
		return pageInfo;
	}
}