/**
 * @filename:CarRouteServiceImpl 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.CarRoute;
import com.lhy.houseflow.dao.CarRouteDao;
import com.lhy.houseflow.service.CarRouteService;

/**   
 *  
 * @Description:  车辆轨迹表——SERVICEIMPL
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Service
@Transactional(rollbackFor=Exception.class)
@Slf4j
public class CarRouteServiceImpl implements CarRouteService {
	
	@Autowired
	public CarRouteDao carRouteDao;
	
	//查询对象
	@Override
	public CarRoute findById(Long id) {
		return carRouteDao.findById(id);
	}
	
	//删除对象
	@Override
	public int deleteById(Long id) {
		return carRouteDao.deleteById(id);
	}
	
	//添加对象
	@Override
	public int add(CarRoute carRoute) {
		return carRouteDao.add(carRoute);
	}
	
	//修改对象
	@Override
	public int update(CarRoute carRoute) {
		return carRouteDao.update(carRoute);
	}
	
	//查询集合
	@Override
	public List<CarRoute> queryCarRouteList(CarRoute carRoute) {
		return carRouteDao.queryCarRouteList(carRoute);
	}
	
	//分页查询
	@Override
	public PageInfo<CarRoute> getCarRouteBySearch(AppPage<CarRoute> page) {
		PageHelper.startPage(page.getPageNum(),page.getPageSize());
		List<CarRoute> list=carRouteDao.queryCarRouteList(page.getParam());
		PageInfo<CarRoute> pageInfo = new PageInfo<CarRoute>(list);
		return pageInfo;
	}
}