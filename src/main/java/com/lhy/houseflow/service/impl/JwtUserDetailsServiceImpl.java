package com.lhy.houseflow.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.lhy.houseflow.common.exception.CustomException;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.dao.SysPermissionDao;
import com.lhy.houseflow.dao.SysUserDao;
import com.lhy.houseflow.entity.JwtUserFactory;
import com.lhy.houseflow.entity.SysPermission;
import com.lhy.houseflow.entity.SysUser;

//@Service("jwtUserDetailsServiceImpl")
@Service
@Transactional(rollbackFor = Exception.class)
public class JwtUserDetailsServiceImpl implements UserDetailsService {
    @Autowired
    private SysUserDao sysUserDao;
    @Autowired
    protected SysPermissionDao permissionDao;

    @Override
    public UserDetails loadUserByUsername(String username) throws CustomException {
        SysUser user = sysUserDao.findByUsername(username);
        if (user == null) {
            throw new CustomException(ResultInfo.Auth_User_Not_Exist);
        } else {
            List<SysPermission> list = permissionDao.findByUsername(username);
            return JwtUserFactory.create(user, list);
        }
    }

}
