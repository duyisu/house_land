/**
 * @filename:UploadFileServiceImpl 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.UploadFile;
import com.lhy.houseflow.dao.UploadFileDao;
import com.lhy.houseflow.service.UploadFileService;

/**   
 *  
 * @Description:  上传文件——SERVICEIMPL
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Service
@Transactional(rollbackFor=Exception.class)
@Slf4j
public class UploadFileServiceImpl implements UploadFileService {
	
	@Autowired
	public UploadFileDao uploadFileDao;
	
	//查询对象
	@Override
	public UploadFile findById(Long id) {
		return uploadFileDao.findById(id);
	}
	
	//删除对象
	@Override
	public int deleteById(Long id) {
		return uploadFileDao.deleteById(id);
	}
	
	//添加对象
	@Override
	public int add(UploadFile uploadFile) {
		return uploadFileDao.add(uploadFile);
	}
	
	//修改对象
	@Override
	public int update(UploadFile uploadFile) {
		return uploadFileDao.update(uploadFile);
	}
	
	//查询集合
	@Override
	public List<UploadFile> queryUploadFileList(UploadFile uploadFile) {
		return uploadFileDao.queryUploadFileList(uploadFile);
	}
	
	//分页查询
	@Override
	public PageInfo<UploadFile> list(AppPage<UploadFile> page) {
		PageHelper.startPage(page.getPageNum(),page.getPageSize());
		List<UploadFile> list = null;
		if (page.getParam() == null)
			list=uploadFileDao.list();		
		else
			list=uploadFileDao.queryUploadFileList(page.getParam());
		PageInfo<UploadFile> pageInfo = new PageInfo<UploadFile>(list);
		return pageInfo;
	}
}