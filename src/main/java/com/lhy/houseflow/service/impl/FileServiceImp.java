package com.lhy.houseflow.service.impl;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;

import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServletResponse;

import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;
import org.springframework.web.multipart.MultipartFile;

import com.lhy.houseflow.dao.UploadFileDao;
import com.lhy.houseflow.entity.UploadFile;
import com.lhy.houseflow.service.FileService;

import lombok.extern.slf4j.Slf4j;

@Slf4j
@Service
public class FileServiceImp implements FileService {

    @Value("${upload.path}")
    private String basepath;
    private static final String PATH_DELIMITADOR = "/";

    @Autowired
    private UploadFileDao uploadFileDao;

    private String currPath(String subPath) {
        String path = basepath;
        if (!StringUtils.endsWith(path, PATH_DELIMITADOR))
            path = path + PATH_DELIMITADOR;
        if (StringUtils.isNoneBlank(subPath)) {
            path = path + subPath;
            if (!StringUtils.endsWith(path, PATH_DELIMITADOR))
                path = path + PATH_DELIMITADOR;
        }
        return path;
    }

    /* (non-Javadoc)
     * @see com.lhy.houseflow.service.impl.FileService#fileUpload(org.springframework.web.multipart.MultipartFile)
     */
    @Override
    public UploadFile fileUpload(Integer businessKind, Long businessId, Integer businessSubKind, MultipartFile file)
        throws Exception {
        UploadFile uploadFile = new UploadFile();
        uploadFile.setOriginalName(file.getOriginalFilename());
        uploadFile.setBusinessId(businessId);
        uploadFile.setBusinessKind(businessKind);
        uploadFile.setBusinessSubKind(businessSubKind);

        DateFormat df = new SimpleDateFormat("yyyyMMddHHmmss");
        Calendar calendar = Calendar.getInstance();

        String path = currPath(getSubPath(businessKind));
        String fileName = getUniqueFileName(path, df.format(calendar.getTime()), file.getOriginalFilename());
        uploadFile.setFilename(fileName);
        uploadFile.setFilepath(path);
        uploadFile.setFileType(StringUtils.substringAfterLast(file.getOriginalFilename(), "."));
        uploadFile(file.getBytes(), path, fileName);
        uploadFileDao.add(uploadFile);
        return uploadFile;
    }

    private String getSubPath(Integer businessKind) {
        if (businessKind == null)
            return null;
        // 0:未分类;1:两违流程;2:两违任务;3:建房审批;4审批任务
        return businessKind.toString();
    }

    private String getUniqueFileName(String filePath, String time, String fileName) {
        String tmpFileName = time + fileName;
        File file = new File(filePath + tmpFileName);
        int idx = 0;
        while (file.exists()) {
            tmpFileName = time + idx + fileName;
            file = new File(filePath + fileName);
            idx++;
        }
        return tmpFileName;
    }

    private boolean uploadFile(byte[] file, String filePath, String fileName) throws IOException {
        FileOutputStream out = null;
        try {
            File targetFile = new File(filePath);
            if (!targetFile.exists()) {
                targetFile.mkdirs();
            }
            fileName = filePath + fileName;
            out = new FileOutputStream(fileName);
            out.write(file);
            out.flush();
            log.info("upload:{}", fileName);
        } finally {
            if (out != null)
                try {
                    out.close();
                } catch (IOException e) {
                    log.error(e.getMessage());
                }
        }
        return new File(fileName).exists();

    }

    /* (non-Javadoc)
     * @see com.lhy.houseflow.service.impl.FileService#downLoadFile(com.lhy.houseflow.entity.UploadFile, javax.servlet.http.HttpServletResponse)
     */
    @Override
    public void downLoadFile(UploadFile fileinfo, HttpServletResponse response) {
        File file = new File(fileinfo.getFilepath() + fileinfo.getFilename());

        if (file.exists()) {
            response.setContentType("application/force-download");
            response.addHeader("Content-Disposition", "attachment;fileName=" + fileinfo.getOriginalName());

            BufferedInputStream bi = null;
            try {
                byte[] buffer = new byte[1024];
                bi = new BufferedInputStream(
                    new FileInputStream(new File(fileinfo.getFilepath() + fileinfo.getFilename())));
                ServletOutputStream outputStream = response.getOutputStream();
                int i = -1;
                while (-1 != (i = bi.read(buffer))) {
                    outputStream.write(buffer, 0, i);
                }

            } catch (Exception e) {
                log.error(e.getMessage());
            } finally {
                if (bi != null) {
                    try {
                        bi.close();
                    } catch (IOException e) {
                        log.error(e.getMessage());
                    }
                }
            }
        }

    }

}
