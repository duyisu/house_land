/**
 * @filename:BuildApplyServiceImpl 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.constant.FlowConst;
import com.lhy.houseflow.entity.BuildApply;
import com.lhy.houseflow.entity.CaseRecord;
import com.lhy.houseflow.entity.SysUser;
import com.lhy.houseflow.entity.WfDefinitionStep;
import com.lhy.houseflow.entity.WfInstance;
import com.lhy.houseflow.entity.WfProcess;
import com.lhy.houseflow.entity.vo.BuildApplyVo;
import com.lhy.houseflow.entity.vo.FlowProcessVo;
import com.lhy.houseflow.dao.BuildApplyDao;
import com.lhy.houseflow.dao.SysUserDao;
import com.lhy.houseflow.dao.SysUserRoleDao;
import com.lhy.houseflow.dao.WfDefinitionStepDao;
import com.lhy.houseflow.dao.WfInstanceDao;
import com.lhy.houseflow.dao.WfProcessDao;
import com.lhy.houseflow.service.BuildApplyService;
import com.lhy.houseflow.service.IllegalBuildingLandService;

/**   
 *  
 * @Description:  建房申请——SERVICEIMPL
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Service
@Transactional(rollbackFor=Exception.class)
@Slf4j
public class BuildApplyServiceImpl implements BuildApplyService {
	
	@Autowired
	public BuildApplyDao buildApplyDao;
	@Autowired
    private SysUserRoleDao userRoleDao;
    @Autowired
    private SysUserDao userDao;
    @Autowired
    private WfInstanceDao insDao;
    @Autowired
    private WfProcessDao processDao;
    @Autowired
    private IllegalBuildingLandService illegalBuildingLandService;
    @Autowired
    private WfDefinitionStepDao wfDefStepDao;
	//查询对象
	@Override
	public BuildApply findById(Long id) {
		return buildApplyDao.findById(id);
	}
	
	//删除对象
	@Override
	public int deleteById(Long id) {
		return buildApplyDao.deleteById(id);
	}
	
	//添加对象
	@Override
	public int add(BuildApply buildApply,Long curUserId) {
		 WfDefinitionStep startupStep = illegalBuildingLandService.findStartUpStep(FlowConst.DEF_BUILD_APPLY);
        Date curDate = new Date();
    	WfInstance ins = new WfInstance();
        ins.setDefId(FlowConst.DEF_BUILD_APPLY);
        ins.setStatus(FlowConst.INS_StartUp); //草稿
        if(buildApply.getStatus() != null && buildApply.getStatus() == FlowConst.CASE_STATUS_PROCESSING) {
        	ins.setStatus(FlowConst.INS_WaitingForSignature);
        }
        ins.setStepId(startupStep.getStepId());
        ins.setCreator(new Long(curUserId).intValue());
        ins.setCreatetime(curDate);
        ins.setFromuser(new Long(curUserId).intValue());
        ins.setFromtime(curDate);
        insDao.add(ins);
        
        Long insId = ins.getInsId();
        
        WfProcess process = new WfProcess();
        process.setInsId(insId);
        process.setStepId(startupStep.getStepId());
        process.setFromuser(new Long(curUserId).intValue());
        process.setFromtime(curDate);
        process.setProcessstatus(FlowConst.PROCESS_STATUS_NotOpen);
        process.setProcesstype(FlowConst.PROCESS_TYPE_Process);
        if(buildApply.getStatus() != null && buildApply.getStatus() == FlowConst.CASE_STATUS_PROCESSING) {
        	process.setTouser(new Long(curUserId).intValue());
            process.setTotime(curDate);
            process.setProcessstatus(FlowConst.PROCESS_STATUS_Finished);
            process.setFinishtime(curDate);
        }
        processDao.add(process);
        if(buildApply.getStatus() != null && buildApply.getStatus() == FlowConst.CASE_STATUS_PROCESSING) {
        	List<WfDefinitionStep> nextStep = illegalBuildingLandService.findNextSteps(startupStep.getStepId()) ;
        	if(nextStep.size()>0) {
        		for(WfDefinitionStep s: nextStep) {
        			Long nextStepId = s.getStepId();
        			List<SysUser> toUserList = illegalBuildingLandService.wfToUser(nextStepId);
    				for(SysUser u: toUserList) {
    					WfProcess nextProcess = new WfProcess();
    					nextProcess.setStepId(nextStepId);
    					nextProcess.setLastprocessId(process.getProcessId());
    					nextProcess.setFromtime(new Date());
    					nextProcess.setInsId(process.getInsId());
    					nextProcess.setFromuser(process.getTouser());
    					nextProcess.setProcessstatus(FlowConst.PROCESS_STATUS_NotOpen);
    					nextProcess.setProcesstype(process.getProcesstype());
    					nextProcess.setTouser(new Long(u.getId()).intValue());
    					processDao.add(nextProcess);
    				}
        		}
        		
        	}
        }
        buildApply.setProcessInstanceId(String.valueOf(insId));
		return buildApplyDao.add(buildApply);
	}
	
	//修改对象
	@Override
	public int update(BuildApply buildApply) {
		return buildApplyDao.update(buildApply);
	}
	
	//查询集合
	@Override
	public List<BuildApply> queryBuildApplyList(BuildApply buildApply) {
		return buildApplyDao.queryBuildApplyList(buildApply);
	}
	
	//分页查询
	@Override
	public PageInfo<BuildApply> list(AppPage<BuildApply> page) {
		PageHelper.startPage(page.getPageNum(),page.getPageSize());
		List<BuildApply> list = null;
		if (page.getParam() == null)
			list=buildApplyDao.list();		
		else
			list=buildApplyDao.queryBuildApplyList(page.getParam());
		PageInfo<BuildApply> pageInfo = new PageInfo<BuildApply>(list);
		return pageInfo;
	}

	@Override
	public void handleProcess(Long userId, Long insId, Long currStepId, Long nextStepId, String processOpinion) {
		WfInstance toHandleIns = insDao.findById(insId);
		if(toHandleIns.getStatus().equals(FlowConst.INS_StartUp)) {
			toHandleIns.setStatus(FlowConst.INS_Processing);
			WfProcess qryProcess = new WfProcess();
			qryProcess.setInsId(insId);
			List<WfProcess> wfProcessList = processDao.queryWfProcessList(qryProcess);
			for(WfProcess p: wfProcessList) {
				p.setTouser(new Long(userId).intValue());
				processDao.update(p);
			}
		}
		if(nextStepId == null || nextStepId == 0l) {
			List<WfDefinitionStep> nextStepList = illegalBuildingLandService.findNextSteps(currStepId);
			if(nextStepList.size() > 0) {
				nextStepId = nextStepList.get(0).getStepId();
			}
		}
		toHandleIns.setStepId(nextStepId);
		insDao.update(toHandleIns);
		
		BuildApply buildApply = new BuildApply();
		//buildApply.setProcessInstanceId(insId);
		List<BuildApply> buildApplyLIst = buildApplyDao.queryBuildApplyList(buildApply);
		if(buildApplyLIst.size() > 0) {
			for(BuildApply c:buildApplyLIst) {
				c.setStatus(FlowConst.CASE_STATUS_PROCESSING);
				buildApplyDao.update(c);
			}
		}
		WfProcess qryProcess = new WfProcess();
		qryProcess.setInsId(insId);
		qryProcess.setTouser(new Long(userId).intValue());
		List<WfProcess> wfProcessList = processDao.queryWfProcessList(qryProcess);
		List<SysUser> userList = illegalBuildingLandService.wfToUser(nextStepId);
		if(wfProcessList.size() > 0) {
			for(WfProcess p : wfProcessList) {
				p.setProcessstatus(FlowConst.PROCESS_STATUS_Finished);
				p.setOpinion(processOpinion);
				p.setFinishtime(new Date());
				processDao.update(p);
				for(SysUser u: userList) {
					WfProcess nextProcess = new WfProcess();
					nextProcess.setStepId(nextStepId);
					nextProcess.setLastprocessId(p.getProcessId());
					nextProcess.setFromtime(new Date());
					nextProcess.setInsId(p.getInsId());
					nextProcess.setFromuser(p.getTouser());
					nextProcess.setProcessstatus(FlowConst.PROCESS_STATUS_NotOpen);
					nextProcess.setProcesstype(p.getProcesstype());
					nextProcess.setTouser(new Long(u.getId()).intValue());
					processDao.add(nextProcess);
				}
			}
		}
	}

	@Override
	public BuildApplyVo viewBuildApply(Long insId) {
		BuildApplyVo vo = new BuildApplyVo();
		BuildApply b = new BuildApply();
		b.setProcessInstanceId(String.valueOf(insId));
		List<BuildApply> list = buildApplyDao.queryBuildApplyList(b);
		if(list.size() > 0) {
			BeanUtils.copyProperties(list.get(0), vo);
		}
		//TODO 附件
		
		List<WfProcess> hasFinishProcessList = illegalBuildingLandService.hasFinishProcess(insId);
		List<FlowProcessVo> hasFinishProcessVoList = new ArrayList<FlowProcessVo>();
		if(hasFinishProcessList.size() > 0) {
			for(WfProcess p: hasFinishProcessList) {
				hasFinishProcessVoList.add(illegalBuildingLandService.copyProcessToVo(p));
			}
		}
		vo.setHasFinishProcessVoList(hasFinishProcessVoList);
		WfProcess curProcess = null;
		List<WfProcess> curProcessList = illegalBuildingLandService.curToHandleProcess(insId);
		if(curProcessList.size() > 0) {
			curProcess = curProcessList.get(0);
			vo.setCurProcess(illegalBuildingLandService.copyProcessToVo(curProcess));
			//是否显示签收 办结 等按钮
			if(curProcess.getProcessstatus().equals(FlowConst.PROCESS_STATUS_NotOpen)) {
				vo.setIsShowSignBtn(FlowConst.SHOW_TRUE);
				vo.setIsShowHandleBtn(FlowConst.SHOW_FALSE);
			}
			if(curProcess.getProcessstatus().equals(FlowConst.PROCESS_STATUS_Processing)) {
				vo.setIsShowSignBtn(FlowConst.SHOW_FALSE);
				vo.setIsShowHandleBtn(FlowConst.SHOW_TRUE);
			}
			WfDefinitionStep curStep = wfDefStepDao.findById(curProcess.getStepId());
			if(curStep.getStepType().equals(FlowConst.STEP_TYPE_FinishStepType)) {
				vo.setIsShowFinishBtn(FlowConst.SHOW_TRUE);
			}else {
				vo.setIsShowFinishBtn(FlowConst.SHOW_FALSE);
			}
		}
		return vo;
	}

	@Override
	public PageInfo<BuildApply> draftList(AppPage<BuildApply> page) {
		PageHelper.startPage(page.getPageNum(),page.getPageSize());
		BuildApply b = new BuildApply();
		b.setStatus(FlowConst.CASE_STATUS_DRAFT);
		
		List<BuildApply> list = buildApplyDao.queryBuildApplyList(b);
		PageInfo<BuildApply> pageInfo = new PageInfo<BuildApply>(list);
		return pageInfo;
	}

	@Override
	public void finishProcess(Long currUserId, Long insId, String processOpinion, int caseStatus) {
		BuildApply b = new BuildApply();
		b.setProcessInstanceId(String.valueOf(insId));
		List<BuildApply> buildApplyList = buildApplyDao.queryBuildApplyList(b);
		if(buildApplyList.size() > 0) {
			BuildApply buildApply = buildApplyList.get(0);
			buildApply.setStatus(caseStatus);
			buildApplyDao.update(buildApply);
		}
		WfInstance wfIns = insDao.findById(insId);
		if(caseStatus == FlowConst.CASE_STATUS_FINISH) {
			wfIns.setStatus(FlowConst.INS_Finished);
		}
		if(caseStatus == FlowConst.CASE_STATUS_ABORTED) {
			wfIns.setStatus(FlowConst.INS_Aborted);
		}
		insDao.update(wfIns);
		WfProcess wfProcess = new WfProcess();
		wfProcess.setInsId(insId);
		List<WfProcess> processList = processDao.unHandleProcessList(insId);
		if(processList.size() > 0) {
			for(WfProcess p : processList) {
				if(p.getTouser() == new Long(currUserId).intValue()) {
					p.setProcessstatus(FlowConst.PROCESS_STATUS_Finished);
					p.setOpinion(processOpinion);
					p.setFinishtime(new Date());
				}else {
					p.setProcessstatus(FlowConst.PROCESS_STATUS_Abandoned);
				}
				processDao.update(p);
			}
		}
		
	}

	@Override
	public PageInfo<BuildApply> myWorkList(Long userId, String myWorkStatus, AppPage<BuildApply> page) {
		PageHelper.startPage(page.getPageNum(),page.getPageSize());
		List<BuildApply> list = new ArrayList<BuildApply>();
		if(myWorkStatus.equals(FlowConst.MY_WORK_TODO)) { //待办
			list = findMyTodo(userId);
		}
		else if(myWorkStatus.equals(FlowConst.MY_WORK_HAS_DONE)) {
			list = findMyHasDone(userId);
		}
		PageInfo<BuildApply> pageInfo = new PageInfo<BuildApply>(list);
		return pageInfo;
	}
	
	private List<BuildApply> findMyTodo(Long userId) {
 		Set<BuildApply> caseRecordSet = new HashSet<BuildApply>();
 		List<WfProcess> processList = processDao.findMyTodoProcess(userId);
 		if(processList.size() > 0) {
 			for (WfProcess p :processList) {
 				BuildApply c = new BuildApply();
 				c.setProcessInstanceId(String.valueOf(p.getInsId()));
 				List<BuildApply> cList = buildApplyDao.queryBuildApplyList(c);
 				caseRecordSet.addAll(cList);
 			}
 		}
 		List<BuildApply> caseRecordList = new ArrayList<BuildApply>();
 		caseRecordList.addAll(caseRecordSet);
 		return caseRecordList;
 	}
 	private List<BuildApply> findMyHasDone(Long userId) {
 		Set<BuildApply> caseRecordSet = new HashSet<BuildApply>();
 		List<WfProcess> processList = processDao.findMyDoneProcess(userId);
 		if(processList.size() > 0) {
 			for (WfProcess p :processList) {
 				BuildApply c = new BuildApply();
 				c.setProcessInstanceId(String.valueOf(p.getInsId()));
 				List<BuildApply> cList = buildApplyDao.queryBuildApplyList(c);
 				caseRecordSet.addAll(cList);
 			}
 		}
 		List<BuildApply> caseRecordList = new ArrayList<BuildApply>();
 		caseRecordList.addAll(caseRecordSet);
 		return caseRecordList;
 	}
}