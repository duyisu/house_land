/**
 * @filename:XzqhServiceImpl 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.Xzqh;
import com.lhy.houseflow.dao.XzqhDao;
import com.lhy.houseflow.service.XzqhService;

/**   
 *  
 * @Description:  地区——SERVICEIMPL
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Service
@Transactional(rollbackFor=Exception.class)
@Slf4j
public class XzqhServiceImpl implements XzqhService {
	
	@Autowired
	public XzqhDao xzqhDao;
	
	//查询对象
	@Override
	public Xzqh findById(Long id) {
		return xzqhDao.findById(id);
	}
	
	//删除对象
	@Override
	public int deleteById(Long id) {
		return xzqhDao.deleteById(id);
	}
	
	//添加对象
	@Override
	public int add(Xzqh xzqh) {
		return xzqhDao.add(xzqh);
	}
	
	//修改对象
	@Override
	public int update(Xzqh xzqh) {
		return xzqhDao.update(xzqh);
	}
	
	//查询集合
	@Override
	public List<Xzqh> queryXzqhList(Xzqh xzqh) {
		return xzqhDao.queryXzqhList(xzqh);
	}
	
	//分页查询
	@Override
	public PageInfo<Xzqh> list(AppPage<Xzqh> page) {
		PageHelper.startPage(page.getPageNum(),page.getPageSize());
		List<Xzqh> list = null;
		if (page.getParam() == null)
			list=xzqhDao.list();		
		else
			list=xzqhDao.queryXzqhList(page.getParam());
		PageInfo<Xzqh> pageInfo = new PageInfo<Xzqh>(list);
		return pageInfo;
	}
}