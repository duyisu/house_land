/**
 * @filename:WfProcessServiceImpl 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.WfProcess;
import com.lhy.houseflow.dao.WfProcessDao;
import com.lhy.houseflow.service.WfProcessService;

/**   
 *  
 * @Description:  实例处理任务表——SERVICEIMPL
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Service
@Transactional(rollbackFor=Exception.class)
@Slf4j
public class WfProcessServiceImpl implements WfProcessService {
	
	@Autowired
	public WfProcessDao wfProcessDao;
	
	//查询对象
	@Override
	public WfProcess findById(Long id) {
		return wfProcessDao.findById(id);
	}
	
	//删除对象
	@Override
	public int deleteById(Long id) {
		return wfProcessDao.deleteById(id);
	}
	
	//添加对象
	@Override
	public int add(WfProcess wfProcess) {
		return wfProcessDao.add(wfProcess);
	}
	
	//修改对象
	@Override
	public int update(WfProcess wfProcess) {
		return wfProcessDao.update(wfProcess);
	}
	
	//查询集合
	@Override
	public List<WfProcess> queryWfProcessList(WfProcess wfProcess) {
		return wfProcessDao.queryWfProcessList(wfProcess);
	}
	
	//分页查询
	@Override
	public PageInfo<WfProcess> list(AppPage<WfProcess> page) {
		PageHelper.startPage(page.getPageNum(),page.getPageSize());
		List<WfProcess> list = null;
		if (page.getParam() == null)
			list=wfProcessDao.list();		
		else
			list=wfProcessDao.queryWfProcessList(page.getParam());
		PageInfo<WfProcess> pageInfo = new PageInfo<WfProcess>(list);
		return pageInfo;
	}
}