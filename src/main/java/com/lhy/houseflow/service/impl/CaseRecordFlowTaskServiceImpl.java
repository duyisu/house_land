/**
 * @filename:CaseRecordFlowTaskServiceImpl 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.CaseRecordFlowTask;
import com.lhy.houseflow.dao.CaseRecordFlowTaskDao;
import com.lhy.houseflow.service.CaseRecordFlowTaskService;

/**   
 *  
 * @Description:  两违流程表——SERVICEIMPL
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Service
@Transactional(rollbackFor=Exception.class)
@Slf4j
public class CaseRecordFlowTaskServiceImpl implements CaseRecordFlowTaskService {
	
	@Autowired
	public CaseRecordFlowTaskDao caseRecordFlowTaskDao;
	
	//查询对象
	@Override
	public CaseRecordFlowTask findById(Long id) {
		return caseRecordFlowTaskDao.findById(id);
	}
	
	//删除对象
	@Override
	public int deleteById(Long id) {
		return caseRecordFlowTaskDao.deleteById(id);
	}
	
	//添加对象
	@Override
	public int add(CaseRecordFlowTask caseRecordFlowTask) {
		return caseRecordFlowTaskDao.add(caseRecordFlowTask);
	}
	
	//修改对象
	@Override
	public int update(CaseRecordFlowTask caseRecordFlowTask) {
		return caseRecordFlowTaskDao.update(caseRecordFlowTask);
	}
	
	//查询集合
	@Override
	public List<CaseRecordFlowTask> queryCaseRecordFlowTaskList(CaseRecordFlowTask caseRecordFlowTask) {
		return caseRecordFlowTaskDao.queryCaseRecordFlowTaskList(caseRecordFlowTask);
	}
	
	//分页查询
	@Override
	public PageInfo<CaseRecordFlowTask> list(AppPage<CaseRecordFlowTask> page) {
		PageHelper.startPage(page.getPageNum(),page.getPageSize());
		List<CaseRecordFlowTask> list = null;
		if (page.getParam() == null)
			list=caseRecordFlowTaskDao.list();		
		else
			list=caseRecordFlowTaskDao.queryCaseRecordFlowTaskList(page.getParam());
		PageInfo<CaseRecordFlowTask> pageInfo = new PageInfo<CaseRecordFlowTask>(list);
		return pageInfo;
	}
}