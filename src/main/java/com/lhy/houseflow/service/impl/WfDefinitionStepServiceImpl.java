/**
 * @filename:WfDefinitionStepServiceImpl 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.extern.slf4j.Slf4j;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.common.model.ResponseInfo;
import com.lhy.houseflow.common.model.ResultInfo;
import com.lhy.houseflow.entity.WfDefinitionStep;
import com.lhy.houseflow.dao.WfDefinitionStepDao;
import com.lhy.houseflow.service.WfDefinitionStepService;

/**   
 *  
 * @Description:  工作流步骤定义表——SERVICEIMPL
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Service
@Transactional(rollbackFor=Exception.class)
@Slf4j
public class WfDefinitionStepServiceImpl implements WfDefinitionStepService {
	
	@Autowired
	public WfDefinitionStepDao wfDefinitionStepDao;
	
	//查询对象
	@Override
	public WfDefinitionStep findById(Long id) {
		return wfDefinitionStepDao.findById(id);
	}
	
	//删除对象
	@Override
	public int deleteById(Long id) {
		return wfDefinitionStepDao.deleteById(id);
	}
	
	//添加对象
	@Override
	public int add(WfDefinitionStep wfDefinitionStep) {
		return wfDefinitionStepDao.add(wfDefinitionStep);
	}
	
	//修改对象
	@Override
	public int update(WfDefinitionStep wfDefinitionStep) {
		return wfDefinitionStepDao.update(wfDefinitionStep);
	}
	
	//查询集合
	@Override
	public List<WfDefinitionStep> queryWfDefinitionStepList(WfDefinitionStep wfDefinitionStep) {
		return wfDefinitionStepDao.queryWfDefinitionStepList(wfDefinitionStep);
	}
	
	//分页查询
	@Override
	public PageInfo<WfDefinitionStep> list(AppPage<WfDefinitionStep> page) {
		PageHelper.startPage(page.getPageNum(),page.getPageSize());
		List<WfDefinitionStep> list = null;
		if (page.getParam() == null)
			list=wfDefinitionStepDao.list();		
		else
			list=wfDefinitionStepDao.queryWfDefinitionStepList(page.getParam());
		PageInfo<WfDefinitionStep> pageInfo = new PageInfo<WfDefinitionStep>(list);
		return pageInfo;
	}
}