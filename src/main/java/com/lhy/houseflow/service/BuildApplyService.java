/**
 * @filename:BuildApplyService 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.entity.BuildApply;
import com.lhy.houseflow.entity.SysUser;
import com.lhy.houseflow.entity.WfProcess;
import com.lhy.houseflow.entity.vo.BuildApplyVo;
import com.lhy.houseflow.entity.vo.CaseRecordBaseInfoVo;
/**   
 *  
 * @Description:  建房申请——SERVICE
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
public interface BuildApplyService {
	
	/**
	 * @explain 查询建房申请对象
	 * @param   对象参数：id
	 * @return  BuildApply
	 * @author  lhy
	 */
	public BuildApply findById(Long id);
	
	/**
	 * @explain 删除建房申请对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 */
	public int deleteById(Long id);
	
	/**
	 * @explain 添加建房申请对象
	 * @param   对象参数：BuildApply
	 * @return  int
	 * @author  lhy
	 */
	public int add(BuildApply buildApply, Long currUserId);
	
	/**
	 * @explain 修改建房申请对象
	 * @param   对象参数：BuildApply
	 * @return  int
	 * @author  lhy
	 */
	public int update(BuildApply buildApply);
	
	/**
	 * @explain 查询建房申请集合
	 * @param   对象参数：BuildApply
	 * @return  List<BuildApply>
	 * @author  lhy
	 */
	public List<BuildApply> queryBuildApplyList(BuildApply buildApply);
	
	/**
	 * @explain 分页查询建房申请
	 * @param   对象参数：BuildApply
	 * @return  PageDataVo<BuildApply>
	 * @author  lhy
	 */
	public PageInfo<BuildApply> list(AppPage<BuildApply> page);
	public PageInfo<BuildApply> draftList(AppPage<BuildApply> page);
	
    void handleProcess(Long userId,Long insId,Long currStepId,Long nextStepId,String processOpinion);
    BuildApplyVo viewBuildApply(Long insId);

	public void finishProcess(Long id, Long insId, String processOpinion, int caseStatus);

	public PageInfo<BuildApply> myWorkList(Long userId, String myWorkStatus, AppPage<BuildApply> page);
}