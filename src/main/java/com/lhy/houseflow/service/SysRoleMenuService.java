/**
 * @filename:SysRoleMenuService 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.service;

import java.util.List;

import com.github.pagehelper.PageInfo;
import com.lhy.houseflow.common.model.AppPage;
import com.lhy.houseflow.entity.SysRoleMenu;
/**   
 *  
 * @Description:  角色菜单——SERVICE
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
public interface SysRoleMenuService {
	
	/**
	 * @explain 查询角色菜单对象
	 * @param   对象参数：id
	 * @return  SysRoleMenu
	 * @author  lhy
	 */
	public SysRoleMenu findById(Long id);
	
	/**
	 * @explain 删除角色菜单对象
	 * @param   对象参数：id
	 * @return  int
	 * @author  lhy
	 */
	public int deleteById(Long id);
	
	/**
	 * @explain 添加角色菜单对象
	 * @param   对象参数：SysRoleMenu
	 * @return  int
	 * @author  lhy
	 */
	public int add(SysRoleMenu sysRoleMenu);
	
	/**
	 * @explain 修改角色菜单对象
	 * @param   对象参数：SysRoleMenu
	 * @return  int
	 * @author  lhy
	 */
	public int update(SysRoleMenu sysRoleMenu);
	
	/**
	 * @explain 查询角色菜单集合
	 * @param   对象参数：SysRoleMenu
	 * @return  List<SysRoleMenu>
	 * @author  lhy
	 */
	public List<SysRoleMenu> querySysRoleMenuList(SysRoleMenu sysRoleMenu);
	
	/**
	 * @explain 分页查询角色菜单
	 * @param   对象参数：SysRoleMenu
	 * @return  PageDataVo<SysRoleMenu>
	 * @author  lhy
	 */
	public PageInfo<SysRoleMenu> list(AppPage<SysRoleMenu> page);
}