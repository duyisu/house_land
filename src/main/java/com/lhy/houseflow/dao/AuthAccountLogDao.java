package com.lhy.houseflow.dao;

import com.lhy.houseflow.entity.AuthAccountLog;
import org.springframework.dao.DataAccessException;

import java.util.List;

/* *
 * @Author tomsun28
 * @Description 
 * @Date 8:27 2018/4/22
 */
public interface AuthAccountLogDao {

    List<AuthAccountLog> selectAccountLogList();

    int insertSelective(AuthAccountLog authAccountLog) throws DataAccessException;

}
