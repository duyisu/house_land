/**
 * @filename:BuildApplyFileDao 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import com.lhy.houseflow.entity.BuildApplyFile;

/**   
 *  
 * @Description:  建房申请附件表——DAO
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Mapper
public interface BuildApplyFileDao {
	
	public BuildApplyFile findById(Long id);
	
	public int deleteById(Long id);
	
	public int add(BuildApplyFile buildApplyFile);
	
	public int update(BuildApplyFile buildApplyFile);
	
	public List<BuildApplyFile> queryBuildApplyFileList(BuildApplyFile buildApplyFile);
	
	public List<BuildApplyFile> list();
}
