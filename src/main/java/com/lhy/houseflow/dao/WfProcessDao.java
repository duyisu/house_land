/**
 * @filename:WfProcessDao 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import com.lhy.houseflow.entity.WfProcess;

/**   
 *  
 * @Description:  实例处理任务表——DAO
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Mapper
public interface WfProcessDao {
    
    public WfProcess findById(Long id);
    
    public int deleteById(Long id);
    
    public int add(WfProcess wfProcess);
    
    public int update(WfProcess wfProcess);
    
    public List<WfProcess> queryWfProcessList(WfProcess wfProcess);
    
    public List<WfProcess> list();
    
    public List<WfProcess> unHandleProcessList(Long insId);
    
    public List<WfProcess> findMyTodoProcess(Long userId);
    
    public List<WfProcess> findMyDoneProcess(Long userId);
}
