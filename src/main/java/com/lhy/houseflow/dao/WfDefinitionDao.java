/**
 * @filename:WfDefinitionDao 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import com.lhy.houseflow.entity.WfDefinition;

/**   
 *  
 * @Description:  工作流定义表——DAO
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Mapper
public interface WfDefinitionDao {
	
	public WfDefinition findById(Long id);
	
	public int deleteById(Long id);
	
	public int add(WfDefinition wfDefinition);
	
	public int update(WfDefinition wfDefinition);
	
	public List<WfDefinition> queryWfDefinitionList(WfDefinition wfDefinition);
	
	public List<WfDefinition> list();
}
