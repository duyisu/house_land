/**
 * @filename:SysRoleMenuDao 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;

import com.lhy.houseflow.entity.SysRoleMenu;

/**   
 *  
 * @Description:  角色菜单——DAO
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Mapper
public interface SysRoleMenuDao {
	
	public SysRoleMenu findById(Long id);
	
	public int deleteById(Long id);
	
	public int add(SysRoleMenu sysRoleMenu);
	
	public int update(SysRoleMenu sysRoleMenu);
	
	public List<SysRoleMenu> querySysRoleMenuList(SysRoleMenu sysRoleMenu);
	
	public List<SysRoleMenu> list();

	@Delete("delete from sys_role_menu where role_id = #{id}")
    public void deleteByRoleId(Long id);
}
