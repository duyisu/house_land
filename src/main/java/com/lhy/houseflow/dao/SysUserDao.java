/**
 * @filename:SysUserDao 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import com.lhy.houseflow.entity.SysUser;
import com.lhy.houseflow.entity.dto.UserDto;

/**   
 *  
 * @Description:  用户——DAO
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Mapper
public interface SysUserDao {
	
	public SysUser findById(Long id);
	
	public int deleteById(Long id);
	
	public int add(SysUser sysUser);
	
	public int update(SysUser sysUser);
	
	public List<SysUser> querySysUserList(SysUser sysUser);
	
	public List<SysUser> list();
	
    @Select("select * from sys_user where user_name=#{userName} limit 1")
    @ResultMap("BaseResultMap")
    public SysUser findByUsername(@Param("userName")  String userName);
    
    @Select("<script>" +
        "select sys_user.*, department.name depart_name, role_ids from sys_user left join " 
        +"(select user_id, group_concat(role_id) role_ids from sys_user_role group by user_id) b " +
        "on sys_user.id = b.user_id " 
        +"left join department on department.id = sys_user.depart_id "
        +"where 1=1"
        + "<if test='departId!=null '>"
        + " and depart_id = #{departId} "
        + "</if>"
        + "<if test='userName!=null '>"
        + " and user_name = #{userName} "
        + "</if>" 
        + "<if test='userTel!=null '>"
        + " and user_tel = #{userTel} "
        + "</if>"
        + "<if test='enabled!=null '>"
        + " and is_enabled = #{enabled} "
        + "</if>"
        +"order by sys_user.id"+
        "</script>")
    @Results(  
    {  
        @Result(column="id" ,  property="id") ,
        @Result(column="user_name" , property="userName") ,
        @Result(column="depart_id" ,  property="departId") ,
        @Result(column="real_name" , property="realName") ,
        @Result(column="employee_no" , property="employeeNo") ,
        @Result(column="user_tel" , property="userTel") ,
        @Result(column="id_card_no" , property="idCardNo") ,
        @Result(column="user_sex" ,  property="userSex") ,
        @Result(column="user_password" , property="userPassword") ,
        @Result(column="salt" , property="salt") ,
        @Result(column="user_desc" , property="userDesc") ,
        @Result(column="is_enabled" ,  property="enabled") ,
        @Result(column="gmt_create" ,  property="gmtCreate") ,
        @Result(column="gmt_modified" ,  property="gmtModified") ,
        @Result(column="depart_name" ,  property="departName") ,
        @Result(typeHandler=com.lhy.houseflow.dao.LongArrayTypeHandler.class, column="role_ids" ,  property="roleIds")
    })  
    public List<UserDto> findUsers(
        @Param("departId")  Long departId, 
        @Param("userName")  String userName, 
        @Param("userTel")  String userTel, 
        @Param("enabled")  Integer enabled);
}
