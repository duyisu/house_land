/**
 * @filename:UserTrackDao 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import com.lhy.houseflow.entity.UserTrack;

/**   
 *  
 * @Description:  人员轨迹——DAO
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Mapper
public interface UserTrackDao {
	
	public UserTrack findById(Long id);
	
	public int deleteById(Long id);
	
	public int add(UserTrack userTrack);
	
	public int update(UserTrack userTrack);
	
	public List<UserTrack> queryUserTrackList(UserTrack userTrack);
	
	public List<UserTrack> list();
}
