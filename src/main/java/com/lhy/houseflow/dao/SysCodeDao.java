/**
 * @filename:SysCodeDao 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.lhy.houseflow.entity.SysCode;

/**   
 *  
 * @Description:  系统编号——DAO
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Mapper
public interface SysCodeDao {
	
	public SysCode findById(Long id);
	
	public int deleteById(Long id);
	
	public int add(SysCode sysCode);
	
	public int update(SysCode sysCode);
	
	public List<SysCode> querySysCodeList(SysCode sysCode);
	
	public List<SysCode> list();
    
    @Select("select * from sys_code where code_key=#{key} and code_date=#{day}")
    @ResultMap("BaseResultMap")
    public SysCode findByKeyAndDate(@Param("key") String key, @Param("day") Long day);
    
    @Update("update sys_code set curr_code=curr_code+1 where code_key=#{key} and code_date=#{day}")
     public void updateAddCode(@Param("key") String key, @Param("day") Long day);    
}
