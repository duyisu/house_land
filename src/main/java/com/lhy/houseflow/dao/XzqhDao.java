/**
 * @filename:XzqhDao 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import com.lhy.houseflow.entity.Xzqh;

/**   
 *  
 * @Description:  地区——DAO
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Mapper
public interface XzqhDao {
	
	public Xzqh findById(Long id);
	
	public int deleteById(Long id);
	
	public int add(Xzqh xzqh);
	
	public int update(Xzqh xzqh);
	
	public List<Xzqh> queryXzqhList(Xzqh xzqh);
	
	public List<Xzqh> list();
}
