/**
 * @filename:WfDefinitionStepDao 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import com.lhy.houseflow.entity.WfDefinitionStep;

/**   
 *  
 * @Description:  工作流步骤定义表——DAO
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Mapper
public interface WfDefinitionStepDao {
	
	public WfDefinitionStep findById(Long id);
	
	public int deleteById(Long id);
	
	public int add(WfDefinitionStep wfDefinitionStep);
	
	public int update(WfDefinitionStep wfDefinitionStep);
	
	public List<WfDefinitionStep> queryWfDefinitionStepList(WfDefinitionStep wfDefinitionStep);
	
	public List<WfDefinitionStep> list();
}
