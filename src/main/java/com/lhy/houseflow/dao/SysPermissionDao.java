/**
 * @filename:SysPermissionDao 2019年5月5日
 * @project HouseFlow V1.0 Copyright(c) 2018 lhy Co. Ltd. All right reserved.
 */
package com.lhy.houseflow.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import com.lhy.houseflow.entity.SysPermission;

/**
 * 
 * @Description: 权限——DAO
 * @Author: lhy
 * @CreateDate: 2019年5月5日
 * @Version: V1.0
 * 
 */
@Mapper
public interface SysPermissionDao {

    SysPermission findById(Long id);

    int deleteById(Long id);

    int add(SysPermission sysPermission);

    int update(SysPermission sysPermission);

    List<SysPermission> querySysPermissionList(SysPermission sysPermission);

    List<SysPermission> list();

    @Select("select * from sys_permission where code=#{code} limit 1")
    @ResultMap("BaseResultMap")
    SysPermission findByCode(@Param("code") String code);

    @Select("select sys_permission.* from sys_permission "
        + "inner join sys_role_menu on sys_permission.id = sys_role_menu.permission_id "
        + "inner join (select role_id from sys_user_role inner join sys_user on sys_user_role.user_id= sys_user.id where "
        + "sys_user.user_name=#{username}) A on sys_role_menu.role_id = A.role_id")

    @ResultMap("BaseResultMap")
    List<SysPermission> findByUsername(@Param("username") String username);

}
