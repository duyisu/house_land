package com.lhy.houseflow.dao;

import com.lhy.houseflow.entity.AuthOperationLog;
import org.springframework.dao.DataAccessException;

import java.util.List;

/* *
 * @Author tomsun28
 * @Description 
 * @Date 8:28 2018/4/22
 */
public interface AuthOperationLogDao {

    List<AuthOperationLog> selectOperationLogList();

    int insertSelective(AuthOperationLog operationLog) throws DataAccessException;
}
