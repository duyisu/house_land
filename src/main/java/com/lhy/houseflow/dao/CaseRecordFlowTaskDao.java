/**
 * @filename:CaseRecordFlowTaskDao 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import com.lhy.houseflow.entity.CaseRecordFlowTask;

/**   
 *  
 * @Description:  两违流程表——DAO
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Mapper
public interface CaseRecordFlowTaskDao {
	
	public CaseRecordFlowTask findById(Long id);
	
	public int deleteById(Long id);
	
	public int add(CaseRecordFlowTask caseRecordFlowTask);
	
	public int update(CaseRecordFlowTask caseRecordFlowTask);
	
	public List<CaseRecordFlowTask> queryCaseRecordFlowTaskList(CaseRecordFlowTask caseRecordFlowTask);
	
	public List<CaseRecordFlowTask> list();
}
