/**
 * @filename:SysRolePermissionDao 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import com.lhy.houseflow.entity.SysRolePermission;

/**   
 *  
 * @Description:  角色权限——DAO
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Mapper
public interface SysRolePermissionDao {
	
	public SysRolePermission findById(Long id);
	
	public int deleteById(Long id);
	
	public int add(SysRolePermission sysRolePermission);
	
	public int update(SysRolePermission sysRolePermission);
	
	public List<SysRolePermission> querySysRolePermissionList(SysRolePermission sysRolePermission);
	
	public List<SysRolePermission> list();
}
