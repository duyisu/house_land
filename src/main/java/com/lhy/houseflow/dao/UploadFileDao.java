/**
 * @filename:UploadFileDao 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import com.lhy.houseflow.entity.UploadFile;
import com.lhy.houseflow.entity.vo.FileInfoVo;

/**   
 *  
 * @Description:  上传文件——DAO
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Mapper
public interface UploadFileDao {
	
	public UploadFile findById(Long id);
	
	public int deleteById(Long id);
	
	public int add(UploadFile uploadFile);
	
	public int update(UploadFile uploadFile);
	
	public List<UploadFile> queryUploadFileList(UploadFile uploadFile);
	
	public List<UploadFile> list();
	
    @Select("select id, original_name as filename, business_sub_kind "
        + "from upload_file "
        + "where business_id = #{businessId} and business_kind = #{businessKind}")
    @Results(  
    {  
        @Result(column="id" ,  property="id") ,
        @Result(column="filename" , property="filename") ,
        @Result(column="businessKind" ,  property="businessKind") 
    })
    public List<FileInfoVo> findCaseRecordFileInfoByCaseId(
        @Param("businessId") Long businessId,
        @Param("businessKind") Integer businessKind);

}
