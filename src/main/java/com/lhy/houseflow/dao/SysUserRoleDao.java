/**
 * @filename:SysUserRoleDao 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.dao;

import java.util.List;

import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Mapper;

import com.lhy.houseflow.entity.SysUserRole;

/**   
 *  
 * @Description:  用户角色——DAO
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Mapper
public interface SysUserRoleDao {
	
	public SysUserRole findById(Long id);
	
	public int deleteById(Long id);
	
	public int add(SysUserRole sysUserRole);
	
	public int update(SysUserRole sysUserRole);
	
	public List<SysUserRole> querySysUserRoleList(SysUserRole sysUserRole);
	
	public List<SysUserRole> list();

	@Delete("delete from sys_user_role where user_id = #{id}")
    void deleteByUserId(Long id);
}
