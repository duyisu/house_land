/**
 * @filename:DictDao 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import com.lhy.houseflow.entity.Dict;

/**   
 *  
 * @Description:  字典类别——DAO
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Mapper
public interface DictDao {
	
	public Dict findById(Long id);
	
	public int deleteById(Long id);
	
	public int add(Dict dict);
	
	public int update(Dict dict);
	
	public List<Dict> queryDictList(Dict dict);
	
	public List<Dict> list();
}
