/**
 * @filename:CaseRecordFileDao 2019年5月5日
 * @project HouseFlow V1.0 Copyright(c) 2018 lhy Co. Ltd. All right reserved.
 */
package com.lhy.houseflow.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import com.lhy.houseflow.entity.CaseRecordFile;
import com.lhy.houseflow.entity.vo.FileInfoVo;

import io.swagger.annotations.ApiModelProperty;

/**
 * 
 * @Description: 立案登记附件表——DAO
 * @Author: lhy
 * @CreateDate: 2019年5月5日
 * @Version: V1.0
 * 
 */
@Mapper
public interface CaseRecordFileDao {

    public CaseRecordFile findById(Long id);

    public int deleteById(Long id);

    public int add(CaseRecordFile caseRecordFile);

    public int update(CaseRecordFile caseRecordFile);

    public List<CaseRecordFile> queryCaseRecordFileList(CaseRecordFile caseRecordFile);

    public List<CaseRecordFile> list();

    @Select("select * from case_record_file where case_id=#{caseId} and file_id=#{fileId} limit 1")
    @ResultMap("BaseResultMap")
    CaseRecordFile findByCaseIdAndFileId(@Param("caseId") Long caseId, @Param("fileId") Long fileId);
}
