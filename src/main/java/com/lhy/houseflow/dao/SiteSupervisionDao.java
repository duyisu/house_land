/**
 * @filename:SiteSupervisionDao 2019年9月4日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import com.lhy.houseflow.entity.SiteSupervision;

/**   
 *  
 * @Description:  现场监督——DAO
 * @Author:       lhy   
 * @CreateDate:   2019年9月4日
 * @Version:      V1.0
 *    
 */
@Mapper
public interface SiteSupervisionDao {
	
	public SiteSupervision findById(Long id);
	
	public int deleteById(Long id);
	
	public int add(SiteSupervision siteSupervision);
	
	public int update(SiteSupervision siteSupervision);
	
	public List<SiteSupervision> querySiteSupervisionList(SiteSupervision siteSupervision);
	
	public List<SiteSupervision> list();
}
