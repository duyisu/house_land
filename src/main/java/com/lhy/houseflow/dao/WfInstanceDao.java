/**
 * @filename:WfInstanceDao 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import com.lhy.houseflow.entity.WfInstance;

/**   
 *  
 * @Description:  工作流实例表——DAO
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Mapper
public interface WfInstanceDao {
	
	public WfInstance findById(Long id);
	
	public int deleteById(Long id);
	
	public int add(WfInstance wfInstance);
	
	public int update(WfInstance wfInstance);
	
	public List<WfInstance> queryWfInstanceList(WfInstance wfInstance);
	
	public List<WfInstance> list();
}
