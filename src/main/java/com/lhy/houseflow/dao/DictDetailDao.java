/**
 * @filename:DictDetailDao 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import com.lhy.houseflow.entity.DictDetail;

/**   
 *  
 * @Description:  字典明细——DAO
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Mapper
public interface DictDetailDao {
	
	public DictDetail findById(Long id);
	
	public int deleteById(Long id);
	
	public int add(DictDetail dictDetail);
	
	public int update(DictDetail dictDetail);
	
	public List<DictDetail> queryDictDetailList(DictDetail dictDetail);
	
	public List<DictDetail> list();

    @Select("select dict_detail.* from dict_detail INNER JOIN dict on dict_detail.dict_id=dict.id where dict.name=#{userName} order by dict_detail.sort")
    @ResultMap("BaseResultMap")
	public List<DictDetail> queryByDictName(String dictName);
}
