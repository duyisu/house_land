/**
 * @filename:CarSchedulingDao 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import com.lhy.houseflow.entity.CarScheduling;

/**   
 *  
 * @Description:  车辆排班——DAO
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Mapper
public interface CarSchedulingDao {
	
	public CarScheduling findById(Long id);
	
	public int deleteById(Long id);
	
	public int add(CarScheduling carScheduling);
	
	public int update(CarScheduling carScheduling);
	
	public List<CarScheduling> queryCarSchedulingList(CarScheduling carScheduling);
	
	public List<CarScheduling> list();
}
