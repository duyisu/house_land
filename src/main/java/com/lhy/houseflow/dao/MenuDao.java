/**
 * @filename:MenuDao 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import com.lhy.houseflow.entity.Menu;

/**   
 *  
 * @Description:  菜单资源表——DAO
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Mapper
public interface MenuDao {
	
	public Menu findById(Long id);
	
	public int deleteById(Long id);
	
	public int add(Menu menu);
	
	public int update(Menu menu);
	
	public List<Menu> queryMenuList(Menu menu);
}
