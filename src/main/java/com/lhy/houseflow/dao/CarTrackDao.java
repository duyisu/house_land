/**
 * @filename:CarTrackDao 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import com.lhy.houseflow.entity.CarTrack;

/**   
 *  
 * @Description:  车辆轨迹——DAO
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Mapper
public interface CarTrackDao {
	
	public CarTrack findById(Long id);
	
	public int deleteById(Long id);
	
	public int add(CarTrack carTrack);
	
	public int update(CarTrack carTrack);
	
	public List<CarTrack> queryCarTrackList(CarTrack carTrack);
	
	public List<CarTrack> list();
}
