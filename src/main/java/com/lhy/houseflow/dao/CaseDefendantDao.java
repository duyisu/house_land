/**
 * @filename:CaseDefendantDao 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import com.lhy.houseflow.entity.CaseDefendant;

/**   
 *  
 * @Description:  立案登记当事人——DAO
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Mapper
public interface CaseDefendantDao {
	
	public CaseDefendant findById(Long id);
	
	public int deleteById(Long id);
	
	public int add(CaseDefendant caseDefendant);
	
	public int update(CaseDefendant caseDefendant);
	
	public List<CaseDefendant> queryCaseDefendantList(CaseDefendant caseDefendant);
	
	public List<CaseDefendant> list();

    @Select("select * from case_defendant where case_id=#{caseId}")
    @ResultMap("BaseResultMap")
    public List<CaseDefendant> findByCaseId(@Param("caseId") Long caseId);
}
