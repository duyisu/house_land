/**
 * @filename:WfDefinitionSteprelaDao 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import com.lhy.houseflow.entity.WfDefinitionSteprela;

/**   
 *  
 * @Description:  步骤关系——DAO
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Mapper
public interface WfDefinitionSteprelaDao {
	
	public WfDefinitionSteprela findById(Long id);
	
	public int deleteById(Long id);
	
	public int add(WfDefinitionSteprela wfDefinitionSteprela);
	
	public int update(WfDefinitionSteprela wfDefinitionSteprela);
	
	public List<WfDefinitionSteprela> queryWfDefinitionSteprelaList(WfDefinitionSteprela wfDefinitionSteprela);
	
	public List<WfDefinitionSteprela> list();
}
