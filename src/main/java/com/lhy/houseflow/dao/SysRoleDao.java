/**
 * @filename:SysRoleDao 2019年5月5日
 * @project HouseFlow V1.0 Copyright(c) 2018 lhy Co. Ltd. All right reserved.
 */
package com.lhy.houseflow.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Result;
import org.apache.ibatis.annotations.Results;
import org.apache.ibatis.annotations.Select;

import com.lhy.houseflow.entity.SysRole;
import com.lhy.houseflow.entity.dto.RoleDto;

/**
 * 
 * @Description: 角色——DAO
 * @Author: lhy
 * @CreateDate: 2019年5月5日
 * @Version: V1.0
 * 
 */
@Mapper
public interface SysRoleDao {

    public SysRole findById(Long id);

    public int deleteById(Long id);

    public int add(SysRole sysRole);

    public int update(SysRole sysRole);

    public List<SysRole> querySysRoleList(SysRole sysRole);

    public List<SysRole> list();

    @Select("<script>" 
        + "select A.* , B.menu_ids, B.permission_ids from sys_role A LEFT JOIN  "
        + "(select role_id, group_concat(menu_id) menu_ids, group_concat(permission_id) permission_ids from sys_role_menu group by role_id) B "
        + "on A.id = B.role_id "
        +"where 1=1 "
        + "<if test='roleName!=null '>"
        + "and name = #{roleName} "
        + "</if>"
        + " order by A.name "
        + "</script>")
    @Results({@Result(column = "id", property = "id"), 
        @Result(column = "name", property = "name"),
        @Result(column = "role_desc", property = "roleDesc"), 
        @Result(column = "role_kind", property = "roleKind"),
        @Result(column = "is_enabled", property = "enabled"), 
//        @Result(column = "gmt_create", property = "gmtCreate"),
//        @Result(column = "gmt_modified", property = "gmtModified"),
        @Result(typeHandler = com.lhy.houseflow.dao.LongArrayTypeHandler.class, column = "menu_ids", property = "menuIds"),
        @Result(typeHandler = com.lhy.houseflow.dao.LongArrayTypeHandler.class, column = "permission_ids", property = "permissionIds")})
    public List<RoleDto> FindRoles(String roleName);
    @Select("<script>" 
        + "select A.* , B.menu_ids, B.permission_ids from sys_role A LEFT JOIN  "
        + "(select role_id, group_concat(menu_id) menu_ids, group_concat(permission_id) permission_ids from sys_role_menu group by role_id) B "
        + "on A.id = B.role_id "
        +"where 1=1 "
        + "<if test='id!=null '>"
        + "and id = #{id} "
        + "</if>"
        + " order by A.name "
        + "</script>")
    @Results({@Result(column = "id", property = "id"), 
        @Result(column = "name", property = "name"),
        @Result(column = "role_desc", property = "roleDesc"), 
        @Result(column = "role_kind", property = "roleKind"),
        @Result(column = "is_enabled", property = "enabled"), 
//        @Result(column = "gmt_create", property = "gmtCreate"),
//        @Result(column = "gmt_modified", property = "gmtModified"),
        @Result(typeHandler = com.lhy.houseflow.dao.LongArrayTypeHandler.class, column = "menu_ids", property = "menuIds"),
        @Result(typeHandler = com.lhy.houseflow.dao.LongArrayTypeHandler.class, column = "permission_ids", property = "permissionIds")})
    public RoleDto findRoleDtoById(Long id);
}
