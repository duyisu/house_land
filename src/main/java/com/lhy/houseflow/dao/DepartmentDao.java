/**
 * @filename:DepartmentDao 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import com.lhy.houseflow.entity.Department;

/**   
 *  
 * @Description:  部门——DAO
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Mapper
public interface DepartmentDao {
	
	public Department findById(Long id);
	
	public int deleteById(Long id);
	
	public int add(Department department);
	
	public int update(Department department);
	
	public List<Department> queryDepartmentList(Department department);
	
	public List<Department> list();
}
