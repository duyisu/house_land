/**
 * @filename:SysMenuDao 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import com.lhy.houseflow.entity.SysMenu;

/**   
 *  
 * @Description:  菜单资源——DAO
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Mapper
public interface SysMenuDao {
	
	public SysMenu findById(Long id);
	
	public int deleteById(Long id);
	
	public int add(SysMenu sysMenu);
	
	public int update(SysMenu sysMenu);
	
	public List<SysMenu> querySysMenuList(SysMenu sysMenu);
	
	public List<SysMenu> list();
	
}
