/**
 * @filename:CaseRecordDao 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.dao;

import java.util.List;

import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import com.lhy.houseflow.entity.CaseRecord;

/**   
 *  
 * @Description:  立案登记——DAO
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Mapper
public interface CaseRecordDao {
	
	public CaseRecord findById(Long id);
	
	public int deleteById(Long id);
	
	public int add(CaseRecord caseRecord);
	
	public int update(CaseRecord caseRecord);
	
	public List<CaseRecord> queryCaseRecordList(CaseRecord caseRecord);
	
	public List<CaseRecord> list();

    @Update("update case_record set process_instance_id=#{processInstanceId} where id=#{id}")
    public int updateProcessInstanceId(@Param("processInstanceId") String processInstanceId, @Param("id") Long id);
    
    @Select("select A.* from case_record A inner join (select case_id from case_record_flow_task where task_id=#{taskId}) B on A.id = B.case_id limit 1")
    @ResultMap("BaseResultMap")
    public CaseRecord findByTaskId(@Param("taskId") String taskId);

    /*
    @Select("select A.* from case_record A inner join (select case_id from case_record_flow_task where task_id=#{taskId}) B on A.id = B.case_id limit 1")
    @ResultMap("BaseResultMap")
    
    public CaseRecord findByTaskId(@Param("taskId") String taskId);
    
    String reportName, String defendantName, Integer sources,
    Integer caseType, Integer caseSubtype, Long areaId, String addr*/
}
