/**
 * @filename:AreaDao 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import com.lhy.houseflow.entity.Area;

/**   
 *  
 * @Description:  区域——DAO
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Mapper
public interface AreaDao {
	
	public Area findById(Long id);
	
	public int deleteById(Long id);
	
	public int add(Area area);
	
	public int update(Area area);
	
	public List<Area> queryAreaList(Area area);
	
	public List<Area> list();
}
