/**
 * @filename:CarRouteDao 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import com.lhy.houseflow.entity.CarRoute;

/**   
 *  
 * @Description:  车辆轨迹表——DAO
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Mapper
public interface CarRouteDao {
	
	public CarRoute findById(Long id);
	
	public int deleteById(Long id);
	
	public int add(CarRoute carRoute);
	
	public int update(CarRoute carRoute);
	
	public List<CarRoute> queryCarRouteList(CarRoute carRoute);
}
