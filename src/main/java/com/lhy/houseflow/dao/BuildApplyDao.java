/**
 * @filename:BuildApplyDao 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import com.lhy.houseflow.entity.BuildApply;

/**   
 *  
 * @Description:  建房申请——DAO
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Mapper
public interface BuildApplyDao {
	
	public BuildApply findById(Long id);
	
	public int deleteById(Long id);
	
	public int add(BuildApply buildApply);
	
	public int update(BuildApply buildApply);
	
	public List<BuildApply> queryBuildApplyList(BuildApply buildApply);
	
	public List<BuildApply> list();
}
