package com.lhy.houseflow.dao;

import java.sql.CallableStatement;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.apache.commons.lang3.StringUtils;
import org.apache.ibatis.type.BaseTypeHandler;
import org.apache.ibatis.type.JdbcType;

public class LongArrayTypeHandler extends BaseTypeHandler<Set<Long>>{

    private static final String SEPARATOR_CHAR = ",";
    @Override
    public Set<Long> getNullableResult(ResultSet rs, String columnName) throws SQLException {        
        if (rs.wasNull())
            return null;
        String str = rs.getString(columnName);
        return getResultFromColumnValue(str);
    }

    @Override
    public Set<Long> getNullableResult(ResultSet rs, int columnIndex) throws SQLException {
        if (rs.wasNull())
            return null;
        String str = rs.getString(columnIndex);
        return getResultFromColumnValue(str);
    }

    private Set<Long> getResultFromColumnValue(String str) {
        String[] arrayIds = StringUtils.split(str, SEPARATOR_CHAR);
        Set<Long> idSet = new HashSet<Long>();
        if (arrayIds != null) {
            for (String id : arrayIds) {
                idSet.add(Long.parseLong(id));
            }
        }
        return idSet;
    }

    @Override
    public Set<Long> getNullableResult(CallableStatement rs, int columnIndex) throws SQLException {
        
        if (rs.wasNull())
            return null;
        String str = rs.getString(columnIndex);
        return getResultFromColumnValue(str);
    }

    @Override
    public void setNonNullParameter(PreparedStatement ps, int i,Set<Long> parameter, JdbcType jdbcType) throws SQLException {
        List<String> list = new ArrayList<String>();
        for (Long item : parameter) {
            list.add(String.valueOf(item));
        }
        ps.setString(i, String.join(",", list));
    }

}
