/**
 * @filename:CaseRecordFlowTaskFileDao 2019年5月5日
 * @project HouseFlow  V1.0
 * Copyright(c) 2018 lhy Co. Ltd. 
 * All right reserved. 
 */
package com.lhy.houseflow.dao;

import java.util.List;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.ResultMap;
import org.apache.ibatis.annotations.Select;

import com.lhy.houseflow.entity.CaseRecordFlowTaskFile;

/**   
 *  
 * @Description:  两违流程附件表——DAO
 * @Author:       lhy   
 * @CreateDate:   2019年5月5日
 * @Version:      V1.0
 *    
 */
@Mapper
public interface CaseRecordFlowTaskFileDao {
	
	public CaseRecordFlowTaskFile findById(Long id);
	
	public int deleteById(Long id);
	
	public int add(CaseRecordFlowTaskFile caseRecordFlowTaskFile);
	
	public int update(CaseRecordFlowTaskFile caseRecordFlowTaskFile);
	
	public List<CaseRecordFlowTaskFile> queryCaseRecordFlowTaskFileList(CaseRecordFlowTaskFile caseRecordFlowTaskFile);
	
	public List<CaseRecordFlowTaskFile> list();
    @Select("select * from case_record_flow_task_file where file_id=#{fileId} limit 1")
    @ResultMap("BaseResultMap")	
	CaseRecordFlowTaskFile findByFileId(Long fileId);
}
