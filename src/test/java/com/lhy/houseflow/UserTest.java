package com.lhy.houseflow;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.test.context.junit4.SpringRunner;

import com.lhy.houseflow.entity.SysUser;
import com.lhy.houseflow.entity.dto.UserDto;
import com.lhy.houseflow.service.SysUserService;

@RunWith(SpringRunner.class)
@SpringBootTest
public class UserTest {
    @Autowired
    private SysUserService userService;

    
    public void testChangePwds() {
        testChangeOldPassWord(1L);
        testChangeOldPassWord(2L);
    }
    @Test
    public void addUser(){
        UserDto userDto = new UserDto();
        userDto.setDepartId(1L);
        userDto.setUserName("lhy1");
        userDto.setUserPassword("");
        userDto.setUserSex(1);
        userDto.setUserTel("13422222222");
        userService.add(userDto);
    }

    private void testChangeOldPassWord(Long id) {
        SysUser user = userService.findById(id);

        BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
        // 加密
        String encodedPassword = passwordEncoder.encode(user.getUserPassword().trim());
        user.setUserPassword(encodedPassword);
        UserDto userDto = new UserDto();
        BeanUtils.copyProperties(user, userDto);
        userService.update(userDto);
    }
    
    

}
