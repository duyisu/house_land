package com.lhy.houseflow.service.impl;

import java.util.List;

import org.apache.tomcat.util.buf.StringUtils;
import org.flowable.bpmn.model.UserTask;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class BaseFlowTaskServiceImplTest {
    @Autowired
    private BaseFlowTaskServiceImpl service;

    @Test
    public void test() {
        List<UserTask> tasks =
            service.getUserTaskFormTypes("IllegalBuildingLand:5:fc59dd02-ca76-11e9-bd0a-9c4e367e7c35");
        if (tasks == null || tasks.isEmpty()) {
            log.info("NULL");
        } else {
            for (UserTask task : tasks) {                
                log.info("UserTasks: name:{}; doc:{}", task.getName(), task.getDocumentation());
            }
        }

    }
}
