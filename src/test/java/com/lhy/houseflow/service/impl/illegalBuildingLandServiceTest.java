package com.lhy.houseflow.service.impl;

import static org.junit.Assert.assertEquals;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.flowable.engine.runtime.ProcessInstance;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import com.lhy.houseflow.entity.CaseDefendant;
import com.lhy.houseflow.entity.CaseRecord;
import com.lhy.houseflow.entity.CaseRecordFile;
import com.lhy.houseflow.entity.JwtUser;
import com.lhy.houseflow.entity.JwtUserFactory;
import com.lhy.houseflow.entity.SysUser;
import com.lhy.houseflow.entity.vo.CaseRecordBaseInfoVo;
import com.lhy.houseflow.entity.vo.TaskVo;
import com.lhy.houseflow.service.IllegalBuildingLandService;
import com.lhy.houseflow.service.SysUserService;

import lombok.extern.slf4j.Slf4j;

@RunWith(SpringRunner.class)
@SpringBootTest
@Slf4j
public class illegalBuildingLandServiceTest {

    @Autowired
    IllegalBuildingLandService service;
    @Autowired
    SysUserService userService;

    // @Test
    public void addCaseRecord() {
        int idx = 0;
        CaseRecord info = new CaseRecord();
        info.setAddress("测试地址" + idx);
        info.setArea(new BigDecimal("100.12"));
        info.setAreaId(0L);
        info.setBuildKind(1);
        info.setBuildStruct(1);
        info.setCaseDesc("测试案件" + idx);
        info.setCaseSubtype(1);
        info.setCaseType(1);
        info.setComplainant("投诉人" + idx);
        info.setComplainantTel("投书电话" + idx);
        info.setEnabled(1);
        info.setLatitude(new BigDecimal("100.12"));
        info.setLongitude(new BigDecimal("100.12"));
        info.setPosition("测试位置" + idx);
        info.setPurposes("测试用途" + idx);
        info.setRealArea(new BigDecimal("100" + idx));
        info.setReportName("测试人员" + idx);
        info.setReportTel("测试电话" + idx);
        info.setSource(1);
        List<CaseDefendant> defendants = new ArrayList<CaseDefendant>();
        CaseDefendant d = new CaseDefendant();
        d.setDefendantIscardno("当事人身份证" + idx * 100 + idx);
        d.setDefendantName("当事人" + idx * 100 + idx);
        d.setDefendantTel("当事人电话" + +idx * 100 + idx);
        defendants.add(d);

        List<CaseRecordFile> files = new ArrayList<CaseRecordFile>();
        CaseRecordFile f = new CaseRecordFile();
        f.setFileId((long)(idx * 100 + idx));
        files.add(f);

        CaseRecord caseRecord = service.addCaseRecord(info, defendants, files);
        assertEquals(caseRecord.getAddress(), "测试地址" + idx);
    }

    @Test
    public void testCreatetask() {
        Long taskId = 2L;
        ProcessInstance inst = service.createTask("lhy", taskId);

    }

    @Test
    public void testMytask() {
        Long taskId = 2L;
        SysUser user = userService.findByUsername("lhy");
        JwtUser jwtUser = JwtUserFactory.create(user);
        List<TaskVo<CaseRecordBaseInfoVo>> list = service.myTask(jwtUser, null, null, null, null, null, null, null);
        for (TaskVo<CaseRecordBaseInfoVo> task : list) {
            log.info("taskname：{} id: {}", task.getName(), task.getId());
            log.info(task.toString());
        }
    }
}
