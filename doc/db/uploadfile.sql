

create table upload_file
(
   id                   bigint not null auto_increment comment 'id',
   filename             varchar(500) comment '文件名称',
   field_type           varchar(100) not null comment '文件类型--扩展名',
   original_name        varchar(500) not null comment '原始文件名称',
   filepath             varchar(500) not null comment '文件路径',
   file_url             varchar(500) comment '文件URL',
   is_enebled           boolean default 1 comment '是否有效（1：有效 0：无效）',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);

alter table upload_file comment '上传文件表';

create unique index udx_upload_file on upload_file
(
   filename,
   filepath
);
