

create table sys_menu
(
   id                   bigint not null auto_increment comment 'id',
   menu_name            varchar(500) not null comment '菜单名称',
   menu_code            varchar(500) comment '菜单代码',
   menu_type            tinyint comment '类型(0:菜单1：功能)',
   menu_order           smallint comment '菜单序号',
   parent_id            bigint comment '上级菜单',
   permission_id        bigint comment '权限ID',
   is_enebled           boolean default 1 comment '是否有效（1：有效0：无效）',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);

alter table sys_menu comment '菜单资源表';

create index idx_menu_parent_order on sys_menu
(
   parent_id,
   menu_order
);

create index idx_menu_parent_order on menu
(
   parent_id,
   menu_order
);



create table sys_permission
(
   id                   bigint not null auto_increment comment 'id',
   permission_code      varchar(500) not null comment '权限代码',
   permission_name      varchar(500) not null comment '权限名称',
   permission_desc      varchar(500) comment '权限描述',
   permission_url       varchar(500) comment '权限URL',
   permission_method    varchar(500) comment '权限方法',
   is_enebled           boolean default 1 comment '是否有效（1：有效0：无效）',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);

alter table sys_permission comment '权限表';

create unique index udx_permission_url_method on sys_permission
(
   permission_url,
   permission_method
);

create unique index udx_permission_code on sys_permission
(
   permission_code
);


create table sys_role_menu
(
   id                   bigint not null auto_increment comment 'id',
   role_id              bigint not null comment '角色ID',
   menu_id              bigint comment '权限ID',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);

alter table sys_role_menu comment '角色菜单表';

create unique index uk_role_permission on sys_role_menu
(
   role_id,
   menu_id
);


