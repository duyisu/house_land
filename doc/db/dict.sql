drop table if exists dict;

/*==============================================================*/
/* Table: dict                                                  */
/*==============================================================*/
create table dict
(
   id                   bigint not null auto_increment comment 'id',
   name                 varchar(255) not null comment '名称',
   remark               varchar(255) comment '描述',
   primary key (id)
);

alter table dict comment '字典';

/*==============================================================*/
/* Index: uk_dict                                               */
/*==============================================================*/
create index uk_dict on dict
(
   name
);
drop table if exists dict_detail;

/*==============================================================*/
/* Table: dict_detail                                           */
/*==============================================================*/
create table dict_detail
(
   id                   bigint not null comment 'id',
   name                 varchar(255) not null comment '标签名称',
   value                integer comment '值',
   sort                 integer comment '排序',
   dict_id              bigint comment '字典ID',
   primary key (id)
);

alter table dict_detail comment '字典明细';


INSERT INTO `dict` VALUES (1, 'use_status', '使用状态');

INSERT INTO `dict_detail` VALUES (1, '正常', 1, 1, 1);
INSERT INTO `dict_detail` VALUES (2, '停用', 0, 2, 1);


