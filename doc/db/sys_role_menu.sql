drop table if exists sys_role_menu;

/*==============================================================*/
/* Table: sys_role_menu                                         */
/*==============================================================*/
create table sys_role_menu
(
   id                   bigint not null auto_increment comment 'id',
   role_id              bigint not null comment '角色ID',
   menu_id              bigint comment '权限ID',
   permission_id        bigint comment '权限ID',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);

alter table sys_role_menu comment '角色菜单表';

/*==============================================================*/
/* Index: uk_role_menu                                          */
/*==============================================================*/
create unique index uk_role_menu on sys_role_menu
(
   role_id,
   menu_id,
   permission_id
);
