create table area
(
   id                   bigint not null auto_increment comment 'id',
   area_name            varchar(255) not null comment '区域名称',
   area_kind            smallint comment '区域类型',
   parent_id            bigint comment '上级区域',
   is_enebled           boolean default 1 comment '是否有效（1：有效0：无效）',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);
alter table area comment '区域表';
create unique index uk_area on area
(
   area_name,
   parent_id
);
create table build_apply
(
   id                   bigint not null auto_increment comment 'id',
   apply_no             varchar(20) comment '申请编号',
   process_instance_id  varchar(64) comment '流程实例ID',
   apply_name           varchar(50) comment '申请人',
   apply_tel            varchar(50) comment '申请电话',
   apply_time           datetime comment '申请时间',
   apply_addr           bigint comment '申请地址',
   apply_kind           smallint comment '申请类型',
   longitude            decimal(12,8) comment '经度',
   latitude             decimal(12,8) comment '纬度',
   holder_idcardno      varchar(30) comment '户主身份证号码',
   holder_name          varchar(50) comment '户主姓名',
   holder_birthday      date comment '户主生日',
   holder_sex           smallint comment '户主性别',
   is_married           bool comment '户主婚否',
   family_size          smallint comment '家庭人数',
   spouse_name          varchar(50) comment '配偶姓名',
   spouse_idcardno      varchar(30) comment '配偶身份证号码',
   address              varchar(255) comment '户口所在地',
   house_addr           varchar(255) comment '现住址',
   house_num            smallint comment '住房数量',
   area                 decimal(8,2) comment '占地面积',
   real_area            decimal(8,2) comment '建筑占地面积',
   house_time           date comment '建筑时间',
   floors               char(10) comment '建筑层数',
   house_desc           varchar(255) comment '房屋说明',
   apply_reason         smallint comment '申请理由',
   sumarea              decimal(8,2) comment '建筑总面积',
   apply_area           decimal(8,2) comment '申请占地面积',
   apply_real_area      decimal(8,2) comment '申请建筑面积',
   apply_house_time     date comment '申请建筑时间',
   apply_floors         smallint comment '申请建筑层数',
   eave_height          decimal(8,2) comment '申请檐口高度',
   apply_house_addr     varchar(255) comment '申请建房地点',
   land_kind            smallint comment '土地性质',
   build_paper_no       varchar(50) comment '建筑图集号',
   east_name            varchar(50) comment '东面姓名',
   south_name           varchar(50) comment '南面姓名',
   west_name            varchar(50) comment '西面姓名',
   north_name           varchar(50) comment '北面姓名',
   east_apply           smallint comment '东面意见',
   south_apply          smallint comment '南面意见',
   west_apply           smallint comment '西面意见',
   north_apply          smallint comment '北面意见',
   east_time            datetime comment '东面时间',
   south_time           datetime comment '南面时间',
   west_time            datetime comment '西面时间',
   north_time           datetime comment '北面时间',
   is_rule2             bool comment '符合规则2',
   is_rule3             bool comment '符合规则3',
   is_rule1             bool comment '符合规则1',
   is_enebled           boolean default 1 comment '是否有效（1：有效 0：无效）',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);
alter table build_apply comment '建房申请表';
create unique index uk_build_apply on build_apply
(
   apply_no
);
create table build_apply_file
(
   id                   bigint not null auto_increment comment 'id',
   apply_id             bigint comment '申请ID',
   file_kind            smallint comment '文件类型',
   filename             varchar(255) comment '文件名称',
   filepath             varchar(255) not null comment '文件路径',
   is_enebled           boolean default 1 comment '是否有效（1：有效 0：无效）',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);
alter table build_apply_file comment '建房申请附件表';
create table car
(
   id                   bigint not null auto_increment comment 'id',
   car_no               varchar(12) not null comment '车牌号码',
   car_name             varchar(50) comment '车辆名称',
   gps_no               varchar(50) comment '车辆端口',
   is_enebled           boolean default 1 comment '是否有效（1：有效0：无效）',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);
alter table car comment '车辆表';
create index uk_car on car
(
   car_no
);
create table car_scheduling
(
   id                   bigint not null auto_increment comment 'id',
   car_id               bigint not null comment '车辆ID',
   user_id              bigint comment '人员ID',
   depart_id            bigint comment '部门ID',
   user_tel             varchar(255) comment '电话',
   start_time           datetime comment '排班开始时间',
   stop_time            datetime comment '排班结束时间',
   input_time           date comment '排班制定时间',
   is_enebled           boolean default 1 comment '是否有效（1：有效0：无效）',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);
alter table car_scheduling comment '车辆排班表';
create unique index uk_car_scheduling on car_scheduling
(
   user_id,
   car_id,
   start_time
);
create table car_scheduling_area
(
   id                   bigint not null auto_increment comment 'id',
   scheduling_id        bigint comment '排班ID',
   area_id              bigint not null comment '区域ID',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);
alter table car_scheduling_area comment '车辆排班表';
create unique index uk_car_scheduling_area on car_scheduling_area
(
   scheduling_id,
   area_id
);
create table car_track
(
   id                   bigint not null auto_increment comment 'id',
   car_id               bigint not null comment '车辆ID',
   track_time           datetime comment '时间',
   locationx            decimal(12,8) comment '经度',
   locationy            decimal(12,8) comment '纬度',
   location_grid        bigint comment '定位网格',
   radius               decimal(12,6) comment '半径',
   loctype              varchar (50) comment 'LOCTYPE',
   satellite_number     int comment '卫星数量',
   isfiltered           boolean comment '是否过滤（1：过滤0：无）',
   track_group          varchar(255) comment '轨迹分组',
   track_flag           int comment '轨迹标志',
   is_valid             boolean default 1 comment '是否有效（1：有效0：无效）',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);
alter table car_track comment '车辆轨迹表';
create unique index uk_car_track on car_track
(
   car_id,
   track_time
);
create table case_defendant
(
   id                   bigint not null auto_increment comment 'id',
   case_id              bigint comment '案件ID',
   defendant_name       varchar(50) comment '被投诉人',
   defendant_tel        varchar(50) comment '被投诉人电话',
   defendant_iscardno   varchar(30) comment '被投诉人身份证号码',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);
alter table case_defendant comment '立案登记当事人表';
create unique index uk_case_defendant on case_defendant
(
   case_id,
   defendant_name
);
create table case_record
(
   id                   bigint not null auto_increment comment 'id',
   case_no              varchar(20) comment '案件编号',
   case_record_no       varchar(20) comment '立案编号',
   case_desc            varchar(2000) comment '案件描述',
   process_instance_id  varchar(64) comment '流程实例ID',
   source               smallint not null comment '来源',
   case_type            smallint comment '两违类型',
   case_subtype         smallint comment '二级类型',
   complainant          varchar(50) comment '投诉人',
   complainant_tel      varchar(50) comment '投诉电话',
   area_id              bigint comment '镇区ID',
   position             varchar(120) comment '位置',
   longitude            decimal(12,8) comment '经度',
   latitude             decimal(12,8) comment '纬度',
   address              varchar(255) comment '地址',
   purposes             varchar(255) comment '用途',
   area                 decimal(8,2) comment '占地面积',
   real_area            decimal(8,2) comment '建筑占地面积',
   build_kind           smallint comment '违建性质',
   build_struct         smallint comment '建筑结构',
   report_name          varchar(50) comment '上报人',
   reportt_tel          varchar(50) comment '上报电话',
   is_enebled           boolean default 1 comment '是否有效（1：有效 0：无效）',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);
alter table case_record comment '立案登记表';
create unique index uk_case_record on case_record
(
   case_no
);
create table case_record_file
(
   id                   bigint not null auto_increment comment 'id',
   case_id              bigint comment '案件ID',
   filename             varchar(255) comment '文件名称',
   filepath             varchar(255) not null comment '文件路径',
   is_enebled           boolean default 1 comment '是否有效（1：有效 0：无效）',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);
alter table case_record_file comment '立案登记附件表';
create index idx_case_record_file on case_record_file
(
   case_id
);
create table department
(
   id                   bigint not null auto_increment comment 'id',
   depart_name          varchar(255) not null comment '部门名称',
   depart_desc          varchar(255) comment '部门描述',
   parent_id            bigint comment '上级部门',
   is_enebled           boolean default 1 comment '是否有效（1：有效0：无效）',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);
alter table department comment '部门表';
create unique index uk_department on department
(
   depart_name
);
create table menu
(
   id                   bigint not null auto_increment comment 'id',
   menu_name            varchar(30) not null comment '菜单名称',
   menu_desc            varchar(255) comment '菜单描述',
   menu_url             varchar(255) comment '菜单地址',
   menu_action          varchar(20) comment '菜单动作',
   menu_order           smallint comment '菜单序号',
   parent_id            bigint comment '上级菜单',
   is_enebled           boolean default 1 comment '是否有效（1：有效0：无效）',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);
alter table menu comment '菜单资源表';
create table sys_permission
(
   id                   bigint not null auto_increment comment 'id',
   permission_name      varchar(30) not null comment '权限名称',
   permission_desc      varchar(255) comment '权限描述',
   menu_id              bigint comment '菜单ID',
   is_enebled           boolean default 1 comment '是否有效（1：有效0：无效）',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);
alter table sys_permission comment '权限表';
create table sys_role
(
   id                   bigint not null auto_increment comment 'id',
   role_name            varchar(50) not null comment '角色名称',
   role_desc            varchar(255) comment '角色描述',
   role_kind            smallint comment '角色级别（0：超级管理员1：管理员）',
   is_enebled           boolean default 1 comment '是否有效（1：有效0：无效）',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);
alter table sys_role comment '角色表';
create index uk_role on sys_role
(
   role_name
);
create table sys_role_permission
(
   id                   bigint not null auto_increment comment 'id',
   role_id              bigint not null comment '角色ID',
   permission_id        bigint comment '权限ID',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);
alter table sys_role_permission comment '角色权限表';
create unique index uk_role_permission on sys_role_permission
(
   role_id,
   permission_id
);
create table sys_user
(
   id                   bigint not null auto_increment comment 'id',
   user_name            varchar(30) not null comment '用户名称',
   depart_id            bigint comment '部门',
   real_name            varchar(50) comment '真实姓名',
   employee_no          varchar(20) comment '工号',
   user_tel             varchar(255) comment '手机号码',
   id_card_no           varchar(30) comment '身份证号码',
   user_sex             smallint comment '性别',
   user_password        varchar(20) comment '用户密码',
   salt                 varchar(16) comment 'salt',
   user_desc            varchar(255) comment '备注',
   is_enebled           boolean default 1 comment '是否有效（1：启用 0：停用）',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);
alter table sys_user comment '用户表';
create unique index uk_user on sys_user
(
   user_name
);
create table sys_user_role
(
   id                   bigint not null auto_increment comment 'id',
   user_id              bigint comment '用户ID',
   role_id              bigint not null comment '角色ID',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);
alter table sys_user_role comment '用户角色表';
create unique index uk_user_role on sys_user_role
(
   user_id,
   role_id
);
create table user_track
(
   id                   bigint not null auto_increment comment 'id',
   user_id              bigint not null comment '人员ID',
   track_time           datetime comment '时间',
   locationx            decimal(12,8) comment '经度',
   locationy            decimal(12,8) comment '纬度',
   location_grid        bigint comment '定位网格',
   radius               decimal(12,6) comment '半径',
   loctype              varchar(50) comment 'LOCTYPE',
   satellite_number     int comment '卫星数量',
   isfiltered           boolean comment '是否过滤（1：过滤0：无）',
   track_group          varchar(255) comment '轨迹分组',
   track_flag           int comment '轨迹标志',
   is_valid             boolean default 1 comment '是否有效（1：有效0：无效）',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);
alter table user_track comment '人员轨迹表';
create unique index uk_user_track on user_track
(
   user_id,
   track_time
);
