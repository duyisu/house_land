/*
Navicat MySQL Data Transfer

Source Server         : 118.24.251.241
Source Server Version : 50727
Source Host           : 118.24.251.241:3306
Source Database       : house_flow

Target Server Type    : MYSQL
Target Server Version : 50727
File Encoding         : 65001

Date: 2019-08-24 21:59:09
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `sys_menu`
-- ----------------------------
DROP TABLE IF EXISTS `sys_menu`;
CREATE TABLE `sys_menu` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT COMMENT 'id',
  `menu_name` varchar(500) NOT NULL COMMENT '菜单名称',
  `menu_code` varchar(500) DEFAULT NULL COMMENT '菜单代码',
  `menu_type` tinyint(4) DEFAULT NULL COMMENT '类型(0:菜单1：功能)',
  `menu_order` smallint(6) DEFAULT NULL COMMENT '菜单序号',
  `parent_id` bigint(20) DEFAULT NULL COMMENT '上级菜单',
  `permission_id` bigint(20) DEFAULT NULL COMMENT '权限ID',
  `is_enebled` tinyint(1) DEFAULT '1' COMMENT '是否有效（1：有效0：无效）',
  `gmt_create` datetime DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `gmt_modified` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '修改时间',
  PRIMARY KEY (`id`),
  KEY `idx_menu_parent_order` (`parent_id`,`menu_order`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of sys_menu
-- ----------------------------
