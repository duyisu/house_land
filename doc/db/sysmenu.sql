create table sys_menu
(
   id                   bigint not null auto_increment comment 'id',
   menu_name            varchar(500) not null comment '菜单名称',
   menu_code            varchar(500) comment '菜单代码',
   menu_type            tinyint comment '类型(0:菜单1：功能)',
   menu_order           smallint comment '菜单序号',
   parent_id            bigint comment '上级菜单',
   permission_id        bigint comment '权限ID',
   is_enebled           boolean default 1 comment '是否有效（1：有效0：无效）',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);

alter table sys_menu comment '菜单资源表';

create index idx_menu_parent_order on sys_menu
(
   parent_id,
   menu_order
);

create index idx_menu_parent_order on menu
(
   parent_id,
   menu_order
);