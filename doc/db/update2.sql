create table upload_file
(
   id                   bigint not null auto_increment comment 'id',
   filename             varchar(500) comment '文件名称',
   field_type           varchar(100) not null comment '文件类型--扩展名',
   original_name        varchar(500) not null comment '原始文件名称',
   filepath             varchar(500) not null comment '文件路径',
   is_enebled           boolean default 1 comment '是否有效（1：有效 0：无效）',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);

alter table upload_file comment '上传文件表';

create unique index udx_upload_file on upload_file
(
   filename,
   filepath
);


drop table if exists case_record_file;

create table case_record_file
(
   id                   bigint not null auto_increment comment 'id',
   case_id              bigint comment '案件ID',
   file_id              bigint comment '文件ID',
   is_enebled           boolean default 1 comment '是否有效（1：有效 0：无效）',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);

alter table case_record_file comment '立案登记附件表';

create unique index udx_case_record_file on case_record_file
(
   case_id,
   file_id
);

drop table if exists case_record_flow_task_file;

create table case_record_flow_task_file
(
   id                   bigint not null auto_increment comment 'id',
   file_id              bigint comment '文件ID',
   flow_task_id         bigint comment '两违流程任务ID',
   is_enebled           boolean default 1 comment '是否有效（1：有效 0：无效）',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);

alter table case_record_flow_task_file comment '两违流程附件表';

create unique index udx_case_record_flow_task_file on case_record_flow_task_file
(
   file_id,
   flow_task_id
);


drop table if exists build_apply_file;

create table build_apply_file
(
   id                   bigint not null auto_increment comment 'id',
   apply_id             bigint comment '申请ID',
   file_id              bigint comment '文件ID',
   is_enebled           boolean default 1 comment '是否有效（1：有效 0：无效）',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);

alter table build_apply_file comment '建房申请附件表';


create index udx_build_apply_file on build_apply_file
(
   apply_id,
   file_id
);


drop table if exists case_record_flow_task;

create table case_record_flow_task
(
   id                   bigint not null auto_increment comment 'id',
   case_Id              bigint comment '案件Id',
   task_id              varchar(64) comment '任务ID',
   process_instance_id  varchar(64) comment '流程实例ID',
   opr_user_id          bigint not null comment '处理人员ID',
   opr_method           varchar(255) comment '处理措施',
   opr_type             varchar(255) comment '处理意见',
   opr_desc             varchar(1000) comment '处理意见描述',
   self_area            decimal(8,2) comment '自行拆除面积',
   other_area           decimal(8,2) comment '协助拆除面积面积',
   new_area             decimal(8,2) comment '即建即拆面积',
   is_enebled           boolean default 1 comment '是否有效（1：有效 0：无效）',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);

alter table case_record_flow_task comment '两违流程表';

create index idx_case_record_flow_task on case_record_flow_task
(
   case_Id,
   task_id,
   process_instance_id
);

