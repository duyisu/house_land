ALTER TABLE area change is_enebled is_enabled boolean default 1;

ALTER TABLE build_apply change is_enebled is_enabled boolean default 1;

ALTER TABLE build_apply_file change is_enebled is_enabled boolean default 1;

ALTER TABLE car change is_enebled is_enabled boolean default 1;

ALTER TABLE car_scheduling change is_enebled is_enabled boolean default 1;

ALTER TABLE car_scheduling_area change is_enebled is_enabled boolean default 1;

ALTER TABLE car_track change is_enebled is_enabled boolean default 1;

ALTER TABLE case_defendant change is_enebled is_enabled boolean default 1;

ALTER TABLE case_record change is_enebled is_enabled boolean default 1;

ALTER TABLE case_record_file change is_enebled is_enabled boolean default 1;

ALTER TABLE case_record_flow_task change is_enebled is_enabled boolean default 1;

ALTER TABLE case_record_flow_task_file change is_enebled is_enabled boolean default 1;

ALTER TABLE department change is_enebled is_enabled boolean default 1;

ALTER TABLE dict change is_enebled is_enabled boolean default 1;

ALTER TABLE dict_detail change is_enebled is_enabled boolean default 1;

ALTER TABLE sys_code change is_enebled is_enabled boolean default 1;

ALTER TABLE sys_menu change is_enebled is_enabled boolean default 1;

ALTER TABLE sys_permission change is_enebled is_enabled boolean default 1;

ALTER TABLE sys_role change is_enebled is_enabled boolean default 1;

ALTER TABLE sys_role_menu change is_enebled is_enabled boolean default 1;

ALTER TABLE sys_user change is_enebled is_enabled boolean default 1;

ALTER TABLE sys_user_role change is_enebled is_enabled boolean default 1;

ALTER TABLE upload_file change is_enebled is_enabled boolean default 1;

ALTER TABLE user_track change is_enebled is_enabled boolean default 1;