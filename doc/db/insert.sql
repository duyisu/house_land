INSERT INTO `sys_user` (`user_name`, `depart_id`, `real_name`, `employee_no`, `user_tel`, `id_card_no`, `user_sex`, `user_password`, `salt`, `user_desc`, `is_enabled`) VALUES ('lhy', 0, '刘浩勇', '0001', '13924939333', '1243243', 1, '$2a$10$IqmMz5gMkTUapZGjGB4wZe51V7UjfSLHxmT8FWrI./6yCVnHplfoe', NULL, 'lhy', 1);
INSERT INTO `sys_user` (`user_name`, `depart_id`, `real_name`, `employee_no`, `user_tel`, `id_card_no`, `user_sex`, `user_password`, `salt`, `user_desc`, `is_enabled`) VALUES ('hyx', 0, '胡艳霞', '0002', '13923939333', '1243233', 0, '$2a$10$IqmMz5gMkTUapZGjGB4wZe51V7UjfSLHxmT8FWrI./6yCVnHplfoe', NULL, 'hyx', 1);

INSERT INTO `dict` VALUES (1, 'use_status', '使用状态');

INSERT INTO `dict_detail` VALUES (1, '正常', 1, 1, 1);
INSERT INTO `dict_detail` VALUES (2, '停用', 0, 2, 1);

insert into sys_menu(id, sort, pid, type, is_iframe, is_enabled, name, path, component, ico) values(1,  1,  0,  0, 1, 1, '应用维护', 'system',null,'system');
insert into sys_menu(id, sort, pid, type, is_iframe, is_enabled, name, path, component, ico) values(2,  2,  1,  1, 1, 1, '用户管理', 'user/index', 'system/user', 'user');
insert into sys_menu(id, sort, pid, type, is_iframe, is_enabled, name, path, component, ico) values(3,  3,  1,  1, 1, 1, '角色管理', 'role/index', 'system/role', 'role');
insert into sys_menu(id, sort, pid, type, is_iframe, is_enabled, name, path, component, ico) values(4,  4,  1,  1, 1, 1, '权限管理', 'permission/index', 'system/permission', 'permission');
insert into sys_menu(id, sort, pid, type, is_iframe, is_enabled, name, path, component, ico) values(5,  5,  1,  1, 1, 1, '菜单管理', 'menu/index', 'system/menu', 'menu');
insert into sys_menu(id, sort, pid, type, is_iframe, is_enabled, name, path, component, ico) values(6,  6,  1,  1, 1, 1, '部门管理', 'dept/index', 'system/dept', 'dept');