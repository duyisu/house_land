
drop table if exists case_record_flow_task_file;

create table case_record_flow_task_file
(
   id                   bigint not null auto_increment comment 'id',
   flow_task_id         bigint comment '两违流程任务ID',
   filename             varchar(500) comment '文件名称',
   filepath             varchar(500) not null comment '文件路径',
   field_type           varchar(100) not null comment '文件类型--扩展名',
   original_name        varchar(500) not null comment '原始文件名称',
   is_enebled           boolean default 1 comment '是否有效（1：有效 0：无效）',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);

alter table case_record_flow_task_file comment '两违流程附件表';

create unique index udx_case_record_flow_task_file on case_record_flow_task_file
(
   filename,
   filepath
);


drop index udx_case_record_file on case_record_file;

drop table if exists case_record_file;


create table case_record_file
(
   id                   bigint not null auto_increment comment 'id',
   case_id              bigint comment '案件ID',
   filename             varchar(500) comment '文件名称',
   filepath             varchar(500) not null comment '文件路径',
   field_type           varchar(100) not null comment '文件类型--扩展名',
   original_name        varchar(500) not null comment '原始文件名称',
   is_enebled           boolean default 1 comment '是否有效（1：有效 0：无效）',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);

alter table case_record_file comment '立案登记附件表';


create unique index udx_case_record_file on case_record_file
(
   filename,
   filepath
);

drop index udx_build_apply_file on build_apply_file;

drop table if exists build_apply_file;

create table build_apply_file
(
   id                   bigint not null auto_increment comment 'id',
   apply_id             bigint comment '申请ID',
   filename             varchar(500) comment '文件名称',
   filepath             varchar(500) not null comment '文件路径',
   field_type           varchar(100) not null comment '文件类型--扩展名',
   original_name        varchar(500) not null comment '原始文件名称',
   is_enebled           boolean default 1 comment '是否有效（1：有效 0：无效）',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);

alter table build_apply_file comment '建房申请附件表';

create index udx_build_apply_file on build_apply_file
(
   filename,
   filepath
);

