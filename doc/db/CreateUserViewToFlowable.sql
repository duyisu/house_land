DROP TABLE  IF EXISTS act_id_membership;  
DROP TABLE  IF EXISTS act_id_user;  
DROP TABLE  IF EXISTS act_id_group;

DROP VIEW  IF EXISTS act_id_membership;  
DROP VIEW  IF EXISTS act_id_user;  
DROP VIEW  IF EXISTS act_id_group;

CREATE VIEW act_id_user(ID_,REV_,FIRST_,LAST_,DISPLAY_NAME_,EMAIL_,PWD_,PICTURE_ID_ ,TENANT_ID_) AS   
  SELECT  
    user_name AS ID_,  
    0          AS REV_,  
    real_name  AS FIRST_,  
    '' AS LAST_,  
	real_name  AS DISPLAY_NAME_,  
    user_tel      AS EMAIL_,  
    ''   AS PWD_,  
    ''          AS PICTURE_ID_ ,
	'' AS TENANT_ID_
  FROM sys_user au;  
  
CREATE VIEW act_id_group   
AS  
  SELECT ar.code AS ID_,
         NULL AS REV_,
         ar.name AS NAME_,
         'assignment' AS TYPE_
  FROM sys_permission ar; 

CREATE VIEW act_id_membership  
AS  
SELECT E.USER_ID_,E.GROUP_ID_ AS GROUP_ID_ 
FROM ( SELECT A.`code` AS USER_ID_,D.`user_name` AS GROUP_ID_ 
FROM sys_permission AS A
INNER JOIN sys_role_menu AS B ON A.menu_id = B.menu_id
INNER JOIN sys_user_role C ON B.role_id = C.role_id
INNER JOIN sys_user D on C.user_id = D.id) E
GROUP BY E.USER_ID_,E.GROUP_ID_
