/*==============================================================*/
/* DBMS name:      MySQL 5.0                                    */
/* Created on:     2019/8/29 8:59:18                            */
/*==============================================================*/


drop table if exists area;

drop table if exists build_apply;

drop table if exists build_apply_file;

drop table if exists car;

drop table if exists car_scheduling;

drop table if exists car_scheduling_area;

drop table if exists car_track;

drop table if exists case_defendant;

drop table if exists case_record;

drop table if exists case_record_file;

drop table if exists case_record_flow_task;

drop table if exists case_record_flow_task_file;

drop table if exists department;

drop table if exists dict;

drop table if exists dict_detail;

drop table if exists sys_code;

drop table if exists sys_menu;

drop table if exists sys_permission;

drop table if exists sys_role;

drop table if exists sys_role_menu;

drop table if exists sys_user;

drop table if exists sys_user_role;

drop table if exists upload_file;

drop table if exists user_track;

/*==============================================================*/
/* Table: area                                                  */
/*==============================================================*/
create table area
(
   id                   bigint not null auto_increment comment 'id',
   area_name            varchar(255) not null comment '区域名称',
   area_kind            smallint comment '区域类型',
   parent_id            bigint comment '上级区域',
   is_enabled           boolean default 1 comment '是否有效（1：有效0：无效）',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);

alter table area comment '区域表';

/*==============================================================*/
/* Index: uk_area                                               */
/*==============================================================*/
create unique index uk_area on area
(
   area_name,
   parent_id
);

/*==============================================================*/
/* Table: build_apply                                           */
/*==============================================================*/
create table build_apply
(
   id                   bigint not null auto_increment comment 'id',
   apply_no             varchar(20) comment '申请编号',
   process_instance_id  varchar(64) comment '流程实例ID',
   apply_name           varchar(50) comment '申请人',
   apply_tel            varchar(50) comment '申请电话',
   apply_time           datetime comment '申请时间',
   apply_addr           bigint comment '申请地址',
   apply_kind           smallint comment '申请类型',
   longitude            decimal(12,8) comment '经度',
   latitude             decimal(12,8) comment '纬度',
   holder_idcardno      varchar(30) comment '户主身份证号码',
   holder_name          varchar(50) comment '户主姓名',
   holder_birthday      date comment '户主生日',
   holder_sex           smallint comment '户主性别',
   is_married           bool comment '户主婚否',
   family_size          smallint comment '家庭人数',
   spouse_name          varchar(50) comment '配偶姓名',
   spouse_idcardno      varchar(30) comment '配偶身份证号码',
   address              varchar(255) comment '户口所在地',
   house_addr           varchar(255) comment '现住址',
   house_num            smallint comment '住房数量',
   area                 decimal(8,2) comment '占地面积',
   real_area            decimal(8,2) comment '建筑占地面积',
   house_time           date comment '建筑时间',
   floors               smallint comment '建筑层数',
   house_desc           varchar(255) comment '房屋说明',
   apply_reason         smallint comment '申请理由',
   sumarea              decimal(8,2) comment '建筑总面积',
   apply_area           decimal(8,2) comment '申请占地面积',
   apply_real_area      decimal(8,2) comment '申请建筑面积',
   apply_house_time     date comment '申请建筑时间',
   apply_floors         smallint comment '申请建筑层数',
   eave_height          decimal(8,2) comment '申请檐口高度',
   apply_house_addr     varchar(255) comment '申请建房地点',
   land_kind            smallint comment '土地性质',
   build_paper_no       varchar(50) comment '建筑图集号',
   east_name            varchar(50) comment '东面姓名',
   south_name           varchar(50) comment '南面姓名',
   west_name            varchar(50) comment '西面姓名',
   north_name           varchar(50) comment '北面姓名',
   east_apply           smallint comment '东面意见',
   south_apply          smallint comment '南面意见',
   west_apply           smallint comment '西面意见',
   north_apply          smallint comment '北面意见',
   east_time            datetime comment '东面时间',
   south_time           datetime comment '南面时间',
   west_time            datetime comment '西面时间',
   north_time           datetime comment '北面时间',
   is_rule2             bool comment '符合规则2',
   is_rule3             bool comment '符合规则3',
   is_rule1             bool comment '符合规则1',
   is_enabled           boolean default 1 comment '是否有效（1：有效 0：无效）',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);

alter table build_apply comment '建房申请表';

/*==============================================================*/
/* Index: uk_build_apply                                        */
/*==============================================================*/
create unique index uk_build_apply on build_apply
(
   apply_no
);

/*==============================================================*/
/* Table: build_apply_file                                      */
/*==============================================================*/
create table build_apply_file
(
   id                   bigint not null auto_increment comment 'id',
   apply_id             bigint comment '申请ID',
   file_id              bigint comment '文件ID',
   is_enabled           boolean default 1 comment '是否有效（1：有效 0：无效）',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);

alter table build_apply_file comment '建房申请附件表';

/*==============================================================*/
/* Index: udx_build_apply_file                                  */
/*==============================================================*/
create index udx_build_apply_file on build_apply_file
(
   apply_id,
   file_id
);

/*==============================================================*/
/* Table: car                                                   */
/*==============================================================*/
create table car
(
   id                   bigint not null auto_increment comment 'id',
   car_no               varchar(12) not null comment '车牌号码',
   car_name             varchar(50) comment '车辆名称',
   gps_no               varchar(50) comment '车辆端口',
   is_enabled           boolean default 1 comment '是否有效（1：有效0：无效）',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);

alter table car comment '车辆表';

/*==============================================================*/
/* Index: uk_car                                                */
/*==============================================================*/
create index uk_car on car
(
   car_no
);

/*==============================================================*/
/* Table: car_scheduling                                        */
/*==============================================================*/
create table car_scheduling
(
   id                   bigint not null auto_increment comment 'id',
   car_id               bigint not null comment '车辆ID',
   user_id              bigint comment '人员ID',
   depart_id            bigint comment '部门ID',
   user_tel             varchar(255) comment '电话',
   start_time           datetime comment '排班开始时间',
   stop_time            datetime comment '排班结束时间',
   input_time           date comment '排班制定时间',
   is_enabled           boolean default 1 comment '是否有效（1：有效0：无效）',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);

alter table car_scheduling comment '车辆排班表';

/*==============================================================*/
/* Index: uk_car_scheduling                                     */
/*==============================================================*/
create unique index uk_car_scheduling on car_scheduling
(
   user_id,
   car_id,
   start_time
);

/*==============================================================*/
/* Table: car_scheduling_area                                   */
/*==============================================================*/
create table car_scheduling_area
(
   id                   bigint not null auto_increment comment 'id',
   scheduling_id        bigint comment '排班ID',
   area_id              bigint not null comment '区域ID',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);

alter table car_scheduling_area comment '车辆排班表';

/*==============================================================*/
/* Index: uk_car_scheduling_area                                */
/*==============================================================*/
create unique index uk_car_scheduling_area on car_scheduling_area
(
   scheduling_id,
   area_id
);

/*==============================================================*/
/* Table: car_track                                             */
/*==============================================================*/
create table car_track
(
   id                   bigint not null auto_increment comment 'id',
   car_id               bigint not null comment '车辆ID',
   track_time           datetime comment '时间',
   locationx            decimal(12,8) comment '经度',
   locationy            decimal(12,8) comment '纬度',
   location_grid        bigint comment '定位网格',
   radius               decimal(12,6) comment '半径',
   loctype              varchar (50) comment 'LOCTYPE',
   satellite_number     int comment '卫星数量',
   is_filtered          boolean comment '是否过滤（1：过滤0：无）',
   track_group          varchar(255) comment '轨迹分组',
   track_flag           int comment '轨迹标志',
   is_valid             boolean default 1 comment '是否有效（1：有效0：无效）',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);

alter table car_track comment '车辆轨迹表';

/*==============================================================*/
/* Index: uk_car_track                                          */
/*==============================================================*/
create unique index uk_car_track on car_track
(
   car_id,
   track_time
);

/*==============================================================*/
/* Table: case_defendant                                        */
/*==============================================================*/
create table case_defendant
(
   id                   bigint not null auto_increment comment 'id',
   case_id              bigint comment '案件ID',
   defendant_name       varchar(50) comment '被投诉人',
   defendant_tel        varchar(50) comment '被投诉人电话',
   defendant_iscardno   varchar(30) comment '被投诉人身份证号码',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);

alter table case_defendant comment '立案登记当事人表';

/*==============================================================*/
/* Index: uk_case_defendant                                     */
/*==============================================================*/
create unique index uk_case_defendant on case_defendant
(
   case_id,
   defendant_name
);

/*==============================================================*/
/* Table: case_record                                           */
/*==============================================================*/
create table case_record
(
   id                   bigint not null auto_increment comment 'id',
   case_no              varchar(20) comment '案件编号',
   case_record_no       varchar(20) comment '立案编号',
   case_desc            varchar(2000) comment '案件描述',
   process_instance_id  varchar(64) comment '流程实例ID',
   source               smallint not null comment '来源',
   case_type            smallint comment '两违类型',
   case_subtype         smallint comment '二级类型',
   complainant          varchar(50) comment '投诉人',
   complainant_tel      varchar(50) comment '投诉电话',
   area_id              bigint comment '镇区ID',
   position             varchar(120) comment '位置',
   longitude            decimal(12,8) comment '经度',
   latitude             decimal(12,8) comment '纬度',
   address              varchar(255) comment '地址',
   purposes             varchar(255) comment '用途',
   area                 decimal(8,2) comment '占地面积',
   real_area            decimal(8,2) comment '建筑占地面积',
   build_kind           smallint comment '违建性质',
   build_struct         smallint comment '建筑结构',
   report_name          varchar(50) comment '上报人',
   reportt_tel          varchar(50) comment '上报电话',
   is_enabled           boolean default 1 comment '是否有效（1：有效 0：无效）',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);

alter table case_record comment '立案登记表';

/*==============================================================*/
/* Index: uk_case_record                                        */
/*==============================================================*/
create unique index uk_case_record on case_record
(
   case_no
);

/*==============================================================*/
/* Table: case_record_file                                      */
/*==============================================================*/
create table case_record_file
(
   id                   bigint not null auto_increment comment 'id',
   case_id              bigint comment '案件ID',
   file_id              bigint comment '文件ID',
   is_enabled           boolean default 1 comment '是否有效（1：有效 0：无效）',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);

alter table case_record_file comment '立案登记附件表';

/*==============================================================*/
/* Index: udx_case_record_file                                  */
/*==============================================================*/
create unique index udx_case_record_file on case_record_file
(
   case_id,
   file_id
);

/*==============================================================*/
/* Table: case_record_flow_task                                 */
/*==============================================================*/
create table case_record_flow_task
(
   id                   bigint not null auto_increment comment 'id',
   case_Id              bigint comment '案件Id',
   task_id              varchar(64) comment '任务ID',
   process_instance_id  varchar(64) comment '流程实例ID',
   opr_user_id          bigint not null comment '处理人员ID',
   opr_method           varchar(255) comment '处理措施',
   opr_type             varchar(255) comment '处理意见',
   opr_desc             varchar(1000) comment '处理意见描述',
   self_area            decimal(8,2) comment '自行拆除面积',
   other_area           decimal(8,2) comment '协助拆除面积面积',
   new_area             decimal(8,2) comment '即建即拆面积',
   is_enabled           boolean default 1 comment '是否有效（1：有效 0：无效）',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);

alter table case_record_flow_task comment '两违流程表';

/*==============================================================*/
/* Index: idx_case_record_flow_task                             */
/*==============================================================*/
create index idx_case_record_flow_task on case_record_flow_task
(
   case_Id,
   task_id,
   process_instance_id
);

/*==============================================================*/
/* Table: case_record_flow_task_file                            */
/*==============================================================*/
create table case_record_flow_task_file
(
   id                   bigint not null auto_increment comment 'id',
   flow_task_id         bigint comment '两违流程任务ID',
   file_id              bigint comment '文件ID',
   is_enabled           boolean default 1 comment '是否有效（1：有效 0：无效）',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);

alter table case_record_flow_task_file comment '两违流程附件表';

/*==============================================================*/
/* Index: udx_case_record_flow_task_file                        */
/*==============================================================*/
create unique index udx_case_record_flow_task_file on case_record_flow_task_file
(
   flow_task_id,
   file_id
);

/*==============================================================*/
/* Table: department                                            */
/*==============================================================*/
create table department
(
   id                   bigint not null auto_increment comment 'id',
   name                 varchar(255) not null comment '部门名称',
   depart_desc          varchar(255) comment '部门描述',
   pid                  bigint comment '上级部门',
   is_enabled           boolean default 1 comment '是否有效（1：有效0：无效）',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);

alter table department comment '部门表';

/*==============================================================*/
/* Index: uk_department                                         */
/*==============================================================*/
create unique index uk_department on department
(
   name
);

/*==============================================================*/
/* Table: dict                                                  */
/*==============================================================*/
create table dict
(
   id                   bigint not null auto_increment comment 'id',
   name                 varchar(255) not null comment '名称',
   remark               varchar(255) comment '描述',
   primary key (id)
);

alter table dict comment '字典';

/*==============================================================*/
/* Index: uk_dict                                               */
/*==============================================================*/
create index uk_dict on dict
(
   name
);

/*==============================================================*/
/* Table: dict_detail                                           */
/*==============================================================*/
create table dict_detail
(
   id                   bigint not null auto_increment comment 'id',
   name                 varchar(255) not null comment '标签名称',
   value                integer comment '值',
   sort                 integer comment '排序',
   dict_id              bigint comment '字典ID',
   Column_6             char(10),
   primary key (id)
);

alter table dict_detail comment '字典明细';

/*==============================================================*/
/* Table: sys_code                                              */
/*==============================================================*/
create table sys_code
(
   id                   bigint not null auto_increment comment 'id',
   code_key             varchar(50) comment '代码类型',
   code_date            bigint comment '前缀日期',
   curr_code            bigint not null comment '当前编号',
   code_length          integer comment '编码长度',
   primary key (id)
);

alter table sys_code comment '流水号表';

/*==============================================================*/
/* Index: udx_sys_code                                          */
/*==============================================================*/
create index udx_sys_code on sys_code
(
   code_key,
   code_date
);

/*==============================================================*/
/* Table: sys_menu                                              */
/*==============================================================*/
create table sys_menu
(
   id                   bigint not null auto_increment comment 'id',
   component            varchar(100) comment '菜单组件',
   name                 varchar(500) not null comment '菜单名称',
   path                 varchar(500) comment '菜单路径',
   pid                  bigint comment '上级菜单',
   ico                  varchar(500) comment '菜单图标',
   sort                 smallint comment '菜单序号',
   type                 tinyint comment '类型(0:菜单1：功能)',
   is_iframe            tinyint comment '外链标志（1：内部0：外链）',
   is_enabled           boolean default 1 comment '是否有效（1：有效0：无效）',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);

alter table sys_menu comment '菜单资源表';

/*==============================================================*/
/* Index: idx_menu_parent_order                                 */
/*==============================================================*/
create index idx_menu_parent_order on sys_menu
(
   pid,
   sort
);

/*==============================================================*/
/* Table: sys_permission                                        */
/*==============================================================*/
create table sys_permission
(
   id                   bigint not null auto_increment comment 'id',
   code                 varchar(100) not null comment '权限代码',
   name                 varchar(500) not null comment '权限名称',
   menu_id              bigint comment '菜单ID',
   sort                 smallint comment '序号',
   is_enabled           boolean default 1 comment '是否有效（1：有效0：无效）',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);

alter table sys_permission comment '权限表';

/*==============================================================*/
/* Index: udx_permission_code                                   */
/*==============================================================*/
create unique index udx_permission_code on sys_permission
(
   code
);

/*==============================================================*/
/* Table: sys_role                                              */
/*==============================================================*/
create table sys_role
(
   id                   bigint not null auto_increment comment 'id',
   name                 varchar(50) not null comment '角色名称',
   role_desc            varchar(255) comment '角色描述',
   role_kind            smallint comment '角色级别（0：超级管理员1：管理员）',
   is_enabled           boolean default 1 comment '是否有效（1：有效0：无效）',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);

alter table sys_role comment '角色表';

/*==============================================================*/
/* Index: uk_role                                               */
/*==============================================================*/
create index uk_role on sys_role
(
   name
);

/*==============================================================*/
/* Table: sys_role_menu                                         */
/*==============================================================*/
create table sys_role_menu
(
   id                   bigint not null auto_increment comment 'id',
   role_id              bigint not null comment '角色ID',
   menu_id              bigint comment '权限ID',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);

alter table sys_role_menu comment '角色菜单表';

/*==============================================================*/
/* Index: uk_role_menu                                          */
/*==============================================================*/
create unique index uk_role_menu on sys_role_menu
(
   role_id,
   menu_id
);

/*==============================================================*/
/* Table: sys_user                                              */
/*==============================================================*/
create table sys_user
(
   id                   bigint not null auto_increment comment 'id',
   user_name            varchar(30) not null comment '用户名称',
   depart_id            bigint comment '部门',
   real_name            varchar(50) comment '真实姓名',
   employee_no          varchar(20) comment '工号',
   user_tel             varchar(255) comment '手机号码',
   id_card_no           varchar(30) comment '身份证号码',
   user_sex             smallint comment '性别',
   user_password        varchar(64) comment '用户密码',
   salt                 varchar(16) comment 'salt',
   user_desc            varchar(255) comment '备注',
   is_enabled           boolean default 1 comment '是否有效（1：启用 0：停用）',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);

alter table sys_user comment '用户表';

/*==============================================================*/
/* Index: uk_user                                               */
/*==============================================================*/
create unique index uk_user on sys_user
(
   user_name
);

/*==============================================================*/
/* Table: sys_user_role                                         */
/*==============================================================*/
create table sys_user_role
(
   id                   bigint not null auto_increment comment 'id',
   user_id              bigint comment '用户ID',
   role_id              bigint not null comment '角色ID',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);

alter table sys_user_role comment '用户角色表';

/*==============================================================*/
/* Index: uk_user_role                                          */
/*==============================================================*/
create unique index uk_user_role on sys_user_role
(
   user_id,
   role_id
);

/*==============================================================*/
/* Table: upload_file                                           */
/*==============================================================*/
create table upload_file
(
   id                   bigint not null auto_increment comment 'id',
   filename             varchar(500) comment '文件名称',
   field_type           varchar(100) not null comment '文件类型--扩展名',
   original_name        varchar(500) not null comment '原始文件名称',
   filepath             varchar(500) not null comment '文件路径',
   file_url             varchar(500) comment '文件URL',
   is_enabled           boolean default 1 comment '是否有效（1：有效 0：无效）',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);

alter table upload_file comment '上传文件表';

/*==============================================================*/
/* Index: udx_upload_file                                       */
/*==============================================================*/
create unique index udx_upload_file on upload_file
(
   filename,
   filepath
);

/*==============================================================*/
/* Table: user_track                                            */
/*==============================================================*/
create table user_track
(
   id                   bigint not null auto_increment comment 'id',
   user_id              bigint not null comment '人员ID',
   track_time           datetime comment '时间',
   locationx            decimal(12,8) comment '经度',
   locationy            decimal(12,8) comment '纬度',
   location_grid        bigint comment '定位网格',
   radius               decimal(12,6) comment '半径',
   loctype              varchar(50) comment 'LOCTYPE',
   satellite_number     int comment '卫星数量',
   is_filtered          boolean comment '是否过滤（1：过滤0：无）',
   track_group          varchar(255) comment '轨迹分组',
   track_flag           int comment '轨迹标志',
   is_valid             boolean default 1 comment '是否有效（1：有效0：无效）',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);

alter table user_track comment '人员轨迹表';

/*==============================================================*/
/* Index: uk_user_track                                         */
/*==============================================================*/
create unique index uk_user_track on user_track
(
   user_id,
   track_time
);

