drop table if exists sys_code;

/*==============================================================*/
/* Table: sys_code                                              */
/*==============================================================*/
create table sys_code
(
   id                   bigint not null auto_increment comment 'id',
   code_key             varchar(50) comment '代码类型',
   code_date            bigint comment '前缀日期',
   curr_code            bigint not null comment '当前编号',
   code_length          integer comment '编码长度',
   primary key (id)
);

alter table sys_code comment '流水号表';

/*==============================================================*/
/* Index: udx_sys_code                                          */
/*==============================================================*/
create index udx_sys_code on sys_code
(
   code_key,
   code_date
);
