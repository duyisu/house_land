create table sys_code
(
   id                   bigint not null auto_increment comment 'id',
   code_key             varchar(50) comment '代码类型',
   code_date            bigint comment '前缀日期',
   curr_code            bigint not null comment '当前编号',
   code_length          integer comment '编码长度',
   primary key (id)
);

alter table sys_code comment '流水号表';

create index udx_sys_code on sys_code
(
   code_key,
   code_date
);

create table case_record_flow_task
(
   id                   bigint not null auto_increment comment 'id',
   case_id              bigint comment '案件Id',
   task_id              varchar(64) comment '任务ID',
   process_instance_id  varchar(64) comment '流程实例ID',
   opr_user_id          bigint not null comment '处理人员ID',
   opr_method           varchar(255) comment '处理措施',
   opr_type             smallint comment '处理意见',
   opr_desc             varchar(1000) comment '处理意见描述',
   self_area            decimal(8,2) comment '自行拆除面积',
   other_area           decimal(8,2) comment '协助拆除面积面积',
   new_area             decimal(8,2) comment '即建即拆面积',
   is_enebled           boolean default 1 comment '是否有效（1：有效 0：无效）',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);

alter table case_record_flow_task comment '两违流程表';

create unique index uk_case_record_flow_task on case_record_flow_task
(
   case_id
);

create table case_record_flow_task_file
(
   id                   bigint not null auto_increment comment 'id',
   flow_task_id         bigint comment '两违流程任务ID',
   filename             varchar(500) comment '文件名称',
   filepath             varchar(500) not null comment '文件路径',
   is_enebled           boolean default 1 comment '是否有效（1：有效 0：无效）',
   gmt_create           datetime default CURRENT_TIMESTAMP comment '创建时间',
   gmt_modified         datetime default CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP comment '修改时间',
   primary key (id)
);

alter table case_record_flow_task_file comment '两违流程附件表';

create unique index idx_case_record_flow_task_file on case_record_flow_task_file
(
   filename,
   filepath
);

create index idx_case_record_task_file_task_id on case_record_flow_task_file
(
   flow_task_id
);


