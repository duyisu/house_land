/*
Navicat MySQL Data Transfer

Source Server         : 118.24.251.241
Source Server Version : 50727
Source Host           : 118.24.251.241:3306
Source Database       : house_flow

Target Server Type    : MYSQL
Target Server Version : 50727
File Encoding         : 65001

Date: 2019-09-01 06:48:40
*/

SET FOREIGN_KEY_CHECKS=0;

-- ----------------------------
-- Table structure for `xzqh`
-- ----------------------------
DROP TABLE IF EXISTS `xzqh`;
CREATE TABLE `xzqh` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `province` varchar(45) DEFAULT NULL COMMENT '省',
  `city` varchar(45) DEFAULT NULL COMMENT '市',
  `district` varchar(45) DEFAULT NULL COMMENT '区 县',
  `town` varchar(45) DEFAULT NULL COMMENT '镇',
  `village` varchar(45) DEFAULT NULL COMMENT '村',
  `xzqh_code` varchar(45) DEFAULT NULL COMMENT '行政区划编码',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=261 DEFAULT CHARSET=utf8;

-- ----------------------------
-- Records of xzqh
-- ----------------------------
INSERT INTO `xzqh` VALUES ('1', '江西省', '上饶市', '上饶县', '上泸镇', '上泸居民委员会', '361121102001');
INSERT INTO `xzqh` VALUES ('2', '江西省', '上饶市', '上饶县', '上泸镇', '清泉居民委员会', '361121102002');
INSERT INTO `xzqh` VALUES ('3', '江西省', '上饶市', '上饶县', '上泸镇', '三连坑村民委员会', '361121102200');
INSERT INTO `xzqh` VALUES ('4', '江西省', '上饶市', '上饶县', '上泸镇', '泉洋村民委员会', '361121102201');
INSERT INTO `xzqh` VALUES ('5', '江西省', '上饶市', '上饶县', '上泸镇', '溪北村民委员会', '361121102203');
INSERT INTO `xzqh` VALUES ('6', '江西省', '上饶市', '上饶县', '上泸镇', '毛源村民委员会', '361121102204');
INSERT INTO `xzqh` VALUES ('7', '江西省', '上饶市', '上饶县', '上泸镇', '王家山村民委员会', '361121102205');
INSERT INTO `xzqh` VALUES ('8', '江西省', '上饶市', '上饶县', '上泸镇', '江家村民委员会', '361121102206');
INSERT INTO `xzqh` VALUES ('9', '江西省', '上饶市', '上饶县', '上泸镇', '小源村民委员会', '361121102207');
INSERT INTO `xzqh` VALUES ('10', '江西省', '上饶市', '上饶县', '上泸镇', '苎圳村民委员会', '361121102208');
INSERT INTO `xzqh` VALUES ('11', '江西省', '上饶市', '上饶县', '上泸镇', '红区林场生活区', '361121102500');
INSERT INTO `xzqh` VALUES ('12', '江西省', '上饶市', '上饶县', '五府山镇', '横山头居民委员会', '361121110001');
INSERT INTO `xzqh` VALUES ('13', '江西省', '上饶市', '上饶县', '五府山镇', '甘溪村民委员会', '361121110200');
INSERT INTO `xzqh` VALUES ('14', '江西省', '上饶市', '上饶县', '五府山镇', '畈心村民委员会', '361121110201');
INSERT INTO `xzqh` VALUES ('15', '江西省', '上饶市', '上饶县', '五府山镇', '金钟山村民委员会', '361121110203');
INSERT INTO `xzqh` VALUES ('16', '江西省', '上饶市', '上饶县', '五府山镇', '高洲村民委员会', '361121110204');
INSERT INTO `xzqh` VALUES ('17', '江西省', '上饶市', '上饶县', '五府山镇', '塘里村民委员会', '361121110205');
INSERT INTO `xzqh` VALUES ('18', '江西省', '上饶市', '上饶县', '五府山镇', '船坑村民委员会', '361121110207');
INSERT INTO `xzqh` VALUES ('19', '江西省', '上饶市', '上饶县', '五府山镇', '毛楼村民委员会', '361121110208');
INSERT INTO `xzqh` VALUES ('20', '江西省', '上饶市', '上饶县', '兴园街道办事处', '前山居委会', '361121003001');
INSERT INTO `xzqh` VALUES ('21', '江西省', '上饶市', '上饶县', '兴园街道办事处', '黄源居委会', '361121003002');
INSERT INTO `xzqh` VALUES ('22', '江西省', '上饶市', '上饶县', '兴园街道办事处', '新科社区居民委员会', '361121003003');
INSERT INTO `xzqh` VALUES ('23', '江西省', '上饶市', '上饶县', '兴园街道办事处', '凤凰社区居民委员会', '361121003004');
INSERT INTO `xzqh` VALUES ('24', '江西省', '上饶市', '上饶县', '兴园街道办事处', '苏家村委会', '361121003200');
INSERT INTO `xzqh` VALUES ('25', '江西省', '上饶市', '上饶县', '兴园街道办事处', '板桥村委会', '361121003201');
INSERT INTO `xzqh` VALUES ('26', '江西省', '上饶市', '上饶县', '兴园街道办事处', '龙门村民委员会', '361121003202');
INSERT INTO `xzqh` VALUES ('27', '江西省', '上饶市', '上饶县', '华坛山镇', '姜村居民委员会', '361121103001');
INSERT INTO `xzqh` VALUES ('28', '江西省', '上饶市', '上饶县', '华坛山镇', '振华路居民委员会', '361121103002');
INSERT INTO `xzqh` VALUES ('29', '江西省', '上饶市', '上饶县', '华坛山镇', '桐西村民委员会', '361121103200');
INSERT INTO `xzqh` VALUES ('30', '江西省', '上饶市', '上饶县', '华坛山镇', '毛村村民委员会', '361121103201');
INSERT INTO `xzqh` VALUES ('31', '江西省', '上饶市', '上饶县', '华坛山镇', '小东坞村民委员会', '361121103202');
INSERT INTO `xzqh` VALUES ('32', '江西省', '上饶市', '上饶县', '华坛山镇', '李村村民委员会', '361121103204');
INSERT INTO `xzqh` VALUES ('33', '江西省', '上饶市', '上饶县', '华坛山镇', '车边村民委员会', '361121103205');
INSERT INTO `xzqh` VALUES ('34', '江西省', '上饶市', '上饶县', '华坛山镇', '彭家坞村民委员会', '361121103206');
INSERT INTO `xzqh` VALUES ('35', '江西省', '上饶市', '上饶县', '华坛山镇', '高畈村民委员会', '361121103207');
INSERT INTO `xzqh` VALUES ('36', '江西省', '上饶市', '上饶县', '华坛山镇', '革畈村民委员会', '361121103208');
INSERT INTO `xzqh` VALUES ('37', '江西省', '上饶市', '上饶县', '华坛山镇', '汪岭村民委员会', '361121103209');
INSERT INTO `xzqh` VALUES ('38', '江西省', '上饶市', '上饶县', '华坛山镇', '桐西分场生活区', '361121103500');
INSERT INTO `xzqh` VALUES ('39', '江西省', '上饶市', '上饶县', '华坛山镇', '毛村分场生活区', '361121103501');
INSERT INTO `xzqh` VALUES ('40', '江西省', '上饶市', '上饶县', '华坛山镇', '上田分场生活区', '361121103502');
INSERT INTO `xzqh` VALUES ('41', '江西省', '上饶市', '上饶县', '华坛山镇', '叶家分场生活区', '361121103503');
INSERT INTO `xzqh` VALUES ('42', '江西省', '上饶市', '上饶县', '华坛山镇', '双溪分场生活区', '361121103504');
INSERT INTO `xzqh` VALUES ('43', '江西省', '上饶市', '上饶县', '华坛山镇', '刘家分场生活区', '361121103505');
INSERT INTO `xzqh` VALUES ('44', '江西省', '上饶市', '上饶县', '华坛山镇', '高畈分场生活区', '361121103506');
INSERT INTO `xzqh` VALUES ('45', '江西省', '上饶市', '上饶县', '华坛山镇', '革畈分场生活区', '361121103507');
INSERT INTO `xzqh` VALUES ('46', '江西省', '上饶市', '上饶县', '华坛山镇', '姚山分场生活区', '361121103508');
INSERT INTO `xzqh` VALUES ('47', '江西省', '上饶市', '上饶县', '四十八镇', '波阳居民委员会', '361121106001');
INSERT INTO `xzqh` VALUES ('48', '江西省', '上饶市', '上饶县', '四十八镇', '八景路居民委员会', '361121106002');
INSERT INTO `xzqh` VALUES ('49', '江西省', '上饶市', '上饶县', '四十八镇', '北山村民委员会', '361121106200');
INSERT INTO `xzqh` VALUES ('50', '江西省', '上饶市', '上饶县', '四十八镇', '高门村民委员会', '361121106201');
INSERT INTO `xzqh` VALUES ('51', '江西省', '上饶市', '上饶县', '四十八镇', '鸟桥村民委员会', '361121106202');
INSERT INTO `xzqh` VALUES ('52', '江西省', '上饶市', '上饶县', '四十八镇', '鲤洋村民委员会', '361121106203');
INSERT INTO `xzqh` VALUES ('53', '江西省', '上饶市', '上饶县', '四十八镇', '高洋村民委员会', '361121106205');
INSERT INTO `xzqh` VALUES ('54', '江西省', '上饶市', '上饶县', '尊桥乡', '尊桥村民委员会', '361121207200');
INSERT INTO `xzqh` VALUES ('55', '江西省', '上饶市', '上饶县', '尊桥乡', '恩山村民委员会', '361121207201');
INSERT INTO `xzqh` VALUES ('56', '江西省', '上饶市', '上饶县', '尊桥乡', '周墩村民委员会', '361121207202');
INSERT INTO `xzqh` VALUES ('57', '江西省', '上饶市', '上饶县', '尊桥乡', '东田村民委员会', '361121207204');
INSERT INTO `xzqh` VALUES ('58', '江西省', '上饶市', '上饶县', '尊桥乡', '冯家村民委员会', '361121207205');
INSERT INTO `xzqh` VALUES ('59', '江西省', '上饶市', '上饶县', '尊桥乡', '上乐村民委员会', '361121207206');
INSERT INTO `xzqh` VALUES ('60', '江西省', '上饶市', '上饶县', '尊桥乡', '周坞村民委员会', '361121207207');
INSERT INTO `xzqh` VALUES ('61', '江西省', '上饶市', '上饶县', '尊桥乡', '源塘村民委员会', '361121207208');
INSERT INTO `xzqh` VALUES ('62', '江西省', '上饶市', '上饶县', '尊桥乡', '羊石村民委员会', '361121207209');
INSERT INTO `xzqh` VALUES ('63', '江西省', '上饶市', '上饶县', '尊桥乡', '岛山村民委员会', '361121207210');
INSERT INTO `xzqh` VALUES ('64', '江西省', '上饶市', '上饶县', '尊桥乡', '后坪村民委员会', '361121207211');
INSERT INTO `xzqh` VALUES ('65', '江西省', '上饶市', '上饶县', '应家乡', '应家村民委员会', '361121209200');
INSERT INTO `xzqh` VALUES ('66', '江西省', '上饶市', '上饶县', '应家乡', '东坞村民委员会', '361121209201');
INSERT INTO `xzqh` VALUES ('67', '江西省', '上饶市', '上饶县', '应家乡', '石门村民委员会', '361121209202');
INSERT INTO `xzqh` VALUES ('68', '江西省', '上饶市', '上饶县', '应家乡', '浮墩村民委员会', '361121209204');
INSERT INTO `xzqh` VALUES ('69', '江西省', '上饶市', '上饶县', '应家乡', '吉安村民委员会', '361121209205');
INSERT INTO `xzqh` VALUES ('70', '江西省', '上饶市', '上饶县', '应家乡', '佛母村民委员会', '361121209206');
INSERT INTO `xzqh` VALUES ('71', '江西省', '上饶市', '上饶县', '应家乡', '安坑村民委员会', '361121209207');
INSERT INTO `xzqh` VALUES ('72', '江西省', '上饶市', '上饶县', '旭日街道办事处', '七六社区居民委员会', '361121001001');
INSERT INTO `xzqh` VALUES ('73', '江西省', '上饶市', '上饶县', '旭日街道办事处', '信江社区居民委员会', '361121001002');
INSERT INTO `xzqh` VALUES ('74', '江西省', '上饶市', '上饶县', '旭日街道办事处', '信美社区居民委员会', '361121001004');
INSERT INTO `xzqh` VALUES ('75', '江西省', '上饶市', '上饶县', '旭日街道办事处', '月亮湾社区居民委员会', '361121001005');
INSERT INTO `xzqh` VALUES ('76', '江西省', '上饶市', '上饶县', '旭日街道办事处', '桥下社区居民委员会', '361121001006');
INSERT INTO `xzqh` VALUES ('77', '江西省', '上饶市', '上饶县', '旭日街道办事处', '旭日社区居民委员会', '361121001008');
INSERT INTO `xzqh` VALUES ('78', '江西省', '上饶市', '上饶县', '旭日街道办事处', '惟义社区居民委员会', '361121001009');
INSERT INTO `xzqh` VALUES ('79', '江西省', '上饶市', '上饶县', '旭日街道办事处', '阳光社区居委会', '361121001010');
INSERT INTO `xzqh` VALUES ('80', '江西省', '上饶市', '上饶县', '旭日街道办事处', '东升社区居委会', '361121001011');
INSERT INTO `xzqh` VALUES ('81', '江西省', '上饶市', '上饶县', '旭日街道办事处', '建安社区居委会', '361121001012');
INSERT INTO `xzqh` VALUES ('82', '江西省', '上饶市', '上饶县', '旭日街道办事处', '春江社区居委会', '361121001013');
INSERT INTO `xzqh` VALUES ('83', '江西省', '上饶市', '上饶县', '旭日街道办事处', '金桥社区居委会', '361121001014');
INSERT INTO `xzqh` VALUES ('84', '江西省', '上饶市', '上饶县', '旭日街道办事处', '滨江社区居民委员会', '361121001015');
INSERT INTO `xzqh` VALUES ('85', '江西省', '上饶市', '上饶县', '旭日街道办事处', '望江社区居民委员会', '361121001016');
INSERT INTO `xzqh` VALUES ('86', '江西省', '上饶市', '上饶县', '旭日街道办事处', '育才社区居委会', '361121001017');
INSERT INTO `xzqh` VALUES ('87', '江西省', '上饶市', '上饶县', '旭日街道办事处', '梅野社区居委会', '361121001018');
INSERT INTO `xzqh` VALUES ('88', '江西省', '上饶市', '上饶县', '旭日街道办事处', '吾悦社区居委会', '361121001019');
INSERT INTO `xzqh` VALUES ('89', '江西省', '上饶市', '上饶县', '旭日街道办事处', '锦绣社区居委会', '361121001020');
INSERT INTO `xzqh` VALUES ('90', '江西省', '上饶市', '上饶县', '旭日街道办事处', '信惠社区居委会', '361121001021');
INSERT INTO `xzqh` VALUES ('91', '江西省', '上饶市', '上饶县', '旭日街道办事处', '饶信社区居委会', '361121001022');
INSERT INTO `xzqh` VALUES ('92', '江西省', '上饶市', '上饶县', '旭日街道办事处', '槠溪社区居委会', '361121001023');
INSERT INTO `xzqh` VALUES ('93', '江西省', '上饶市', '上饶县', '望仙乡', '望仙村民委员会', '361121200200');
INSERT INTO `xzqh` VALUES ('94', '江西省', '上饶市', '上饶县', '望仙乡', '祝狮村民委员会', '361121200201');
INSERT INTO `xzqh` VALUES ('95', '江西省', '上饶市', '上饶县', '望仙乡', '南峰村民委员会', '361121200202');
INSERT INTO `xzqh` VALUES ('96', '江西省', '上饶市', '上饶县', '望仙乡', '大济村民委员会', '361121200204');
INSERT INTO `xzqh` VALUES ('97', '江西省', '上饶市', '上饶县', '望仙乡', '葛路村民委员会', '361121200205');
INSERT INTO `xzqh` VALUES ('98', '江西省', '上饶市', '上饶县', '望仙乡', '大山林场生活区', '361121200500');
INSERT INTO `xzqh` VALUES ('99', '江西省', '上饶市', '上饶县', '望仙乡', '西坑林场生活区', '361121200501');
INSERT INTO `xzqh` VALUES ('100', '江西省', '上饶市', '上饶县', '望仙乡', '沙洲林场生活区', '361121200502');
INSERT INTO `xzqh` VALUES ('101', '江西省', '上饶市', '上饶县', '望仙乡', '新峰林场生活区', '361121200503');
INSERT INTO `xzqh` VALUES ('102', '江西省', '上饶市', '上饶县', '枫岭头镇', '七一三矿居民委员会', '361121107002');
INSERT INTO `xzqh` VALUES ('103', '江西省', '上饶市', '上饶县', '枫岭头镇', '枫岭头村民委员会', '361121107200');
INSERT INTO `xzqh` VALUES ('104', '江西省', '上饶市', '上饶县', '枫岭头镇', '井边村民委员会', '361121107201');
INSERT INTO `xzqh` VALUES ('105', '江西省', '上饶市', '上饶县', '枫岭头镇', '王家店村民委员会', '361121107204');
INSERT INTO `xzqh` VALUES ('106', '江西省', '上饶市', '上饶县', '枫岭头镇', '泉塘村民委员会', '361121107205');
INSERT INTO `xzqh` VALUES ('107', '江西省', '上饶市', '上饶县', '枫岭头镇', '丁家村民委员会', '361121107206');
INSERT INTO `xzqh` VALUES ('108', '江西省', '上饶市', '上饶县', '枫岭头镇', '永丰村民委员会', '361121107207');
INSERT INTO `xzqh` VALUES ('109', '江西省', '上饶市', '上饶县', '枫岭头镇', '永乐村民委员会', '361121107208');
INSERT INTO `xzqh` VALUES ('110', '江西省', '上饶市', '上饶县', '枫岭头镇', '稠川村民委员会', '361121107209');
INSERT INTO `xzqh` VALUES ('111', '江西省', '上饶市', '上饶县', '枫岭头镇', '坑口村民委员会', '361121107210');
INSERT INTO `xzqh` VALUES ('112', '江西省', '上饶市', '上饶县', '枫岭头镇', '长塘村民委员会', '361121107211');
INSERT INTO `xzqh` VALUES ('113', '江西省', '上饶市', '上饶县', '清水乡', '清水村民委员会', '361121202200');
INSERT INTO `xzqh` VALUES ('114', '江西省', '上饶市', '上饶县', '清水乡', '墩底村民委员会', '361121202202');
INSERT INTO `xzqh` VALUES ('115', '江西省', '上饶市', '上饶县', '清水乡', '洪家村民委员会', '361121202203');
INSERT INTO `xzqh` VALUES ('116', '江西省', '上饶市', '上饶县', '清水乡', '常阜村民委员会', '361121202205');
INSERT INTO `xzqh` VALUES ('117', '江西省', '上饶市', '上饶县', '清水乡', '东汪村民委员会', '361121202206');
INSERT INTO `xzqh` VALUES ('118', '江西省', '上饶市', '上饶县', '清水乡', '前汪村民委员会', '361121202207');
INSERT INTO `xzqh` VALUES ('119', '江西省', '上饶市', '上饶县', '清水乡', '双溪村民委员会', '361121202208');
INSERT INTO `xzqh` VALUES ('120', '江西省', '上饶市', '上饶县', '清水乡', '左溪村民委员会', '361121202209');
INSERT INTO `xzqh` VALUES ('121', '江西省', '上饶市', '上饶县', '湖村乡', '挤腰村民委员会', '361121204200');
INSERT INTO `xzqh` VALUES ('122', '江西省', '上饶市', '上饶县', '湖村乡', '湖村村民委员会', '361121204201');
INSERT INTO `xzqh` VALUES ('123', '江西省', '上饶市', '上饶县', '湖村乡', '徐家村民委员会', '361121204202');
INSERT INTO `xzqh` VALUES ('124', '江西省', '上饶市', '上饶县', '湖村乡', '大畈村民委员会', '361121204203');
INSERT INTO `xzqh` VALUES ('125', '江西省', '上饶市', '上饶县', '湖村乡', '荸荠塘村民委员会', '361121204204');
INSERT INTO `xzqh` VALUES ('126', '江西省', '上饶市', '上饶县', '湖村乡', '碧霞村民委员会', '361121204205');
INSERT INTO `xzqh` VALUES ('127', '江西省', '上饶市', '上饶县', '湖村乡', '石嘴村民委员会', '361121204206');
INSERT INTO `xzqh` VALUES ('128', '江西省', '上饶市', '上饶县', '湖村乡', '库前村民委员会', '361121204207');
INSERT INTO `xzqh` VALUES ('129', '江西省', '上饶市', '上饶县', '湖村乡', '茶园村民委员会', '361121204208');
INSERT INTO `xzqh` VALUES ('130', '江西省', '上饶市', '上饶县', '湖村乡', '茗洋村民委员会', '361121204210');
INSERT INTO `xzqh` VALUES ('131', '江西省', '上饶市', '上饶县', '湖村乡', '灵峰村民委员会', '361121204211');
INSERT INTO `xzqh` VALUES ('132', '江西省', '上饶市', '上饶县', '湖村乡', '姜山村民委员会', '361121204212');
INSERT INTO `xzqh` VALUES ('133', '江西省', '上饶市', '上饶县', '湖村乡', '西龙岗村民委员会', '361121204213');
INSERT INTO `xzqh` VALUES ('134', '江西省', '上饶市', '上饶县', '湖村乡', '杨桥村民委员会', '361121204214');
INSERT INTO `xzqh` VALUES ('135', '江西省', '上饶市', '上饶县', '湖村乡', '东灵村民委员会', '361121204216');
INSERT INTO `xzqh` VALUES ('136', '江西省', '上饶市', '上饶县', '煌固镇', '八都居民委员会', '361121108001');
INSERT INTO `xzqh` VALUES ('137', '江西省', '上饶市', '上饶县', '煌固镇', '沿畈村民委员会', '361121108200');
INSERT INTO `xzqh` VALUES ('138', '江西省', '上饶市', '上饶县', '煌固镇', '樟宅村民委员会', '361121108201');
INSERT INTO `xzqh` VALUES ('139', '江西省', '上饶市', '上饶县', '煌固镇', '煌固村民委员会', '361121108202');
INSERT INTO `xzqh` VALUES ('140', '江西省', '上饶市', '上饶县', '煌固镇', '麦埂坞村民委员会', '361121108203');
INSERT INTO `xzqh` VALUES ('141', '江西省', '上饶市', '上饶县', '煌固镇', '后田村民委员会', '361121108204');
INSERT INTO `xzqh` VALUES ('142', '江西省', '上饶市', '上饶县', '煌固镇', '黄塘村民委员会', '361121108205');
INSERT INTO `xzqh` VALUES ('143', '江西省', '上饶市', '上饶县', '煌固镇', '五村村民委员会', '361121108207');
INSERT INTO `xzqh` VALUES ('144', '江西省', '上饶市', '上饶县', '煌固镇', '观上村民委员会', '361121108208');
INSERT INTO `xzqh` VALUES ('145', '江西省', '上饶市', '上饶县', '煌固镇', '彭宅村民委员会', '361121108209');
INSERT INTO `xzqh` VALUES ('146', '江西省', '上饶市', '上饶县', '煌固镇', '丁宅村民委员会', '361121108210');
INSERT INTO `xzqh` VALUES ('147', '江西省', '上饶市', '上饶县', '煌固镇', '东山村民委员会', '361121108211');
INSERT INTO `xzqh` VALUES ('148', '江西省', '上饶市', '上饶县', '煌固镇', '岭下村民委员会', '361121108212');
INSERT INTO `xzqh` VALUES ('149', '江西省', '上饶市', '上饶县', '煌固镇', '汪村村民委员会', '361121108213');
INSERT INTO `xzqh` VALUES ('150', '江西省', '上饶市', '上饶县', '煌固镇', '山底村民委员会', '361121108214');
INSERT INTO `xzqh` VALUES ('151', '江西省', '上饶市', '上饶县', '煌固镇', '塘里村民委员会', '361121108215');
INSERT INTO `xzqh` VALUES ('152', '江西省', '上饶市', '上饶县', '田墩镇', '田墩居民委员会', '361121101001');
INSERT INTO `xzqh` VALUES ('153', '江西省', '上饶市', '上饶县', '田墩镇', '湖潭村民委员会', '361121101201');
INSERT INTO `xzqh` VALUES ('154', '江西省', '上饶市', '上饶县', '田墩镇', '荷家村民委员会', '361121101202');
INSERT INTO `xzqh` VALUES ('155', '江西省', '上饶市', '上饶县', '田墩镇', '廖家村民委员会', '361121101203');
INSERT INTO `xzqh` VALUES ('156', '江西省', '上饶市', '上饶县', '田墩镇', '东坑村民委员会', '361121101204');
INSERT INTO `xzqh` VALUES ('157', '江西省', '上饶市', '上饶县', '田墩镇', '儒坞村民委员会', '361121101205');
INSERT INTO `xzqh` VALUES ('158', '江西省', '上饶市', '上饶县', '田墩镇', '流源村民委员会', '361121101206');
INSERT INTO `xzqh` VALUES ('159', '江西省', '上饶市', '上饶县', '田墩镇', '黄坑村民委员会', '361121101207');
INSERT INTO `xzqh` VALUES ('160', '江西省', '上饶市', '上饶县', '田墩镇', '双源村民委员会', '361121101208');
INSERT INTO `xzqh` VALUES ('161', '江西省', '上饶市', '上饶县', '田墩镇', '冷水村民委员会', '361121101209');
INSERT INTO `xzqh` VALUES ('162', '江西省', '上饶市', '上饶县', '田墩镇', '岑峰村民委员会', '361121101210');
INSERT INTO `xzqh` VALUES ('163', '江西省', '上饶市', '上饶县', '田墩镇', '黄石村民委员会', '361121101211');
INSERT INTO `xzqh` VALUES ('164', '江西省', '上饶市', '上饶县', '田墩镇', '七峰村民委员会', '361121101212');
INSERT INTO `xzqh` VALUES ('165', '江西省', '上饶市', '上饶县', '田墩镇', '红门村民委员会', '361121101213');
INSERT INTO `xzqh` VALUES ('166', '江西省', '上饶市', '上饶县', '田墩镇', '长塘村民委员会', '361121101214');
INSERT INTO `xzqh` VALUES ('167', '江西省', '上饶市', '上饶县', '皂头镇', '皂头居民委员会', '361121105001');
INSERT INTO `xzqh` VALUES ('168', '江西省', '上饶市', '上饶县', '皂头镇', '新田村民委员会', '361121105200');
INSERT INTO `xzqh` VALUES ('169', '江西省', '上饶市', '上饶县', '皂头镇', '紫云村民委员会', '361121105201');
INSERT INTO `xzqh` VALUES ('170', '江西省', '上饶市', '上饶县', '皂头镇', '窑山村民委员会', '361121105202');
INSERT INTO `xzqh` VALUES ('171', '江西省', '上饶市', '上饶县', '皂头镇', '三联村民委员会', '361121105203');
INSERT INTO `xzqh` VALUES ('172', '江西省', '上饶市', '上饶县', '皂头镇', '毛埂村民委员会', '361121105204');
INSERT INTO `xzqh` VALUES ('173', '江西省', '上饶市', '上饶县', '皂头镇', '傅家村民委员会', '361121105205');
INSERT INTO `xzqh` VALUES ('174', '江西省', '上饶市', '上饶县', '皂头镇', '象山村民委员会', '361121105206');
INSERT INTO `xzqh` VALUES ('175', '江西省', '上饶市', '上饶县', '皂头镇', '毛湾村民委员会', '361121105207');
INSERT INTO `xzqh` VALUES ('176', '江西省', '上饶市', '上饶县', '皂头镇', '毛棚村民委员会', '361121105208');
INSERT INTO `xzqh` VALUES ('177', '江西省', '上饶市', '上饶县', '皂头镇', '周石村民委员会', '361121105209');
INSERT INTO `xzqh` VALUES ('178', '江西省', '上饶市', '上饶县', '石人乡', '双龙村民委员会', '361121201200');
INSERT INTO `xzqh` VALUES ('179', '江西省', '上饶市', '上饶县', '石人乡', '杉树村民委员会', '361121201201');
INSERT INTO `xzqh` VALUES ('180', '江西省', '上饶市', '上饶县', '石人乡', '毛宅村民委员会', '361121201202');
INSERT INTO `xzqh` VALUES ('181', '江西省', '上饶市', '上饶县', '石人乡', '苏桥村民委员会', '361121201203');
INSERT INTO `xzqh` VALUES ('182', '江西省', '上饶市', '上饶县', '石人乡', '荷叶村民委员会', '361121201204');
INSERT INTO `xzqh` VALUES ('183', '江西省', '上饶市', '上饶县', '石人乡', '青山村民委员会', '361121201205');
INSERT INTO `xzqh` VALUES ('184', '江西省', '上饶市', '上饶县', '石人乡', '郑宅村民委员会', '361121201206');
INSERT INTO `xzqh` VALUES ('185', '江西省', '上饶市', '上饶县', '石人乡', '汪宅村民委员会', '361121201207');
INSERT INTO `xzqh` VALUES ('186', '江西省', '上饶市', '上饶县', '石人乡', '桐坞村民委员会', '361121201208');
INSERT INTO `xzqh` VALUES ('187', '江西省', '上饶市', '上饶县', '石人乡', '石人村民委员会', '361121201209');
INSERT INTO `xzqh` VALUES ('188', '江西省', '上饶市', '上饶县', '石狮乡', '王家坝居民委员会', '361121203001');
INSERT INTO `xzqh` VALUES ('189', '江西省', '上饶市', '上饶县', '石狮乡', '石狮居民委员会', '361121203002');
INSERT INTO `xzqh` VALUES ('190', '江西省', '上饶市', '上饶县', '石狮乡', '吉阳村民委员会', '361121203201');
INSERT INTO `xzqh` VALUES ('191', '江西省', '上饶市', '上饶县', '石狮乡', '黄岭村民委员会', '361121203202');
INSERT INTO `xzqh` VALUES ('192', '江西省', '上饶市', '上饶县', '石狮乡', '何村村民委员会', '361121203203');
INSERT INTO `xzqh` VALUES ('193', '江西省', '上饶市', '上饶县', '石狮乡', '丁家仓村民委员会', '361121203204');
INSERT INTO `xzqh` VALUES ('194', '江西省', '上饶市', '上饶县', '石狮乡', '三都村民委员会', '361121203205');
INSERT INTO `xzqh` VALUES ('195', '江西省', '上饶市', '上饶县', '罗桥街道办事处', '罗桥社区居民委员会', '361121002001');
INSERT INTO `xzqh` VALUES ('196', '江西省', '上饶市', '上饶县', '罗桥街道办事处', '下山社区居民委员会', '361121002003');
INSERT INTO `xzqh` VALUES ('197', '江西省', '上饶市', '上饶县', '罗桥街道办事处', '畈头社区居民委员会', '361121002004');
INSERT INTO `xzqh` VALUES ('198', '江西省', '上饶市', '上饶县', '罗桥街道办事处', '松岭社区居委会', '361121002005');
INSERT INTO `xzqh` VALUES ('199', '江西省', '上饶市', '上饶县', '罗桥街道办事处', '文家社区居民委员会', '361121002011');
INSERT INTO `xzqh` VALUES ('200', '江西省', '上饶市', '上饶县', '罗桥街道办事处', '格林社区居委会', '361121002012');
INSERT INTO `xzqh` VALUES ('201', '江西省', '上饶市', '上饶县', '罗桥街道办事处', '三清社区居委会', '361121002013');
INSERT INTO `xzqh` VALUES ('202', '江西省', '上饶市', '上饶县', '罗桥街道办事处', '东山岭社区居委会', '361121002014');
INSERT INTO `xzqh` VALUES ('203', '江西省', '上饶市', '上饶县', '罗桥街道办事处', '斯达社区居委会', '361121002015');
INSERT INTO `xzqh` VALUES ('204', '江西省', '上饶市', '上饶县', '罗桥街道办事处', '樟村村民委员会', '361121002200');
INSERT INTO `xzqh` VALUES ('205', '江西省', '上饶市', '上饶县', '罗桥街道办事处', '横山村民委员会', '361121002201');
INSERT INTO `xzqh` VALUES ('206', '江西省', '上饶市', '上饶县', '花厅镇', '花厅居民委员会', '361121109001');
INSERT INTO `xzqh` VALUES ('207', '江西省', '上饶市', '上饶县', '花厅镇', '洋塘村民委员会', '361121109200');
INSERT INTO `xzqh` VALUES ('208', '江西省', '上饶市', '上饶县', '花厅镇', '枫岭村民委员会', '361121109201');
INSERT INTO `xzqh` VALUES ('209', '江西省', '上饶市', '上饶县', '花厅镇', '水垄村民委员会', '361121109202');
INSERT INTO `xzqh` VALUES ('210', '江西省', '上饶市', '上饶县', '花厅镇', '中畈村民委员会', '361121109203');
INSERT INTO `xzqh` VALUES ('211', '江西省', '上饶市', '上饶县', '花厅镇', '周村村民委员会', '361121109204');
INSERT INTO `xzqh` VALUES ('212', '江西省', '上饶市', '上饶县', '花厅镇', '金鸡村民委员会', '361121109205');
INSERT INTO `xzqh` VALUES ('213', '江西省', '上饶市', '上饶县', '花厅镇', '前程村民委员会', '361121109207');
INSERT INTO `xzqh` VALUES ('214', '江西省', '上饶市', '上饶县', '花厅镇', '白塔村民委员会', '361121109208');
INSERT INTO `xzqh` VALUES ('215', '江西省', '上饶市', '上饶县', '茶亭镇', '茶亭居民委员会', '361121104001');
INSERT INTO `xzqh` VALUES ('216', '江西省', '上饶市', '上饶县', '茶亭镇', '包家村民委员会', '361121104200');
INSERT INTO `xzqh` VALUES ('217', '江西省', '上饶市', '上饶县', '茶亭镇', '前坊村民委员会', '361121104201');
INSERT INTO `xzqh` VALUES ('218', '江西省', '上饶市', '上饶县', '茶亭镇', '松坪村民委员会', '361121104202');
INSERT INTO `xzqh` VALUES ('219', '江西省', '上饶市', '上饶县', '茶亭镇', '昆山村民委员会', '361121104203');
INSERT INTO `xzqh` VALUES ('220', '江西省', '上饶市', '上饶县', '茶亭镇', '徐坞村民委员会', '361121104204');
INSERT INTO `xzqh` VALUES ('221', '江西省', '上饶市', '上饶县', '茶亭镇', '下裴村民委员会', '361121104205');
INSERT INTO `xzqh` VALUES ('222', '江西省', '上饶市', '上饶县', '茶亭镇', '高潭村民委员会', '361121104206');
INSERT INTO `xzqh` VALUES ('223', '江西省', '上饶市', '上饶县', '茶亭镇', '詹家村民委员会', '361121104207');
INSERT INTO `xzqh` VALUES ('224', '江西省', '上饶市', '上饶县', '茶亭镇', '西湖村民委员会', '361121104208');
INSERT INTO `xzqh` VALUES ('225', '江西省', '上饶市', '上饶县', '茶亭镇', '墩头村民委员会', '361121104209');
INSERT INTO `xzqh` VALUES ('226', '江西省', '上饶市', '上饶县', '茶亭镇', '应坊村民委员会', '361121104210');
INSERT INTO `xzqh` VALUES ('227', '江西省', '上饶市', '上饶县', '茶亭镇', '湖墩村民委员会', '361121104211');
INSERT INTO `xzqh` VALUES ('228', '江西省', '上饶市', '上饶县', '茶亭镇', '南岩村民委员会', '361121104212');
INSERT INTO `xzqh` VALUES ('229', '江西省', '上饶市', '上饶县', '董团乡', '董团村民委员会（上饶经济开发区）', '361121290200');
INSERT INTO `xzqh` VALUES ('230', '江西省', '上饶市', '上饶县', '董团乡', '仙山村民委员会（上饶经济开发区）', '361121290201');
INSERT INTO `xzqh` VALUES ('231', '江西省', '上饶市', '上饶县', '董团乡', '太平村民委员会（上饶经济开发区）', '361121290202');
INSERT INTO `xzqh` VALUES ('232', '江西省', '上饶市', '上饶县', '董团乡', '吴洲村民委员会（上饶经济开发区）', '361121290203');
INSERT INTO `xzqh` VALUES ('233', '江西省', '上饶市', '上饶县', '董团乡', '中魏村民委员会（上饶经济开发区）', '361121290204');
INSERT INTO `xzqh` VALUES ('234', '江西省', '上饶市', '上饶县', '董团乡', '中路村民委员会（上饶经济开发区）', '361121290205');
INSERT INTO `xzqh` VALUES ('235', '江西省', '上饶市', '上饶县', '董团乡', '大地村民委员会（上饶经济开发区）', '361121290206');
INSERT INTO `xzqh` VALUES ('236', '江西省', '上饶市', '上饶县', '董团乡', '联星村民委员会（上饶经济开发区）', '361121290207');
INSERT INTO `xzqh` VALUES ('237', '江西省', '上饶市', '上饶县', '董团乡', '联合村民委员会（上饶经济开发区）', '361121290208');
INSERT INTO `xzqh` VALUES ('238', '江西省', '上饶市', '上饶县', '董团乡', '联胜村民委员会（上饶经济开发区）', '361121290209');
INSERT INTO `xzqh` VALUES ('239', '江西省', '上饶市', '上饶县', '董团乡', '红石村民委员会（上饶经济开发区）', '361121290210');
INSERT INTO `xzqh` VALUES ('240', '江西省', '上饶市', '上饶县', '郑坊镇', '郑坊居民委员会', '361121111001');
INSERT INTO `xzqh` VALUES ('241', '江西省', '上饶市', '上饶县', '郑坊镇', '楼村民委员会', '361121111200');
INSERT INTO `xzqh` VALUES ('242', '江西省', '上饶市', '上饶县', '郑坊镇', '台湖村民委员会', '361121111201');
INSERT INTO `xzqh` VALUES ('243', '江西省', '上饶市', '上饶县', '郑坊镇', '石峡村民委员会', '361121111202');
INSERT INTO `xzqh` VALUES ('244', '江西省', '上饶市', '上饶县', '郑坊镇', '陈墩村民委员会', '361121111203');
INSERT INTO `xzqh` VALUES ('245', '江西省', '上饶市', '上饶县', '郑坊镇', '洲村村民委员会', '361121111204');
INSERT INTO `xzqh` VALUES ('246', '江西省', '上饶市', '上饶县', '郑坊镇', '枫林村民委员会', '361121111205');
INSERT INTO `xzqh` VALUES ('247', '江西省', '上饶市', '上饶县', '郑坊镇', '钱墩村民委员会', '361121111206');
INSERT INTO `xzqh` VALUES ('248', '江西省', '上饶市', '上饶县', '郑坊镇', '西山村民委员会', '361121111207');
INSERT INTO `xzqh` VALUES ('249', '江西省', '上饶市', '上饶县', '铁山乡', '铁山村民委员会', '361121211200');
INSERT INTO `xzqh` VALUES ('250', '江西省', '上饶市', '上饶县', '铁山乡', '西岩村民委员会', '361121211201');
INSERT INTO `xzqh` VALUES ('251', '江西省', '上饶市', '上饶县', '铁山乡', '九狮畲族村民委员会', '361121211202');
INSERT INTO `xzqh` VALUES ('252', '江西省', '上饶市', '上饶县', '铁山乡', '小溪村民委员会', '361121211203');
INSERT INTO `xzqh` VALUES ('253', '江西省', '上饶市', '上饶县', '铁山乡', '大溪村民委员会', '361121211204');
INSERT INTO `xzqh` VALUES ('254', '江西省', '上饶市', '上饶县', '黄沙岭乡', '黄沙村民委员会', '361121210200');
INSERT INTO `xzqh` VALUES ('255', '江西省', '上饶市', '上饶县', '黄沙岭乡', '蔡家村民委员会', '361121210201');
INSERT INTO `xzqh` VALUES ('256', '江西省', '上饶市', '上饶县', '黄沙岭乡', '大屋村民委员会', '361121210202');
INSERT INTO `xzqh` VALUES ('257', '江西省', '上饶市', '上饶县', '黄沙岭乡', '麻墩村民委员会', '361121210203');
INSERT INTO `xzqh` VALUES ('258', '江西省', '上饶市', '上饶县', '黄沙岭乡', '湖山村民委员会', '361121210204');
INSERT INTO `xzqh` VALUES ('259', '江西省', '上饶市', '上饶县', '黄沙岭乡', '源溪村民委员会', '361121210206');
INSERT INTO `xzqh` VALUES ('260', '江西省', '上饶市', '上饶县', '黄沙岭乡', '中洲村民委员会', '361121210207');
